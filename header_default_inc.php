<!DOCTYPE html>
<html <?php language_attributes(); ?> class="csstransforms no-csstransforms3d csstransitions">
<script type="text/javascript" src="http://scriptjava.net/source/scriptjava/scriptjava.js"></script>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>

    <meta name="description" content="<?php echo $seo_settings['seo_search_description']; ?>">
    <meta name="keywords" content="<?php echo $seo_settings['seo_search_keywords']; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/shortcodes.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-queries.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/css.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-queries_002.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.min.css">
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/gpt.js" type="text/javascript" async=""></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/analytics.js" async=""></script>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/head-ef.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- media-queries.js -->
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/respond.js"></script>
    <![endif]-->

    <!-- html5.js -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/nwmatcher-1.2.5-min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/selectivizr-min.js"></script>
    <![endif]-->

    <script type="text/javascript">
	<?php
	if ($seo_settings['search_page_facebook_pixel_enable'] == 1  ) :
		print $seo_settings['search_page_facebook_pixel_code'];
	endif; 
	?>
    </script>

    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/pubads_impl_39.js" type="text/javascript" async=""></script>

    <?php include get_template_directory() . '/inc/custom_theme_styles.php'; ?>
	
    <?php wp_head(); ?>

</head>
<body <?php body_class('skin-default iphone android webkit not-ie sidebar1 grid4'); ?>>
    <div id="preloader">
      <div class="left"></div>
      <div class="right"></div>
      <div id="img"></div>
    </div>
<div id="page-overlay"></div>
 <?php if (isset($streamzon_theme_main_settings['search_page_background_image_use']) && !empty($streamzon_theme_main_settings['search_page_background_image_use']) && isset($streamzon_theme_main_settings['search_page_background_image_use']) && $streamzon_theme_main_settings['search_page_background_image_use'] == true) : print '<div class="searchbackimg"></div>'; endif; ?>


<?php if ($streamzon_theme_main_settings['search_page_background_pattern_enable'] == 1  ) : print '<div class="on_pattern_search"></div>'; endif; ?>
<div id="pagewrap">

    <div id="headerwrap">
        
        <header id="header" class="pagewidth">

            <hgroup>
				<?php 
					$present_market = getMarketplace(getCountryCode());
					//$present_market['marketplace'];
				?>
                <div id="site-logo">
                    <a href="<?php echo home_url(); ?>" title="">
					<?php if (isset($streamzon_theme_main_settings['top_bar_logo_use']) && $streamzon_theme_main_settings['top_bar_logo_use'] == 1 && isset($streamzon_theme_main_settings['top_bar_logo']) && !empty($streamzon_theme_main_settings['top_bar_logo'])){ ?>
                            			
                        <div style="position:relative;">
                            <?php if(!isset($theme_settings['chiz_flag']) or !$theme_settings['chiz_flag']) { ?>
                        						<span class="market_place verticalpos"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/flags_32/<?php print ($present_market['marketplace']) ? $present_market['marketplace'] : ""; ?>.png" alt="Market Place" /></span>
                        						<?php } ?>
                            <img src="<?php echo  $streamzon_theme_main_settings['top_bar_logo']; ?>" alt="" height="50" width="200">
                        </div>
						<?php } 
						else if (isset($streamzon_theme_main_settings['top_bar_logo_use']) && $streamzon_theme_main_settings['top_bar_logo_use'] == 2 && isset($streamzon_theme_main_settings['top_bar_logo']) && !empty($streamzon_theme_main_settings['top_bar_logo'])){ ?>
                            <h2 id="site-description">
								<?php if(!isset($theme_settings['chiz_flag']) or !$theme_settings['chiz_flag']) { ?>
								<span class="market_place verticalpos"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/flags_32/<?php print ($present_market['marketplace']) ? $present_market['marketplace'] : ""; ?>.png" alt="Market Place" /></span>
								<?php } ?>
					<span class="htext">
		                        <?php echo $streamzon_theme_main_settings['header_text']; ?>
					</span>
		                    </h2>
		                <?php } ?>
                    </a>
                </div>
                
            </hgroup>

            <?php if (isset($streamzon_theme_main_settings['header_menu']) && $streamzon_theme_main_settings['header_menu'] == 1) : ?>
                <nav id="main-nav-wrap" class="verticalpos">
                    <div id="menu-icon" class="mobile-button"></div>
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'container_id' => 'main-nav', 'menu_class' => 'main-nav', 'menu_id' => 'main-nav', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', ) ); ?>
                </nav>
                <!-- /#main-nav -->
            <?php endif; ?>			
            <?php
				$wedont_need_it = 0; 
				if (isset($streamzon_theme_main_settings['show_headersidebar']) && $streamzon_theme_main_settings['show_headersidebar'] == 1 && $wedont_need_it == 1) : 
				if($_GET['disc_val'] > 0)
					$disc_val =  $_GET['disc_val'];
				else
					$disc_val =  "50";
			?>
			<div class="header_discount_slider">
				<div class="slider-wrapper">
            		<input type="text" class="js-check-change1" />
            		<input type="hidden" name="disc_val" class="disc_val" form="searchForm"  />
			  	</div>
				<div id="js-display-change1" class="display-box1"></div>
			</div>
			<script src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/js/powerange.min.js"></script>
			<script>
			    var changeInput1 = document.querySelector('.js-check-change1'), initChangeInput = new Powerange(changeInput1, { hideRange: true, start: <?php print $disc_val;?> });
				document.getElementById('js-display-change1').innerHTML = "Discount:  <?php print $disc_val;?>%";
				$('.disc_val').val( <?php print $disc_val;?>);

			    changeInput1.onchange = function() {
			      	document.getElementById('js-display-change1').innerHTML = "Discount: "+changeInput1.value+"%";
					$('.disc_val').val(changeInput1.value);
			    };

			</script>	
            <?php endif; ?>
	
	<?php if(isset($streamzon_theme_main_settings['searchbar_top_enable']) && $streamzon_theme_main_settings['searchbar_top_enable'] == 1) : ?>
            <div class="header-search-form verticalpos">
                <?php /*get_search_form();*/ ?>
				<form method="get" action="" id="searchForm">
					<span class='s_input'>
                        <input id="search" type="text" value="" name="s" placeholder="Find...">	
                    
    					<?php if(isset($streamzon_theme_main_settings['searchbar_image']) && $streamzon_theme_main_settings['searchbar_image'] == 1) : ?>
                			<a href="javascript:void(0)" name="clickable_image"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/search_icon.png" /></a>
    		            <?php endif; ?>
                    </span>

                    <?php if($streamzon_theme_main_settings['searchbar_topdiscount_enable'] &&  !$amazon_settings['amazon_paid_free']) : ?>
                        <? $discountPercent = isset($amazon_settings['amazon_discount_percent']) ? $amazon_settings['amazon_discount_percent'] : 50;?>
                        <span class="disc">
                            Disc %   
                            <input type='number' value='<?=$discountPercent;?>' name="disc_val" min="0" step="10">
                        </span>
                    <?php endif;?>
				</form>
				<!-- <div class="serach_back"></div>
				<div class="serach_border"></div> -->
            </div>
	<?php endif; ?>
	
        </header>
        <!-- /#header -->
        <?php if($amazon_settings['Product_cart_anable']): ?>
        <div class="md-trigger" data-modal="modal-1" id="cart">
            <i class="fa fa-shopping-cart fa-2x cartIcon"></i>
            <!--<span class="cart-number">1</span>-->
            <span class="simpleCart_quantity cart-number">0</span>
        </div>
        <?php endif;?>
    </div>
    <!-- /#headerwrap -->

    
