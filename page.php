<?php
$streamzon_theme_settings = get_option('streamzon_theme_settings_option');
$amazon_settings = get_option('streamzon_amazon_settings_option');

?>

<?php get_header(); ?>


    <div id="body" class="clearfix">

        <!-- layout -->
        <div id="layout" class="pagewidth clearfix layout-fix ">

            <?php if ((isset($streamzon_theme_settings['banner_image_use']) && $streamzon_theme_settings['banner_image_use'] == 1) || (isset($streamzon_theme_settings['banner_code_use']) && $streamzon_theme_settings['banner_code_use'] == 1)): ?>

                <div class="banner">

                    <?php if (isset($streamzon_theme_settings['banner_image_use']) && $streamzon_theme_settings['banner_image_use'] == 1): ?>

                        <a target="_blank" href="<?php echo $streamzon_theme_settings['banner_image_link']; ?>">
                            <img src="<?php echo $streamzon_theme_settings['banner_image_file']; ?>" alt=""/>
                        </a>

                    <?php endif; ?>

                    <?php if (isset($streamzon_theme_settings['banner_code_use']) && $streamzon_theme_settings['banner_code_use'] == 1): ?>

                        <?php echo $streamzon_theme_settings['banner_code']; ?>

                    <?php endif; ?>

                </div>

            <?php endif; ?>

            <?php if (isset($amazon_settings['show_sidebar']) && $amazon_settings['show_sidebar'] == 0) : ?>
                <div class="content-search-form">
                    <?php //get_search_form(); ?>
                </div>
            <?php endif; ?>



            <?php if (isset($amazon_settings['show_sidebar']) && $amazon_settings['show_sidebar'] == 1) : ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>



            <!-- content -->
            <div id="content" class="clearfix">


                <?php
                // Start the Loop.
                while (have_posts()) : the_post();

                    // Include the page content template.
                    get_template_part('content', 'page');

                    // If comments are open or we have at least one comment, load up the comment template.
                    /*if (comments_open() || get_comments_number()) {
                        comments_template();
                    }*/
                endwhile;
                ?>


            </div>
            <!-- /#content -->


        </div>
        <!-- /#layout -->

    </div>
    <!-- /body -->

<?php get_footer(); ?>