<?php
/*
//////////////////////////////////////////////////
This file update the database with analytical data
VJ
//////////////////////////////////////////////////
*/

function clicks(){

	$act=$_GET['act'];

	$impress = $click = $redir	=	0;

	if($act == 1)
	{
		require('../../../wp-blog-header.php' );
		$impress	=	1;
	}
	else if($act == 2)
	{
		require('../../../wp-blog-header.php' );
		$click		=	1;
	}
	else if($act == 3)
	{
		require('../../../wp-blog-header.php' );
		$redir		=	1;
	}
	else if($act == 4)
	{
		require('../../../../wp-blog-header.php' );
		$impress		=	1;
	}


	global $wpdb;
	

	$table_name = $wpdb->prefix . "streamzon_amazon_analytics"; 

	$wpdb->insert( 
				$table_name, 
				array( 
					'impressions' 		=> 	$impress, 
					'clicks' 			=> 	$click, 
					'redirects' 		=> 	$redir,  
					'search' 			=> 	0,
					'user_ip'			=> 	getip2long(),
					'country'			=>	getCountry(),	
					'device'			=>	getDeviceType(),
					'user_agent'		=>	@$_SERVER['HTTP_USER_AGENT'],
					'searchfor'			=>	the_search_query(),
					'page'				=>	getSlug(),
					'referer'			=>	@$_SERVER['HTTP_REFERER'],
					'visitDate'			=>	date('Y-m-d'),
					'visittime'			=>	date('H:i:s'),
				) 
			);
	//print $wpdb->last_query;
	}

	clicks();

?>