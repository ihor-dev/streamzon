<?php
/*
Template Name: Individual_product_page
*/

$streamzon_theme_settings = get_option('streamzon_theme_settings_option');
$streamzon_amazon_settings = get_option('streamzon_amazon_settings_option');

if (isset($_GET['search']) && !empty($_GET['search'])) {
    $search_query = $_GET['search'];
}
if (isset($_GET['disc']) && !empty($_GET['disc'])) {
    $search_query_discount = $_GET['disc'];
}
if ((!isset($_GET['disc']) || empty($_GET['disc'])) ) {
    $search_query_discount = 30;
}

if ((!isset($_GET['search']) || empty($_GET['search'])) ) {
    $search_query = '*';
}
$_GET['product_page'] = 1;
?>

<?php get_header(); ?>
					
    <div id="body" class="clearfix">

        <!-- layout -->
        <div id="layout" class="pagewidth clearfix layout-fix ">

            <?php if (isset($streamzon_amazon_settings['show_sidebar']) && $streamzon_amazon_settings['show_sidebar'] == 0) : ?>
                <div class="content-search-form">
                    <?php //get_search_form(); ?>
                </div>
            <?php endif; ?>

            <?php if (isset($streamzon_amazon_settings['show_sidebar']) && $streamzon_amazon_settings['show_sidebar'] == 1) : ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>

            <?php if(!$streamzon_amazon_settings['show_sidebar']):?>
				<style> 
					#content{
						float:none !important;
						margin:auto;
					}
				</style>
			 <?php endif; ?>


            <!-- content -->
            <div id="content" class="clearfix ppage simpleCart_shelfItem" >
            <? 
            $amazon_settings = get_option('streamzon_amazon_settings_option');	
            
            $discVal = $amazon_settings['amazon_paid_free'] == "1" ? "" : "disc_val=".$search_query_discount."&"; ?>
				<a href="<?php echo home_url( '/' ).'?'.$discVal.'s='.$search_query; ?>" >
					<div style="width:30px; margin-top: -23px;">
						<div id="backbtnpage">
						<span>&lt;</span>
						<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/back.svg">-->
						</div>
					</div>
				</a>
				<?php
					$lookup = streamzon_amazon_asin_lookup($_REQUEST ['asin']);
					
					$twitterTxt = (strlen($lookup['ItemAttributes']['Title']) > 118) ? substr($lookup['ItemAttributes']['Title'], 0, 114)."..." : $lookup['ItemAttributes']['Title'];		

				?>
				<div class="title">
					
				</div>
				<div class="picandshort">
				<? //var_dump($lookup);?>
					<div class="pic">
                        <img class="item_image" src="<?php print $lookup['LargeImage']['URL']?>" data-status="0" data-zoom-image="<?=$lookup['LargeImage']['URL']?>"/>		
					</div>
					<div class="item_asin"><?php echo $lookup['ASIN']; ?></div>
					<div class="SpecialInfo">
						<div><h1 class="item_name"><?php print $lookup['ItemAttributes']['Title'];?></h1></div>
						<?php if($lookup['ItemAttributes']['ListPrice']['FormattedPrice']){?>
							<div class='product-price'><h5>List Price: <?php print ($lookup['ItemAttributes']['ListPrice']['FormattedPrice']) ? $lookup['ItemAttributes']['ListPrice']['FormattedPrice'] : "N/A" ?></h5></div>
						<?php }?>
						<?php $amount=str_replace(",",".",$lookup['Offers']['Offer']['OfferListing']['Price']['FormattedPrice']);?>
                            <div class='product-price'><h5>Sale Price: <b class="item_price"><?php print $amount ? $amount : getPricefromRaw($_REQUEST ['asin']) ?></b></h5></div>
						<?php if($lookup['Offers']['Offer']['OfferListing']['AmountSaved']['FormattedPrice']){?>
							<div class='product-price' style="  padding: 12px 0px 6px 0px;"><p>You Save: <b><?php print $lookup['Offers']['Offer']['OfferListing']['AmountSaved']['FormattedPrice'] ? $lookup['Offers']['Offer']['OfferListing']['AmountSaved']['FormattedPrice'] : "N/A" ?></b></div>
						<?php }?>						
						
						<?php if($streamzon_amazon_settings['Product_cart_anable']): ?>
							<div class="product-price quantity-wrapper">								
								<div class="input-group" id="quantity">
									<span class="input-group-addon dec">-</span>
									<input type="text" class="form-control item_Quantity quantity value">
									<span class="input-group-addon inc">+</span>
								</div>
							</div>
						<? endif;?>


						<div class="cartbtn">
	                        <?php if($streamzon_amazon_settings['Product_cart_anable']): ?>
	                        <a class="item_add color-11 puerto-btn-2" href="javascript:;" >
								<span><i class="fa fa-shopping-cart"></i></span>
								<small>Add to cart</small>
	                        </a>
	                        <?php endif; ?>
                        </div>
                        <div class="buybtn">
	                        <?php if($streamzon_amazon_settings['Product_buy_anable']): ?>
	                           <a href="<?php print $lookup['DetailPageURL'];?>" class="color-11 puerto-btn-2">
								<span><i class="fa fa-shopping-cart"></i></span>
								<small>Buy Now</small>
	                            </a>
	                        <?php endif;?>
						</div>	
						
						
						<div>
							<div class="post-social">
								<a href="javascript:void(0);" class="fbClass" name="fblinks"
										data-peru="<?php echo $lookup['ItemAttributes']['Title']; ?>" 
										data-detail="<?php echo $lookup['DetailPageURL']; ?>"
										data-photo="<?php echo $lookup['LargeImage']['URL']; ?>"
									><img src="<?php echo bloginfo('stylesheet_directory'); ?>/landing-page/images/facebook_circle.png" /></a>
								<a href="https://twitter.com/intent/tweet" class="twClass" name="twlinks" data-peru="<?php echo urlencode($twitterTxt); ?>" data-detail="<?php echo urlencode($lookup['DetailPageURL']); ?>"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/landing-page/images/twitter_circle.png" /></a>
								<a href="javascript:void(0);" class="gpClass" name="gplinks" data-detail="<?php echo urlencode($lookup['DetailPageURL']); ?>"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/landing-page/images/google_circle.png" /></a>
							</div>
						</div>
					</div>
				</div>
				<?php
				if($lookup['EditorialReviews']['EditorialReview']['Content']){?>
					<div class="shortDesc">
						<h4>Product Description</h4>
						<?php print $lookup['EditorialReviews']['EditorialReview']['Content'];?>
					</div>
				<?php } ?>
				<div class="specs">

					<h4>Product Specifications</h4>
					<?php
						/*	*/

						foreach($lookup['ItemAttributes'] as $key=>$val)
						{
							if($key != 'ListPrice')
							{
								print "<div class='specrow'>";
								print "<div class='titelDiv'>".$key."</div>";
								print "<div class='valuDiv' >";
	
								if(is_array($val)){
									print "<ul>";
									foreach($val as $li)
									{
										print "<li>";
										if(is_array($li))
										{
											foreach($li as $vi)
											{
												if(is_array($vi))
													print implode(", ",$vi);
												else
													print $vi;
												
												print " ";
											}
										}
										else
											print $li;
										print "</li>";
									}
									print "</ul>";	
								}				
								else
									print $val;			
	
								print "</div>";
								print "</div>";
							}
						}
					
					?>	
				</div>

				<iframe src="<?=$lookup['CustomerReviews']['IFrameURL'];?>" height="auto" frameborder="0"></iframe>

            </div>
            <!-- /#content -->
		

        </div>
        <!-- /#layout 

    </div>
    <!-- /body -->
<!-- container-->
<div class="pfooter_row">
	<?php if (isset($streamzon_theme_main_settings['l_page_show_footer']) && $streamzon_theme_main_settings['l_page_show_footer'] == 1) : ?>

                    <?php wp_nav_menu(array(
                        'theme_location' => 'footer_menu',
                        'menu_class' => 'footer-nav',
                        'menu_id' => 'footer-nav',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    )); ?>

                <?php endif; ?>
</div>


<?php get_footer(); ?>