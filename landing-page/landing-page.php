<?php
global $streamzon_theme_main_settings;
$amazon_settings = get_option('streamzon_amazon_settings_option');
$theme_settings = get_option('streamzon_theme_settings_option');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php echo $seo_settings['seo_home_title'];?></title>

	<meta name="description" content="<?php echo $seo_settings['seo_home_description']; ?>">
	<meta name="keywords" content="<?php echo $seo_settings['seo_home_keywords']; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
	<meta property="og:description" content="<?php bloginfo('description'); ?>" />
	<meta property="og:image" content="<?php echo isset($streamzon_theme_main_settings['l_page_center_logo_use']) && $streamzon_theme_main_settings['l_page_center_logo_use'] == 1 && isset($streamzon_theme_main_settings['l_page_center_logo']) && !empty($streamzon_theme_main_settings['l_page_center_logo']) ? $streamzon_theme_main_settings['l_page_center_logo'] : ''; ?>" />

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/shortcodes.css" type="text/css" media="all">
	<link rel='stylesheet prefetch' href='<?php bloginfo('stylesheet_directory'); ?>/landing-page/css/jquery.nouislider.min.css'>

	<link href="<?php bloginfo('stylesheet_directory'); ?>/landing-page/css/bootstrap.css" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/landing-page/css/bootstrap-responsive.css" rel="stylesheet" media="screen">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<link href="<?php bloginfo('stylesheet_directory'); ?>/landing-page/css/main.css" rel="stylesheet" media="all">

	<?php include get_template_directory() . '/inc/custom_theme_styles.php'; ?>

	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.tubular.1.0.js"></script>
	
	<script type="text/javascript">
	<?php
	if ($seo_settings['land_page_facebook_pixel_enable'] == 1  ) :
		print $seo_settings['land_page_facebook_pixel_code'];
	endif; 
	?>
	</script>
	
	<?php include get_template_directory() . '/inc/landing_page_video_background.php'; ?>
	<style>
	html{
	padding: 0 !important;
	}
	::-webkit-scrollbar { 
	    width: 0 !important
	}
	scrollbar[orient="vertical"] scrollbarbutton,
	scrollbar[orient="vertical"] slider,
	scrollbar[orient="horizontal"] scrollbarbutton,
	scrollbar[orient="horizontal"] slider{
	height: 0 !important
	width: 0 !important
	}
	</style>

	<?php wp_head(); ?>
	<style>
	html{
	margin: 0 !important
	}
	h1{
	  margin: 20px 0 .2em;
	}
	media="all"
	.container, .navbar-static-top .container, .navbar-fixed-top .container, .navbar-fixed-bottom .container {
	   width: auto !important;
	}
	</style>


</head>
<body id="layout2" <?php body_class('landing-page'); ?>>

<?php if (isset($streamzon_theme_main_settings['l_page_background_video_use']) && $streamzon_theme_main_settings['l_page_background_video_use'] == 1) :
	print '<div id="pattern" class="on_pattern"></div>';
endif; ?>

<div>
    <?php
    $market_place	=	getMarketplace(getCountryCode());
    $mct_title		=	"_".$market_place['ct_title'];
    $mct_title = $mct_title == "_" ? "_USA" : $mct_title;
    //streamzon_amazon_settings_option[amazon_discount]
    $settings = get_option( 'streamzon_amazon_settings_option' );

    if (isset($streamzon_theme_main_settings['bitly_login_id']) && !empty($streamzon_theme_main_settings['bitly_login_id'])){
            $bitly_login_id	=	 $streamzon_theme_main_settings['bitly_login_id'];
    }

    if (isset($streamzon_theme_main_settings['bitly_api_key']) && !empty($streamzon_theme_main_settings['bitly_api_key'])){
            $bitly_api_key	=	 $streamzon_theme_main_settings['bitly_api_key'];
    }
    ?>
    <div id="header-logo-layer">
	<?php if ($streamzon_theme_main_settings['l_page_background_video_pattern_enable'] == 1  ) :
            print '<div id="pattern1" class="on_pattern_1"></div>';
	endif; ?>
    </div>

    <div id="wrapper">
	<div class="jumbotron masthead">
            <div class="container" style="width:auto !important;">
                <div class="landcontent">
                    <?php $present_market = getMarketplace(getCountryCode()); ?>
                    <?php if(!isset($theme_settings['chiz_flag']) or !$theme_settings['chiz_flag']) { ?>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/flags_32/<?php print ($present_market['marketplace']) ? $present_market['marketplace'] : ""; ?>.png" alt="Market Place" />
                    <?php } ?>
                    <div class="landing-page-logo">
			<a href="<?php echo home_url(); ?>" title="">                            
                            <?php if (isset($streamzon_theme_main_settings['l_page_center_logo_use']) && $streamzon_theme_main_settings['l_page_center_logo_use'] == 1 && isset($streamzon_theme_main_settings['l_page_center_logo']) && !empty($streamzon_theme_main_settings['l_page_center_logo'])){?>
                            	<img src="<?=$streamzon_theme_main_settings['l_page_center_logo'];?>" alt="">	
                            <?}?>
                            
			</a>
                    </div>
		<?php
		//vj code starts
		if (isset($streamzon_theme_main_settings['l_page_landing_headertxt_enable']) && $streamzon_theme_main_settings['l_page_landing_headertxt_enable'] == 1) :
		?>
            <h1>
            	<? $mct_title1 = $mct_title == '_USA' ? '' : $mct_title; ?>
                <?php echo isset($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_header_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_header_text'.$mct_title1] : 'Hello.'; ?>
            </h1>
		<?php endif; ?>
		<?php
		//vj code starts
		if (isset($streamzon_theme_main_settings['l_page_landing_subheading_enable']) && $streamzon_theme_main_settings['l_page_landing_subheading_enable'] == 1) : ?>
            <p>
			<?php					
                	echo $mtitle	=	isset($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) && !empty($streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1]) ? $streamzon_theme_main_settings['l_page_subheader_text'.$mct_title1] : 'A brand new start-up is reviving its engine';
			?>
            </p>		
		<?php endif; ?>
		<div class="row-fluid">
                    <div class="span10 offset1 subscribe">
			<form class="form-search" id="searchform" action="<?php echo home_url( '/' ); ?>" method="get">
                            <?php if($streamzon_theme_main_settings['l_page_discountbar_enable'] &&  !$amazon_settings['amazon_paid_free']) : ?>
							<div class="rangeNumbers">							
								<div align="center"><div class="display-box"><?php print $streamzon_theme_main_settings['l_page_discountbar_display_text'.$mct_title]?><span id="js-display-change"></span></div></div>
								<span class="range-min1">0%</span><span class="range-max1">100%</span>
							</div>
							<div class="slider-wrapper">
								<input type="text" class="js-check-change" />
								<input type="hidden"name="disc_val" id="disc_val" value="100"/>
							</div>
                            <?php endif; ?>
                            <div class="form_row">
				<?php if (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 1) : ?>
				<div>
                                    <input class="input-xxlarge" type="text" value="<?php the_search_query(); ?>" id="search" name="s" placeholder="<?php echo isset($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title]) && !empty($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title]) ? $streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title] : 'Find...'; ?>">
				</div>
				<div>
                                    <button class="btn btn-large btn-warning" type="button" id="btn-search" data-def="<?php print $amazon_settings['default_search_keyword'];?>">
					<?php echo isset($streamzon_theme_main_settings['l_page_button_text'.$mct_title]) && !empty($streamzon_theme_main_settings['l_page_button_text'.$mct_title]) ? $streamzon_theme_main_settings['l_page_button_text'.$mct_title] : 'Find'; ?>
                                    </button>
				</div>
				<?php elseif (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 2) : ?>
                                    <input class="input-xxlarge rrrrrr" type="text" value="<?php the_search_query(); ?>" id="search" name="s" placeholder="<?php echo isset($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title]) && !empty($streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title]) ? $streamzon_theme_main_settings['l_page_searchbar_text'.$mct_title] : 'Find...'; ?>">
                                    <button class="btn btn-large btn-warning" type="button" id="btn-search" data-def="<?php print $amazon_settings['default_search_keyword'];?>">
					<?php echo isset($streamzon_theme_main_settings['l_page_button_text'.$mct_title]) && !empty($streamzon_theme_main_settings['l_page_button_text'.$mct_title]) ? $streamzon_theme_main_settings['l_page_button_text'.$mct_title] : 'Find'; ?>
                                    </button>
				<?php endif; ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div id="footer_row" >
        <div class="backfooter"></div>
            <div class="footercontent">
                <div class="landcontent">
                    <div class="row-fluid">
                        <div class="span8 offset2 LightTwitter">
                                <span>
                                    <a href="javascript:void(0);" class="fbClass" name="fblinks"
                                        data-detail = "<?php echo get_site_url(); ?>"
                                        data-photo = "<?php print $streamzon_theme_main_settings['l_page_center_logo'] ;?>"
                                        data-peru="<?php echo $mtitle; ?>"
                                    >
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/images/facebook_circle.png" />
                                    </a>
                                </span>
                                <span>
                                    <a href="https://twitter.com/intent/tweet" class="twClass" name="twlinks" data-peru="<?php echo $mtitle; ?>" data-detail="<?php echo urlencode(get_site_url()); ?>">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/images/twitter_circle.png" />
                                    </a>
                                </span>
                            <span>
                                <a href="javascript:void(0);" class="gpClass" name="gplinks" data-detail="<?php echo urlencode(get_site_url()); ?>">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/images/google_circle.png" />
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span8 offset2 footer">
                            <?php if (isset($streamzon_theme_main_settings['l_page_show_footer']) && $streamzon_theme_main_settings['l_page_show_footer'] == 1) : ?>
                                <?php wp_nav_menu(array(
                                                    'theme_location' => 'footer_menu',
                                                    'menu_class' => 'footer-nav',
                                                    'menu_id' => 'footer-nav',
                                                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                )); ?>
                                <?php if (isset($streamzon_theme_main_settings['l_page_footer_text']) && !empty($streamzon_theme_main_settings['l_page_footer_text'])) : ?>
                                    <p class="footertext">
                                         <?php echo $streamzon_theme_main_settings['l_page_footer_text'.$mct_title]; ?>
                                    </p>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<script src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/js/powerange.min.js"></script>
<script>
	$.get( "<?php bloginfo('stylesheet_directory'); ?>/wp-stat.php?act=1", function( data ) {});

	<?php if (isset($streamzon_theme_main_settings['l_page_discountbar_enable']) && $streamzon_theme_main_settings['l_page_discountbar_enable'] == 1) : ?>

	<?php
$discountPercent = isset($amazon_settings['amazon_discount_percent']) ? $amazon_settings['amazon_discount_percent'] : 50;
$hideFlage = isset($amazon_settings['amazon_flag']) ? $amazon_settings['amazon_flag'] : 0;
 	?>

	// On change.
	var changeInput = document.querySelector('.js-check-change'), initChangeInput = new Powerange(changeInput, { hideRange: true, start: <?php echo $discountPercent;?> });
	document.getElementById('js-display-change').innerHTML = ": <?php echo $discountPercent;?>%";
	$('#disc_val').val(<?php echo $discountPercent;?>);
	changeInput.onchange = function() {
		document.getElementById('js-display-change').innerHTML = ": "+changeInput.value+"%";
		$('#disc_val').val(changeInput.value);
	};
	<?php endif; ?>

</script>
<script type="text/javascript">
	$(document).ready(function()
	{	
	
		//HEIGHT FOOTER
		vieportheight=$( window ).height();
		elempaddin=parseInt($('.footercontent').css('padding-bottom'));
		elemheight=$('.footercontent').height();
		heightfooter=vieportheight- 548-(elempaddin+elemheight);
		$('.footercontent').css('padding-bottom',heightfooter+'px');
		$(window).bind('resize', function () { 
			vieportheight=$( window ).height();
			heightfooter=vieportheight-(548+elempaddin+elemheight);
			$('.footercontent').css('padding-bottom',heightfooter+'px');
			//console.log(heightfooter);
		});
		$(".btn").on("click", function () {
			var def = $(this).data('def');
			var userval = $("#search").val();
			if(!userval){
				//$("#search").val(def);
				$("#search").val("*");
			}
			$( "#searchform" ).submit();
		});
		$('.input-xxlarge').keyup(function(e){
		    if(e.keyCode == 13)
		    {
		       var def = $(this).data('def');
			var userval = $("#search").val();
			if(!userval){
				$("#search").val(def);
			}
			$( "#searchform" ).submit();
		    }
		});
		$(".twClass").on("click", function () {
			var long_url = $(this).data('detail');
			var input_msg	=	$(this).data('peru');
			var msg = "?text=" + input_msg;
			var tag = encodeURIComponent(get_short_url("<?php echo $bitly_login_id;?>","<?php echo $bitly_api_key;?>",long_url));
			var msg_tag = msg +' '+ tag;

			var width  = 575,
				height = 400,
				left   = ($(window).width()  - width)  / 2,
				top    = ($(window).height() - height) / 2,
				url    = this.href + msg_tag,
				opts   = 'status=1' +
					',width='  + width  +
					',height=' + height +
					',top='    + top    +
					',left='   + left;

			window.open(url, 'twitter', opts);

			return false;

		});//end of TW

		$(".fbClass").on("click", function ()
		{
			var tag 		= $(this).data('detail');
			var input_msg	= $(this).data('peru');
			var img 		= $(this).data('photo');

			fbShare(tag, input_msg, input_msg, img, 600, 600);
		});
		function fbShare(url, title, descr, image, winWidth, winHeight) {
			var winTop = (screen.height / 2) - (winHeight / 2);
			var winLeft = (screen.width / 2) - (winWidth / 2);
			window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
		}

		$("a[name=gplinks]").on("click", function () {
			var long_url = $(this).data('detail');

			var tag = encodeURIComponent(get_short_url("","",long_url));

			var msg = "https://plus.google.com/share?url=" + long_url;

			var width  = 600,
				height = 600,
				left   = ($(window).width()  - width)  / 2,
				top    = ($(window).height() - height) / 2,
				url    = msg,
				opts   = 'status=1' +
					',width='  + width  +
					',height=' + height +
					',top='    + top    +
					',left='   + left;

			window.open(url, 'google', opts);
		});
		function get_short_url(login,api,long_url)
		{
			var myurl	=	null;
			var xhr 	= 	new XMLHttpRequest();
			var my_url	=	"http://api.bitly.com/v3/shorten?login="+ login + "&apiKey="+ api + "&longUrl="+ long_url;

			xhr.open("GET", my_url,false);
			xhr.onreadystatechange = function() {
				if(xhr.readyState == 4) {
					if(xhr.status==200) {
						json = JSON.parse(xhr.responseText);
						myurl = json.data.url;
					}
				}
			}
			xhr.send();
			return myurl;
		}


	});	//end of Jquery
</script>
<?php //wp_footer(); ?>
</body>
</html>