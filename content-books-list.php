<?php

$streamzon_error_message = null;
$streamzon_books = null;

$streamzon_theme_settings = get_option('streamzon_theme_settings_option');
$streamzon_amazon_settings = get_option('streamzon_amazon_settings_option');
$credentials = get_option('streamzon_amazon_credentials_option');

$search_query = '';
if (isset($_GET['s']) && !empty($_GET['s'])) {
    if(isset($streamzon_amazon_settings['default_search_keyword']) && $streamzon_amazon_settings['default_search_keyword'] !=="") {
        $result = strpos($_GET['s'], $streamzon_amazon_settings['default_search_keyword'] ) === false ? "+". trim($streamzon_amazon_settings['default_search_keyword']) : '';
        $query_vars['s'] = $_GET['s'] . $result;
        $search_query = $_GET['s'] . $result;
    }else {
        $search_query = $_GET['s'];
    }
    
}
if (isset($_GET['disc_val']) && !empty($_GET['disc_val'])) {
    $search_query_discount = $_GET['disc_val'];
}

if ((!isset($_GET['s']) || empty($_GET['s'])) && isset($streamzon_amazon_settings['default_search_keyword']) && !empty($streamzon_amazon_settings['default_search_keyword'])) {
    $search_query = $streamzon_amazon_settings['default_search_keyword'];
}
$search_query = $search_query == "" ? "*" : $search_query;

do {

    try {        
        error_log("content-books-list.php: ".$search_query);
        
        $extra_step = $streamzon_amazon_settings['amazon_additional_search_parameter'] == '1' ? mt_rand(1,24) : 0;

        $streamzon_books = streamzon_amazon_search_books($search_query, 0, $extra_step, $search_query_discount);                        

        $status = 1;
    } catch (Exception $e) {
        //IF limit is down

        echo $streamzon_error_message = $e->getMessage();        
die;
        $error_db = array(
            'The AWS Access Key Id you provided does not exist in our records.',
            );

        if(in_array($streamzon_error_message, $error_db)){
            die($streamzon_error_message);
        }

        if( $_SESSION['api_status'] == 1 && 
            $credentials['high_traffic_site'] == "1" && 
            trim($credentials['access_key_id_2']) !== "" && 
            trim($credentials['secret_access_key_2']) ) {
            //go to second api
            $_SESSION['api_secretkey'] = $credentials['secret_access_key_2'];
            $_SESSION['api_keyid'] = $credentials['access_key_id_2'];
            $_SESSION['api_status'] = 2;
        }else {
            usleep(200000); //0.2 sec
            $_SESSION['api_secretkey'] = $credentials['secret_access_key'];
            $_SESSION['api_keyid'] = $credentials['access_key_id'];
            $_SESSION['api_status'] = 1;
        }
    }

    
} while ($status !== 1);



?>




<?php if ($streamzon_books && is_array($streamzon_books)): ?>

    <input type="hidden" name="search_extra-step" value="<?=$extra_step;?>">

    <!-- content -->
    <div id="content" class="clearfix">

        <!-- loops-wrapper -->
        <div id="loops-wrapper" class="loops-wrapper infinite-scrolling AutoWidthElement isotope">

            <?php streamzon_display_books($streamzon_books,$search_query,$search_query_discount); ?>

        </div>
        <!-- /loops-wrapper -->

        <div id="ajax-loading">
            <img src="<?php echo IMAGES ?>/ajax-loader.gif" width="16" height="16" alt=""/>
        </div>

        <!--<div class="pagenav clearfix">-->
       
        <!--</div>-->

    </div>
    <!-- /#content -->

<?php else: ?>

    <!-- content -->
    <div style="width: 70%;" id="content" class="clearfix">

        <?php if ($streamzon_error_message === null): ?>

            <p>Searching for : <em><?php echo $_GET['s'] ?></em></p>
            <p>Sorry but there are no results related to <em><?php echo $_GET['s'] ?></em> at this time. Would you like to perform another search?</p>
            <hr>
            <p>Refine your search</p>

            <?php get_search_form(); ?>

        <?php else: ?>
                <? var_dump($streamzon_error_message);?>
			<?php //$streamzon_error_message='Please set up your "Amazon credentials"';?>
            <p class="error-msg"><?php echo $streamzon_error_message; ?></p>

        <?php endif; ?>

    </div>
    <!-- /#content -->

<?php endif; ?>