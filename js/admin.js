
jQuery(document).ready(function ($) {
    function setpgbar(val){
      if(val > 100) val = 100;
      var progr = $('.progressbar span');
      progr.animate({'width':val+"%"},500);
      progr.html(val+"%");
    }


    // import ajax
    if($("#ajax-import").length > 0){
        $("#ajax-import").click(function(){
            var file = $("#import-file").val();
            if(file == "") return false;

            $('.progressbar').show(600);
            setpgbar(0);

            
            var formData = new FormData();
            formData.append('file', $('#import-file')[0].files[0]);
            formData.append('action', 'streamzon-import');

            $.ajax({
                type:'POST',
                url:ajaxurl,
                processData: false,
                contentType: false,
                data:formData,
                success:function(res){
                    var resArr = res.split("#");
                    var count = resArr[1];
                    if(resArr[0] == 'uploaded'){
                        var status = 0;
                        var curr = 0;
                        function ajax_loadimg_fromserver(){

                            if(status !== '1'){
                                $.ajax({
                                    type: 'GET',
                                    url: ajaxurl,
                                    async: true,
                                    data: 'action=streamzon-import-dn',
                                    success: function(result){
                                        status = result;
                                        curr++;

                                        progress = parseInt((100/count)*curr);
                                        console.log(curr, progress,count);
                                        setpgbar(progress);

                                        ajax_loadimg_fromserver();
                                    }
                                });
                            }else {
                                //The end
                                location.reload();
                            }
                        }
                        
                        $.ajax({
                            type: 'POST',
                            url: ajaxurl,
                            async: true,
                            data: 'action=streamzon-import-dn',
                            success: function(result){
                                status = result;
                                curr++;

                                progress = parseInt((100/count)*curr);
                                setpgbar(progress);

                                ajax_loadimg_fromserver();
                            }
                        });
                            
                        
                    }
                }
            });



        })
    }




    jQuery('.streamzon-setting-section').isotope({
        // options...
        itemSelector: '.streamzon-setting-column'
    });

    //AMAZON SETTINGS

    if($("#streamzon_amazon_paid_free").attr('ch') == "1") {
        $("#discount").hide();
    }else{
        $("#discount").show();
    }

    $("#streamzon_amazon_paid_free").change(function(){
        var st = $("#streamzon_amazon_paid_free").attr('checked');
        if(st == "checked") {          
          $("#streamzon_amazon_paid_free").attr('ch','0');
          //show discount
          $("#discount").hide();
          $("#AmazonSearchByPrice").removeAttr('checked');
          $("#AmazonSearchByText").attr('checked', 'checked');
        }else{
          $("#streamzon_amazon_paid_free").attr('ch','1');
          //show discount
          $("#discount").show();
        }
        $('.streamzon-setting-section').isotope('layout');
    });


    //wizard ajax submit 
    $("#finish").click(function(){                                        
        $("#page_ajax").ajaxSubmit({
            success: function(){
                location.href = $("input[name=sumbitRequestUrl]").val();
            }
        });
        return false;
    });

    var landpage = $("#streamzon_l_page_landing_page_enable");
    var onepage  = $("#streamzon_l_page_onepage_landing");

    landpage.change(function(){
        if($(this).attr('checked') == 'checked'){
            onepage.prop('checked', false);
        }
    });

    onepage.change(function(){
        if($(this).attr('checked') == 'checked'){
            landpage.prop('checked', false);
        }
    });



    jQuery("textarea[name='streamzon_theme_settings_option[revolution_slider_code_USA]'],input[name='streamzon_theme_settings_option[side_discountbar_display_text_USA]'] ,input[name='streamzon_theme_settings_option[header_text_USA]'],input[name='streamzon_theme_settings_option[l_page_button_text_USA]'],input[name='streamzon_theme_settings_option[l_page_searchbar_text_USA]'],input[name='streamzon_theme_settings_option[l_page_footer_text_USA]'],input[name='streamzon_theme_settings_option[l_page_subheader_text_USA]'],input[name='streamzon_theme_settings_option[l_page_discountbar_display_text_USA]']").each(function() {
	jQuery(this).parent().parent().hide();
    });

    jQuery('.streamzon-setting-column').hover(function() {
        jQuery(this).css('z-index', '9999');
    }, function() {
        jQuery('.streamzon-setting-column').css('z-index', 'auto');
        jQuery('.wp-picker-open').trigger('click');
    });

    // only one opened color picker
    jQuery('.wp-color-result').click(function(e) {
        var self = jQuery(this);

        if(!self.hasClass('wp-picker-open')) return;

        self.addClass('.color-picker-current-open');

        jQuery('.wp-picker-open').each(function() {
            if(!jQuery(this).hasClass('.color-picker-current-open')) {
                jQuery(this).trigger('click');
            }
        });

        self.removeClass('.color-picker-current-open');
    });


    // Landing page background checkboxes
    jQuery('#streamzon_l_page_background_image_use').click(function() {
        var color = jQuery('#streamzon_l_page_background_color_use');
        if (jQuery(this).prop('checked')) {
            color.prop('checked', false);
        }
    });
    jQuery('#streamzon_l_page_background_color_use').click(function() {
        var image = jQuery('#streamzon_l_page_background_image_use');
        if (jQuery(this).prop('checked')) {
            image.prop('checked', false);
        }
    });

    jQuery('#theme_options_block > div').hide();
    jQuery('#theme_options_block > h3:last-of-type').addClass('active-section');
    jQuery('#theme_options_block > div:last-of-type').show();


    jQuery('#theme_options_block > h3').on('click', function() {
        jQuery('#theme_options_block > h3').removeClass('active-section');
        jQuery(this).addClass('active-section');
        jQuery('#theme_options_block > div').hide('slow');
        jQuery(this).next().slideDown(
	    function() {jQuery('.streamzon-setting-section').isotope('layout');}
	);
	
    });

    jQuery('#import_settings').click(function (e) {

        if(jQuery('#import-file').val() == '') {
            e.preventDefault();
        } else {
           jQuery(this).unbind('click').click();
        }


        //console.log(jQuery('#import-file').val());
        //jQuery('#import-file').val();
    });
    
    // image/video checkbox auto-disabler
    var videoChbx = jQuery("#streamzon_l_page_background_video_use");
    var imageChbx = jQuery("#streamzon_l_page_background_image_use");

    videoChbx.change(function(){

      var videostatus = videoChbx.attr('checked');
      var imagestatus = imageChbx.attr('checked');

      if(videostatus == 'checked') {
        imageChbx.removeAttr('checked');
      }
    });

    imageChbx.change(function(){

      var videostatus = videoChbx.attr('checked');
      var imagestatus = imageChbx.attr('checked');

      if(imagestatus == 'checked') {
        videoChbx.removeAttr('checked');
      }
    });

    //banner image/code checkbox auto-disabler
    var imgChbx  = jQuery("#streamzon_banner_image_use");
    var codeChbx = jQuery("#streamzon_banner_code_use");

    imgChbx.change(function(){
        var imgstatus  = imgChbx.attr('checked');
        var codestatus = codeChbx.attr('checked');

        if(imgstatus == 'checked') {
            codeChbx.removeAttr('checked');
        }
    });

    codeChbx.change(function(){
        var imgstatus  = imgChbx.attr('checked');
        var codestatus = codeChbx.attr('checked');

        if(codestatus == 'checked') {
            imgChbx.removeAttr('checked');
        }
    });


    //set default preset title
    function setDefaulPresetTitle(){
        var maxnumH = 0;
        function isDigit(x){
          y = parseFloat(x);
          return !isNaN(y);
        }

        jQuery.each(jQuery("div.streamzon-setting-section:has(#streamzon_l_page_preset) h4"),function(i,k){
          status = isDigit(jQuery(k).html());
          if(status=='true') {
            currentN = parseFloat(jQuery(k).html());
            maxnumH  = currentN > maxnumH ? currentN : maxnumH;
          }
        });
        maxnumH = parseInt(maxnumH)+1;
        title = "0"+maxnumH;
        jQuery('#streamzon_l_page_preset').val(title);
        jQuery('#streamzon_l_page_preset_thumbnail').val("NULL");
    }

    jQuery('.streamzon-setting-column:has(#streamzon_l_page_preset) #submit_settings').click(function(){        
        if( jQuery.trim(jQuery('#streamzon_l_page_preset').val()) == ""){
          setDefaulPresetTitle();
        }
    });

    jQuery(".streamzon-setting-field:has(.flag) input").change(function(){        
        jQuery("#amazon_associate_auto option").remove();
        //jQuery("#amazon_associate_auto").append('<option value="">Select</option>');
      
        jQuery.each(jQuery(".streamzon-setting-field:has(.flag) input"), function(i,k){
            text = jQuery(k).val();
            if(text != "") {
                curr = jQuery(k).attr("data-country");
                jQuery("#amazon_associate_auto").append('<option value="'+curr+'">'+curr+' Listings</option>');
            }
        });  
    });    
    
    //set visibility of second api keys
    var HTS = jQuery("#streamzon_high_traffic_site");
    function setHTSfields_visible(){
      if(HTS.prop('checked')){
        jQuery(".hide-high-traffic").show(600);
      }else{
        jQuery(".hide-high-traffic").hide(30);
      }
    }
    setHTSfields_visible();
    HTS.on('change',function(){  
      setHTSfields_visible();
    });
});
