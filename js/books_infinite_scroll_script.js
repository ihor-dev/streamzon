function center_content(){
    if(!jQuery("#slidebarsubmit").length){
        jQuery("#loops-wrapper").css({"margin-left":"0"});
        var wrapperW = jQuery("#loops-wrapper").width();
        var currMargin = parseInt(jQuery("#loops-wrapper article").css("margin-left").replace("px",""));
        var elW = jQuery("#loops-wrapper article").width() + currMargin;
        var cnt = wrapperW / elW >> 0;
        var mrgn = wrapperW - (elW * cnt);
        var newMargin = (mrgn);

        jQuery("#loops-wrapper").css({"margin-left":newMargin/2});        
    }


}

var searchResultsPage = 2;
var readyToLoad = true;
var count   =   0;
var currentStep = jQuery("input[name=search_extra-step]").val();
var lastResponse = 1;

function load_content(){

    readyToLoad = false;
                
    jQuery('#ajax-loading').css('opacity', '0');

    var data = {
        action: 'streamzon_get_books',
        page: searchResultsPage,
        extra_step: currentStep,
        ajax: 1,
        search_query: streamzon_object.search_query,
        disc: streamzon_object.search_query_discount,
        a_cat: streamzon_object.a_cat
    };  
    console.log(data);

    jQuery.post(streamzon_object.ajaxurl, data, function (response) {
        if (response == 0) {
            console.log("response == 0");
            if (lastResponse == 0) {
                console.log("--- got 0 results two times in a row");
                searchResultsPage = 0;
            } else {
                searchResultsPage = 1;
                if(count == 0)
                {
                    jQuery.fn.myFunction();
                    console.log("--- first timeonly ");
                }
                if(currentStep >= 25){
                    currentStep = 0;
                }
                currentStep++;
            }
        } else {
            jQuery('#loops-wrapper').append(response);
            if(count == 0)
            {
                jQuery.fn.myFunction();
                console.log("--- first timeonly 2");
            }
            searchResultsPage++;
        }

        lastResponse = response;
        jQuery('#ajax-loading').css('opacity', '0');
        var yCoord = window.scrollY;
        jQuery('#loops-wrapper').isotope('destroy');
        jQuery('#loops-wrapper').isotope({transformsEnabled:false});

        window.scrollTo(0, yCoord);
        readyToLoad = true;
        center_content();
    });
}


jQuery(document).ready(function () {
    jQuery(window).resize(function(){
        center_content();
    });



    load_content();


    jQuery(window).scroll(function () 
	{
        //console.log(jQuery('#content').height() + jQuery('#content').offset().top - jQuery(window).height());
        

        if (jQuery(window).scrollTop() >= jQuery('#content').height() + jQuery('#content').offset().top - jQuery(window).height() - 1000) {
            if (searchResultsPage != 0 && searchResultsPage <= 10 && readyToLoad) {
		
                load_content();
            }
        }
    });

    jQuery.fn.myFunction = function()
	{
		//if (jQuery(window).scrollTop() >= jQuery('#content').height() + jQuery('#content').offset().top - jQuery(window).height() - 1000) 
		{
            if (searchResultsPage != 0 && searchResultsPage <= 10 && readyToLoad) {

                readyToLoad = false;
				count++;

                jQuery('#ajax-loading').css('opacity', '1');

                var data = {
                    action: 'streamzon_get_books',
                    page: searchResultsPage,
                    extra_step: currentStep,
                    ajax: 1,
                    search_query: streamzon_object.search_query,
					disc: streamzon_object.search_query_discount,
                    a_cat: streamzon_object.a_cat
                };	
                console.log(data);

                jQuery.post(streamzon_object.ajaxurl, data, function (response) {
                    if (response == 0) {
                        console.log("response == 0");
                        if (lastResponse == 0) {
                            console.log("--- got 0 results two times in a row");
                            searchResultsPage = 0;
                        } else {
                            searchResultsPage = 1;
                            if(currentStep >= 25){
                                currentStep = 0;
                            }
                            currentStep++;
                        }
                    } else {
                        jQuery('#loops-wrapper').append(response);
                        searchResultsPage++;
                    }

                    lastResponse = response;
                    jQuery('#ajax-loading').css('opacity', '0');
                    var yCoord = window.scrollY;
                    jQuery('#loops-wrapper').isotope('destroy');
                    jQuery('#loops-wrapper').isotope({transformsEnabled:false});

                    window.scrollTo(0, yCoord);
                    readyToLoad = true;
                });
            }
        }
		if(count <= 4)	
		{
			console.log("so far "+count+" time executed the script.");
			setTimeout(jQuery.fn.myFunction, 1000);
		}
		else
			console.log("End of auto load after 15 times");
		    center_content();
	}

});