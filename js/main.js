jQuery(document).ready(function ($) {
    
    jQuery("#cart").click(function(e) {
        e.preventDefault();
     jQuery("#modal-1").addClass( "md-show" );
    });
    jQuery(".md-close").click(function() {
      jQuery(this).parent().parent().parent().removeClass( "md-show" );
    });

//resize header//
jQuery("#header").css({"width":"100%"});
jQuery(window).resize(function(){
    if(jQuery(window).width() > 0 ){
        jQuery("#header").css({"width":"100%"});
    }
});

// ========= PRELOADER ============= //
  $(window).load(function() {
    setTimeout(function() {
      $('#preloader > #img').fadeOut('slow', function() {
        $("#preloader > .left, #preloader > .right").animate({
          "width": 0,
          "opacity": 0
        }, 500, function(){
            $('#preloader').remove();
        });
        
      });
    }, 2000);

  });


// ========== set count of cart items add ======= //
var q = jQuery('input.quantity.value');
q.val(1);

jQuery("#quantity .inc").on('click',function(){
  q.val( parseInt(q.val())+1 );
});

jQuery("#quantity .dec").on('click',function(){
  if(q.val()>0){
    q.val( parseInt(q.val())-1 );
  }
});

jQuery(".cartbtn a").on('click', function(){
    setTimeout(function(){
        jQuery('input.quantity.value').val(1);
    },500);
});





jQuery(".footer-nav a:contains('products')").remove();

    /*******************************************/
    
    jQuery(".simpleCart_checkout").click(function(event){
        event.preventDefault();
        var associateid=jQuery("#associate_id").html();
			associateauto=jQuery("#associate_auto").html();
			switch (associateauto) {
						case 'JP':
						amazonurl="http://www.amazon.co.jp/gp/aws/cart/add.html";
					break;
						case 'GB':
						amazonurl="http://www.amazon.co.uk/gp/aws/cart/add.html";
					break;
						case 'DE':
						amazonurl="http://www.amazon.de/gp/aws/cart/add.html";
					break;
						case 'FR':
						amazonurl="http://www.amazon.fr/gp/aws/cart/add.html";
					break;
						case 'CA':
						amazonurl="http://www.amazon.ca/gp/aws/cart/add.html";
					break;
						case 'CN':
						amazonurl="http://www.amazon.cn/gp/aws/cart/add.html";
					break;
						case 'IN':
						amazonurl="http://www.amazon.in/gp/aws/cart/add.html";
					break;
						case 'IN':
						amazonurl="http://www.amazon.it/gp/aws/cart/add.html";
					break;
						case 'ES':
						amazonurl="http://www.amazon.es/gp/aws/cart/add.html";
					break;
						default:
						amazonurl="http://www.amazon.com/gp/aws/cart/add.html";
					break;
			};
            url=amazonurl+"?AssociateTag="+associateid+"&";
            i=1;
        jQuery(".simpleCart_items .itemRow").each(function(){
            var asin=jQuery(this).children(".item-asin").html();
            var qty=jQuery(this).children(".item-quantity").html();
            url=url+'ASIN.'+i+'='+asin+'&Quantity.'+i+'='+qty+'&';
            i++;
        })
        url=url+'add=add';
        location.href=url;
    })
    /*******************************************/

    setSearchMargin();
    jQuery(window).resize(setSearchMargin);

    function setSearchMargin() {
        var wrapperWidth = jQuery('#content').width(),
            articleFullWidth = jQuery(jQuery('#loops-wrapper article')[0]).outerWidth(true),
            countArticles = 0,
            searchMargin = 0;

        countArticles = Math.floor(wrapperWidth / articleFullWidth);
        searchMargin = Math.floor( (wrapperWidth - articleFullWidth * countArticles) / 2 ) - 20;
        jQuery('#loops-wrapper').css('margin-left', searchMargin + 'px');
        jQuery('#content').css('overflow', 'hidden');
    }


	jQuery("a[name=clickable_image2]").on("click", function () {
		//alert("as");	
		//( "#slidevarsubmit" ).submit();
		var form = document.getElementById("slidebarsubmit");
		document.getElementById("submitslidebarsubmit").addEventListener("click", function () {
		  form.submit();
		});
	});// end of code

	jQuery('.slidebarsinput').keyup(function(e){
	if(e.keyCode == 13)
	    {
		var form = document.getElementById("slidebarsubmit");
		document.getElementById("submitslidebarsubmit").addEventListener("click", function(){
		  form.submit();
		});
		console.log(form);
		form.submit();
	    }
	});



jQuery(".form-search").submit(function(){
   // Let's find the input to check
   var $input = jQuery(this).find("input[name=s]");
   if (!$input.val()) {
     // Value is falsey (i.e. null), lets set a new one
     $input.val("*");
   }
});

	////////////////////////////////////////
    var marketSelect_us = jQuery('#streamzon_amazon_market_us');
    var marketSelect_uk = jQuery('#streamzon_amazon_market_uk');
    var marketSelect_ca = jQuery('#streamzon_amazon_market_ca');
    var marketSelect_de = jQuery('#streamzon_amazon_market_de');
    var marketSelect_fr = jQuery('#streamzon_amazon_market_fr');
    var marketSelect_in = jQuery('#streamzon_amazon_market_in');
    var marketSelect_it = jQuery('#streamzon_amazon_market_it');
    var marketSelect_es = jQuery('#streamzon_amazon_market_es');
    var marketSelect_jp = jQuery('#streamzon_amazon_market_jp');
    var marketSelect_cn = jQuery('#streamzon_amazon_market_cn');
	
	////////////////////////////////////////
    var categorySelect_us = jQuery('.streamzom_amazon_category_us');
    var categorySelect_uk = jQuery('.streamzom_amazon_category_uk');
    var categorySelect_ca = jQuery('.streamzom_amazon_category_ca');
    var categorySelect_de = jQuery('.streamzom_amazon_category_de');
    var categorySelect_fr = jQuery('.streamzom_amazon_category_fr');
    var categorySelect_in = jQuery('.streamzom_amazon_category_in');
    var categorySelect_it = jQuery('.streamzom_amazon_category_it');
    var categorySelect_es = jQuery('.streamzom_amazon_category_es');
    var categorySelect_jp = jQuery('.streamzom_amazon_category_jp');
    var categorySelect_cn = jQuery('.streamzom_amazon_category_cn');

	////////////////////////////////////////
    var discountCheckbox = jQuery('#streamzon_amazon_discount');
	var discountInput = jQuery('#streamzon_amazon_discount_amount');

	////////////////////////////////////////
    marketSelect_us.change(function() {
        clearCategorySelect_us();
        loadCategories_us();
    });
    marketSelect_uk.change(function() {
        clearCategorySelect_uk();
        loadCategories_uk();
    });
    marketSelect_ca.change(function() {
        clearCategorySelect_ca();
        loadCategories_ca();
    });
    marketSelect_de.change(function() {
		clearCategorySelect_de();
        loadCategories_de();
    });
    marketSelect_fr.change(function() {
        clearCategorySelect_fr();
        loadCategories_fr();
    });
    marketSelect_in.change(function() {
        clearCategorySelect_in();
        loadCategories_in();
    });
    marketSelect_it.change(function() {
        clearCategorySelect_it();
        loadCategories_it();
    });
    marketSelect_es.change(function() {
        clearCategorySelect_es();
        loadCategories_es();
    });
    marketSelect_jp.change(function() {
        clearCategorySelect_jp();
        loadCategories_jp();
    });
    marketSelect_cn.change(function() {
        clearCategorySelect_cn();
        loadCategories_cn();
    });


	////////////////////////////////////////
    jQuery(document).on("change", ".streamzom_amazon_category_us", function () {
        var self3 = jQuery(this),
            categoryIndex_us = self3.data('category-index'),
            selectedValue_us = self3.find(":selected").val();

        removeAllChildCategoriesSelects_us(categoryIndex_us);
        if (selectedValue_us) {
            loadSubcategories_us();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_uk", function () {
        var self4 = jQuery(this),
            categoryIndex_uk = self4.data('category-index'),
            selectedValue_uk = self4.find(":selected").val();

        removeAllChildCategoriesSelects_uk(categoryIndex_uk);
        if (selectedValue_uk) {
            loadSubcategories_uk();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_ca", function () {
        var self5 = jQuery(this),
            categoryIndex_ca = self5.data('category-index'),
            selectedValue_ca = self5.find(":selected").val();

        removeAllChildCategoriesSelects_ca(categoryIndex_ca);
        if (selectedValue_ca) {
            loadSubcategories_ca();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_de", function () {
		var self6 = jQuery(this),
            categoryIndex_de = self6.data('category-index'),
            selectedValue_de = self6.find(":selected").val();

        removeAllChildCategoriesSelects_de(categoryIndex_de);
        if (selectedValue_de) {
            loadSubcategories_de();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_fr", function () {
		var self7 = jQuery(this),
            categoryIndex_fr = self7.data('category-index'),
            selectedValue_fr = self7.find(":selected").val();

        removeAllChildCategoriesSelects_fr(categoryIndex_fr);
        if (selectedValue_fr) {
            loadSubcategories_fr();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_in", function () {
		var self8 = jQuery(this),
            categoryIndex_in = self8.data('category-index'),
            selectedValue_in = self8.find(":selected").val();

        removeAllChildCategoriesSelects_in(categoryIndex_in);
        if (selectedValue_in) {
            loadSubcategories_in();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_it", function () {
		var self9 = jQuery(this),
            categoryIndex_it = self9.data('category-index'),
            selectedValue_it = self9.find(":selected").val();

        removeAllChildCategoriesSelects_it(categoryIndex_it);
        if (selectedValue_it) {
            loadSubcategories_it();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_es", function () {
		var self10 = jQuery(this),
            categoryIndex_es = self10.data('category-index'),
            selectedValue_es = self10.find(":selected").val();

        removeAllChildCategoriesSelects_es(categoryIndex_es);
        if (selectedValue_es) {
            loadSubcategories_es();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_jp", function () {
		var self11 = jQuery(this),
            categoryIndex_jp = self11.data('category-index'),
            selectedValue_jp = self11.find(":selected").val();

        removeAllChildCategoriesSelects_jp(categoryIndex_jp);
        if (selectedValue_jp) {
            loadSubcategories_jp();
        }
    });
    jQuery(document).on("change", ".streamzom_amazon_category_cn", function () {
		var self12 = jQuery(this),
            categoryIndex_cn = self12.data('category-index'),
            selectedValue_cn = self12.find(":selected").val();

        removeAllChildCategoriesSelects_cn(categoryIndex_cn);
        if (selectedValue_cn) {
            loadSubcategories_cn();
        }
    });

	
	////////////////////////////////////////
    discountCheckbox.change(function() {
        if (discountCheckbox[0].checked) {
            discountInput.prop('disabled', false);
        } else {
            discountInput.prop('disabled', true);
        }
    });


	////////////////////////////////////////
    function clearCategorySelect_us() {
        var options_us = categorySelect_us.find('option');
        categorySelect_us.val('');
        options_us.not( ":first").remove();
    }
    function clearCategorySelect_uk() {
        var options_uk = categorySelect_uk.find('option');
        categorySelect_uk.val('');
        options_uk.not( ":first").remove();
    }
    function clearCategorySelect_ca() {
        var options_ca = categorySelect_ca.find('option');
        categorySelect_ca.val('');
        options_ca.not( ":first").remove();
    }
    function clearCategorySelect_de() {
        var options_de = categorySelect_de.find('option');
        categorySelect_de.val('');
        options_de.not( ":first").remove();
    }
    function clearCategorySelect_fr() {
		var options_fr = categorySelect_fr.find('option');
        categorySelect_fr.val('');
        options_fr.not( ":first").remove();
    }
    function clearCategorySelect_in() {
		var options_in = categorySelect_in.find('option');
        categorySelect_in.val('');
        options_in.not( ":first").remove();
    }
    function clearCategorySelect_it() {
		var options_it = categorySelect_it.find('option');
        categorySelect_it.val('');
        options_it.not( ":first").remove();
    }
    function clearCategorySelect_es() {
		var options_es = categorySelect_es.find('option');
        categorySelect_es.val('');
        options_es.not( ":first").remove();
    }
    function clearCategorySelect_jp() {
		var options_jp = categorySelect_jp.find('option');
        categorySelect_jp.val('');
        options_jp.not( ":first").remove();
    }
    function clearCategorySelect_cn() {
		var options_cn = categorySelect_cn.find('option');
        categorySelect_cn.val('');
        options_cn.not( ":first").remove();
    }


	////////////////////////////////////////
    function clearAndDisableSubcategorySelect_us() {
        var options_us = subcategorySelect_us.find('option');
        subcategorySelect_us.val('');
        subcategorySelect_us.prop('disabled', true);
        options_us.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_uk() {
        var options_uk = subcategorySelect_uk.find('option');
        subcategorySelect_uk.val('');
        subcategorySelect_uk.prop('disabled', true);
        options_uk.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_ca() {
        var options_ca = subcategorySelect_ca.find('option');
        subcategorySelect_ca.val('');
        subcategorySelect_ca.prop('disabled', true);
        options_ca.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_de() {
        var options_de = subcategorySelect_de.find('option');
        subcategorySelect_de.val('');
        subcategorySelect_de.prop('disabled', true);
        options_de.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_fr() {
		var options_fr = subcategorySelect_fr.find('option');
        subcategorySelect_fr.val('');
        subcategorySelect_fr.prop('disabled', true);
        options_fr.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_in() {
		var options_in = subcategorySelect_in.find('option');
        subcategorySelect_in.val('');
        subcategorySelect_in.prop('disabled', true);
        options_in.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_it() {
		var options_it = subcategorySelect_it.find('option');
        subcategorySelect_it.val('');
        subcategorySelect_it.prop('disabled', true);
        options_it.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_es() {
		var options_es = subcategorySelect_es.find('option');
        subcategorySelect_es.val('');
        subcategorySelect_es.prop('disabled', true);
        options_es.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_jp() {
		var options_jp = subcategorySelect_jp.find('option');
        subcategorySelect_jp.val('');
        subcategorySelect_jp.prop('disabled', true);
        options_jp.not( ":first").remove();
    }
    function clearAndDisableSubcategorySelect_cn() {
		var options_cn = subcategorySelect_cn.find('option');
        subcategorySelect_cn.val('');
        subcategorySelect_cn.prop('disabled', true);
        options_cn.not( ":first").remove();
    }


	////////////////////////////////////////
    function loadSubcategories_us() {
        var categoriesSelects_us = jQuery('.streamzom_amazon_category_us'), categories_us = [];

        categoriesSelects_us.each(function() {
            var self = jQuery(this);categories_us.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_us.find(":selected").val(),
            category: categories_us,
            ajax: 1
        };

        var categoriesList_us = jQuery('#streamzom_amazon_categories_list_us');
        categoriesList_us.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }
            categoriesList_us.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout'); 
        });
    }

    function loadSubcategories_uk() {
        var categoriesSelects_uk = jQuery('.streamzom_amazon_category_uk'), categories_uk = [];

        categoriesSelects_uk.each(function() {
            var self = jQuery(this);categories_uk.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_uk.find(":selected").val(),
            category: categories_uk,
            ajax: 1
        };

        var categoriesList_uk = jQuery('#streamzom_amazon_categories_list_uk');
        categoriesList_uk.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_uk.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_ca() {
        var categoriesSelects_ca = jQuery('.streamzom_amazon_category_ca'), categories_ca = [];

        categoriesSelects_ca.each(function() {
            var self = jQuery(this);categories_ca.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_ca.find(":selected").val(),
            category: categories_ca,
            ajax: 1
        };

        var categoriesList_ca = jQuery('#streamzom_amazon_categories_list_ca');
        categoriesList_ca.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_ca.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_de() {
        var categoriesSelects_de = jQuery('.streamzom_amazon_category_de'), categories_de = [];

        categoriesSelects_de.each(function() {
            var self = jQuery(this);categories_de.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_de.find(":selected").val(),
            category: categories_de,
            ajax: 1
        };

        var categoriesList_de = jQuery('#streamzom_amazon_categories_list_de');
        categoriesList_de.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_de.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_fr() {
        var categoriesSelects_fr = jQuery('.streamzom_amazon_category_fr'), categories_fr = [];

        categoriesSelects_fr.each(function() {
            var self = jQuery(this);categories_fr.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_fr.find(":selected").val(),
            category: categories_fr,
            ajax: 1
        };

        var categoriesList_fr = jQuery('#streamzom_amazon_categories_list_fr');
        categoriesList_fr.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_fr.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_in() {
        var categoriesSelects_in = jQuery('.streamzom_amazon_category_in'), categories_in = [];

        categoriesSelects_in.each(function() {
            var self = jQuery(this);categories_in.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_in.find(":selected").val(),
            category: categories_in,
            ajax: 1
        };

        var categoriesList_in = jQuery('#streamzom_amazon_categories_list_in');
        categoriesList_in.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_in.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_it() {
        var categoriesSelects_it = jQuery('.streamzom_amazon_category_it'), categories_it = [];

        categoriesSelects_it.each(function() {
            var self = jQuery(this);categories_it.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_it.find(":selected").val(),
            category: categories_it,
            ajax: 1
        };

        var categoriesList_it = jQuery('#streamzom_amazon_categories_list_it');
        categoriesList_it.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_it.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_es() {
        var categoriesSelects_es = jQuery('.streamzom_amazon_category_es'), categories_es = [];

        categoriesSelects_es.each(function() {
            var self = jQuery(this);categories_es.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_es.find(":selected").val(),
            category: categories_es,
            ajax: 1
        };

        var categoriesList_es = jQuery('#streamzom_amazon_categories_list_es');
        categoriesList_es.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_es.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_jp() {
        var categoriesSelects_jp = jQuery('.streamzom_amazon_category_jp'), categories_jp = [];

        categoriesSelects_jp.each(function() {
            var self = jQuery(this);categories_jp.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_jp.find(":selected").val(),
            category: categories_jp,
            ajax: 1
        };

        var categoriesList_jp = jQuery('#streamzom_amazon_categories_list_jp');
        categoriesList_jp.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_jp.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }
    function loadSubcategories_cn() {
        var categoriesSelects_cn = jQuery('.streamzom_amazon_category_cn'), categories_cn = [];

        categoriesSelects_cn.each(function() {
            var self = jQuery(this);categories_cn.push(self.find(":selected").val());
        });

        var data = {
            action: 'streamzon_load_subcategories',
            market: marketSelect_cn.find(":selected").val(),
            category: categories_cn,
            ajax: 1
        };

        var categoriesList_cn = jQuery('#streamzom_amazon_categories_list_cn');
        categoriesList_cn.append('<li><select class="subcategories-loading" disabled="disabled"><option value="">Loading...</option></select></li>');

        jQuery.post(streamzon_object.ajaxurl, data, function (response) {

            if (response == 0) {
                jQuery('.subcategories-loading').html('<option value="">Error loading subcategories.</option>');
                return;
            }

            categoriesList_cn.replaceWith(response);
            jQuery('.streamzon-setting-section').isotope('layout');
        });
    }



	////////////////////////////////////////
    function loadCategories_us() {
        var data3 = {
            action: 'streamzon_load_categories',
            market: marketSelect_us.find(":selected").val(),
            ajax: 1
        };

        categorySelect_us.prop('disabled', true);
        categorySelect_us.find('option').remove();
        categorySelect_us.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data3, function (response) {
            categorySelect_us.find('option').remove();

            if (response == 0) {
                categorySelect_us.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_us.append(response);
            categorySelect_us.prop('disabled', false);
        });
    }
    function loadCategories_uk() {
        var data4 = {
            action: 'streamzon_load_categories',
            market: marketSelect_uk.find(":selected").val(),
            ajax: 1
        };

        categorySelect_uk.prop('disabled', true);
        categorySelect_uk.find('option').remove();
        categorySelect_uk.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data4, function (response) {
            categorySelect_uk.find('option').remove();

            if (response == 0) {
                categorySelect_uk.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_uk.append(response);
            categorySelect_uk.prop('disabled', false);
        });
    }
    function loadCategories_ca() {
        var data4 = {
            action: 'streamzon_load_categories',
            market: marketSelect_ca.find(":selected").val(),
            ajax: 1
        };

        categorySelect_ca.prop('disabled', true);
        categorySelect_ca.find('option').remove();
        categorySelect_ca.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data4, function (response) {
            categorySelect_ca.find('option').remove();

            if (response == 0) {
                categorySelect_ca.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_ca.append(response);
            categorySelect_ca.prop('disabled', false);
        });
    }
    function loadCategories_de() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_de.find(":selected").val(),
            ajax: 1
        };

        categorySelect_de.prop('disabled', true);
        categorySelect_de.find('option').remove();
        categorySelect_de.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_de.find('option').remove();

            if (response == 0) {
                categorySelect_de.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_de.append(response);
            categorySelect_de.prop('disabled', false);
        });
    }
    function loadCategories_fr() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_fr.find(":selected").val(),
            ajax: 1
        };

        categorySelect_fr.prop('disabled', true);
        categorySelect_fr.find('option').remove();
        categorySelect_fr.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_fr.find('option').remove();

            if (response == 0) {
                categorySelect_fr.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_fr.append(response);
            categorySelect_fr.prop('disabled', false);
        });
    }
    function loadCategories_in() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_in.find(":selected").val(),
            ajax: 1
        };

        categorySelect_in.prop('disabled', true);
        categorySelect_in.find('option').remove();
        categorySelect_in.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_in.find('option').remove();

            if (response == 0) {
                categorySelect_in.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_in.append(response);
            categorySelect_in.prop('disabled', false);
        });
    }
    function loadCategories_it() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_it.find(":selected").val(),
            ajax: 1
        };

        categorySelect_it.prop('disabled', true);
        categorySelect_it.find('option').remove();
        categorySelect_it.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_it.find('option').remove();

            if (response == 0) {
                categorySelect_it.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_it.append(response);
            categorySelect_it.prop('disabled', false);
        });
    }
    function loadCategories_es() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_es.find(":selected").val(),
            ajax: 1
        };

        categorySelect_es.prop('disabled', true);
        categorySelect_es.find('option').remove();
        categorySelect_es.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_es.find('option').remove();

            if (response == 0) {
                categorySelect_es.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_es.append(response);
            categorySelect_es.prop('disabled', false);
        });
    }
    function loadCategories_jp() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_jp.find(":selected").val(),
            ajax: 1
        };

        categorySelect_jp.prop('disabled', true);
        categorySelect_jp.find('option').remove();
        categorySelect_jp.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_jp.find('option').remove();

            if (response == 0) {
                categorySelect_jp.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_jp.append(response);
            categorySelect_jp.prop('disabled', false);
        });
    }
    function loadCategories_cn() {
        var data5 = {
            action: 'streamzon_load_detegories',
            market: marketSelect_cn.find(":selected").val(),
            ajax: 1
        };

        categorySelect_cn.prop('disabled', true);
        categorySelect_cn.find('option').remove();
        categorySelect_cn.append('<option value="">Loading...</option>');

        jQuery.post(streamzon_object.ajaxurl, data5, function (response) {
            categorySelect_cn.find('option').remove();

            if (response == 0) {
                categorySelect_cn.append('<option value="">Error loading categories.</option>');
                return;
            }

            categorySelect_cn.append(response);
            categorySelect_cn.prop('disabled', false);
        });
    }


    function removeAllChildCategoriesSelects_us(category_index) {
        var selects = jQuery('.streamzom_amazon_category_us');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_uk(category_index) {
        var selects = jQuery('.streamzom_amazon_category_uk');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_ca(category_index) {
        var selects = jQuery('.streamzom_amazon_category_ca');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_de(category_index) {
        var selects = jQuery('.streamzom_amazon_category_de');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_fr(category_index) {
        var selects = jQuery('.streamzom_amazon_category_fr');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_in(category_index) {
        var selects = jQuery('.streamzom_amazon_category_in');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_it(category_index) {
        var selects = jQuery('.streamzom_amazon_category_it');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_es(category_index) {
        var selects = jQuery('.streamzom_amazon_category_es');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_jp(category_index) {
        var selects = jQuery('.streamzom_amazon_category_jp');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }
	function removeAllChildCategoriesSelects_cn(category_index) {
        var selects = jQuery('.streamzom_amazon_category_cn');
        selects.each(function() {
            var self = jQuery(this);
            if (self.data('category-index') > category_index) {
                self.remove();
            }
        });
        jQuery('.subcategories-loading').remove();
    }

});