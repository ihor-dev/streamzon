simpleCart({
    cartColumns: [
        { view: "image", attr: "image", label: '&nbsp;'},
		{ attr: "name", label: "Name"},
		{ attr: "asin", label: "id"},
		{ view: "currency", attr: "price", label: "Price"},
		{ view: "decrement", label: false},
		{ attr: "quantity", label: "Qty"},
		{ view: "increment", label: false},
		{ view: "currency", attr: "total", label: "SubTotal" },
        { view: "remove", text: "Remove", label: '&nbsp;'}
    ],
    cartStyle: "div", 
    checkout: { 
	type: "custom" , 
    },
    data: {},
    language: "english-us",
    excludeFromCheckout: [],
    shippingCustom: null,
    shippingFlatRate: 0,
    shippingQuantityRate: 0,
    shippingTotalRate: 0,
    taxRate: 0,
    taxShipping: false,
});
// log the total when simpleCart has initialized
simpleCart.bind( 'ready' , function(){
  console.log( "simpleCart total: " + simpleCart.toCurrency( simpleCart.total() ) ); 
});
simpleCart.bind( 'load' , function(){
  console.log( "simpleCart has loaded " + simpleCart.quantity() + " items from from localStorage" ); 
});
simpleCart.bind( 'error' , function(message){
    alert( message );
	simpleCart.empty();
});


simpleCart.currency({
    code: "CA2" , name: "My Awesome Currency" , symbol: "CDN$" , delimiter: " " , decimal: "," , after: false , accuracy: 2
});
simpleCart.currency({
    code: "YEN" , name: "My Awesome Currency" , symbol: "&yen;" , delimiter: " " , decimal: "," , after: false , accuracy: 2
});
simpleCart.currency({
    code: "EUR" , name: "My Awesome Currency" , symbol: "EUR" , delimiter: " " , decimal: "," , after: false , accuracy: 2
});
simpleCart.currency({
    code: "INR" , name: "My Awesome Currency" , symbol: "INR" , delimiter: "," , decimal: "." , after: false , accuracy: 2
});
simpleCart.currency({
    code: "JPY" , name: "Japanese Yen" , symbol: "&yen;" , delimiter: "," , decimal: "." , after: false , accuracy: 3
});

associateauto=jQuery("#associate_auto").html();
//console.log(associateauto);
	switch (associateauto) {
		case 'JP':
		simpleCart({
			currency: "JPY",
		});
	break;
		case 'CN':
		simpleCart({
			currency: "YEN",
		});
	break;
		case 'GB':
		simpleCart({
			currency: "GBP",
		});
	break;
		case 'DE':
		simpleCart({
			currency: "EUR",
		});
	break;
	case 'IT':
		simpleCart({
			currency: "EUR",
		});
	break;
		case 'ES':
		simpleCart({
			currency: "EUR",
		});
	break; 
	case 'IN':
		simpleCart({
			currency: "INR",
		});
	break;
		case 'FR':
		simpleCart({
			currency: "EUR",
		});
	break;
		case 'CA':
		simpleCart({
			currency: "CA2",
		});
	break;
		default:
		simpleCart({
			currency: "USD",
		});
	break;
    };
