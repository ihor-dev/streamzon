

function zoom_init(){
	var zoomedIMG = jQuery(".pic .item_image");

	if(zoomedIMG.attr('data-status')=='1') return false;

	zoomedIMG.attr('data-status',1);

	var ImgW = zoomedIMG.width() ;
	var ImgH = zoomedIMG.height() ;

	zoomedIMG.ezPlus({
		easing: true,
		scrollZoom: true,
		zoomLevel : 0.7,
		zoomWindowPosition: 1,
		zoomWindowOffsetX: 20,
		zoomWindowHeight: ImgH, 
		zoomWindowWidth: ImgW
	});
}

function zoom_destroy(){
	var zoomedIMG = jQuery(".pic .item_image");

	if(zoomedIMG.attr('data-status')=='0') return false;

	zoomedIMG.attr('data-status',0);

	jQuery('.zoomContainer').remove();
	zoomedIMG.removeData('elevateZoom');
	zoomedIMG.removeData('zoomImage');
}
jQuery(document).ready(function($){
	zoom_init();

	jQuery(window).resize(function(){		

		if(jQuery(window).width()>965){
			zoom_init();
		}else{
			zoom_destroy();
		}
	});
})