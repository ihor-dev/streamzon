

jQuery(document).ready(function($){
	// =========================== YOUTUBE ============ //

	//RESIZE BACKGROUND IMAGE ON ONEPAGE HEADER
	jQuery(window).resize(function(){
		setTimeout(function(){
			set_ytvideo_bg();
		},2000);
	});		
	setTimeout(function(){
		set_ytvideo_bg();
	},2000);

	

// =========================== other =========== //

	//show header, subheader
	jQuery(".subscribe, .masthead").animate({opacity: 1},1600);

	//set pattern size
	set_pattern_size();


// ============== resize header text =========== //	
	$(window).resize();
	text_resize($("span.clone"), $("h1.origin"), $(".masthead"), 16);
	text_resize($("span.clone-1"), $(".origin-1"), $(".masthead"), 14);
	$(window).resize(function() {
	  text_resize($("span.clone"), $("h1.origin"), $(".masthead"), 16);
	  text_resize($("span.clone-1"), $(".origin-1"), $(".masthead"), 14);
	});
});



// ========== #LIBRARIES ==============================================
//---------------------------------------------------------------------

/*
	Rsize text. Params: (cloned element, origin element, container element, min font size)
*/

function text_resize(clone, origin, container, minFS) {
  var clone = clone;
  var origin = origin;
  var minFS = minFS;
  var outW = parseInt(container.width());
  var textW = parseInt(clone.width());
  
  
  if (!clone.attr('fs')) {
    clone.attr('fs', clone.css('font-size').replace("px", ""));
  }
  
  var currFS = clone.attr('fs');

  for (var i = currFS; i >= minFS; i--) {
    clone.css({'font-size': i + 'px'});
    textW = clone.width();
    
    if(outW > textW){
      origin.css({'font-size': i + 'px'});
      break;
    }    
    
  }
  

}




//set pattern size
function set_pattern_size(){
	jQuery("#header_video_bg_container, #pattern, .jq_img, .video").animate({
			height: jQuery("#header").height()+40
		}, 200	
	);
}
function set_ytvideo_bg(){
	console.log('YT');
	var hVh = jQuery("#header").height()+40;
	var hVw = jQuery("#header").width().toFixed(2);			
	var bpoint = 1188;//788; //969;//799;
	jQuery("#header_video_bg_container, #pattern, .jq_img").height(hVh);
	
	if(!jQuery(".jq_img").length){				
		
		//выставить ширину и высоту для бекграунда по контенту
		jQuery("#header_video_bg_container, #pattern").width(jQuery(document).width());
		jQuery(".video").css({ "position":"absolute", "left":0});		
		//если ширина больше высоты - считать от ширины
		if(hVw > hVh){
			jQuery(".video").css({"width":"100%"});					
			
			var vH = hVw / 1.78;
			
			//если высота видео меньше бекграунда - расчитывать от высоты
			if(vH < hVh){
				jQuery(".video").height(hVh);
				var vW = hVh * 1.78;
				jQuery(".video").width(vW);
			}else{
				jQuery(".video").height(vH);	
													
			}
			
			
		}else{//если высота больше ширины - считать от высоты
			jQuery(".video").height(hVh);
			var vW = hVh * 1.78;
			jQuery(".video").width(vW);
		}
		

		//если видеобъект получился шире - отцентровать
		if(vW > hVw){
			var diff = jQuery(".video").width()-hVw;
			diff = (diff / 2) * -1;
			jQuery(".video").css({"left":diff});
		}else{
			jQuery(".video").css({"left":0});
		}

	}
}