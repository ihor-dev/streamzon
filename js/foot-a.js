;
(function(a, b, c) {
    "use strict";
    var d = a.document,
        e = a.Modernizr,
        f = function(a) {
            return a.charAt(0).toUpperCase() + a.slice(1)
        },
        g = "Moz Webkit O Ms".split(" "),
        h = function(a) {
            var b = d.documentElement.style,
                c;
            if (typeof b[a] == "string") return a;
            a = f(a);
            for (var e = 0, h = g.length; e < h; e++) {
                c = g[e] + a;
                if (typeof b[c] == "string") return c
            }
        },
        i = h("transform"),
        j = h("transitionProperty"),
        k = {
            csstransforms: function() {
                return !!i
            },
            csstransforms3d: function() {
                var a = !!h("perspective");
                if (a) {
                    var c = " -o- -moz- -ms- -webkit- -khtml- ".split(" "),
                        d = "@media (" + c.join("transform-3d),(") + "modernizr)",
                        e = b("<style>" + d + "{#modernizr{height:3px}}" + "</style>").appendTo("head"),
                        f = b('<div id="modernizr" />').appendTo("html");
                    a = f.height() === 3, f.remove(), e.remove()
                }
                return a
            },
            csstransitions: function() {
                return !!j
            }
        },
        l;
    if (e)
        for (l in k) e.hasOwnProperty(l) || e.addTest(l, k[l]);
    else {
        e = a.Modernizr = {
            _version: "1.6ish: miniModernizr for Isotope"
        };
        var m = " ",
            n;
        for (l in k) n = k[l](), e[l] = n, m += " " + (n ? "" : "no-") + l;
        b("html").addClass(m)
    }
    if (e.csstransforms) {
        var o = e.csstransforms3d ? {
                translate: function(a) {
                    return "translate3d(" + a[0] + "px, " + a[1] + "px, 0) "
                },
                scale: function(a) {
                    return "scale3d(" + a + ", " + a + ", 1) "
                }
            } : {
                translate: function(a) {
                    return "translate(" + a[0] + "px, " + a[1] + "px) "
                },
                scale: function(a) {
                    return "scale(" + a + ") "
                }
            },
            p = function(a, c, d) {
                var e = b.data(a, "isoTransform") || {},
                    f = {},
                    g, h = {},
                    j;
                f[c] = d, b.extend(e, f);
                for (g in e) j = e[g], h[g] = o[g](j);
                var k = h.translate || "",
                    l = h.scale || "",
                    m = k + l;
                b.data(a, "isoTransform", e), a.style[i] = m
            };
        b.cssNumber.scale = !0, b.cssHooks.scale = {
            set: function(a, b) {
                p(a, "scale", b)
            },
            get: function(a, c) {
                var d = b.data(a, "isoTransform");
                return d && d.scale ? d.scale : 1
            }
        }, b.fx.step.scale = function(a) {
            b.cssHooks.scale.set(a.elem, a.now + a.unit)
        }, b.cssNumber.translate = !0, b.cssHooks.translate = {
            set: function(a, b) {
                p(a, "translate", b)
            },
            get: function(a, c) {
                var d = b.data(a, "isoTransform");
                return d && d.translate ? d.translate : [0, 0]
            }
        }
    }
    var q, r;
    e.csstransitions && (q = {
        WebkitTransitionProperty: "webkitTransitionEnd",
        MozTransitionProperty: "transitionend",
        OTransitionProperty: "oTransitionEnd otransitionend",
        transitionProperty: "transitionend"
    }[j], r = h("transitionDuration"));
    var s = b.event,
        t = b.event.handle ? "handle" : "dispatch",
        u;
    s.special.smartresize = {
        setup: function() {
            b(this).bind("resize", s.special.smartresize.handler)
        },
        teardown: function() {
            b(this).unbind("resize", s.special.smartresize.handler)
        },
        handler: function(a, b) {
            var c = this,
                d = arguments;
            a.type = "smartresize", u && clearTimeout(u), u = setTimeout(function() {
                s[t].apply(c, d)
            }, b === "execAsap" ? 0 : 100)
        }
    }, b.fn.smartresize = function(a) {
        return a ? this.bind("smartresize", a) : this.trigger("smartresize", ["execAsap"])
    }, b.Isotope = function(a, c, d) {
        this.element = b(c), this._create(a), this._init(d)
    };
    var v = ["width", "height"],
        w = b(a);
    b.Isotope.settings = {
        resizable: !0,
        layoutMode: "masonry",
        containerClass: "isotope",
        itemClass: "isotope-item",
        hiddenClass: "isotope-hidden",
        hiddenStyle: {
            opacity: 0,
            scale: .001
        },
        visibleStyle: {
            opacity: 1,
            scale: 1
        },
        containerStyle: {
            position: "relative",
            overflow: "hidden"
        },
        animationEngine: "best-available",
        animationOptions: {
            queue: !1,
            duration: 800
        },
        sortBy: "original-order",
        sortAscending: !0,
        resizesContainer: !0,
        transformsEnabled: !0,
        itemPositionDataEnabled: !1
    }, b.Isotope.prototype = {
        _create: function(a) {
            this.options = b.extend({}, b.Isotope.settings, a), this.styleQueue = [], this.elemCount = 0;
            var c = this.element[0].style;
            this.originalStyle = {};
            var d = v.slice(0);
            for (var e in this.options.containerStyle) d.push(e);
            for (var f = 0, g = d.length; f < g; f++) e = d[f], this.originalStyle[e] = c[e] || "";
            this.element.css(this.options.containerStyle), this._updateAnimationEngine(), this._updateUsingTransforms();
            var h = {
                "original-order": function(a, b) {
                    return b.elemCount++, b.elemCount
                },
                random: function() {
                    return Math.random()
                }
            };
            this.options.getSortData = b.extend(this.options.getSortData, h), this.reloadItems(), this.offset = {
                left: parseInt(this.element.css("padding-left") || 0, 10),
                top: parseInt(this.element.css("padding-top") || 0, 10)
            };
            var i = this;
            setTimeout(function() {
                i.element.addClass(i.options.containerClass)
            }, 0), this.options.resizable && w.bind("smartresize.isotope", function() {
                i.resize()
            }), this.element.delegate("." + this.options.hiddenClass, "click", function() {
                return !1
            })
        },
        _getAtoms: function(a) {
            var b = this.options.itemSelector,
                c = b ? a.filter(b).add(a.find(b)) : a,
                d = {
                    position: "absolute",
                };
            return c = c.filter(function(a, b) {
                return b.nodeType === 1
            }), this.usingTransforms && (d.left = 0, d.top = 0), c.css(d).addClass(this.options.itemClass), this.updateSortData(c, !0), c
        },
        _init: function(a) {
            this.$filteredAtoms = this._filter(this.$allAtoms), this._sort(), this.reLayout(a)
        },
        option: function(a) {
            if (b.isPlainObject(a)) {
                this.options = b.extend(!0, this.options, a);
                var c;
                for (var d in a) c = "_update" + f(d), this[c] && this[c]()
            }
        },
        _updateAnimationEngine: function() {
            var a = this.options.animationEngine.toLowerCase().replace(/[ _\-]/g, ""),
                b;
            switch (a) {
                case "css":
                case "none":
                    b = !1;
                    break;
                case "jquery":
                    b = !0;
                    break;
                default:
                    b = !e.csstransitions
            }
            this.isUsingJQueryAnimation = b, this._updateUsingTransforms()
        },
        _updateTransformsEnabled: function() {
            this._updateUsingTransforms()
        },
        _updateUsingTransforms: function() {
            var a = this.usingTransforms = this.options.transformsEnabled && e.csstransforms && e.csstransitions && !this.isUsingJQueryAnimation;
            a || (delete this.options.hiddenStyle.scale, delete this.options.visibleStyle.scale), this.getPositionStyles = a ? this._translate : this._positionAbs
        },
        _filter: function(a) {
            var b = this.options.filter === "" ? "*" : this.options.filter;
            if (!b) return a;
            var c = this.options.hiddenClass,
                d = "." + c,
                e = a.filter(d),
                f = e;
            if (b !== "*") {
                f = e.filter(b);
                var g = a.not(d).not(b).addClass(c);
                this.styleQueue.push({
                    $el: g,
                    style: this.options.hiddenStyle
                })
            }
            return this.styleQueue.push({
                $el: f,
                style: this.options.visibleStyle
            }), f.removeClass(c), a.filter(b)
        },
        updateSortData: function(a, c) {
            var d = this,
                e = this.options.getSortData,
                f, g;
            a.each(function() {
                f = b(this), g = {};
                for (var a in e) !c && a === "original-order" ? g[a] = b.data(this, "isotope-sort-data")[a] : g[a] = e[a](f, d);
                b.data(this, "isotope-sort-data", g)
            })
        },
        _sort: function() {
            var a = this.options.sortBy,
                b = this._getSorter,
                c = this.options.sortAscending ? 1 : -1,
                d = function(d, e) {
                    var f = b(d, a),
                        g = b(e, a);
                    return f === g && a !== "original-order" && (f = b(d, "original-order"), g = b(e, "original-order")), (f > g ? 1 : f < g ? -1 : 0) * c
                };
            this.$filteredAtoms.sort(d)
        },
        _getSorter: function(a, c) {
            return b.data(a, "isotope-sort-data")[c]
        },
        _translate: function(a, b) {
            return {
                translate: [a, b]
            }
        },
        _positionAbs: function(a, b) {
            return {
                left: a,
                top: b
            }
        },
        _pushPosition: function(a, b, c) {
            b = Math.round(b + this.offset.left), c = Math.round(c + this.offset.top);
            var d = this.getPositionStyles(b, c);
            this.styleQueue.push({
                $el: a,
                style: d
            }), this.options.itemPositionDataEnabled && a.data("isotope-item-position", {
                x: b,
                y: c
            })
        },
        layout: function(a, b) {
            var c = this.options.layoutMode;
            this["_" + c + "Layout"](a);
            if (this.options.resizesContainer) {
                var d = this["_" + c + "GetContainerSize"]();
                this.styleQueue.push({
                    $el: this.element,
                    style: d
                })
            }
            this._processStyleQueue(a, b), this.isLaidOut = !0
        },
        _processStyleQueue: function(a, c) {
            var d = this.isLaidOut ? this.isUsingJQueryAnimation ? "animate" : "css" : "css",
                f = this.options.animationOptions,
                g = this.options.onLayout,
                h, i, j, k;
            i = function(a, b) {
                b.$el[d](b.style, f)
            };
            if (this._isInserting && this.isUsingJQueryAnimation) i = function(a, b) {
                h = b.$el.hasClass("no-transition") ? "css" : d, b.$el[h](b.style, f)
            };
            else if (c || g || f.complete) {
                var l = !1,
                    m = [c, g, f.complete],
                    n = this;
                j = !0, k = function() {
                    if (l) return;
                    var b;
                    for (var c = 0, d = m.length; c < d; c++) b = m[c], typeof b == "function" && b.call(n.element, a, n);
                    l = !0
                };
                if (this.isUsingJQueryAnimation && d === "animate") f.complete = k, j = !1;
                else if (e.csstransitions) {
                    var o = 0,
                        p = this.styleQueue[0],
                        s = p && p.$el,
                        t;
                    while (!s || !s.length) {
                        t = this.styleQueue[o++];
                        if (!t) return;
                        s = t.$el
                    }
                    var u = parseFloat(getComputedStyle(s[0])[r]);
                    u > 0 && (i = function(a, b) {
                        b.$el[d](b.style, f).one(q, k)
                    }, j = !1)
                }
            }
            b.each(this.styleQueue, i), j && k(), this.styleQueue = []
        },
        resize: function() {
            this["_" + this.options.layoutMode + "ResizeChanged"]() && this.reLayout()
        },
        reLayout: function(a) {
            this["_" + this.options.layoutMode + "Reset"](), this.layout(this.$filteredAtoms, a)
        },
        addItems: function(a, b) {
            var c = this._getAtoms(a);
            this.$allAtoms = this.$allAtoms.add(c), b && b(c)
        },
        insert: function(a, b) {
            this.element.append(a);
            var c = this;
            this.addItems(a, function(a) {
                var d = c._filter(a);
                c._addHideAppended(d), c._sort(), c.reLayout(), c._revealAppended(d, b)
            })
        },
        appended: function(a, b) {
            var c = this;
            this.addItems(a, function(a) {
                c._addHideAppended(a), c.layout(a), c._revealAppended(a, b)
            })
        },
        _addHideAppended: function(a) {
            this.$filteredAtoms = this.$filteredAtoms.add(a), a.addClass("no-transition"), this._isInserting = !0, this.styleQueue.push({
                $el: a,
                style: this.options.hiddenStyle
            })
        },
        _revealAppended: function(a, b) {
            var c = this;
            setTimeout(function() {
                a.removeClass("no-transition"), c.styleQueue.push({
                    $el: a,
                    style: c.options.visibleStyle
                }), c._isInserting = !1, c._processStyleQueue(a, b)
            }, 10)
        },
        reloadItems: function() {
            this.$allAtoms = this._getAtoms(this.element.children())
        },
        remove: function(a, b) {
            this.$allAtoms = this.$allAtoms.not(a), this.$filteredAtoms = this.$filteredAtoms.not(a);
            var c = this,
                d = function() {
                    a.remove(), b && b.call(c.element)
                };
            a.filter(":not(." + this.options.hiddenClass + ")").length ? (this.styleQueue.push({
                $el: a,
                style: this.options.hiddenStyle
            }), this._sort(), this.reLayout(d)) : d()
        },
        shuffle: function(a) {
            this.updateSortData(this.$allAtoms), this.options.sortBy = "random", this._sort(), this.reLayout(a)
        },
        destroy: function() {
            var a = this.usingTransforms,
                b = this.options;
            this.$allAtoms.removeClass(b.hiddenClass + " " + b.itemClass).each(function() {
                var b = this.style;
                b.position = "", b.top = "", b.left = "", b.opacity = "", a && (b[i] = "")
            });
            var c = this.element[0].style;
            for (var d in this.originalStyle) c[d] = this.originalStyle[d];
            this.element.unbind(".isotope").undelegate("." + b.hiddenClass, "click").removeClass(b.containerClass).removeData("isotope"), w.unbind(".isotope")
        },
        _getSegments: function(a) {
            var b = this.options.layoutMode,
                c = a ? "rowHeight" : "columnWidth",
                d = a ? "height" : "width",
                e = a ? "rows" : "cols",
                g = this.element[d](),
                h, i = this.options[b] && this.options[b][c] || this.$filteredAtoms["outer" + f(d)](!0) || g;
            h = Math.floor(g / i), h = Math.max(h, 1), this[b][e] = h, this[b][c] = i
        },
        _checkIfSegmentsChanged: function(a) {
            var b = this.options.layoutMode,
                c = a ? "rows" : "cols",
                d = this[b][c];
            return this._getSegments(a), this[b][c] !== d
        },
        _masonryReset: function() {
            this.masonry = {}, this._getSegments();
            var a = this.masonry.cols;
            this.masonry.colYs = [];
            while (a--) this.masonry.colYs.push(0)
        },
        _masonryLayout: function(a) {
            var c = this,
                d = c.masonry;
            a.each(function() {
                var a = b(this),
                    e = Math.ceil(a.outerWidth(!0) / d.columnWidth);
                e = Math.min(e, d.cols);
                if (e === 1) c._masonryPlaceBrick(a, d.colYs);
                else {
                    var f = d.cols + 1 - e,
                        g = [],
                        h, i;
                    for (i = 0; i < f; i++) h = d.colYs.slice(i, i + e), g[i] = Math.max.apply(Math, h);
                    c._masonryPlaceBrick(a, g)
                }
            })
        },
        _masonryPlaceBrick: function(a, b) {
            var c = Math.min.apply(Math, b),
                d = 0;
            for (var e = 0, f = b.length; e < f; e++)
                if (b[e] === c) {
                    d = e;
                    break
                }
            var g = this.masonry.columnWidth * d,
                h = c;
            this._pushPosition(a, g, h);
            var i = c + a.outerHeight(!0),
                j = this.masonry.cols + 1 - f;
            for (e = 0; e < j; e++) this.masonry.colYs[d + e] = i
        },
        _masonryGetContainerSize: function() {
            var a = Math.max.apply(Math, this.masonry.colYs);
            return {
                height: a
            }
        },
        _masonryResizeChanged: function() {
            return this._checkIfSegmentsChanged()
        },
        _fitRowsReset: function() {
            this.fitRows = {
                x: 0,
                y: 0,
                height: 0
            }
        },
        _fitRowsLayout: function(a) {
            var c = this,
                d = this.element.width(),
                e = this.fitRows;
            a.each(function() {
                var a = b(this),
                    f = a.outerWidth(!0),
                    g = a.outerHeight(!0);
                e.x !== 0 && f + e.x > d && (e.x = 0, e.y = e.height), c._pushPosition(a, e.x, e.y), e.height = Math.max(e.y + g, e.height), e.x += f
            })
        },
        _fitRowsGetContainerSize: function() {
            return {
                height: this.fitRows.height
            }
        },
        _fitRowsResizeChanged: function() {
            return !0
        },
        _cellsByRowReset: function() {
            this.cellsByRow = {
                index: 0
            }, this._getSegments(), this._getSegments(!0)
        },
        _cellsByRowLayout: function(a) {
            var c = this,
                d = this.cellsByRow;
            a.each(function() {
                var a = b(this),
                    e = d.index % d.cols,
                    f = Math.floor(d.index / d.cols),
                    g = (e + .5) * d.columnWidth - a.outerWidth(!0) / 2,
                    h = (f + .5) * d.rowHeight - a.outerHeight(!0) / 2;
                c._pushPosition(a, g, h), d.index++
            })
        },
        _cellsByRowGetContainerSize: function() {
            return {
                height: Math.ceil(this.$filteredAtoms.length / this.cellsByRow.cols) * this.cellsByRow.rowHeight + this.offset.top
            }
        },
        _cellsByRowResizeChanged: function() {
            return this._checkIfSegmentsChanged()
        },
        _straightDownReset: function() {
            this.straightDown = {
                y: 0
            }
        },
        _straightDownLayout: function(a) {
            var c = this;
            a.each(function(a) {
                var d = b(this);
                c._pushPosition(d, 0, c.straightDown.y), c.straightDown.y += d.outerHeight(!0)
            })
        },
        _straightDownGetContainerSize: function() {
            return {
                height: this.straightDown.y
            }
        },
        _straightDownResizeChanged: function() {
            return !0
        },
        _masonryHorizontalReset: function() {
            this.masonryHorizontal = {}, this._getSegments(!0);
            var a = this.masonryHorizontal.rows;
            this.masonryHorizontal.rowXs = [];
            while (a--) this.masonryHorizontal.rowXs.push(0)
        },
        _masonryHorizontalLayout: function(a) {
            var c = this,
                d = c.masonryHorizontal;
            a.each(function() {
                var a = b(this),
                    e = Math.ceil(a.outerHeight(!0) / d.rowHeight);
                e = Math.min(e, d.rows);
                if (e === 1) c._masonryHorizontalPlaceBrick(a, d.rowXs);
                else {
                    var f = d.rows + 1 - e,
                        g = [],
                        h, i;
                    for (i = 0; i < f; i++) h = d.rowXs.slice(i, i + e), g[i] = Math.max.apply(Math, h);
                    c._masonryHorizontalPlaceBrick(a, g)
                }
            })
        },
        _masonryHorizontalPlaceBrick: function(a, b) {
            var c = Math.min.apply(Math, b),
                d = 0;
            for (var e = 0, f = b.length; e < f; e++)
                if (b[e] === c) {
                    d = e;
                    break
                }
            var g = c,
                h = this.masonryHorizontal.rowHeight * d;
            this._pushPosition(a, g, h);
            var i = c + a.outerWidth(!0),
                j = this.masonryHorizontal.rows + 1 - f;
            for (e = 0; e < j; e++) this.masonryHorizontal.rowXs[d + e] = i
        },
        _masonryHorizontalGetContainerSize: function() {
            var a = Math.max.apply(Math, this.masonryHorizontal.rowXs);
            return {
                width: a
            }
        },
        _masonryHorizontalResizeChanged: function() {
            return this._checkIfSegmentsChanged(!0)
        },
        _fitColumnsReset: function() {
            this.fitColumns = {
                x: 0,
                y: 0,
                width: 0
            }
        },
        _fitColumnsLayout: function(a) {
            var c = this,
                d = this.element.height(),
                e = this.fitColumns;
            a.each(function() {
                var a = b(this),
                    f = a.outerWidth(!0),
                    g = a.outerHeight(!0);
                e.y !== 0 && g + e.y > d && (e.x = e.width, e.y = 0), c._pushPosition(a, e.x, e.y), e.width = Math.max(e.x + f, e.width), e.y += g
            })
        },
        _fitColumnsGetContainerSize: function() {
            return {
                width: this.fitColumns.width
            }
        },
        _fitColumnsResizeChanged: function() {
            return !0
        },
        _cellsByColumnReset: function() {
            this.cellsByColumn = {
                index: 0
            }, this._getSegments(), this._getSegments(!0)
        },
        _cellsByColumnLayout: function(a) {
            var c = this,
                d = this.cellsByColumn;
            a.each(function() {
                var a = b(this),
                    e = Math.floor(d.index / d.rows),
                    f = d.index % d.rows,
                    g = (e + .5) * d.columnWidth - a.outerWidth(!0) / 2,
                    h = (f + .5) * d.rowHeight - a.outerHeight(!0) / 2;
                c._pushPosition(a, g, h), d.index++
            })
        },
        _cellsByColumnGetContainerSize: function() {
            return {
                width: Math.ceil(this.$filteredAtoms.length / this.cellsByColumn.rows) * this.cellsByColumn.columnWidth
            }
        },
        _cellsByColumnResizeChanged: function() {
            return this._checkIfSegmentsChanged(!0)
        },
        _straightAcrossReset: function() {
            this.straightAcross = {
                x: 0
            }
        },
        _straightAcrossLayout: function(a) {
            var c = this;
            a.each(function(a) {
                var d = b(this);
                c._pushPosition(d, c.straightAcross.x, 0), c.straightAcross.x += d.outerWidth(!0)
            })
        },
        _straightAcrossGetContainerSize: function() {
            return {
                width: this.straightAcross.x
            }
        },
        _straightAcrossResizeChanged: function() {
            return !0
        }
    }, b.fn.imagesLoaded = function(a) {
        function h() {
            a.call(c, d)
        }

        function i(a) {
            var c = a.target;
            c.src !== f && b.inArray(c, g) === -1 && (g.push(c), --e <= 0 && (setTimeout(h), d.unbind(".imagesLoaded", i)))
        }
        var c = this,
            d = c.find("img").add(c.filter("img")),
            e = d.length,
            f = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",
            g = [];
        return e || h(), d.bind("load.imagesLoaded error.imagesLoaded", i).each(function() {
            var a = this.src;
            this.src = f, this.src = a
        }), c
    };
    var x = function(b) {
        a.console && a.console.error(b)
    };
    b.fn.isotope = function(a, c) {
        if (typeof a == "string") {
            var d = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var c = b.data(this, "isotope");
                if (!c) {
                    x("cannot call methods on isotope prior to initialization; attempted to call method '" + a + "'");
                    return
                }
                if (!b.isFunction(c[a]) || a.charAt(0) === "_") {
                    x("no such method '" + a + "' for isotope instance");
                    return
                }
                c[a].apply(c, d)
            })
        } else this.each(function() {
            var d = b.data(this, "isotope");
            d ? (d.option(a), d._init(c)) : b.data(this, "isotope", new b.Isotope(a, this, c))
        });
        return this
    }
})(window, jQuery);
/*!
 * imagesLoaded PACKAGED v3.0.3
 */
(function() {
    "use strict";

    function e() {}

    function t(e, t) {
        for (var n = e.length; n--;)
            if (e[n].listener === t) return n;
        return -1
    }
    var n = e.prototype;
    n.getListeners = function(e) {
        var t, n, i = this._getEvents();
        if ("object" == typeof e) {
            t = {};
            for (n in i) i.hasOwnProperty(n) && e.test(n) && (t[n] = i[n])
        } else t = i[e] || (i[e] = []);
        return t
    }, n.flattenListeners = function(e) {
        var t, n = [];
        for (t = 0; e.length > t; t += 1) n.push(e[t].listener);
        return n
    }, n.getListenersAsObject = function(e) {
        var t, n = this.getListeners(e);
        return n instanceof Array && (t = {}, t[e] = n), t || n
    }, n.addListener = function(e, n) {
        var i, r = this.getListenersAsObject(e),
            s = "object" == typeof n;
        for (i in r) r.hasOwnProperty(i) && -1 === t(r[i], n) && r[i].push(s ? n : {
            listener: n,
            once: !1
        });
        return this
    }, n.on = n.addListener, n.addOnceListener = function(e, t) {
        return this.addListener(e, {
            listener: t,
            once: !0
        })
    }, n.once = n.addOnceListener, n.defineEvent = function(e) {
        return this.getListeners(e), this
    }, n.defineEvents = function(e) {
        for (var t = 0; e.length > t; t += 1) this.defineEvent(e[t]);
        return this
    }, n.removeListener = function(e, n) {
        var i, r, s = this.getListenersAsObject(e);
        for (r in s) s.hasOwnProperty(r) && (i = t(s[r], n), -1 !== i && s[r].splice(i, 1));
        return this
    }, n.off = n.removeListener, n.addListeners = function(e, t) {
        return this.manipulateListeners(!1, e, t)
    }, n.removeListeners = function(e, t) {
        return this.manipulateListeners(!0, e, t)
    }, n.manipulateListeners = function(e, t, n) {
        var i, r, s = e ? this.removeListener : this.addListener,
            o = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (i = n.length; i--;) s.call(this, t, n[i]);
        else
            for (i in t) t.hasOwnProperty(i) && (r = t[i]) && ("function" == typeof r ? s.call(this, i, r) : o.call(this, i, r));
        return this
    }, n.removeEvent = function(e) {
        var t, n = typeof e,
            i = this._getEvents();
        if ("string" === n) delete i[e];
        else if ("object" === n)
            for (t in i) i.hasOwnProperty(t) && e.test(t) && delete i[t];
        else delete this._events;
        return this
    }, n.emitEvent = function(e, t) {
        var n, i, r, s, o = this.getListenersAsObject(e);
        for (r in o)
            if (o.hasOwnProperty(r))
                for (i = o[r].length; i--;) n = o[r][i], s = n.listener.apply(this, t || []), (s === this._getOnceReturnValue() || n.once === !0) && this.removeListener(e, o[r][i].listener);
        return this
    }, n.trigger = n.emitEvent, n.emit = function(e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, n.setOnceReturnValue = function(e) {
        return this._onceReturnValue = e, this
    }, n._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, n._getEvents = function() {
        return this._events || (this._events = {})
    }, "function" == typeof define && define.amd ? define(function() {
        return e
    }) : "undefined" != typeof module && module.exports ? module.exports = e : this.EventEmitter = e
}).call(this),
    function(e) {
        "use strict";
        var t = document.documentElement,
            n = function() {};
        t.addEventListener ? n = function(e, t, n) {
            e.addEventListener(t, n, !1)
        } : t.attachEvent && (n = function(t, n, i) {
            t[n + i] = i.handleEvent ? function() {
                var t = e.event;
                t.target = t.target || t.srcElement, i.handleEvent.call(i, t)
            } : function() {
                var n = e.event;
                n.target = n.target || n.srcElement, i.call(t, n)
            }, t.attachEvent("on" + n, t[n + i])
        });
        var i = function() {};
        t.removeEventListener ? i = function(e, t, n) {
            e.removeEventListener(t, n, !1)
        } : t.detachEvent && (i = function(e, t, n) {
            e.detachEvent("on" + t, e[t + n]);
            try {
                delete e[t + n]
            } catch (i) {
                e[t + n] = void 0
            }
        });
        var r = {
            bind: n,
            unbind: i
        };
        "function" == typeof define && define.amd ? define(r) : e.eventie = r
    }(this),
    function(e) {
        "use strict";

        function t(e, t) {
            for (var n in t) e[n] = t[n];
            return e
        }

        function n(e) {
            return "[object Array]" === h.call(e)
        }

        function i(e) {
            var t = [];
            if (n(e)) t = e;
            else if ("number" == typeof e.length)
                for (var i = 0, r = e.length; r > i; i++) t.push(e[i]);
            else t.push(e);
            return t
        }

        function r(e, n) {
            function r(e, n, o) {
                if (!(this instanceof r)) return new r(e, n);
                "string" == typeof e && (e = document.querySelectorAll(e)), this.elements = i(e), this.options = t({}, this.options), "function" == typeof n ? o = n : t(this.options, n), o && this.on("always", o), this.getImages(), s && (this.jqDeferred = new s.Deferred);
                var a = this;
                setTimeout(function() {
                    a.check()
                })
            }

            function h(e) {
                this.img = e
            }
            r.prototype = new e, r.prototype.options = {}, r.prototype.getImages = function() {
                this.images = [];
                for (var e = 0, t = this.elements.length; t > e; e++) {
                    var n = this.elements[e];
                    "IMG" === n.nodeName && this.addImage(n);
                    for (var i = n.querySelectorAll("img"), r = 0, s = i.length; s > r; r++) {
                        var o = i[r];
                        this.addImage(o)
                    }
                }
            }, r.prototype.addImage = function(e) {
                var t = new h(e);
                this.images.push(t)
            }, r.prototype.check = function() {
                function e(e, r) {
                    return t.options.debug && a && o.log("confirm", e, r), t.progress(e), n++, n === i && t.complete(), !0
                }
                var t = this,
                    n = 0,
                    i = this.images.length;
                if (this.hasAnyBroken = !1, !i) return this.complete(), void 0;
                for (var r = 0; i > r; r++) {
                    var s = this.images[r];
                    s.on("confirm", e), s.check()
                }
            }, r.prototype.progress = function(e) {
                var t = this;
                this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, setTimeout(function() {
                    t.emit("progress", t, e), t.jqDeferred && t.jqDeferred.notify(t, e)
                })
            }, r.prototype.complete = function() {
                var e = this.hasAnyBroken ? "fail" : "done";
                if (this.isComplete = !0, this.emit(e, this), this.emit("always", this), this.jqDeferred) {
                    var t = this.hasAnyBroken ? "reject" : "resolve";
                    this.jqDeferred[t](this)
                }
            }, s && (s.fn.imagesLoaded = function(e, t) {
                var n = new r(this, e, t);
                return n.jqDeferred.promise(s(this))
            });
            var c = {};
            return h.prototype = new e, h.prototype.check = function() {
                var e = c[this.img.src];
                if (e) return this.useCached(e), void 0;
                if (c[this.img.src] = this, this.img.complete && void 0 !== this.img.naturalWidth) return this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), void 0;
                var t = this.proxyImage = new Image;
                n.bind(t, "load", this), n.bind(t, "error", this), t.src = this.img.src
            }, h.prototype.useCached = function(e) {
                if (e.isConfirmed) this.confirm(e.isLoaded, "cached was confirmed");
                else {
                    var t = this;
                    e.on("confirm", function(e) {
                        return t.confirm(e.isLoaded, "cache emitted confirmed"), !0
                    })
                }
            }, h.prototype.confirm = function(e, t) {
                this.isConfirmed = !0, this.isLoaded = e, this.emit("confirm", this, t)
            }, h.prototype.handleEvent = function(e) {
                var t = "on" + e.type;
                this[t] && this[t](e)
            }, h.prototype.onload = function() {
                this.confirm(!0, "onload"), this.unbindProxyEvents()
            }, h.prototype.onerror = function() {
                this.confirm(!1, "onerror"), this.unbindProxyEvents()
            }, h.prototype.unbindProxyEvents = function() {
                n.unbind(this.proxyImage, "load", this), n.unbind(this.proxyImage, "error", this)
            }, r
        }
        var s = e.jQuery,
            o = e.console,
            a = o !== void 0,
            h = Object.prototype.toString;
        "function" == typeof define && define.amd ? define(["eventEmitter", "eventie"], r) : e.imagesLoaded = r(e.EventEmitter, e.eventie)
    }(window);
(function(o, i, k) {
    i.infinitescroll = function z(D, F, E) {
        this.element = i(E);
        if (!this._create(D, F)) {
            this.failed = true
        }
    };
    i.infinitescroll.defaults = {
        loading: {
            finished: k,
            finishedMsg: "",
            img: "",
            msg: null,
            msgText: "",
            selector: null,
            speed: "fast",
            start: k
        },
        state: {
            isDuringAjax: false,
            isInvalidPage: false,
            isDestroyed: false,
            isDone: false,
            isPaused: false,
            currPage: 1
        },
        debug: false,
        behavior: k,
        binder: i(o),
        nextSelector: "div.navigation a:first",
        navSelector: "div.navigation",
        contentSelector: null,
        extraScrollPx: 150,
        itemSelector: "div.post",
        animate: false,
        pathParse: k,
        dataType: "html",
        appendCallback: true,
        bufferPx: 40,
        errorCallback: function() {},
        infid: 0,
        pixelsFromNavToBottom: k,
        path: k,
        prefill: false,
        maxPage: k
    };
    i.infinitescroll.prototype = {
        _binding: function g(F) {
            var D = this,
                E = D.options;
            E.v = "2.0b2.120520";
            if (!!E.behavior && this["_binding_" + E.behavior] !== k) {
                this["_binding_" + E.behavior].call(this);
                return
            }
            if (F !== "bind" && F !== "unbind") {
                this._debug("Binding value  " + F + " not valid");
                return false
            }
            if (F === "unbind") {
                (this.options.binder).unbind("smartscroll.infscr." + D.options.infid)
            } else {
                (this.options.binder)[F]("smartscroll.infscr." + D.options.infid, function() {
                    D.scroll()
                })
            }
            this._debug("Binding", F)
        },
        _create: function t(F, J) {
            var G = i.extend(true, {}, i.infinitescroll.defaults, F);
            this.options = G;
            var I = i(o);
            var D = this;
            if (!D._validate(F)) {
                return false
            }
            var H = i(G.nextSelector).attr("href");
            if (!H) {
                this._debug("Navigation selector not found");
                return false
            }
            G.path = G.path || this._determinepath(H);
            G.contentSelector = G.contentSelector || this.element;
            G.loading.selector = G.loading.selector || G.contentSelector;
            G.loading.msg = G.loading.msg || i('<div id="infscr-loading"><img alt="Loading..." src="' + G.loading.img + '" /><div>' + G.loading.msgText + "</div></div>");
            (new Image()).src = G.loading.img;
            if (G.pixelsFromNavToBottom === k) {
                G.pixelsFromNavToBottom = i(document).height() - i(G.navSelector).offset().top
            }
            var E = this;
            G.loading.start = G.loading.start || function() {
                i(G.navSelector).hide();
                G.loading.msg.appendTo(G.loading.selector).show(G.loading.speed, i.proxy(function() {
                    this.beginAjax(G)
                }, E))
            };
            G.loading.finished = G.loading.finished || function() {
                G.loading.msg.fadeOut(G.loading.speed)
            };
            G.callback = function(K, M, L) {
                if (!!G.behavior && K["_callback_" + G.behavior] !== k) {
                    K["_callback_" + G.behavior].call(i(G.contentSelector)[0], M, L)
                }
                if (J) {
                    J.call(i(G.contentSelector)[0], M, G, L)
                }
                if (G.prefill) {
                    I.bind("resize.infinite-scroll", K._prefill)
                }
            };
            if (F.debug) {
                if (Function.prototype.bind && (typeof console === "object" || typeof console === "function") && typeof console.log === "object") {
                    ["log", "info", "warn", "error", "assert", "dir", "clear", "profile", "profileEnd"].forEach(function(K) {
                        console[K] = this.call(console[K], console)
                    }, Function.prototype.bind)
                }
            }
            this._setup();
            if (G.prefill) {
                this._prefill()
            }
            return true
        },
        _prefill: function n() {
            var D = this;
            var G = i(document);
            var F = i(o);

            function E() {
                return (G.height() <= F.height())
            }
            this._prefill = function() {
                if (E()) {
                    D.scroll()
                }
                F.bind("resize.infinite-scroll", function() {
                    if (E()) {
                        F.unbind("resize.infinite-scroll");
                        D.scroll()
                    }
                })
            };
            this._prefill()
        },
        _debug: function q() {
            if (true !== this.options.debug) {
                return
            }
            if (typeof console !== "undefined" && typeof console.log === "function") {
                if ((Array.prototype.slice.call(arguments)).length === 1 && typeof Array.prototype.slice.call(arguments)[0] === "string") {
                    console.log((Array.prototype.slice.call(arguments)).toString())
                } else {
                    console.log(Array.prototype.slice.call(arguments))
                }
            } else {
                if (!Function.prototype.bind && typeof console !== "undefined" && typeof console.log === "object") {
                    Function.prototype.call.call(console.log, console, Array.prototype.slice.call(arguments))
                }
            }
        },
        _determinepath: function A(E) {
            var D = this.options;
            if (!!D.behavior && this["_determinepath_" + D.behavior] !== k) {
                return this["_determinepath_" + D.behavior].call(this, E)
            }
            if (!!D.pathParse) {
                this._debug("pathParse manual");
                return D.pathParse(E, this.options.state.currPage + 1)
            } else {
                if (E.match(/^(.*?)\b2\b(?!.*\b2\b)(.*?$)/)) {
                    E = E.match(/^(.*?)\b2\b(?!.*\b2\b)(.*?$)/).slice(1)
                } else {
                    if (E.match(/^(.*?)2(.*?$)/)) {
                        if (E.match(/^(.*?page=)2(\/.*|$)/)) {
                            E = E.match(/^(.*?page=)2(\/.*|$)/).slice(1);
                            return E
                        }
                        E = E.match(/^(.*?)2(.*?$)/).slice(1)
                    } else {
                        if (E.match(/^(.*?page=)1(\/.*|$)/)) {
                            E = E.match(/^(.*?page=)1(\/.*|$)/).slice(1);
                            return E
                        } else {
                            this._debug("Sorry, we couldn't parse your Next (Previous Posts) URL. Verify your the css selector points to the correct A tag. If you still get this error: yell, scream, and kindly ask for help at infinite-scroll.com.");
                            D.state.isInvalidPage = true
                        }
                    }
                }
            }
            this._debug("determinePath", E);
            return E
        },
        _error: function v(E) {
            var D = this.options;
            if (!!D.behavior && this["_error_" + D.behavior] !== k) {
                this["_error_" + D.behavior].call(this, E);
                return
            }
            if (E !== "destroy" && E !== "end") {
                E = "unknown"
            }
            this._debug("Error", E);
            if (E === "end") {
                this._showdonemsg()
            }
            D.state.isDone = true;
            D.state.currPage = 1;
            D.state.isPaused = false;
            this._binding("unbind")
        },
        _loadcallback: function c(H, G, E) {
            var D = this.options,
                J = this.options.callback,
                L = (D.state.isDone) ? "done" : (!D.appendCallback) ? "no-append" : "append",
                K;
            if (!!D.behavior && this["_loadcallback_" + D.behavior] !== k) {
                this["_loadcallback_" + D.behavior].call(this, H, G);
                return
            }
            switch (L) {
                case "done":
                    this._showdonemsg();
                    return false;
                case "no-append":
                    if (D.dataType === "html") {
                        G = "<div>" + G + "</div>";
                        G = i(G).find(D.itemSelector)
                    }
                    break;
                case "append":
                    var F = H.children();
                    if (F.length === 0) {
                        return this._error("end")
                    }
                    K = document.createDocumentFragment();
                    while (H[0].firstChild) {
                        K.appendChild(H[0].firstChild)
                    }
                    this._debug("contentSelector", i(D.contentSelector)[0]);
                    i(D.contentSelector)[0].appendChild(K);
                    G = F.get();
                    break
            }
            D.loading.finished.call(i(D.contentSelector)[0], D);
            if (D.animate) {
                var I = i(o).scrollTop() + i("#infscr-loading").height() + D.extraScrollPx + "px";
                i("html,body").animate({
                    scrollTop: I
                }, 800, function() {
                    D.state.isDuringAjax = false
                })
            }
            if (!D.animate) {
                D.state.isDuringAjax = false
            }
            J(this, G, E);
            if (D.prefill) {
                this._prefill()
            }
        },
        _nearbottom: function u() {
            var E = this.options,
                D = 0 + i(document).height() - (E.binder.scrollTop()) - i(o).height();
            if (!!E.behavior && this["_nearbottom_" + E.behavior] !== k) {
                return this["_nearbottom_" + E.behavior].call(this)
            }
            this._debug("math:", D, E.pixelsFromNavToBottom);
            return (D - E.bufferPx < E.pixelsFromNavToBottom)
        },
        _pausing: function l(E) {
            var D = this.options;
            if (!!D.behavior && this["_pausing_" + D.behavior] !== k) {
                this["_pausing_" + D.behavior].call(this, E);
                return
            }
            if (E !== "pause" && E !== "resume" && E !== null) {
                this._debug("Invalid argument. Toggling pause value instead")
            }
            E = (E && (E === "pause" || E === "resume")) ? E : "toggle";
            switch (E) {
                case "pause":
                    D.state.isPaused = true;
                    break;
                case "resume":
                    D.state.isPaused = false;
                    break;
                case "toggle":
                    D.state.isPaused = !D.state.isPaused;
                    break
            }
            this._debug("Paused", D.state.isPaused);
            return false
        },
        _setup: function r() {
            var D = this.options;
            if (!!D.behavior && this["_setup_" + D.behavior] !== k) {
                this["_setup_" + D.behavior].call(this);
                return
            }
            this._binding("bind");
            return false
        },
        _showdonemsg: function a() {
            var D = this.options;
            if (!!D.behavior && this["_showdonemsg_" + D.behavior] !== k) {
                this["_showdonemsg_" + D.behavior].call(this);
                return
            }
            D.loading.msg.find("img").hide().parent().find("div").html(D.loading.finishedMsg).animate({
                opacity: 1
            }, 2000, function() {
                i(this).parent().fadeOut(D.loading.speed)
            });
            D.errorCallback.call(i(D.contentSelector)[0], "done")
        },
        _validate: function w(E) {
            for (var D in E) {
                if (D.indexOf && D.indexOf("Selector") > -1 && i(E[D]).length === 0) {
                    this._debug("Your " + D + " found no elements.");
                    return false
                }
            }
            return true
        },
        bind: function p() {
            this._binding("bind")
        },
        destroy: function C() {
            this.options.state.isDestroyed = true;
            this.options.loading.finished();
            return this._error("destroy")
        },
        pause: function e() {
            this._pausing("pause")
        },
        resume: function h() {
            this._pausing("resume")
        },
        beginAjax: function B(G) {
            var E = this,
                I = G.path,
                F, D, K, J;
            G.state.currPage++;
            if (G.maxPage != k && G.state.currPage > G.maxPage) {
                this.destroy();
                return
            }
            F = i(G.contentSelector).is("table") ? i("<tbody/>") : i("<div/>");
            D = (typeof I === "function") ? I(G.state.currPage) : I.join(G.state.currPage);
            E._debug("heading into ajax", D);
            K = (G.dataType === "html" || G.dataType === "json") ? G.dataType : "html+callback";
            if (G.appendCallback && G.dataType === "html") {
                K += "+callback"
            }
            switch (K) {
                case "html+callback":
                    E._debug("Using HTML via .load() method");
                    F.load(D + " " + G.itemSelector, k, function H(L) {
                        E._loadcallback(F, L, D)
                    });
                    break;
                case "html":
                    E._debug("Using " + (K.toUpperCase()) + " via $.ajax() method");
                    i.ajax({
                        url: D,
                        dataType: G.dataType,
                        complete: function H(L, M) {
                            J = (typeof(L.isResolved) !== "undefined") ? (L.isResolved()) : (M === "success" || M === "notmodified");
                            if (J) {
                                E._loadcallback(F, L.responseText, D)
                            } else {
                                E._error("end")
                            }
                        }
                    });
                    break;
                case "json":
                    E._debug("Using " + (K.toUpperCase()) + " via $.ajax() method");
                    i.ajax({
                        dataType: "json",
                        type: "GET",
                        url: D,
                        success: function(N, O, M) {
                            J = (typeof(M.isResolved) !== "undefined") ? (M.isResolved()) : (O === "success" || O === "notmodified");
                            if (G.appendCallback) {
                                if (G.template !== k) {
                                    var L = G.template(N);
                                    F.append(L);
                                    if (J) {
                                        E._loadcallback(F, L)
                                    } else {
                                        E._error("end")
                                    }
                                } else {
                                    E._debug("template must be defined.");
                                    E._error("end")
                                }
                            } else {
                                if (J) {
                                    E._loadcallback(F, N, D)
                                } else {
                                    E._error("end")
                                }
                            }
                        },
                        error: function() {
                            E._debug("JSON ajax request failed.");
                            E._error("end")
                        }
                    });
                    break
            }
        },
        retrieve: function b(F) {
            F = F || null;
            var D = this,
                E = D.options;
            if (!!E.behavior && this["retrieve_" + E.behavior] !== k) {
                this["retrieve_" + E.behavior].call(this, F);
                return
            }
            if (E.state.isDestroyed) {
                this._debug("Instance is destroyed");
                return false
            }
            E.state.isDuringAjax = true;
            E.loading.start.call(i(E.contentSelector)[0], E)
        },
        scroll: function f() {
            var D = this.options,
                E = D.state;
            if (!!D.behavior && this["scroll_" + D.behavior] !== k) {
                this["scroll_" + D.behavior].call(this);
                return
            }
            if (E.isDuringAjax || E.isInvalidPage || E.isDone || E.isDestroyed || E.isPaused) {
                return
            }
            if (!this._nearbottom()) {
                return
            }
            this.retrieve()
        },
        toggle: function y() {
            this._pausing()
        },
        unbind: function m() {
            this._binding("unbind")
        },
        update: function j(D) {
            if (i.isPlainObject(D)) {
                this.options = i.extend(true, this.options, D)
            }
        }
    };
    i.fn.infinitescroll = function d(F, G) {
        var E = typeof F;
        switch (E) {
            case "string":
                var D = Array.prototype.slice.call(arguments, 1);
                this.each(function() {
                    var H = i.data(this, "infinitescroll");
                    if (!H) {
                        return false
                    }
                    if (!i.isFunction(H[F]) || F.charAt(0) === "_") {
                        return false
                    }
                    H[F].apply(H, D)
                });
                break;
            case "object":
                this.each(function() {
                    var H = i.data(this, "infinitescroll");
                    if (H) {
                        H.update(F)
                    } else {
                        H = new i.infinitescroll(F, G, this);
                        if (!H.failed) {
                            i.data(this, "infinitescroll", H)
                        }
                    }
                });
                break
        }
        return this
    };
    var x = i.event,
        s;
    x.special.smartscroll = {
        setup: function() {
            i(this).bind("scroll", x.special.smartscroll.handler)
        },
        teardown: function() {
            i(this).unbind("scroll", x.special.smartscroll.handler)
        },
        handler: function(G, D) {
            var F = this,
                E = arguments;
            G.type = "smartscroll";
            if (s) {
                clearTimeout(s)
            }
            s = setTimeout(function() {
                i(F).trigger("smartscroll", E)
            }, D === "execAsap" ? 0 : 100)
        }
    };
    i.fn.smartscroll = function(D) {
        return D ? this.bind("smartscroll", D) : this.trigger("smartscroll", ["execAsap"])
    }
})(window, jQuery);
(function($, undefined) {
    $.extend($.infinitescroll.prototype, {
        _setup_twitter: function infscr_setup_twitter() {
            var opts = this.options,
                instance = this;
            $(opts.nextSelector).click(function(e) {
                if (e.which == 1 && !e.metaKey && !e.shiftKey) {
                    e.preventDefault();
                    instance.retrieve();
                }
            });
            instance.options.loading.start = function(opts) {
                opts.loading.msg.appendTo(opts.loading.selector).show(opts.loading.speed, function() {
                    instance.beginAjax(opts);
                });
            };
        },
        _showdonemsg_twitter: function infscr_showdonemsg_twitter() {
            var opts = this.options,
                instance = this;
            opts.loading.msg.find("img").hide().parent().find("div").html(opts.loading.finishedMsg).animate({
                opacity: 1
            }, 2000, function() {
                $(this).parent().fadeOut("normal");
            });
            $(opts.navSelector).fadeOut("normal");
            opts.errorCallback.call($(opts.contentSelector)[0], "done");
        }
    });
})(jQuery);;
if (navigator.userAgent.match(/iPhone/i)) {
    (function(doc) {
        var addEvent = 'addEventListener',
            type = 'gesturestart',
            qsa = 'querySelectorAll',
            scales = [1, 1],
            meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

        function fix() {
            meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
            doc.removeEventListener(type, fix, true);
        }
        if ((meta = meta[meta.length - 1]) && addEvent in doc) {
            fix();
            scales = [.25, 1.6];
            doc[addEvent](type, fix, true);
        }
    }(document));
}
var FixedHeader = {};
(function($) {
    function is_touch_device() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }
    var ItemBoard = {
        init: function(config) {
            this.config = config;
            this.bindEvents();
        },
        columns: 0,
        itemMargin: 20,
        itemPadding: 0,
        sidebarGap: 30,
        bindEvents: function() {
            var _self = this;
            jQuery(document).ready(function() {
                _self.elementSetup()
            });
            jQuery(window).resize(function() {
                _self.elementSetup()
            });
        },
        elementSetup: function() {
            var item = jQuery(this.config.itemElement),
                viewport_width = this.viewportWidth()-110,
                fixwidth, maxWidth, content_w;
            this.itemWidthOuter = this.itemWidthInner() + this.itemMargin + this.itemPadding;
            this.columns = parseInt(viewport_width / this.itemWidthOuter);
            fixwidth = (this.columns+1) * this.itemWidthInner() + ((this.columns) * this.itemMargin);
            maxWidth = '100%';
            if (jQuery('#sidebar').length > 0) {
                fixwidth -= 135;
                content_w = fixwidth - jQuery('#sidebar').width() + 15;
                jQuery('#content').width("calc(100% - 254px)");
                fixwidth = fixwidth + this.sidebarGap;
                if (viewport_width < 674) {
                    jQuery('#content').css({
                        width: ''
                    });
		
                    fixwidth = (this.columns+1) * this.itemWidthInner() + ((this.columns) * this.itemMargin);
                }
            } else {
                jQuery('#content').css({
                    width: '100%'
                });
            }
            if ((viewport_width <= 505) || (jQuery('.list-post').length > 0)) {
                fixwidth = '100%';
                maxWidth = '94%';
            }
		 
            jQuery(this.config.appliedTo).each(function() {
		
                jQuery(this).css({ 
                    'width': fixwidth + 'px',
                    'max-width': maxWidth
                });
            });
        },
        itemWidthInner: function() {
            var innerwidth = jQuery(this.config.itemElement).width();
            return innerwidth;
        },
        viewportWidth: function() {
            return jQuery(window).width();
        }
    };
    $.fn.bindWithDelay = function(type, data, fn, timeout, throttle) {
        if ($.isFunction(data)) {
            throttle = timeout;
            timeout = fn;
            fn = data;
            data = undefined;
        }
        fn.guid = fn.guid || ($.guid && $.guid++);
        return this.each(function() {
            var wait = null;

            function cb() {
                var e = $.extend(true, {}, arguments[0]);
                var ctx = this;
                var throttler = function() {
                    wait = null;
                    fn.apply(ctx, [e]);
                };
                if (!throttle) {
                    clearTimeout(wait);
                    wait = null;
                }
                if (!wait) {
                    wait = setTimeout(throttler, timeout);
                }
            }
            cb.guid = fn.guid;
            $(this).bind(type, data, cb);
        });
    }
    FixedHeader = {
        init: function() {
            var cons = is_touch_device() ? 10 : 0;
            this.headerHeight = $('#headerwrap').height() - cons;
            $(window).on('scroll', this.activate).on('touchstart.touchScroll', this.activate).on('touchmove.touchScroll', this.activate);
        },
        activate: function() {
            var $window = $(window),
                scrollTop = $window.scrollTop();
            if (scrollTop > FixedHeader.headerHeight) {
                FixedHeader.scrollEnabled();
            } else {
                FixedHeader.scrollDisabled();
            }
        },
        scrollDisabled: function() {
            $('#headerwrap').removeClass('fixed-header');
            $('#header').removeClass('header-on-scroll');
        },
        scrollEnabled: function() {
            $('#headerwrap').addClass('fixed-header');
            $('#header').addClass('header-on-scroll');
        }
    };
    $(document).ready(function() {
        var isTyping = null,
            delayCheck = 500;
        if (is_touch_device()) {
            $('body').addClass('is-touch');
        }
        if (themeStream.fixedHeader) {
            FixedHeader.init();
        }


        $('#blogname, #blog_title').on('keyup', function() {
            if ('' == $(this).val()) return;
            $sitenamecheck = $('#sitename_check');
            $sitetitlecheck = $('#sitetitle_check');
            $('#sitename_check, #sitetitle_check, .signup-messages').text('');
            clearTimeout(isTyping);
            isTyping = setTimeout(function() {
                clearTimeout(isTyping);
                if ('' != $('#blogname').val()) {
                    $sitenamecheck.html(themeStream.checking);
                    $.post(themeStream.ajax_url, {
                        action: 'stream_check_site',
                        nonce: themeStream.ajax_nonce,
                        blogname: $('#blogname').val(),
                        blog_title: $('#blog_title').val()
                    }, function(response, statusText) {
                        if ('success' != statusText) {
                            $('.signup-messages').text(themeStream.networkError);
                            return;
                        }
                        json = jQuery.parseJSON(response);
                        $sitetitlecheck.html('');
                        $sitenamecheck.html('');
                        if (json.errors) {
                            if (json.errors.blogname)
                                for (var error in json.errors.blogname) {
                                    $sitenamecheck.append('<p class="error-check error-site_name">' + json.errors.blogname[error] + '</p>');
                                }
                            for (var error in json.errors.blog_title) {
                                $sitetitlecheck.append('<p class="error-check error-site_title">' + json.errors.blog_title[error] + '</p>');
                            }
                        }
                    });
                }
            }, delayCheck);
        });
        $('#user_name, #user_email').on('keyup', function() {
            if ('' == $(this).val()) return;
            $usercheck = $('#username_check');
            $emailcheck = $('#email_check');
            $('#username_check, #email_check, .signup-messages').text('');
            clearTimeout(isTyping);
            isTyping = setTimeout(function() {
                clearTimeout(isTyping);
                if ('' == $('#user_email').val()) {
                    return;
                } else {
                    var emailOkPattern = /^([\w-\+\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!emailOkPattern.test($('#user_email').val())) {
                        $emailcheck.html('<p class="error-check error-user_email">' + themeStream.invalidEmail + '</p>');
                        return;
                    }
                }
                if ('' != $('#user_name').val()) {
                    $usercheck.html(themeStream.checking);
                    $emailcheck.html(themeStream.checking);
                    $.post(themeStream.ajax_url, {
                        action: 'stream_check_user',
                        nonce: themeStream.ajax_nonce,
                        user_name: $('#user_name').val(),
                        user_email: $('#user_email').val()
                    }, function(response, statusText) {
                        if ('success' != statusText) {
                            $('.signup-messages').text(themeStream.networkError);
                            return;
                        }
                        json = jQuery.parseJSON(response);
                        $usercheck.html('');
                        $emailcheck.html('');
                        if (json.errors) {
                            if (json.errors.user_name)
                                for (var error in json.errors.user_name) {
                                    $usercheck.append('<p class="error-check error-user_name">' + json.errors.user_name[error] + '</p>');
                                }
                            if (json.errors.user_email)
                                for (var error in json.errors.user_email) {
                                    $emailcheck.html('<p class="error-check error-user_email">' + json.errors.user_email[error] + '</p>');
                                }
                            if (json.errors.user_email_used)
                                for (var error in json.errors.user_email_used) {
                                    $emailcheck.html('<p class="error-check error-user_email_used">' + json.errors.user_email_used[error] + '</p>');
                                }
                        }
                    });
                }
            }, delayCheck);
        });
        if ($('#stream_setupform').length > 0) {
            $('#stream_setupform').ajaxForm({
                data: {
                    action: 'stream_add_user_and_site',
                    nonce: themeStream.ajax_nonce,
                    url: themeStream.ajax_url,
                    dataType: 'json'
                },
                beforeSubmit: function(formData, jqForm, options) {
                    if ('' == $('#user_name').val() || '' == $('#user_email').val() || '' == $('#blogname').val() || '' == $('#blog_title').val()) {
                        $('.signup-messages').text(themeStream.fillfields);
                        return false;
                    }
                },
                success: function(responseText, statusText, xhr, $form) {
                    $msg = $('.signup-messages');
                    $msg.text('');
                    if ('success' != statusText) {
                        $msg.text(themeStream.networkError);
                        return;
                    }
                    $sitenamecheck = $('#sitename_check');
                    $sitenamecheck.text('');
                    $sitetitlecheck = $('#sitetitle_check');
                    $sitetitlecheck.text('');
                    $usercheck = $('#username_check');
                    $usercheck.text('');
                    $emailcheck = $('#email_check');
                    $emailcheck.text('');
                    json = jQuery.parseJSON(responseText);
                    $sitenamecheck.html('');
                    $sitetitlecheck.html('');
                    $usercheck.html('');
                    $emailcheck.html('');
                    if (json.errors) {
                        if (json.errors.blogname)
                            for (var error in json.errors.blogname) {
                                $sitenamecheck.append('<p class="error-check error-site_name">' + json.errors.blogname[error] + '</p>');
                            }
                        if (json.errors.user_name)
                            for (var error in json.errors.user_name) {
                                $usercheck.append('<p class="error-check error-user_name">' + json.errors.user_name[error] + '</p>');
                            }
                        if (json.errors.user_email)
                            for (var error in json.errors.user_email) {
                                $emailcheck.html('<p class="error-check error-user_email">' + json.errors.user_email[error] + '</p>');
                            }
                        if (json.errors.user_email_used)
                            for (var error in json.errors.user_email_used) {
                                $emailcheck.html('<p class="error-check error-user_email_used">' + json.errors.user_email_used[error] + '</p>');
                            }
                    } else {
                        $('#stream_setupform').slideUp();
                        $msg.text(themeStream.creationOk);
                    }
                }
            });
        }
        //remove placeholder
        /*
        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
        */
        
        $('.back-top a').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        $("#menu-icon").click(function() {
            //$("#headerwrap #main-nav").fadeToggle();
            $("#headerwrap .main-nav").fadeToggle();
            $("#headerwrap #searchform").hide();
            $(this).toggleClass("active");
        });
        $("#search-icon").click(function() {
            $("#headerwrap #searchform").fadeToggle();
            $("#headerwrap #main-nav").hide();
            $('#headerwrap #s').focus();
            $(this).toggleClass("active");
        });
        if ('yes' == themeStream.itemBoard && $('#loops-wrapper').length > 0 && $('body.grid4, body.grid3, body.grid2').length > 0) {
            ItemBoard.init({
                itemElement: '.AutoWidthElement .post',
                appliedTo: '.pagewidth'
            });
        }
    });
    $(window).load(function() {
        $(document).on('click', '.likeit', function(e) {
            e.preventDefault();
            var $self = $(this),
                $parent = $self.parent(),
                post_id = $self.data('postid');
            $.post(themeStream.ajax_url, {
                action: 'stream_likeit',
                nonce: themeStream.ajax_nonce,
                post_id: post_id
            }, function(response) {
                data = jQuery.parseJSON(response);
                if ('new' == data.status) {
                    $('.newliker', $parent).fadeIn();
                    $('ins', $self).fadeOut('slow', function() {
                        $(this).text(data.likers).fadeIn('slow');
                    });
                }
                if ('isliker' == data.status) {
                    $('.newliker', $parent).hide();
                    $('.isliker', $parent).fadeIn();
                }
            });
        });
        var extraLightboxArgs = {};
        if (!jQuery('body').hasClass('post-lightbox-iframe')) {
            extraLightboxArgs = {
                'displayIframeContentsInParent': false
            };
        } else {
            extraLightboxArgs = {
                'displayIframeContentsInParent': true
            };
            jQuery('.post').contents().find("a:not([class=comment-reply-link], [id=cancel-comment-reply-link], .lightbox, .post-content a[href$=jpg], .post-content a[href$=gif], .post-content a[href$=png], .post-content a[href$=JPG], .post-content a[href$=GIF], .post-content a[href$=PNG], .post-content a[target=_new], .post-content a[target=_blank])").click(function() {
                var href = jQuery(this).attr('href');
                window.parent.location.replace(href);
                return false;
            });
        }
        if (typeof streamGallery !== 'undefined') {
            streamGallery.init({
                'context': jQuery(themeStream.lightboxContext),
                'extraLightboxArgs': extraLightboxArgs
            });
        }
        if (screen.width >= 600) {
            var $streamLightbox = $(".stream-lightbox");
            if ($streamLightbox.length > 0 && (typeof($.fn.prettyPhoto) !== 'undefined')) {
                $streamLightbox.each(function() {
                    $(this).attr('href', $(this).attr('href') + '&iframe=true&width=700&height=90%').prettyPhoto(themeStream.lightbox);
                });
            }
        }
        var $container = jQuery('#loops-wrapper');
        addSocialButtons('#body');
        if (jQuery('.list-post').length == 0) {
            $container.isotope({
                itemSelector: '#loops-wrapper > .post, .new-items > .post',
                transformsEnabled: false
            });
            jQuery(window).resize();
        }
        var scrollMaxPages = themeStream.maxPages;
        if (typeof qp_max_pages != 'undefined')
            scrollMaxPages = qp_max_pages;
        $container.infinitescroll({
            navSelector: '#load-more a',
            nextSelector: '#load-more a',
            itemSelector: '#content .post',
            loadingText: '',
            donetext: '',
            loading: {
                img: themeStream.loadingImg
            },
            maxPage: scrollMaxPages,
            behavior: 'auto' != themeStream.autoInfinite ? 'twitter' : '',
            pathParse: function(path, nextPage) {
                return path.match(/^(.*?)\b2\b(?!.*\b2\b)(.*?$)/).slice(1);
            }
        }, function(newElements) {
            var $newElems = jQuery(newElements).wrap('<div class="new-items" />').hide();
            $newElems.imagesLoaded(function() {
                $newElems.fadeIn();
                $container.isotope('appended', $newElems);
                jQuery('#infscr-loading').fadeOut('normal');
                if (1 == scrollMaxPages) {
                    jQuery('#load-more, #infscr-loading').remove();
                }
                if (typeof streamGallery !== 'undefined') {
                    streamGallery.init({
                        'context': jQuery(themeStream.lightboxContext),
                        'extraLightboxArgs': extraLightboxArgs
                    });
                }
                if (screen.width >= 600) {
                    var $streamLightbox = $(".stream-lightbox", $newElems);
                    if ($streamLightbox.length > 0 && (typeof($.fn.prettyPhoto) !== 'undefined')) {
                        $streamLightbox.each(function() {
                            $(this).attr('href', $(this).attr('href') + '&iframe=true&width=700&height=90%').prettyPhoto(themeStream.lightbox);
                        });
                    }
                }
                addSocialButtons($newElems);
                $(document).trigger('newElements');
            });
            scrollMaxPages = scrollMaxPages - 1;
            if (1 < scrollMaxPages && 'auto' != themeStream.autoInfinite)
                jQuery('#load-more a').show();
        });
        if ('auto' == themeStream.autoInfinite) {
            jQuery('#load-more, #load-more a').hide();
        }
        $('.isotope').imagesLoaded(function() {
            $('.isotope').isotope("reLayout");
        });
    });

    function addSocialButtons($context) {
        if (typeof $context === 'undefined') $context = jQuery('#content');
        if (jQuery('.social-share', $context).length > 0) {
            jQuery('.twitter-share', $context).sharrre({
                share: {
                    twitter: true
                },
                template: themeStream.sharehtml,
                enableHover: false,
                click: function(api, options) {
                    api.simulateClick();
                    api.openPopup('twitter');
                }
            });
            jQuery('.facebook-share', $context).sharrre({
                share: {
                    facebook: true
                },
                template: themeStream.sharehtml,
                enableHover: false,
                click: function(api, options) {
                    api.simulateClick();
                    api.openPopup('facebook');
                }
            });
            jQuery('.pinterest-share', $context).each(function(index, domElem) {
                jQuery(this).sharrre({
                    share: {
                        pinterest: true
                    },
                    buttons: {
                        pinterest: {
                            url: jQuery(this).attr('data-url'),
                            media: jQuery(this).attr('data-media'),
                            description: jQuery(this).attr('data-description')
                        }
                    },
                    template: themeStream.sharehtml,
                    enableHover: false,
                    urlCurl: themeStream.sharrrephp,
                    click: function(api, options) {
                        api.simulateClick();
                        api.openPopup('pinterest');
                    }
                });
            });
            jQuery('.googleplus-share', $context).sharrre({
                share: {
                    googlePlus: true
                },
                template: themeStream.sharehtml,
                enableHover: false,
                urlCurl: themeStream.sharrrephp,
                click: function(api, options) {
                    api.simulateClick();
                    api.openPopup('googlePlus');
                }
            });
        }
    }
}(jQuery));
// jQuery(document).ready(function($) {
//    $('.grid4 .post-image').hover(function() {
//        $(this).css('opacity', '0.7');
//    }, function() {
//        $(this).css('opacity', '1');
//    });
// });
(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o), m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create');
ga('send', 'pageview');
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
})();
googletag.cmd.push(function() {
    googletag.defineSlot('', [300, 250], '').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
});
googletag.cmd.push(function() {
    googletag.display('');
});