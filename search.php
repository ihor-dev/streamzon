<?php
$streamzon_theme_settings = get_option('streamzon_theme_settings_option');
$amazon_settings = get_option('streamzon_amazon_settings_option');
$AmRandSymb = $amazon_settings['amazon_default_Rand_Symbol'] == 'on' ? rand(1,24) : 0;
function myShutdownHandler() {
  if (@is_array($e = @error_get_last())) {
    $code = isset($e['type']) ? $e['type'] : 0;
    $msg = isset($e['message']) ? $e['message'] : '';
    $file = isset($e['file']) ? $e['file'] : '';
    $line = isset($e['line']) ? $e['line'] : '';
    
  }
}
register_shutdown_function('myShutdownHandler');
?>

<?php get_header(); ?>


    <div id="body" class="clearfix">
        <input type="hidden" id='extra_step' value='<?=$AmRandSymb;?>'>
        
        <?php if (isset($streamzon_theme_settings['revolution_slider_code_use'])
            && $streamzon_theme_settings['revolution_slider_code_use'] == 1
            && $amazon_settings['l_page_onepage_landing'] != 1
            && $streamzon_theme_settings['revolution_slider_code']): ?>

            <div class="rev-slider">
                <?php
                $code = $streamzon_theme_settings['revolution_slider_code'.$mct_title];                     
                echo do_shortcode($code);
                ?>
            </div>
        <?php endif; ?>


        <!-- layout -->
        <div id="layout" class="pagewidth clearfix layout-fix ">

            <?php if ((isset($streamzon_theme_settings['banner_image_use']) && $streamzon_theme_settings['banner_image_use'] == 1) || (isset($streamzon_theme_settings['banner_code_use']) && $streamzon_theme_settings['banner_code_use'] == 1)): ?>

                <div class="banner">

                    <?php if (isset($streamzon_theme_settings['banner_image_use']) && $streamzon_theme_settings['banner_image_use'] == 1): ?>

                        <a target="_blank" href="<?php echo $streamzon_theme_settings['banner_image_link']; ?>">
                            <img src="<?php echo $streamzon_theme_settings['banner_image_file']; ?>" alt=""/>
                        </a>

                    <?php endif; ?>

                    <?php if (isset($streamzon_theme_settings['banner_code_use']) && $streamzon_theme_settings['banner_code_use'] == 1): ?>

                        <?php echo $streamzon_theme_settings['banner_code']; ?>

                    <?php endif; ?>

                </div>

            <?php endif; ?>

            <?php if (isset($amazon_settings['show_sidebar']) && $amazon_settings['show_sidebar'] == 0) : ?>
                <div class="content-search-form">
                    <?php //get_search_form(); ?>
                </div>
            <?php endif; ?>



            <?php if (isset($amazon_settings['show_sidebar']) && $amazon_settings['show_sidebar'] == 1) : ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>

            <?php get_template_part( 'content', 'books-list' ); ?>



        </div>
        <!-- /#layout -->

    </div>
    <!-- /body -->


    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/books_infinite_scroll_script.js" async=""></script>

<?php get_footer(); ?>