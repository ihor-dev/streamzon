<?php

global $streamzon_theme_main_settings;
$seo_settings = get_option('streamzon_seo_settings_option');
$amazon_settings = get_option('streamzon_amazon_settings_option');
$theme_settings = get_option('streamzon_theme_settings_option');

$s = (get_query_var('s')) ? get_query_var('s') : '';
if($lookup['ItemAttributes']['Title']){

    $title = $lookup['ItemAttributes']['Title'].' - '.$seo_settings['seo_product_title'];
}elseif($s){

    $seotitle = isset($seo_settings['seo_search_title']) && $seo_settings['seo_search_title'] != "" ? " - " . $seo_settings['seo_search_title'] : "";
    $rpl = array("+",$amazon_settings['default_search_keyword']);
    $title = str_replace($rpl, "", $s) . $seotitle;
}else{
    $title='';
}



if($amazon_settings['l_page_onepage_landing']=='0' || ($_GET['product_page'] == 1) ) {
    //SMALL HEADER
    include('header_default_inc.php');
}else{
    //ONEPAGE BIG HEADER
    include('header_onepage_landing_inc.php');
}

?>
