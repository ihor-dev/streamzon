<?php 
function fx_testForLicensePro()
{
    $current_url = 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

    require 'SwiftMember_API.php';
    //$license_key = '7J8-NEQ-J7X-M';

    $api_url = 'http://members.streamstore.net/?smb_action=api';
    $auth = get_option('StreamStoreTheme_auth_key');
    $domain = get_bloginfo('url'); // The domain

    $swiftmember_api = new SwiftMember_API ( $api_url, $auth, $domain );
    $val_lic = false;
    $val_lic = true;
    

    //echo '--yes auth---'.$auth;

    if(trim($auth) != '')
    {
        $auth_valid = $swiftmember_api->validate_auth ( $auth );
       // echo '----'.$auth_valid;
        if ( $auth_valid )
        {
            $val_lic = true;
        }
    }

    if(!$val_lic)
    {
        if(isset($_POST['license_key']))
        {
            $license_key = trim($_POST['license_key']);
            $license_valid = $swiftmember_api->validate_license ( $license_key );
            if ( !$license_valid ) {
                $er_msgx = '<span style="color: red; font-weight: bold;">Invalid License Key!</span>';
            }
            else
            {
                    $client_install = $swiftmember_api->client_install ( $license_key );
                    //p($client_install);
                    if ( $client_install ) {
                        $client_install_data = $client_install[ 'data' ];
                        $auth = $client_install[ 'auth' ];

                        if(trim($client_install_data) != 'StreamStoreTheme')
                        {
                            $er_msgx = '<span style="color: red; font-weight: bold;">This license key does not belong to this product!</span>';
                        }
                        else
                        {
                            update_option('StreamStoreTheme_auth_key', $auth);
                            ?>
                            <script type="text/javascript">
                                document.location = '<?php echo $current_url; ?>';
                            </script>
                            <?php
                            exit();
                        }
                    } else {
                        $er_msgx = '<span style="color: red; font-weight: bold;">'.$swiftmember_api->error.'</span>';
                    }
            }
        }
        ?>
        <div class="wrap">
                <div class="options-page-logo"></div>
                <hr class="options-page-hr" />
                <h2>License Validation</h2>
                <div class="aj-content-area clearfix">
                    <form id="l_form" method="post">
                    You need to validate the License Key. Please enter your license key.
                    <br/><br/>
                    <input type="text" id="license_key" name="license_key" />
                    <?php
                    if(isset($er_msgx))
                    {
                        if(trim($er_msgx) != ''){
                            echo $er_msgx;
                        }
                    }
                    ?>
                    <p class="submit">
                        <input onclick="return validateLic(); return false;" name="submit" id="submit" class="lic_btnx button button-primary" value="Save" type="submit">
                    </p>
                    </form>
                </div>
                <style type="text/css">
                    .lic_btnx#submit {
                                background: #0085ba;
                                border-color: #0073aa #006799 #006799;
                                -webkit-box-shadow: 0 1px 0 #006799;
                                box-shadow: 0 1px 0 #006799;
                                color: #fff;
                                text-decoration: none;
                                text-shadow: 0 -1px 1px #006799,1px 0 1px #006799,0 1px 1px #006799,-1px 0 1px #006799;
                        }
                     #wpcontent{background-color: #fff;}
                </style>
                <script type="text/javascript">
                    function validateLic()
                    {
                        if(jQuery.trim(jQuery('#license_key').val()) == '')
                        {
                            alert('License Key Missing !');
                            jQuery('#license_key').focus();
                            return false;
                        }
                        else
                            {
                                return true;
                            }
                    }
                </script>
        </div>
         <?php
    }
    else
    {
        return true;
    }

}

if(!function_exists('p'))
{
    function p($e)
    {
        echo '<pre>';
        print_r($e);
        echo '</pre>';
    }
}

?>