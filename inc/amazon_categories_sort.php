<?php
function getSortParameter($locale, $category) {
	error_log("locale = ".$locale." -- category = ".$category);
	$options = array(
		"ca" => array(
					"3561346011" => array("Baby", ""),
					"6205124011" => array("Beauty", ""),
					"927726" => array("Books", "daterank"),
					"962454" => array("Classical", "orig-rel-date"),
					"14113311" => array("DVD", ""),
					"677211011" => array("Electronics", ""),
					"927726" => array("ForeignBooks", "daterank"),
					"6205177011" => array("HealthPersonalCare", ""),
					"2972705011" => array("KindleStore", "daterank"),
					"2206275011" => array("Kitchen", ""),
					"6205499011" => array("LawnGarden", ""),
					"962454" => array("Music", "orig-rel-date"),
					"6205514011" => array("PetSupplies", ""),
					"3234171" => array("Software", ""),
					"3323751" => array("SoftwareVideoGames", ""),
					"6205517011" => array("Toys", ""),
					"962072" => array("VHS", ""),
					"962454" => array("Video", ""),
					"110218011" => array("VideoGames", "")
					),
		"cn" => array(
					"2016156051" => array("Apparel", "-launchdate"),
					"80207071" => array("Appliances", "-launchdate"),
					"1947899051" => array("Automotive", "-launchdate"),
					"746776051" => array("Beauty", ""),
					"658390051" => array("Books", "daterank"),
					"2016116051" => array("Electronics", "-release-date"),
					"2127215051" => array("Grocery", ""),
					"852803051" => array("HealthPersonalCare", "-release-date"),
					"2016126051" => array("Home", ""),
					"1952920051" => array("HomeImprovement", ""),
					"816482051" => array("Jewelry", ""),
					"116087071" => array("KindleStore", "daterank"),
					"899280051" => array("Miscellaneous", "-launch-date"),
					"754386051" => array("Music", "orig-rel-date"),
					"2127218051" => array("MusicalInstruments", ""),
					"2127221051" => array("OfficeProducts", ""),
					"118863071" => array("PetSupplies", "launchdate"),
					"755653051" => array("Photo", "-launchdate"),
					"2029189051" => array("Shoes", "-launch-date"),
					"863872051" => array("Software", "-releasedate"),
					"836312051" => array("SportingGoods", "-releasedate"),
					"647070051" => array("Toys", "-releasedate"),
					"2016136051" => array("Video", "-orig-rel-date"),
					"897415051" => array("VideoGames", "-releasedate"),
					"1953164051" => array("Watches", "")
					),
		"de" => array(
					"78689031" => array("Apparel", ""),
					"78191031" => array("Automotive", ""),
					"357577011" => array("Baby", ""),
					"64257031" => array("Beauty", ""),
					"541686" => array("Books", ""),
					"542676" => array("Classical", "pubdate"),
					"547664" => array("DVD", ""),
					"569604" => array("Electronics", ""),
					"54071011" => array("ForeignBooks", ""),
					"340846031" => array("Grocery", ""),
					"64257031" => array("HealthPersonalCare", ""),
					"10925241" => array("HomeGarden", ""),
					"327473011" => array("Jewelry", ""),
					"530484031" => array("KindleStore", "daterank"),
					"3169011" => array("Kitchen", ""),
					"213083031" => array("Lighting", ""),
					"2454118031" => array("Luggage & Bags", ""),
					"1161658" => array("Magazines", ""),
					"1661648031" => array("MobileApps", ""),
					"77195031" => array("MP3Downloads", "-releasedate"),
					"542676" => array("Music", "publicationdate"),
					"340849031" => array("MusicalInstruments", ""),
					"192416031" => array("OfficeProducts", ""),
					"10925051" => array("OutdoorLiving", ""),
					"569604" => array("PCHardware", ""),
					"569604" => array("Photo", ""),
					"542064" => array("Software", "-date"),
					"541708" => array("SoftwareVideoGames", "-date"),
					"16435121" => array("SportingGoods", "-release-date"),
					"12950661" => array("Toys", ""),
					"547082" => array("VHS", ""),
					"547664" => array("Video", ""),
					"541708" => array("VideoGames", "-date"),
					"193708031" => array("Watches", "")
					),
		"es" => array(
					"1951051031" => array("Automotive", ""),
					"1703495031" => array("Baby", ""),
					"599364031" => array("Books", "-releasedate"),
					"599379031" => array("DVD", ""),
					"667049031" => array("Electronics", ""),
					"599367031" => array("ForeignBooks", ""),
					"530484031" => array("KindleStore", "daterank"),
					"599391031" => array("Kitchen", ""),
					"1661649031" => array("MobileApps", ""),
					"1748200031" => array("MP3Downloads", "-releasedate"),
					"599373031" => array("Music", "-releasedate"),
					"1571262031" => array("Shoes", "-launch-date"),
					"599376031" => array("Software", "-releasedate"),
					"2454136031" => array("SportingGoods", ""),
					"599385031" => array("Toys", ""),
					"599382031" => array("VideoGames", "-releasedate"),
					"599388031" => array("Watches", "")
					),
		"fr" => array(
					"340855031" => array("Apparel", ""),
					"1571265031" => array("Automotive", ""),
					"206617031" => array("Baby", ""),
					"197858031" => array("Beauty", ""),
					"468256" => array("Books", ""),
					"537366" => array("Classical", ""),
					"578608" => array("DVD", ""),
					"1058082" => array("Electronics", ""),
					"69633011" => array("ForeignBooks", ""),
					"197861031" => array("HealthPersonalCare", ""),
					"590748031" => array("HomeImprovement", ""),
					"193711031" => array("Jewelry", ""),
					"818936031" => array("KindleStore", "daterank"),
					"57686031" => array("Kitchen", ""),
					"213080031" => array("Lighting", ""),
					"2454145031" => array("Luggage & Bags", ""),
					"1661654031" => array("MobileApps", ""),
					"206442031" => array("MP3Downloads", "-releasedate"),
					"537366" => array("Music", "releasedate"),
					"340862031" => array("MusicalInstruments", ""),
					"192420031" => array("OfficeProducts", ""),
					"1571268031" => array("PetSupplies", ""),
					"215934031" => array("Shoes", ""),
					"548012" => array("Software", ""),
					"548014" => array("SoftwareVideoGames", ""),
					"548014" => array("Toys", ""),
					"578610" => array("VHS", ""),
					"578608" => array("Video", ""),
					"548014" => array("VideoGames", "date"),
					"60937031" => array("Watches", "")
					),
		"in" => array(
					"976389031" => array("Books", ""),
					"976416031" => array("DVD", ""),
					"976419031" => array("Electronics", ""),
					"976442031" => array("Home & Kitchen", ""),
					"1951048031" => array("Jewelry", ""),
					"976392031" => array("PCHardware", ""),
					"1350380031" => array("Toys", ""),
					"1350387031" => array("Watches", "")
					),
		"it" => array(
					"1571280031" => array("Automotive", ""),
					"1571286031" => array("Baby", ""),
					"411663031" => array("Books", ""),
					"412606031" => array("DVD", ""),
					"412609031" => array("Electronics", ""),
					"433842031" => array("ForeignBooks", ""),
					"635016031" => array("Garden", ""),
					"818937031" => array("KindleStore", "daterank"),
					"524015031" => array("Kitchen", ""),
					"1571292031" => array("Lighting", ""),
					"2454148031" => array("Luggage & Bags", ""),
					"1661660031" => array("MobileApps", ""),
					"1748203031" => array("MP3Downloads", ""),
					"412600031" => array("Music", "-releasedate"),
					"524006031" => array("Shoes", "-releasedate"),
					"412612031" => array("Software", "-releasedate"),
					"523997031" => array("Toys", ""),
					"412603031" => array("VideoGames", "-releasedate"),
					"524009031" => array("Watches", "")
					),
		"co.jp" => array(
					"361299011" => array("Apparel", ""),
					"2277724051" => array("Appliances", ""),
					"2017304051" => array("Automotive", ""),
					"13331821" => array("Baby", ""),
					"52391051" => array("Beauty", ""),
					"465610" => array("Books", "daterank"),
					"562032" => array("Classical", "releasedate"),
					"562002" => array("DVD", "-releasedate"),
					"3210991" => array("Electronics", "-releasedate"),
					"388316011" => array("ForeignBooks", "daterank"),
					"57239051" => array("Grocery", ""),
					"161669011" => array("HealthPersonalCare", ""),
					"13331821" => array("Hobbies", "-release-date"),
					"85896051" => array("Jewelry", ""),
					"2250738051" => array("KindleStore", "daterank"),
					"3839151" => array("Kitchen", "-release-date"),
					"2381130051" => array("MobileApps", ""),
					"2128134051" => array("MP3Downloads", ""),
					"562032" => array("Music", "-releasedate"),
					"2123629051" => array("MusicalInstruments", ""),
					"2016926051" => array("Shoes", ""),
					"637630" => array("Software", "-release-date"),
					"14304371" => array("SportingGoods", "-releasedate"),
					"13331821" => array("Toys", "-release-date"),
					"2130989051" => array("VHS", "-releasedate"),
					"561972" => array("Video", "-releasedate"),
					"637872" => array("VideoGames", "-release-date"),
					"324025011" => array("Watches", "")
					),
		"co.uk" => array(
					"83451031" => array("Apparel", "-launch-date"),
					"248877031" => array("Automotive", ""),
					"60032031" => array("Baby", ""),
					"66280031" => array("Beauty", ""),
					"1025612" => array("Books", ""),
					"505510" => array("Classical", ""),
					"283926" => array("DVD", ""),
					"560800" => array("Electronics", ""),
					"340834031" => array("Grocery", ""),
					"66280031" => array("HealthPersonalCare", ""),
					"11052591" => array("HomeGarden", ""),
					"2016929051" => array("HomeImprovement", ""),
					"193717031" => array("Jewelry", ""),
					"341677031" => array("KindleStore", "daterank"),
					"11052591" => array("Kitchen", ""),
					"213077031" => array("Lighting", ""),
					"2454166031" => array("Luggage & Bags", ""),
					"1661657031" => array("MobileApps", ""),
					"77198031" => array("MP3Downloads", "-releasedate"),
					"505510" => array("Music", "releasedate"),
					"340837031" => array("MusicalInstruments", ""),
					"560800" => array("OfficeProducts", ""),
					"11052591" => array("OutdoorLiving", ""),
					"1025614" => array("Software", ""),
					"1025616" => array("SoftwareVideoGames", ""),
					"319530011" => array("SportingGoods", ""),
					"11052591" => array("Tools", ""),
					"712832" => array("Toys", ""),
					"283926" => array("VHS", ""),
					"283926" => array("Video", ""),
					"1025616" => array("VideoGames", ""),
					"595312" => array("Watches", "-launch-date")
					),
		"com" => array(
					"1036592" => array("Apparel", "-launch-date"),
					"2619525011" => array("Appliances", ""),
					"2617941011" => array("ArtsAndCrafts", ""),
					"15690151" => array("Automotive", ""),
					"165796011" => array("Baby", ""),
					"11055981" => array("Beauty", "-launch-date"),
					"1000" => array("Books", "daterank"),
					"301668" => array("Classical", "releasedate"),
					"4991425011" => array("Collectibles", ""),
					"195208011" => array("DigitalMusic", ""),
					"2625373011" => array("DVD", "-video-release-date"),
					"172282" => array("Electronics", ""),
					"3580501" => array("GourmetFood", ""),
					"16310101" => array("Grocery", "launch-date"),
					"3760931" => array("HealthPersonalCare", "launch-date"),
					"285080" => array("HomeGarden", ""),
					"228239" => array("Industrial", ""),
					"3880591" => array("Jewelry", "launch-date"),
					"133141011" => array("KindleStore", "daterank"),
					"284507" => array("Kitchen", ""),
					"2972638011" => array("LawnGarden", ""),
					"599872" => array("Magazines", "daterank"),
					"10304191" => array("Miscellaneous", ""),
					"2350149011" => array("MobileApps", ""),
					"195211011" => array("MP3Downloads", "-releasedate"),
					"301668" => array("Music", "release-date"),
					"11091801" => array("MusicalInstruments", "-launch-date"),
					"1084128" => array("OfficeProducts", ""),
					"286168" => array("OutdoorLiving", ""),
					"541966" => array("PCHardware", ""),
					"12923371" => array("PetSupplies", ""),
					"502394" => array("Photo", ""),
					"409488" => array("Software", ""),
					"3375251" => array("SportingGoods", "launch-date"),
					"468240" => array("Tools", ""),
					"165793011" => array("Toys", ""),
					"404272" => array("VHS", "-video-release-date"),
					"130" => array("Video", "-video-release-date"),
					"468642" => array("VideoGames", ""),
					"377110011" => array("Watches", ""),
					"508494" => array("Wireless", "daterank"),
					"13900851" => array("WirelessAccessories", "")
					)
	);

	return trim($options[$locale][$category][1]);
}
?>