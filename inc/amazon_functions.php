<?php

require_once 'apai-io-custom-operations/StreamzonAmazonCustomSearch.php';
require_once 'apai-io-custom-operations/StreamzonAmazonLookup.php';
require_once 'amazon_categories_sort.php';

function streamzon_amazon_search_books($search_query, $page = 0, $step = 0, $MinPercentageOff=60)
{
    $maxReached = 0;    
    error_log("amazon_functions.php - streamzon_amazon_search_books: ".$search_query." -- page = ".$page." -- step = ".$step);

    $credentials = get_option( 'streamzon_amazon_credentials_option' );
    $settings = get_option( 'streamzon_amazon_settings_option' );
	$stores = get_option( 'streamzon_amazon_stores_option' );
			
    if (!empty($settings['amazon_subcategory']) && is_array($settings['amazon_subcategory'])) {
        foreach ($settings['amazon_subcategory'] as $k => $v) {
            if ($v == '') unset($settings['amazon_subcategory'][$k]);
        }
    }

	$market_dynamic = getMarketplace(getCountryCode());
	$market	=	$market_dynamic['marketplace'];

	if($market){
		switch($market)
		{
			case 'com':
				$amazoncategory	=	'amazon_category';
				break;
			case 'co.uk':
				$amazoncategory	=	'amazon_uk_category';
				break;
			case 'ca':
				$amazoncategory	=	'amazon_ca_category';
				break;
			case 'cn':
				$amazoncategory	=	'amazon_cn_category';
				break;
			case 'de':
				$amazoncategory	=	'amazon_de_category';
				break;
			case 'es':
				$amazoncategory	=	'amazon_es_category';
				break;
			case 'fr':
				$amazoncategory	=	'amazon_fr_category';
				break;
			case 'in':
				$amazoncategory	=	'amazon_in_category';
				break;
			case 'it':
				$amazoncategory	=	'amazon_it_category';
				break;
			case 'co.jp':
				$amazoncategory	=	'amazon_jp_category';
				break;
			default:
				$amazoncategory	=	'amazon_category';
				break;
		}
	}    

    $dbg1 = print_r($stores[$amazoncategory], true);
    $dbg2 = print_r($settings['amazon_subcategory'], true);
    
    $browseNodes = null;
    if (!empty($settings['amazon_subcategory'])  || $stores[$amazoncategory]) {
        $browseNodes = !empty($settings['amazon_subcategory']) ? $settings['amazon_subcategory'] : $stores[$amazoncategory];
    }

	$marketPlace = getMarketplace(getCountryCode());


    if(!isset($_SESSION['api_secretkey']) || $_SESSION['api_status'] == ""){
        $_SESSION['api_status'] = 1;
        $_SESSION['api_keyid'] = $credentials['access_key_id'];
        $_SESSION['api_secretkey'] = $credentials['secret_access_key'];
    }



    $conf = new \ApaiIO\Configuration\GenericConfiguration();
    $conf
        ->setCountry($marketPlace['marketplace'])
        ->setAccessKey($_SESSION['api_keyid'])
        ->setSecretKey($_SESSION['api_secretkey'])
        ->setAssociateTag($marketPlace['associateId'])
        ->setRequest('\ApaiIO\Request\Soap\Request')
        ->setResponseTransformer('\ApaiIO\ResponseTransformer\ObjectToArray');

    $search = new StreamzonAmazonCustomSearch();
    $search->setCategory('All');
    $search->setKeywords($search_query);
    $search->setResponseGroup(array('Large'));

    if ($page != 0) {
        $search->setPage((int)$page);
    }
    if ($settings['amazon_discount'] && $settings['amazon_discount_amount']) {
        //$search->setMinPercentageOff($settings['amazon_discount_amount']);
    }
    if ($MinPercentageOff > 0) {
		$search->setMinPercentageOff($MinPercentageOff);
    }

    if ($settings['amazon_additional_search_parameter'] == 0) {
        $price_start    = $settings['amazon_additional_price_start'] * 100;
        $price_step     = $settings['amazon_additional_price_step'] * 100;
        $price_max      = $settings['amazon_additional_price_max'] * 100;
        error_log("--- additional parameter. start = ".$price_start.", step = ".$price_step.", max = ".$price_max);

        $minimum = $step * $price_step + $price_start;
        $maximum = ($step + 1) * $price_step + $price_start;
        error_log("min = ".$minimum." max = ".$maximum);

 	
	        $search->setMaximumPrice($maximum);
	        $search->setMinimumPrice($minimum);
			error_log("--- working on price");
	

        if ($maximum > $price_max) {
            $maxReached = 1;
            error_log("--- we are over the max set price");
        }
    } else {
        // deal with the letter part
        $letter = chr($step + 97);
        error_log("--- additional parameter. letter = ".$letter.", query = ".$search_query);
        $search->setKeywords($search_query." ".$letter);

        if ($step > 25) {
            $maxReached = 1;
            error_log("--- we are over letter z");   
            
        }
        // end letter part
		//if($settings['l_page_discountbar_enable'] == 1)
        {
			$search->setMinPercentageOff($MinPercentageOff);
			error_log("--- working on Discount");

			if ($settings['amazon_paid_free'] == 1) {
	            $search->setMaximumPrice(0);
	            $search->setMinimumPrice(0);
        	}

	        if ($settings['amazon_paid_free'] == 2) {
	            $search->setMinimumPrice(1);
	        }
		}
        
    }

    //$search->setSort('daterank');

    $apaiIo = new \ApaiIO\ApaiIO($conf);

    if (!is_array($browseNodes) && $browseNodes) {
        $search->setBrowseNode($browseNodes);
    }

    if (is_array($browseNodes)) {
        $last_node = '';
        while (!$last_node) {
            $last_node = array_pop($browseNodes);
            if ($last_node === null) break;
        }
        $search->setBrowseNode($last_node);
    }

    if (isset($_REQUEST['a_cat']) && !empty($_REQUEST['a_cat'])) {
        $search->setBrowseNode($_REQUEST['a_cat']);
    }

    // first call to the api with category or first subcategory
    $response = $apaiIo->runOperation($search);

    //print "<pre>";
    //print_r($response);
    //print "</pre>";


    if (isset($response['Items']['Item']) && is_array($response['Items']['Item'])) {
        if (isset($response['Items']['Item'][0]) && is_array($response['Items']['Item'][0])) {
            $items = $response['Items']['Item'];
        } else {
            $items = array($response['Items']['Item']);
        }
    } else {
        $items = array();
    }

    error_log("results: ".sizeof($items));
    if ($maxReached) {
        return 0;
    } else {
        return $items;
    }
}

function streamzon_amazon_asin_lookup($asin)
{

	$options = get_option( 'streamzon_amazon_credentials_option' );
	$marketPlace = getMarketplace(getCountryCode());    

    $conf = new \ApaiIO\Configuration\GenericConfiguration();
    $conf
        ->setCountry($marketPlace['marketplace'])
        ->setAccessKey($options['access_key_id'])
        ->setSecretKey($options['secret_access_key'])
        ->setAssociateTag($marketPlace['associateId'])
        ->setRequest('\ApaiIO\Request\Soap\Request')
        ->setResponseTransformer('\ApaiIO\ResponseTransformer\ObjectToArray');
		
	$lookup = new \ApaiIO\Operations\Lookup();
	$lookup->setItemId($asin);
	$lookup->setResponseGroup(array('Large'));

	$apaiIO = new \ApaiIO\ApaiIO($conf);
	$formattedResponse = $apaiIO->runOperation($lookup, $conf);
    
	return $formattedResponse['Items']['Item'];
}	

function streamzon_amazon_browse_node_lookup($country, $category, $recursion = true)
{    
    $options = get_option( 'streamzon_amazon_credentials_option' );

    $conf = new \ApaiIO\Configuration\GenericConfiguration();
    $conf
        ->setCountry($country)
        ->setAccessKey($options['access_key_id'])
        ->setSecretKey($options['secret_access_key'])
        ->setAssociateTag($options['amazon_associate_id'])
        ->setRequest('\ApaiIO\Request\Soap\Request')
        ->setResponseTransformer('\ApaiIO\ResponseTransformer\ObjectToArray');

    $browse_node_lookup = new \ApaiIO\Operations\BrowseNodeLookup();
    $browse_node_lookup->setNodeId($category);

    $apaiIo = new \ApaiIO\ApaiIO($conf);
    $response = $apaiIo->runOperation($browse_node_lookup);

    if (isset($response['BrowseNodes']['BrowseNode']['Children']['BrowseNode'])) {
        $nodes = $response['BrowseNodes']['BrowseNode']['Children']['BrowseNode'];
    } else {
        return array();
    }

    // if only 1 subnode
    if (isset($nodes['BrowseNodeId']) && isset($nodes['Name'])) {
        $nodes = array($nodes);
    }


    $subnodes = array();

    $all_nodes = array_merge($nodes, $subnodes);

    return $all_nodes;
}