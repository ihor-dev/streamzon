<?php
	function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);
	
	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   //return implode(",", $rgb); // returns the rgb values separated by commas
	   return $rgb; // returns an array with the rgb values
	}

	function getFontFamily($fontfamily)
	{
		switch($fontfamily)
		{
			case 'Open+Sans':
				$font_link		=	"http://fonts.googleapis.com/css?family=Open+Sans";
				$font_family	=	"'Open Sans', sans-serif";
				break;
			case 'Roboto':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto";
				$font_family	=	"'Roboto', sans-serif";
				break;
			case 'Oswald':
				$font_link		=	"http://fonts.googleapis.com/css?family=Oswald";
				$font_family	=	"'Oswald', sans-serif";
				break;
			case 'Lato':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lato";
				$font_family	=	"'Lato', sans-serif";
				break;
			case 'Roboto+Condensed':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto+Condensed";
				$font_family	=	"'Roboto Condensed', sans-serif";
				break;
			case 'Open+Sans+Condensed:300':
				$font_link		=	"http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300";
				$font_family	=	"'Open Sans Condensed', sans-serif";
				break;
			case 'Raleway':
				$font_link		=	"http://fonts.googleapis.com/css?family=Raleway";
				$font_family	=	"'Raleway', sans-serif";
				break;
			case 'Droid+Serif':
				$font_link		=	"http://fonts.googleapis.com/css?family=Droid+Serif";
				$font_family	=	"'Droid Serif', sans-serif";
				break;
			case 'Montserrat':
				$font_link		=	"http://fonts.googleapis.com/css?family=Montserrat";
				$font_family	=	"'Montserrat', sans-serif";
				break;
			case 'Roboto+Slab':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto+Slab";
				$font_family	=	"'Roboto Slab', sans-serif";
				break;
			case 'PT+Sans+Narrow':
				$font_link		=	"http://fonts.googleapis.com/css?family=PT+Sans+Narrow";
				$font_family	=	"'PT Sans Narrow', sans-serif";
				break;
			case 'Lora':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lora";
				$font_family	=	"'Lora', serif";
				break;
			case 'Arimo':
				$font_link		=	"http://fonts.googleapis.com/css?family=Arimo";
				$font_family	=	"'Arimo', sans-serif";
				break;
			case 'Bitter':
				$font_link		=	"http://fonts.googleapis.com/css?family=Bitter";
				$font_family	=	"'Bitter', serif";
				break;
			case 'Merriweather':
				$font_link		=	"http://fonts.googleapis.com/css?family=Merriweather";
				$font_family	=	"'Merriweather', serif";
				break;
			case 'Oxygen':
				$font_link		=	"http://fonts.googleapis.com/css?family=Oxygen";
				$font_family	=	"'Merriweather', sans-serif";
				break;
			case 'Lobster':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lobster";
				$font_family	=	"'Lobster', cursive";
				break;
			case 'Titillium+Web':
				$font_link		=	"http://fonts.googleapis.com/css?family=Titillium+Web";
				$font_family	=	"'Titillium Web', sans-serif";
				break;
			case 'Poiret+One':
				$font_link		=	"http://fonts.googleapis.com/css?family=Poiret+One";
				$font_family	=	"'Poiret One', cursive";
				break;
			case 'Play':
				$font_link		=	"http://fonts.googleapis.com/css?family=Play";
				$font_family	=	"'Play', sans-serif";
				break;
			case 'Pacifico':
				$font_link		=	"http://fonts.googleapis.com/css?family=Pacifico";
				$font_family	=	"'Pacifico', cursive";
				break;
			case 'Questrial':
				$font_link		=	"http://fonts.googleapis.com/css?family=Questrial";
				$font_family	=	"'Questrial', sans-serif";
				break;
			case 'Dancing+Script':
				$font_link		=	"http://fonts.googleapis.com/css?family=Dancing+Script";
				$font_family	=	"'Dancing Script', cursive";
				break;
			case 'Exo+2':
				$font_link		=	"http://fonts.googleapis.com/css?family=Exo+2";
				$font_family	=	"'Exo 2', sans-serif";
				break;
			case 'Comfortaa':
				$font_link		=	"http://fonts.googleapis.com/css?family=Comfortaa";
				$font_family	=	"'Comfortaa', cursive";
				break;
			case 'Ropa+Sans':
				$font_link		=	"http://fonts.googleapis.com/css?family=Ropa+Sans";
				$font_family	=	"'Ropa Sans', sans-serif";
				break;
			case 'Lobster+Two':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lobster+Two";
				$font_family	=	"'Lobster Two', cursive";
				break;
			case 'EB+Garamond':
				$font_link		=	"http://fonts.googleapis.com/css?family=EB+Garamond";
				$font_family	=	"'EB Garamond', serif";
				break;
			default:
				$font_link		=	null;
				$font_family	=	"'Open Sans',Arial,Helvetica,sans-serif";
				break;
			
		}
		return array('link' => $font_link, 'family'=>$font_family);
	}
?>

<style type="text/css">
  	
  	/* hide menu element 'product' */
  	<? $page_id = get_option('streamzon_page_id');?>
  	.page-item-<?=$page_id;?> {
  		display: none;
  	}

  	/* show/hide discount slider on right widget 
  	================================================*/
  	<? 
  		$amazon_settings = get_option('streamzon_amazon_settings_option');	
  		$rSlidreCss = $amazon_settings['amazon_paid_free'] == "1" ? "none" : "block";
  	?>
  	#rightDiscSlider {
  		display: <?=$rSlidreCss;?>;
  	}


    <?php if (isset($amazon_settings['show_sidebar']) && !empty($amazon_settings['show_sidebar']) && isset($amazon_settings['show_sidebar']) && $amazon_settings['show_sidebar'] == 1) : ?>
  		<?php if( $streamzon_theme_main_settings['revolution_slider_code_use'] != "1" ): ?>
		  	#body {
		  		margin-top: 2em;
		  	}		

		<?php endif;?>
		.page-template #body {
		    margin-top: 2em;
		}
	    #contenttop {
	        width: 100%;
	        height: 35px;
	    }
    <?php else: ?>
	    #contenttop {
	        width: calc(100% - 294px);
	    }
    <?php endif; ?>
    .buybtn .color-11.puerto-btn-2 span { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['buyitnow_button_background'])).", 1"; ?> ) !important; }
    .buybtn .color-11.puerto-btn-2 span:before { border-top-color:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['buyitnow_button_background'])).", 1"; ?> ) !important; }
    .buybtn .color-11 { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['buyitnow_button_background2'])).", 1"; ?> ) !important; }
    .buybtn .puerto-btn-2 small {
        color: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['buyitnow_button_color'])).", 1"; ?> ) !important;
    }
    .cartbtn .color-11.puerto-btn-2 span { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['add_to_cart_button_background'])).", 1"; ?> ) !important; }
    .cartbtn .color-11.puerto-btn-2 span:before { border-top-color:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['add_to_cart_button_background'])).", 1"; ?> ) !important; }
    .cartbtn .color-11 { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['add_to_cart_button_background2'])).", 1"; ?> ) !important; }
    .cartbtn .puerto-btn-2 small {
        color: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['add_to_cart_button_color'])).", 1"; ?> ) !important;
    }
	.post-inner .cartbtn .color-11.puerto-btn-2 span { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['small_cart_button_background'])).", 1"; ?> ) !important; }
	.post-inner .cartbtn .color-11 { background:rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['small_cart_button_background'])).", 1"; ?> ) !important; }
	.post-inner .cartbtn .puerto-btn-2 small {
        color: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['small_cart_button_color'])).", 1"; ?> ) !important;
    }
    /************************************************/
    <?php if (isset($streamzon_theme_main_settings['text_link_color']) && !empty($streamzon_theme_main_settings['text_link_color'])) : ?>
    a {
        color: <?php echo $streamzon_theme_main_settings['text_link_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['sidebar_header_color']) && !empty($streamzon_theme_main_settings['sidebar_header_color'])) : ?>
    #sidebar .widget-title {
        color: <?php echo $streamzon_theme_main_settings['sidebar_header_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['top_bar_color']) && !empty($streamzon_theme_main_settings['top_bar_color'])) : ?>
    <?php
    $streamzon_rgb_color = streamzon_hex2rgb($streamzon_theme_main_settings['top_bar_color']);
    ?>
    .md-content {
        background: rgba(255,255,255,1);
		display: table;
    }
	.md-content h3, #headerwrap, #cart, .pfooter_row {
        background: rgb(
            <?php echo $streamzon_rgb_color[0]; ?>,
            <?php echo $streamzon_rgb_color[1]; ?>,
            <?php echo $streamzon_rgb_color[2]; ?>
        );
		display: table;
    }
    .pfooter_row {
    	display: block;
    }
	#backbtnpage{
		background: rgb(
            <?php echo $streamzon_rgb_color[0]; ?>,
            <?php echo $streamzon_rgb_color[1]; ?>,
            <?php echo $streamzon_rgb_color[2]; ?>
        );
		width: 30px;
		height: 30px;
		border-radius: 38px;
		border: 2px solid #fff;
		margin-bottom: 6px;
	}


    <?php endif; ?>
	#site-description{
		//height:55px !important;
		//max-height:55px;
		//padding: 10px 0 0 !important;
	}
    <?php if (isset($streamzon_theme_main_settings['header_text_px'])) : ?>
    #site-description {
        font-size: <?php echo $streamzon_theme_main_settings['header_text_px']; ?>px;
        line-height: <?php echo $streamzon_theme_main_settings['header_text_px']; ?>px;
    }
    <?php endif; ?>

   <?php if (isset($streamzon_theme_main_settings['header_text_font_family']) && !empty($streamzon_theme_main_settings['header_text_font_family'])) : 

		$product_header_family	=	$streamzon_theme_main_settings['header_text_font_family'];
		switch($product_header_family)
		{
			case 'Open+Sans':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Open+Sans";
				$prohead_family	=	"'Open Sans', sans-serif";
				break;
			case 'Roboto':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Roboto";
				$prohead_family	=	"'Roboto', sans-serif";
				break;
			case 'Oswald':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Oswald";
				$prohead_family	=	"'Oswald', sans-serif";
				break;
			case 'Lato':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Lato";
				$prohead_family	=	"'Lato', sans-serif";
				break;
			case 'Roboto+Condensed':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Roboto+Condensed";
				$prohead_family	=	"'Roboto Condensed', sans-serif";
				break;
			case 'Open+Sans+Condensed:300':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300";
				$prohead_family	=	"'Open Sans Condensed', sans-serif";
				break;
			case 'Raleway':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Raleway";
				$prohead_family	=	"'Raleway', sans-serif";
				break;
			case 'Droid+Serif':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Droid+Serif";
				$prohead_family	=	"'Droid Serif', sans-serif";
				break;
			case 'Montserrat':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Montserrat";
				$prohead_family	=	"'Montserrat', sans-serif";
				break;
			case 'Roboto+Slab':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Roboto+Slab";
				$prohead_family	=	"'Roboto Slab', sans-serif";
				break;
			case 'PT+Sans+Narrow':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=PT+Sans+Narrow";
				$prohead_family	=	"'PT Sans Narrow', sans-serif";
				break;
			case 'Lora':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Lora";
				$prohead_family	=	"'Lora', serif";
				break;
			case 'Arimo':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Arimo";
				$prohead_family	=	"'Arimo', sans-serif";
				break;
			case 'Bitter':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Bitter";
				$prohead_family	=	"'Bitter', serif";
				break;
			case 'Merriweather':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Merriweather";
				$prohead_family	=	"'Merriweather', serif";
				break;
			case 'Oxygen':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Oxygen";
				$prohead_family	=	"'Merriweather', sans-serif";
				break;
			case 'Lobster':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Lobster";
				$prohead_family	=	"'Lobster', cursive";
				break;
			case 'Titillium+Web':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Titillium+Web";
				$prohead_family	=	"'Titillium Web', sans-serif";
				break;
			case 'Poiret+One':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Poiret+One";
				$prohead_family	=	"'Poiret One', cursive";
				break;
			case 'Play':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Play";
				$prohead_family	=	"'Play', sans-serif";
				break;
			case 'Pacifico':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Pacifico";
				$prohead_family	=	"'Pacifico', cursive";
				break;
			case 'Questrial':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Questrial";
				$prohead_family	=	"'Questrial', sans-serif";
				break;
			case 'Dancing+Script':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Dancing+Script";
				$prohead_family	=	"'Dancing Script', cursive";
				break;
			case 'Exo+2':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Exo+2";
				$prohead_family	=	"'Exo 2', sans-serif";
				break;
			case 'Comfortaa':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Comfortaa";
				$prohead_family	=	"'Comfortaa', cursive";
				break;
			case 'Ropa+Sans':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Ropa+Sans";
				$prohead_family	=	"'Ropa Sans', sans-serif";
				break;
			case 'Lobster+Two':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=Lobster+Two";
				$prohead_family	=	"'Lobster Two', cursive";
				break;
			case 'EB+Garamond':
				$prohead_link		=	"http://fonts.googleapis.com/css?family=EB+Garamond";
				$prohead_family	=	"'EB Garamond', serif";
				break;
			default:
				$prohead_link		=	null;
				$prohead_family	=	"'Open Sans',Arial,Helvetica,sans-serif";
				break;

		}
	?>
    #site-description {
		font-family: <?php echo $prohead_family; ?> !important;   
    }
    <?php endif; ?>
.slidebarsearch input[type=text] {
	  padding: 6px 32px 6px 10px !important;
	  box-sizing: border-box !important;
	  max-width:100%;
	  color:<?php echo $streamzon_theme_main_settings['page_search_button_text_color'] ?>;
	  border: 1px solid <?php echo $streamzon_theme_main_settings['page_search_button_text_border_color'] ?>;
	  background: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['page_search_button_color'])).",".$streamzon_theme_main_settings['page_search_button_text_opacity_px']; ?> ) !important;
    }
	<?php if (isset($streamzon_theme_main_settings['header_text_enable']) && $streamzon_theme_main_settings['header_text_enable'] == 1) : ?>
    #site-description {
        margin:10px 0 11px ;
    }
    
    <?php endif; ?>

	<?php if (
				isset($streamzon_theme_main_settings['header_text_enable']) && 
				$streamzon_theme_main_settings['header_text_enable'] == 1 &&
				!isset($streamzon_theme_main_settings['top_bar_logo_use']) && 
				!$streamzon_theme_main_settings['top_bar_logo_use'] == 1 ) : ?>
		#site-description {
		        margin:11px 0; /*when title only enabled*/
		  }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_text_bold']) && $streamzon_theme_main_settings['header_text_bold'] == 1) : ?>
    #site-description {
        font-weight: bold;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_text_color']) && !empty($streamzon_theme_main_settings['header_text_color'])) : ?>
    #site-description, 
    #cart .cartIcon, 
    #backbtnpage > span,
    .md-close, .simpleCart_checkout, .footer-nav a
     {
        color: <?php echo $streamzon_theme_main_settings['header_text_color']; ?>!important;
    }
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['searchbar_border_color']) && !empty($streamzon_theme_main_settings['searchbar_border_color'])) : ?>
    .header-search-form input[type="text"] {
        //border-color: <?php echo $streamzon_theme_main_settings['searchbar_border_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['searchbar_text_color']) && !empty($streamzon_theme_main_settings['searchbar_text_color'])) : ?>
    .header-search-form input[type="text"] {
        color: <?php echo $streamzon_theme_main_settings['searchbar_text_color']; ?>;
		margin-bottom:0;
		z-index:45;	
    }

    <?php endif; ?>

	a, a:hover {
	  
	}
    <?php if (isset($streamzon_theme_main_settings['searchbar_background_color']) && !empty($streamzon_theme_main_settings['searchbar_background_color'])){

		if (isset($streamzon_theme_main_settings['searchbar_opacity'])) 
			$opct	=	($streamzon_theme_main_settings['searchbar_opacity'])/100; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['searchbar_background_color'])).",".$opct;
	?>
   
    #searchForm > span {
        background: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['searchbar_background_color'])).",".$streamzon_theme_main_settings['searchbar_opacity']/100; ?>);
		border: 1px solid <?php echo $streamzon_theme_main_settings['searchbar_border_color']; ?>;
		color: <?php echo $streamzon_theme_main_settings['searchbar_text_color']; ?>;

    }
    .disc, .disc input {
    	color: <?php echo $streamzon_theme_main_settings['searchbar_text_color']; ?>;
    }
	.header-search-form img {
		position:absolute !important;
		right:10px;
		top:7px;
	}
	.serach_border{
		position:absolute;
		top:0;
		opacity:0.6;
		height:100%;
		width:100%;
		z-index:-1;
		border: 1px solid <?php echo $streamzon_theme_main_settings['searchbar_border_color']; ?>;
	}
    <?php } ?>


	<?php /*///////landing page  range//////////*/ ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_line_on_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_line_on_color'])) : ?>
		.subscribe .range-quantity{
			background-color: <?php echo $streamzon_theme_main_settings['l_page_discountbar_line_on_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_line_off_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_line_off_color'])) : ?>
		.subscribe .range-bar{
			background-color: <?php echo $streamzon_theme_main_settings['l_page_discountbar_line_off_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_ball_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_ball_color'])) : ?>
		.subscribe .range-handle{
			background-color: <?php echo $streamzon_theme_main_settings['l_page_discountbar_ball_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_display_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_display_color'])){

		if (isset($streamzon_theme_main_settings['l_page_discountbar_display_opacity'])) 
			$opct	=	$streamzon_theme_main_settings['l_page_discountbar_display_opacity']; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['l_page_discountbar_display_color'])).",".$opct;
	?>
    .subscribe .display-box {
        background: rgba(<?php print $background;?>);
		display: inline;
    }
    <?php } ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_display_text_family']) && !empty($streamzon_theme_main_settings['l_page_discountbar_display_text_family'])) : 
		$returns = 	getFontFamily($streamzon_theme_main_settings['l_page_discountbar_display_text_family']);
	?>
		.subscribe .display-box{
			font-family: <?php echo $returns['family']; ?> !important;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_display_border_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_display_border_color'])) : ?>
		.subscribe .display-box,.display-box1 {
			border:  solid 1px <?php echo $streamzon_theme_main_settings['l_page_discountbar_display_border_color']; ?> !important ;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_display_text_px']) && !empty($streamzon_theme_main_settings['l_page_discountbar_display_text_px'])) : ?>
		.subscribe .display-box{
			font-size: <?php echo $streamzon_theme_main_settings['l_page_discountbar_display_text_px']; ?>px !important ;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_discountbar_display_text_color']) && !empty($streamzon_theme_main_settings['l_page_discountbar_display_text_color'])) : ?>
		.subscribe .display-box{
			color: <?php echo $streamzon_theme_main_settings['l_page_discountbar_display_text_color']; ?> !important ;
		}	
    <?php endif; ?>
	
	<?php if (isset($streamzon_theme_main_settings['page_search_button_color']) && !empty($streamzon_theme_main_settings['page_search_button_color'])) : ?>
	#sidebar .btn-warning{
	    background: <?php echo $streamzon_theme_main_settings['page_search_button_color']; ?> !important ;
	    padding: 7px 20px !important;
	    color: <?php echo $streamzon_theme_main_settings['page_search_button_text_color']; ?> !important ;
	    border: 1px solid <?php echo $streamzon_theme_main_settings['page_search_button_text_border_color']; ?> !important ;
	    opacity: <?php echo $streamzon_theme_main_settings['page_search_button_text_opacity_px']; ?> !important ;
	}
	#sidebar .btn-warning:hover{
	    background: <?php echo $streamzon_theme_main_settings['page_search_button_hover_color']; ?> !important ;
	}
	<?php endif; ?>


	<?php /*///////side range//////////*/ ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_line_on_color']) && !empty($streamzon_theme_main_settings['side_discountbar_line_on_color'])) : ?>
		.widget .range-quantity{
			background-color: <?php echo $streamzon_theme_main_settings['side_discountbar_line_on_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_line_off_color']) && !empty($streamzon_theme_main_settings['side_discountbar_line_off_color'])) : ?>
		.widget .range-bar{
			background-color: <?php echo $streamzon_theme_main_settings['side_discountbar_line_off_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_ball_color']) && !empty($streamzon_theme_main_settings['side_discountbar_ball_color'])) : ?>
		.widget .range-handle{
			background-color: <?php echo $streamzon_theme_main_settings['side_discountbar_ball_color']; ?>;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_display_color']) && !empty($streamzon_theme_main_settings['side_discountbar_display_color'])){

		if (isset($streamzon_theme_main_settings['side_discountbar_display_opacity'])) 
			$opct	=	$streamzon_theme_main_settings['side_discountbar_display_opacity']; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['side_discountbar_display_color'])).",".$opct;
	?>
    .widget .display-box {
        background: rgba(<?php print $background;?>);
		display: inline;
    }
    <?php } ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_display_text_family']) && !empty($streamzon_theme_main_settings['side_discountbar_display_text_family'])) : 
		$returns = 	getFontFamily($streamzon_theme_main_settings['side_discountbar_display_text_family']);
	?>
		.widget .display-box{
			font-family: <?php echo $returns['family']; ?> !important;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_display_border_color']) && !empty($streamzon_theme_main_settings['side_discountbar_display_border_color'])) : ?>
		.widget .display-box,.display-box1 {
			border:  solid 1px <?php echo $streamzon_theme_main_settings['side_discountbar_display_border_color']; ?> !important ;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_display_text_px']) && !empty($streamzon_theme_main_settings['side_discountbar_display_text_px'])) : ?>
		.widget .display-box{
			font-size: <?php echo $streamzon_theme_main_settings['side_discountbar_display_text_px']; ?>px !important ;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['side_discountbar_display_text_color']) && !empty($streamzon_theme_main_settings['side_discountbar_display_text_color'])) : ?>
		.widget .display-box{
			color: <?php echo $streamzon_theme_main_settings['side_discountbar_display_text_color']; ?> !important ;
		}	
    <?php endif; ?>
	<?php ///////////////////////////////////// header discount
	?>
    <?php if (isset($streamzon_theme_main_settings['header_discountbar_line_on_color']) && !empty($streamzon_theme_main_settings['header_discountbar_line_on_color'])) : ?>
		.header_discount_slider .range-quantity{
			background-color: <?php echo $streamzon_theme_main_settings['header_discountbar_line_on_color']; ?>;
		}	
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_discountbar_line_off_color']) && !empty($streamzon_theme_main_settings['header_discountbar_line_off_color'])) : ?>
		.header_discount_slider .range-bar{
			background-color: <?php echo $streamzon_theme_main_settings['header_discountbar_line_off_color']; ?>;
		}	
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_discountbar_ball_color']) && !empty($streamzon_theme_main_settings['header_discountbar_ball_color'])) : ?>
		.header_discount_slider .range-handle{
			background-color: <?php echo $streamzon_theme_main_settings['header_discountbar_ball_color']; ?>;
		}	
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_discountbar_display_color']) && !empty($streamzon_theme_main_settings['header_discountbar_display_color'])){

		if (isset($streamzon_theme_main_settings['header_discountbar_display_opacity'])) 
			$opct	=	$streamzon_theme_main_settings['header_discountbar_display_opacity']; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['header_discountbar_display_color'])).",".$opct;
	?>
    .header_discount_slider .display-box {
        background: rgba(<?php print $background;?>);
		display: inline;
    }
	
    <?php } ?>

    <?php if (isset($streamzon_theme_main_settings['header_discountbar_display_text_family']) && !empty($streamzon_theme_main_settings['header_discountbar_display_text_family'])) : 
		$returns = 	getFontFamily($streamzon_theme_main_settings['header_discountbar_display_text_family']);
	?>
		.header_discount_slider .display-box{
			font-family: <?php echo $returns['family']; ?> !important;
		}	
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['header_discountbar_display_border_color']) && !empty($streamzon_theme_main_settings['header_discountbar_display_border_color'])) : ?>
		.header_discount_slider .display-box,.header_discount_slider .display-box1 {
			border:  solid 1px <?php echo $streamzon_theme_main_settings['header_discountbar_display_border_color']; ?> !important ;
		}	
    <?php endif; ?>


    <?php if (isset($streamzon_theme_main_settings['header_discountbar_display_text_px']) && !empty($streamzon_theme_main_settings['header_discountbar_display_text_px'])) : ?>
		.header_discount_slider .display-box{
			font-size: <?php echo $streamzon_theme_main_settings['header_discountbar_display_text_px']; ?>px !important ;
		}	
    <?php endif; ?>
    <?php if (isset($streamzon_theme_main_settings['header_discountbar_display_text_color']) && !empty($streamzon_theme_main_settings['header_discountbar_display_text_color'])) : ?>
	.header_discount_slider 	.display-box{
			color: <?php echo $streamzon_theme_main_settings['header_discountbar_display_text_color']; ?> !important ;
		}	
    <?php endif; ?>
	<?php /*//////////////////END OF bar////////////////*/ ?>
    <?php if (isset($streamzon_theme_main_settings['search_page_background']) && !empty($streamzon_theme_main_settings['search_page_background'])) : ?>
    #page-overlay {
        background: <?php echo $streamzon_theme_main_settings['search_page_background']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['search_page_background_opacity'])) : ?>
    .searchbackimg {
        opacity: <?php echo ($streamzon_theme_main_settings['search_page_background_opacity']) / 100; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['search_page_background_image_use']) && !empty($streamzon_theme_main_settings['search_page_background_image_use']) && isset($streamzon_theme_main_settings['search_page_background_image_use']) && $streamzon_theme_main_settings['search_page_background_image_use'] == 1) : ?>
    <?php if ($streamzon_theme_main_settings['search_page_background_image_repeat']==1) {$repitbg="repeat";}else{$repitbg="no-repeat fixed center center / cover  rgba(0, 0, 0, 0)";}  ?>
    .searchbackimg {
        background: url("<?php echo $streamzon_theme_main_settings['search_page_background_image']; ?>" ) <?= $repitbg ?>;
	position: fixed;
	width:100%;
	height:100%;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['boxes_color']) && !empty($streamzon_theme_main_settings['boxes_color'])) : ?>
    <?php
    $streamzon_rgb_color = streamzon_hex2rgb($streamzon_theme_main_settings['boxes_color']);
    ?>
	.page #content{
		background: rgba(
            <?php echo $streamzon_rgb_color[0]; ?>,
            <?php echo $streamzon_rgb_color[1]; ?>,
            <?php echo $streamzon_rgb_color[2]; ?>,
            <?php echo isset($streamzon_theme_main_settings['boxes_opacity']) && !empty($streamzon_theme_main_settings['boxes_opacity']) ? $streamzon_theme_main_settings['boxes_opacity'] / 100 : 1; ?>
        );
	}
    .post, .widget {
        background: rgba(
            <?php echo $streamzon_rgb_color[0]; ?>,
            <?php echo $streamzon_rgb_color[1]; ?>,
            <?php echo $streamzon_rgb_color[2]; ?>,
            <?php echo isset($streamzon_theme_main_settings['boxes_opacity']) && !empty($streamzon_theme_main_settings['boxes_opacity']) ? $streamzon_theme_main_settings['boxes_opacity'] / 100 : 1; ?>
        );
    }

    <?php endif; ?>


    <?php if (isset($streamzon_theme_main_settings['box_text_color']) && !empty($streamzon_theme_main_settings['box_text_color'])) : ?>
    .post .book-attribute,
    .post .amazon-prices,
    .widget,
    .product-price p, .product-price b, .product-price h5,
    .specrow .titelDiv, .specrow .valuDiv    
     {
        color: <?php echo $streamzon_theme_main_settings['box_text_color']; ?>;
    }
	.shortDesc{
		color: <?php echo $streamzon_theme_main_settings['box_text_color']; ?>;
	}

    <?php endif; ?>

    <?php if( isset($streamzon_theme_main_settings['box_background_text_color']) && !empty($streamzon_theme_main_settings['box_background_text_color'])) : ?>
	    .specrow .titelDiv, .specrow .valuDiv {
	        background: <?php echo $streamzon_theme_main_settings['box_background_text_color']; ?>;
	    }

    <?php endif; ?>

	.post-title a, .ppage h1, .ppage h4{
		color: <?php echo $streamzon_theme_main_settings['box_text_url_color']; ?>;
	}
    <?php if (isset($streamzon_theme_main_settings['box_bar_background']) && !empty($streamzon_theme_main_settings['box_bar_background'])) : ?>
    .post-footer {
        background-color: <?php echo $streamzon_theme_main_settings['box_bar_background']; ?>;
	    text-align: left;
    }

    <?php endif; ?>

 	<?php if (isset($streamzon_theme_main_settings['l_page_center_logo_topsapce']) && !empty($streamzon_theme_main_settings['l_page_center_logo_topsapce'])) : ?>
    .landing-page-logo {
        padding-top: <?php echo $streamzon_theme_main_settings['l_page_center_logo_topsapce']; ?>px !important;
    }

    <?php endif; ?>

	<?php if (isset($streamzon_theme_main_settings['l_page_searchbar_toppadding_px']) && !empty($streamzon_theme_main_settings['l_page_searchbar_toppadding_px'])) : ?>
   .jumbotron .subscribe{
        padding-top: <?php echo $streamzon_theme_main_settings['l_page_searchbar_toppadding_px']; ?>px !important;
    }

    <?php endif; ?>

	<?php if (isset($streamzon_theme_main_settings['l_page_searchbar_opacity_px']) && !empty($streamzon_theme_main_settings['l_page_searchbar_opacity_px'])) : ?>
   .jumbotron .subscribe .input-xxlarge, .subscribe .btn-warning{
	//background:rgba(22,22,22,0.1);
        -webkit-box-shadow: none;
		-moz-box-shadow: none;
		box-shadow: none;
    }
	.footercontent{
	padding: <?php echo $streamzon_theme_main_settings['l_page_footer_height_px']; ?>px 0;
	z-index: 101; 
	position: relative;
	}
	#footer_row{
	position: relative;
	bottom:0;
	  height: 100%;
	}
	#footer_rows{
	  position: relative;
  	width: 100%;
	z-index: 100; 
	min-height:170px;
	height:15%;
	}
	.backfooter{
	opacity: <?php echo $streamzon_theme_main_settings['l_page_footer_opacity']/100; ?> !important;
	background: <?php echo $streamzon_theme_main_settings['l_page_background_footer']; ?> !important;
	width:100%;
	z-index: 100; 
	height:100%;
	position:absolute;
	}
	
	.input-wrapper{
		opacity:1;
	}	
    <?php endif; ?>

	<?php if (isset($streamzon_theme_main_settings['l_page_button_search_gap']) && $streamzon_theme_main_settings['l_page_button_search_gap'] == 1) : ?>
	<?php if (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 2) : ?>
		.subscribe .input-xxlarge{
	        margin-right: -5px !important;
	    }
		.subscribe .input-xxlarge{
			border-right:0px solid #ccc !important;
		}
		.subscribe .btn-warning{
			border-left:0px solid #ccc !important;
			border-top-left-radius:0px;
			border-bottom-left-radius:0px;
		}
    <?php endif; ?>
    <?php endif; ?>

	<?php if (isset($streamzon_theme_main_settings['l_page_footer_height_px']) && !empty($streamzon_theme_main_settings['l_page_footer_height_px'])) : ?>
   .footer_row{
        padding: <?php echo $streamzon_theme_main_settings['l_page_footer_height_px']; ?>px 0 !important;
    }
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_button_color']) && !empty($streamzon_theme_main_settings['l_page_button_color'])) : ?>
    .subscribe .btn-warning {
        /*background-color: <?php echo $streamzon_theme_main_settings['l_page_button_color']; ?>;*/
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_button_text_color']) && !empty($streamzon_theme_main_settings['l_page_button_text_color'])) : ?>
    .subscribe .btn-warning {
        color: <?php echo $streamzon_theme_main_settings['l_page_button_text_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_background_image_use']) && $streamzon_theme_main_settings['l_page_background_image_use'] == 1 && isset($streamzon_theme_main_settings['l_page_background_image']) && !empty($streamzon_theme_main_settings['l_page_background_image'])) : ?>
	<?php if ($streamzon_theme_main_settings['l_page_background_image_repeat']==1) {$repitbg="repeat";}else{$repitbg="no-repeat fixed center center / cover  rgba(0, 0, 0, 0)";}  ?>
    #header-logo-layer {
	position:fixed;
        background: url(<?php echo $streamzon_theme_main_settings['l_page_background_image']; ?>)  <?= $repitbg ?>;
   }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_background_color_use']) && $streamzon_theme_main_settings['l_page_background_color_use'] == 1 && isset($streamzon_theme_main_settings['l_page_background_color']) && !empty($streamzon_theme_main_settings['l_page_background_color'])) : ?>
    #header-logo-layer {
        background: <?php echo $streamzon_theme_main_settings['l_page_background_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_background_opacity'])) : ?>
    #header-logo-layer {
        opacity: <?php echo $streamzon_theme_main_settings['l_page_background_opacity'] / 100; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_header_text_px']) && !empty($streamzon_theme_main_settings['l_page_header_text_px'])) : ?>
    .masthead h1 {
        font-size: <?php echo $streamzon_theme_main_settings['l_page_header_text_px']; ?>px;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_header_color']) && !empty($streamzon_theme_main_settings['l_page_header_color'])) : ?>
	
    .masthead h1 {
        color: <?php echo $streamzon_theme_main_settings['l_page_header_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_header_font_family']) && !empty($streamzon_theme_main_settings['l_page_header_font_family'])) : 

		$fontfamily	=	$streamzon_theme_main_settings['l_page_header_font_family'];
		switch($fontfamily)
		{
			case 'Open+Sans':
				$font_link		=	"http://fonts.googleapis.com/css?family=Open+Sans";
				$font_family	=	"'Open Sans', sans-serif";
				break;
			case 'Roboto':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto";
				$font_family	=	"'Roboto', sans-serif";
				break;
			case 'Oswald':
				$font_link		=	"http://fonts.googleapis.com/css?family=Oswald";
				$font_family	=	"'Oswald', sans-serif";
				break;
			case 'Lato':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lato";
				$font_family	=	"'Lato', sans-serif";
				break;
			case 'Roboto+Condensed':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto+Condensed";
				$font_family	=	"'Roboto Condensed', sans-serif";
				break;
			case 'Open+Sans+Condensed:300':
				$font_link		=	"http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300";
				$font_family	=	"'Open Sans Condensed', sans-serif";
				break;
			case 'Raleway':
				$font_link		=	"http://fonts.googleapis.com/css?family=Raleway";
				$font_family	=	"'Raleway', sans-serif";
				break;
			case 'Droid+Serif':
				$font_link		=	"http://fonts.googleapis.com/css?family=Droid+Serif";
				$font_family	=	"'Droid Serif', sans-serif";
				break;
			case 'Montserrat':
				$font_link		=	"http://fonts.googleapis.com/css?family=Montserrat";
				$font_family	=	"'Montserrat', sans-serif";
				break;
			case 'Roboto+Slab':
				$font_link		=	"http://fonts.googleapis.com/css?family=Roboto+Slab";
				$font_family	=	"'Roboto Slab', sans-serif";
				break;
			case 'PT+Sans+Narrow':
				$font_link		=	"http://fonts.googleapis.com/css?family=PT+Sans+Narrow";
				$font_family	=	"'PT Sans Narrow', sans-serif";
				break;
			case 'Lora':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lora";
				$font_family	=	"'Lora', serif";
				break;
			case 'Arimo':
				$font_link		=	"http://fonts.googleapis.com/css?family=Arimo";
				$font_family	=	"'Arimo', sans-serif";
				break;
			case 'Bitter':
				$font_link		=	"http://fonts.googleapis.com/css?family=Bitter";
				$font_family	=	"'Bitter', serif";
				break;
			case 'Merriweather':
				$font_link		=	"http://fonts.googleapis.com/css?family=Merriweather";
				$font_family	=	"'Merriweather', serif";
				break;
			case 'Oxygen':
				$font_link		=	"http://fonts.googleapis.com/css?family=Oxygen";
				$font_family	=	"'Merriweather', sans-serif";
				break;
			case 'Lobster':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lobster";
				$font_family	=	"'Lobster', cursive";
				break;
			case 'Titillium+Web':
				$font_link		=	"http://fonts.googleapis.com/css?family=Titillium+Web";
				$font_family	=	"'Titillium Web', sans-serif";
				break;
			case 'Poiret+One':
				$font_link		=	"http://fonts.googleapis.com/css?family=Poiret+One";
				$font_family	=	"'Poiret One', cursive";
				break;
			case 'Play':
				$font_link		=	"http://fonts.googleapis.com/css?family=Play";
				$font_family	=	"'Play', sans-serif";
				break;
			case 'Pacifico':
				$font_link		=	"http://fonts.googleapis.com/css?family=Pacifico";
				$font_family	=	"'Pacifico', cursive";
				break;
			case 'Questrial':
				$font_link		=	"http://fonts.googleapis.com/css?family=Questrial";
				$font_family	=	"'Questrial', sans-serif";
				break;
			case 'Dancing+Script':
				$font_link		=	"http://fonts.googleapis.com/css?family=Dancing+Script";
				$font_family	=	"'Dancing Script', cursive";
				break;
			case 'Exo+2':
				$font_link		=	"http://fonts.googleapis.com/css?family=Exo+2";
				$font_family	=	"'Exo 2', sans-serif";
				break;
			case 'Comfortaa':
				$font_link		=	"http://fonts.googleapis.com/css?family=Comfortaa";
				$font_family	=	"'Comfortaa', cursive";
				break;
			case 'Ropa+Sans':
				$font_link		=	"http://fonts.googleapis.com/css?family=Ropa+Sans";
				$font_family	=	"'Ropa Sans', sans-serif";
				break;
			case 'Lobster+Two':
				$font_link		=	"http://fonts.googleapis.com/css?family=Lobster+Two";
				$font_family	=	"'Lobster Two', cursive";
				break;
			case 'EB+Garamond':
				$font_link		=	"http://fonts.googleapis.com/css?family=EB+Garamond";
				$font_family	=	"'EB Garamond', serif";
				break;
			default:
				$font_link		=	null;
				$font_family	=	"'Open Sans',Arial,Helvetica,sans-serif";
				break;

		}
	?>
    .masthead h1 {
        font-family: <?php echo $font_family; ?> !important;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_header_bold'])) : ?>
    .masthead h1 {
        font-weight: <?php echo ($streamzon_theme_main_settings['l_page_header_bold'] == 1) ? 'bold' : 'normal'; ?>;
    }

    <?php endif; ?>
    <?php 
    $widthmargin=$streamzon_theme_main_settings['landing_page_context_width'];
    $widthcontent=($widthmargin*2)+625;
		if (isset($streamzon_theme_main_settings['box_big_small_img']) && !empty($streamzon_theme_main_settings['box_big_small_img'])) : 
			
			if($streamzon_theme_main_settings['box_big_small_img'] == 1){
				?>
				.post-image {
					margin:0 0 8px !important;
				}
				.post-image img {
					width:171px;
				}
				.grid4 .post {
				  width: 171px;
				}
				.post-inner .puerto-btn-2 small {
					 display: none!important;
					 
				}
				.simpleCart_shelfItem a.item_add {
					min-width: 36px;
				}

			<?php
			}
			elseif($streamzon_theme_main_settings['box_big_small_img'] == 2){
			?>
				.post-image {
					margin:0px 0 8px !important;
				}
				.post-image img {
					width:195px;
				}
				.grid4 .post {
				  width: 195px;
				}
				.fa-hover {
					margin-left: 0px!important;
				}
				.post-social {
					margin: 5px 9px!important;
				}
				.post-inner .puerto-btn-2 small {
					 font-size: 12px!important;
					 
				}
				.simpleCart_shelfItem a.item_add {
					min-width: 98px;
				}

			<?php
			}
			elseif($streamzon_theme_main_settings['box_big_small_img'] == 3){
			?>
				.post-image {
					margin:0px 0 8px !important;
				}
				.post-image img {
					width:222px;
				}
				.grid4 .post {
				  width: 222px;
				}
			<?php
			}
		?>


    <?php endif; ?>

    <?php 
		if (isset($streamzon_theme_main_settings['l_page_landing_page_align']) && !empty($streamzon_theme_main_settings['l_page_landing_page_align'])) : 
		
		if($streamzon_theme_main_settings['l_page_landing_page_align'] == 1){
		?>
			.row-fluid .offset1:first-child,
			.row-fluid .offset2:first-child{
				margin-left:0px !important;
			}
			.row-fluid .offset2:first-child{
				width: 100%;
				text-align:center;	
			}
			.subscribe{
				width: 100% !important;
				margin:auto !important;
			}
			.LightTwitter{
				margin-bottom:20px;
			}
            .landcontent{
                width: 625px;
                float: left;
                box-sizing: border-box;
                margin: 0 <?php echo $streamzon_theme_main_settings['landing_page_context_width']?>px;
            }
            @media (max-width: <?php echo $widthcontent;?>px) {
                    .landcontent{
                        width: 100%;
                        float: left;
                        margin: 0;
                    }
            }

			@media (max-width: 480px) {
				.row-fluid .span10{
					 width: 100% !important;
				}
                .landcontent{
                    width: 100%;
                    float: left;
                    margin: 0;
                }
			}
		<?php	
		}
		else if($streamzon_theme_main_settings['l_page_landing_page_align'] == 2){
		?>
			.row-fluid .offset1:first-child,
			.row-fluid .offset2:first-child{
				margin-left:0px !important;
			}
			.row-fluid .offset2:first-child{
				width: 100%;
				text-align:center;	
			}
			.subscribe{
				width: 100% !important;
				margin:auto !important;
			}
			.LightTwitter{
				margin-bottom:20px;
			}
            .landcontent{
                width: 625px;
                margin: 0 auto;
            }
            @media (max-width: 625px) {
                .landcontent{
                    width: 100%;
                    float: none;
                    margin: 0;
                }
            }

			@media (max-width: 480px) {
				.row-fluid .span10{
					 width: 100% !important;
				}
                .landcontent{
                    width: 100%;
                    float: none;
                    margin: 0;
                }
			}

		<?php
		}
		else if($streamzon_theme_main_settings['l_page_landing_page_align'] == 3){
		?>
			.row-fluid .offset1:first-child,
			.row-fluid .offset2:first-child{
				margin-left:0px !important;
			}
			.row-fluid .offset2:first-child{
				width: 100%;
				text-align:center;	
			}
			.subscribe{
				width: 100% !important;
				margin:auto !important;
			}
			.LightTwitter{
				margin-bottom:20px;
			}
            .landcontent{
                width: 625px;
                float: right;
                margin: 0 <?php echo $streamzon_theme_main_settings['landing_page_context_width']?>px;
            }
            @media (max-width: <?php echo $widthcontent;?>px) {
                    .landcontent{
                        width: 100%;
                        float: right;
                        margin: 0;
                    }
            }

			@media (max-width: 480px) {
				.row-fluid .span10{
					 width: 100% !important;
				}
                .landcontent{
                    width: 100%;
                    float: right;
                    margin: 0;
                }
			}
		<?php
		}
				
    endif; ?>
    <?php if (isset($streamzon_theme_main_settings['l_page_subheader_font_family']) && !empty($streamzon_theme_main_settings['l_page_subheader_font_family'])) : 

		$subfontfamily	=	$streamzon_theme_main_settings['l_page_subheader_font_family'];
		switch($subfontfamily)
		{
			case 'Open+Sans':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Open+Sans";
				$subfont_family	=	"'Open Sans', sans-serif";
				break;
			case 'Roboto':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Roboto";
				$subfont_family	=	"'Roboto', sans-serif";
				break;
			case 'Oswald':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Oswald";
				$subfont_family	=	"'Oswald', sans-serif";
				break;
			case 'Lato':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Lato";
				$subfont_family	=	"'Lato', sans-serif";
				break;
			case 'Roboto+Condensed':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Roboto+Condensed";
				$subfont_family	=	"'Roboto Condensed', sans-serif";
				break;
			case 'Open+Sans+Condensed:300':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300";
				$subfont_family	=	"'Open Sans Condensed', sans-serif";
				break;
			case 'Raleway':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Raleway";
				$subfont_family	=	"'Raleway', sans-serif";
				break;
			case 'Droid+Serif':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Droid+Serif";
				$subfont_family	=	"'Droid Serif', sans-serif";
				break;
			case 'Montserrat':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Montserrat";
				$subfont_family	=	"'Montserrat', sans-serif";
				break;
			case 'Roboto+Slab':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Roboto+Slab";
				$subfont_family	=	"'Roboto Slab', sans-serif";
				break;
			case 'PT+Sans+Narrow':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=PT+Sans+Narrow";
				$subfont_family	=	"'PT Sans Narrow', sans-serif";
				break;
			case 'Lora':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Lora";
				$subfont_family	=	"'Lora', serif";
				break;
			case 'Arimo':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Arimo";
				$subfont_family	=	"'Arimo', sans-serif";
				break;
			case 'Bitter':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Bitter";
				$subfont_family	=	"'Bitter', serif";
				break;
			case 'Merriweather':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Merriweather";
				$subfont_family	=	"'Merriweather', serif";
				break;
			case 'Oxygen':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Oxygen";
				$subfont_family	=	"'Merriweather', sans-serif";
				break;
			case 'Lobster':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Lobster";
				$subfont_family	=	"'Lobster', cursive";
				break;
			case 'Titillium+Web':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Titillium+Web";
				$subfont_family	=	"'Titillium Web', sans-serif";
				break;
			case 'Poiret+One':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Poiret+One";
				$subfont_family	=	"'Poiret One', cursive";
				break;
			case 'Play':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Play";
				$subfont_family	=	"'Play', sans-serif";
				break;
			case 'Pacifico':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Pacifico";
				$subfont_family	=	"'Pacifico', cursive";
				break;
			case 'Questrial':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Questrial";
				$subfont_family	=	"'Questrial', sans-serif";
				break;
			case 'Dancing+Script':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Dancing+Script";
				$subfont_family	=	"'Dancing Script', cursive";
				break;
			case 'Exo+2':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Exo+2";
				$subfont_family	=	"'Exo 2', sans-serif";
				break;
			case 'Comfortaa':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Comfortaa";
				$subfont_family	=	"'Comfortaa', cursive";
				break;
			case 'Ropa+Sans':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Ropa+Sans";
				$subfont_family	=	"'Ropa Sans', sans-serif";
				break;
			case 'Lobster+Two':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=Lobster+Two";
				$subfont_family	=	"'Lobster Two', cursive";
				break;
			case 'EB+Garamond':
				$subfont_link		=	"http://fonts.googleapis.com/css?family=EB+Garamond";
				$subfont_family	=	"'EB Garamond', serif";
				break;
			default:
				$subfont_link		=	null;
				$subfont_family	=	"'Open Sans',Arial,Helvetica,sans-serif";
				break;

		}
	?>
    .masthead p {
        font-family: <?php echo $subfont_family; ?> !important;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_subheader_font_px']) && !empty($streamzon_theme_main_settings['l_page_subheader_font_px'])) : ?>
    .masthead p {
        font-size: <?php echo $streamzon_theme_main_settings['l_page_subheader_font_px']; ?>px;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_subheader_color']) && !empty($streamzon_theme_main_settings['l_page_subheader_color'])) : ?>
    .masthead p {
        color: <?php echo $streamzon_theme_main_settings['l_page_subheader_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_subheader_bold'])) : ?>
    .masthead p {
        font-weight: <?php echo ($streamzon_theme_main_settings['l_page_subheader_bold'] == 1) ? 'bold' : 'normal'; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_body_background_color']) && !empty($streamzon_theme_main_settings['l_page_body_background_color'])) : ?>
    body.landing-page {
        background: <?php echo $streamzon_theme_main_settings['l_page_body_background_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_searchbar_height_px']) && !empty($streamzon_theme_main_settings['l_page_searchbar_height_px'])) : ?>
    .subscribe .input-xxlarge,.subscribe .btn-warning {
        height: <?php echo $streamzon_theme_main_settings['l_page_searchbar_height_px']; ?>px;
    }
	.subscribe .input-xxlarge{
		line-height: <?php echo $streamzon_theme_main_settings['l_page_searchbar_height_px']; ?>px;
	}
	.subscribe .btn-warning{
        line-height: 0px !important;
		padding: 4px 6px;
	}
    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_searchbar_height_px']) && !empty($streamzon_theme_main_settings['l_page_searchbar_height_px'])) : ?>
    /*.subscribe button.btn.btn-large{
        padding-top: <?php echo ($streamzon_theme_main_settings['l_page_searchbar_height_px'] + 10 - 26) / 2; ?>px;
        padding-bottom: <?php echo ($streamzon_theme_main_settings['l_page_searchbar_height_px'] + 10 - 24) / 2; ?>px;
    }*/
	.subscribe .btn-warning {
        padding: <?php echo ($streamzon_theme_main_settings['l_page_searchbar_height_px'] + 8) / 2; ?>px 20px;
	}

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_searchbar_border_color']) && !empty($streamzon_theme_main_settings['l_page_searchbar_border_color'])) : ?>
    .subscribe .input-xxlarge {
        border: solid 1px <?php echo $streamzon_theme_main_settings['l_page_searchbar_border_color']; ?>;
    }

    <?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_searchbar_background_color']) && !empty($streamzon_theme_main_settings['l_page_searchbar_background_color'])) : 
		if (isset($streamzon_theme_main_settings['l_page_searchbar_opacity_px'])) 
			$opct	=	$streamzon_theme_main_settings['l_page_searchbar_opacity_px'] ; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['l_page_searchbar_background_color'])).",".$opct;
	
	?>
	.subscribe .input-xxlarge {
		background: rgba(<?php print $background;?>);
	}	
    <?php endif; ?>

   <?php if ($streamzon_theme_main_settings['l_page_background_video_pattern_enable']==1) : ?>
	.on_pattern{
	    background: url('<?php bloginfo('stylesheet_directory'); ?>/landing-page/patterns/<?php print $streamzon_theme_main_settings['l_page_background_video_pattern'];?>.png') rgba(0, 0, 0, 0.1);
		position: fixed; z-index: 2; width: 100%; height: 100%;
	}
	#header-logo-layer > #pattern1{
	    background: url('<?php bloginfo('stylesheet_directory'); ?>/landing-page/patterns/<?php print $streamzon_theme_main_settings['l_page_background_video_pattern'];?>.png') rgba(0, 0, 0, 0.1);
		background-attachment: fixed;
		position: fixed; z-index: 2; width: 100%; height: 100%;
		opacity:<?php echo $streamzon_theme_main_settings['l_page_background_pattern_opacity']/100; ?>;
	}
	<?php endif; ?>


	<?php if (!empty($streamzon_theme_main_settings['search_page_background_pattern_enable']) && $streamzon_theme_main_settings['search_page_background_pattern_enable']==1 ) : ?>
	.on_pattern_search{
	    background: url('<?php bloginfo('stylesheet_directory'); ?>/landing-page/patterns/<?php print $streamzon_theme_main_settings['search_page_background_pattern'];?>.png') rgba(0, 0, 0, 0.1);
		position: fixed; z-index: 2; width: 100%; height: 100%;
	    opacity:<?php echo $streamzon_theme_main_settings['search_page_pattern_opacity']/100;?>
	}
	<?php endif; ?>

    <?php if (isset($streamzon_theme_main_settings['l_page_background_image_use']) && $streamzon_theme_main_settings['l_page_background_image_use'] == 1 && isset($streamzon_theme_main_settings['l_page_background_image']) && !empty($streamzon_theme_main_settings['l_page_background_image'])) : ?>
	<?php if ($streamzon_theme_main_settings['l_page_background_image_repeat']==1) {$repitbg="repeat";}else{$repitbg="no-repeat fixed center center / cover  rgba(0, 0, 0, 0)";}  ?>
	#header-logo-layer {
        background: url(<?php echo $streamzon_theme_main_settings['l_page_background_image']; ?>) <?= $repitbg ?>;
   }

    <?php endif; ?>

   <?php if (isset($streamzon_theme_main_settings['l_page_button_color']) && !empty($streamzon_theme_main_settings['l_page_button_color'])) : 
	
		if (isset($streamzon_theme_main_settings['l_page_button_text_opacity_px'])) 
			$opct	=	$streamzon_theme_main_settings['l_page_button_text_opacity_px'] ; 
		else
			$opct	=	1;  				
	
		$background	=	implode(",",hex2rgb($streamzon_theme_main_settings['l_page_button_color'])).",".$opct;

	?>
    .subscribe .btn-warning {
		background: rgba(<?php print $background;?>);
    }
    <?php endif; ?>


    <?php 
		if (isset($streamzon_theme_main_settings['l_page_button_hover_color']) && !empty($streamzon_theme_main_settings['l_page_button_hover_color'])) 
		{ 
			//$background	=	implode(",",hex2rgb()).",".$opct;
	?>
		    .subscribe .btn-warning:hover {
	    	    background: <?php print $streamzon_theme_main_settings['l_page_button_hover_color'];?>;
	    	}
    <?php } ?>



    <?php if (isset($streamzon_theme_main_settings['l_page_button_text_border_color']) && !empty($streamzon_theme_main_settings['l_page_button_text_border_color'])) : ?>
    .subscribe .btn-warning {
        border: solid 1px <?php echo $streamzon_theme_main_settings['l_page_button_text_border_color']; ?>;
    }

    <?php endif; ?>

	<?php if (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 1) : ?>
	    .subscribe .input-xxlarge {
	       	width: 100%;
			max-width:555px;
			margin-bottom:5px;
	    }				
		.subscribe .btn-warning{
			max-width:575px !important;
			width:100% !important;
		}
	<?php elseif (isset($streamzon_theme_main_settings['l_page_button_long_short']) && $streamzon_theme_main_settings['l_page_button_long_short'] == 2) : ?>
		.subscribe .input-xxlarge {
	        color: <?php echo $streamzon_theme_main_settings['l_page_searchbar_text_color']; ?>;
	    }
    <?php endif; ?>
	
    <?php if (isset($streamzon_theme_main_settings['l_page_searchbar_text_color']) && !empty($streamzon_theme_main_settings['l_page_searchbar_text_color'])) : ?>
    .subscribe .input-xxlarge {
        color: rgba(<?php echo implode(",",hex2rgb($streamzon_theme_main_settings['l_page_searchbar_text_color'])).","."1"; ?>);
    }
	input::-webkit-input-placeholder { /* WebKit browsers */
 	   color:    <?php echo $streamzon_theme_main_settings['l_page_searchbar_text_color']; ?>;
		opacity:  0.6;
	}
	input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
	   color:   <?php echo $streamzon_theme_main_settings['l_page_searchbar_text_color']; ?>;
	   opacity:  0.6;
	}
	input::-moz-placeholder { /* Mozilla Firefox 19+ */
	   color:   <?php echo $streamzon_theme_main_settings['l_page_searchbar_text_color']; ?>;
	   opacity:  0.6;
	}
	input:-ms-input-placeholder { /* Internet Explorer 10+ */
	   color:   <?php echo $streamzon_theme_main_settings['l_page_searchbar_text_color']; ?>;
	}


    <?php endif; ?>

	    .footertext {
			<?php if (isset($streamzon_theme_main_settings['l_page_footer_text_color']) && !empty($streamzon_theme_main_settings['l_page_footer_text_color'])) : ?>
				color: <?php echo isset($streamzon_theme_main_settings['l_page_footer_text_color']) ? $streamzon_theme_main_settings['l_page_footer_text_color'] : ''; ?>;
		    <?php endif; ?>
			<?php if (isset($streamzon_theme_main_settings['l_page_footer_text_font_family']) && !empty($streamzon_theme_main_settings['l_page_footer_text_font_family'])) : 
				$footer_link = getFontFamily($streamzon_theme_main_settings['l_page_footer_text_font_family']);
			?>
				font-family: <?php echo $footer_link['family']; ?> !important;
		    <?php endif; ?>
			<?php if (isset($streamzon_theme_main_settings['l_page_footer_text_font_px']) && !empty($streamzon_theme_main_settings['l_page_footer_text_font_px'])) : ?>
				font-size: <?php echo isset($streamzon_theme_main_settings['l_page_footer_text_font_px']) ? $streamzon_theme_main_settings['l_page_footer_text_font_px'] : ''; ?>px;
		    <?php endif; ?>
	    }
		.jumbotron .container{
			<?php if (isset($streamzon_theme_main_settings['1_page_landing_page_body_size']) && !empty($streamzon_theme_main_settings['1_page_landing_page_body_size'])) : ?>
				margin: <?php echo isset($streamzon_theme_main_settings['1_page_landing_page_body_size']) ? $streamzon_theme_main_settings['1_page_landing_page_body_size'] : ''; ?>px auto;
		    <?php endif; ?>
		}
    

    <?php if (isset($streamzon_theme_main_settings['l_page_background_video_opacity'])) : ?>
    #tubular-container {
        opacity: <?php echo $streamzon_theme_main_settings['l_page_background_video_opacity'] / 100; ?>;
    }
	
    <?php endif; ?>


.noUi-connect,.noUi-base{
    background: none repeat scroll 0 0 rgba(255,255,255,0.5) !important;
    box-shadow: 0 0 3px rgba(51, 51, 51, 0.45) inset;
    transition: background 450ms ease 0s;
}
.tooltip_noui {
    display: block;
	position: absolute;
	border: 1px solid #D9D9D9;
	font: 400 12px/12px Arial;
	border-radius: 3px;
	background: #fff;
	top: -9px;
	padding: 5px;
	left: -20px;
	text-align: center;
	width: 70px;
	color:#000;
}
.tooltip_noui strong {
	display: block;
	padding: 2px;
	color:#000;
}
//footer opacity
</style>
<?php if($font_link) : ?>
	<link href='<?php print $font_link;?>' rel='stylesheet' type='text/css'>
<?php endif; ?>
<?php if($subfont_link) : ?>
	<link href='<?php print $subfont_link;?>' rel='stylesheet' type='text/css'>
<?php endif; ?>
<?php if($prohead_link) : ?>
	<link href='<?php print $prohead_link;?>' rel='stylesheet' type='text/css'>
<?php endif; ?>
<?php if($returns['link']) : ?>
	<link href='<?php print $returns['link'];?>' rel='stylesheet' type='text/css'>
<?php endif; ?>
<?php if($footer_link['link']) : ?>
	<link href='<?php print $footer_link['link'];?>' rel='stylesheet' type='text/css'>
<?php endif; ?>