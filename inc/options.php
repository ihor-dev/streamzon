<?php
class StreamzonSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $test_settings;
    private $amazon_credentials;
    private $amazon_stores;
    private $amazon_settings;
    private $seo_settings;
    private $amazon_item_settings;
    private $theme_settings;

    private $theme_settings_defaults = array();

    public static $amazon_locales = array(
        'ca' => 'CA',
        'cn' => 'CN',
        'de' => 'DE',
        'es' => 'ES',
        'fr' => 'FR',
        'in' => 'IN',
        'it' => 'IT',
        'co.jp' => 'JP',
        'co.uk' => 'UK',
        'com' => 'US',
    );
    public static $fonts	=	array(
        "Open+Sans"					=> array("Open+Sans","Open Sans","'Open Sans Condensed', sans-serif"),
        "Roboto"					=> array("Roboto","Roboto","'Roboto', sans-serif"),
        "Oswald"					=> array("Oswald","Oswald","'Oswald'"),
        "Lato"						=> array("Lato"	,"Lato","'Lato'"),
        "Roboto+Condensed"			=> array("Roboto+Condensed","Roboto Condensed","'Roboto Condensed'"	),
        "Open+Sans+Condensed:300"	=> array("Open+Sans+Condensed:300","Open Sans Condensed","'Open Sans Condensed'"	),
        "Raleway"					=> array("Raleway","Raleway","'Raleway', sans-serif"),
        "Droid+Serif"				=> array("Droid+Serif","Droid Serif","'Droid Serif'"),
        "Montserrat"				=> array("Montserrat","Montserrat","'Montserrat'"),
        "Roboto+Slab"				=> array("Roboto+Slab","Roboto Slab","'Roboto Slab'"),
        "PT+Sans+Narrow"			=> array("PT+Sans+Narrow","PT Sans Narrow","'PT Sans Narrow'"),
        "Lora"						=> array("Lora","Lora","'Lora'"	),
        "Arimo"						=> array("Arimo","Arimo","'Arimo'"	),
        "Bitter"					=> array("Bitter","Bitter","'Bitter'"	),
        "Merriweather"				=> array("Merriweather","Merriweather","'Merriweather'"	),
        "Oxygen"					=> array("Oxygen","Oxygen","'Oxygen'"	),
        "Lobster"					=> array("Lobster","Lobster","'Lobster'"	),
        "Titillium+Web"				=> array("Titillium+Web","Titillium Web","'Titillium Web'"	),
        "Poiret+One"				=> array("Poiret+One","Poiret One","'Poiret One'"	),
        "Play"						=> array("Play","Play","'Play'"	),
        "Pacifico"					=> array("Pacifico","Pacifico","'Pacifico'"	),
        "Questrial"					=> array("Questrial","Questrial","'Questrial'"	),
        "Dancing+Script"			=> array("Dancing+Script","Dancing Script","'Dancing Script'"	),
        "Exo+2"						=> array("Exo+2","Exo 2","'Exo 2'"	),
        "Comfortaa"					=> array("Comfortaa","Comfortaa","'Comfortaa'"	),
        "Ropa+Sans"					=> array("Ropa+Sans","Ropa Sans","'Ropa Sans'"	),
        "Lobster+Two"				=> array("Lobster+Two","Lobster Two","'Lobster Two'"	),
        "EB+Garamond"				=> array("EB+Garamond","EB Garamond","'EB Garamond'"	),
    );

    private $amazon_credential_columns = array(
        'streamzon-amazon-credentials' /*page*/ => array(
            'your_amazon_credentials' /*section*/ => array(
                array(
                    'column_title' => 'Your Amazon Credentials',
                    'options' => array(
                        'access_key_id',                        
                        'secret_access_key',
                        'high_traffic_site',
                        'access_key_id_2',
                        'secret_access_key_2'
                    ),
                ),array(
                    'column_title' => 'Amazon Markets IDs',
                    'options' => array(
                        'amazon_associate_id',
                        'amazon_associate_id_ca',
                        'amazon_associate_id_uk',
                        'amazon_associate_id_de',
                        'amazon_associate_id_fr',
                        'amazon_associate_id_in',
                        'amazon_associate_id_it',
                        'amazon_associate_id_es',
                        'amazon_associate_id_jp',
                        'amazon_associate_id_cn',
                    ),
                ),array(
                    'column_title' => 'Default Market',
                    'options' => array(
                        'amazon_associate_auto',
                    ),
                )
            ),
        ),
    );

    //==================Dashboard ===============
    private $streamzon_theme_test_columns = array(
        'streamzon-theme-test' /*page*/ => array(
            'streamzon_theme_test_section' /*section*/ => array(                
                array(
                    'column_title' => 'Your Amazon Credentials',
                    'options' => array(
                        'access_key_id',                        
                        'secret_access_key',
                        'high_traffic_site',
                        'access_key_id_2',
                        'secret_access_key_2'
                    )  
                ),
                array(
                    'column_title' => 'Amazon Markets IDs',
                    'options' => array(
                        'amazon_associate_id',
                        'amazon_associate_id_ca',
                        'amazon_associate_id_uk',
                        'amazon_associate_id_de',
                        'amazon_associate_id_fr',
                        'amazon_associate_id_in',
                        'amazon_associate_id_it',
                        'amazon_associate_id_es',
                        'amazon_associate_id_jp',
                        'amazon_associate_id_cn',
                    ),
                ),                
                array(
                    'column_title' => 'Default Market',
                    'options' => array(
                        'amazon_associate_auto',
                    ),
                ),
                //part2
                array(
                    'column_title' => 'Store Keyword',
                    'options' => array(
                        'default_search_keyword',
                    ),
                ),
                array(
                    'column_title' => 'Default Discount',
                    'options' => array(
                        'amazon_discount_percent'
                    ),
                ),  

                //part3
                array(
                    'column_title' => 'Amazon US Market',
                    'options' => array(
                        'amazon_category',
                    ),
                ),array(
                    'column_title' => 'Amazon UK Market',
                    'options' => array(
                        'amazon_uk_category',
                    ),
                ),array(
                    'column_title' => 'Amazon CA Market',
                    'options' => array(
                        'amazon_ca_category',
                    ),
                ),array(
                    'column_title' => 'Amazon DE Market',
                    'options' => array(
                        'amazon_de_category',
                    ),
                ),array(
                    'column_title' => 'Amazon FR Market',
                    'options' => array(
                        'amazon_fr_category',
                    ),
                ),
                array(
                    'column_title' => 'Amazon IN Market',
                    'options' => array(
                        'amazon_in_category',
                    ),
                ),array(
                    'column_title' => 'Amazon IT Market',
                    'options' => array(
                        'amazon_it_category',
                    ),
                ),array(
                    'column_title' => 'Amazon ES Market',
                    'options' => array(
                        'amazon_es_category',
                    ),
                ),array(
                    'column_title' => 'Amazon JP Market',
                    'options' => array(
                        'amazon_jp_category',
                    ),
                ),array(
                    'column_title' => 'Amazon CN Market',
                    'options' => array(
                        'amazon_cn_category',
                    ),
                ),
                //part4



            )
        ),
    );

    private $amazon_stores_columns = array(
        'streamzon-amazon-stores' /*page*/ => array(
            'streamzon_amazon_stores_section' /*section*/ => array(
                array(
                    'column_title' => 'Amazon US Market',
                    'options' => array(
                        'amazon_category',
                    ),
                ),array(
                    'column_title' => 'Amazon UK Market',
                    'options' => array(
                        'amazon_uk_category',
                    ),
                ),array(
                    'column_title' => 'Amazon CA Market',
                    'options' => array(
                        'amazon_ca_category',
                    ),
                ),array(
                    'column_title' => 'Amazon DE Market',
                    'options' => array(
                        'amazon_de_category',
                    ),
                ),array(
                    'column_title' => 'Amazon FR Market',
                    'options' => array(
                        'amazon_fr_category',
                    ),
                ),
                array(
                    'column_title' => 'Amazon IN Market',
                    'options' => array(
                        'amazon_in_category',
                    ),
                ),array(
                    'column_title' => 'Amazon IT Market',
                    'options' => array(
                        'amazon_it_category',
                    ),
                ),array(
                    'column_title' => 'Amazon ES Market',
                    'options' => array(
                        'amazon_es_category',
                    ),
                ),array(
                    'column_title' => 'Amazon JP Market',
                    'options' => array(
                        'amazon_jp_category',
                    ),
                ),array(
                    'column_title' => 'Amazon CN Market',
                    'options' => array(
                        'amazon_cn_category',
                    ),
                ),/**/
            ),
        ),
    );
    private $amazon_settings_columns = array(
        'streamzon-amazon-settings' /*page*/ => array(
            'streamzon_amazon_settings_section' /*section*/ => array(
                
                array(
                     'column_title' => 'Additional Search Parameters',
                     'options' => array(
                         'amazon_additional_search_parameter',                         
                     ),
                ),                
                array(
                    'column_title' => 'Store Keyword',
                    'options' => array(
                        'default_search_keyword',
                    ),
                ),
                /*array(
                    'column_title' => 'Discount',
                    'options' => array(
                        'amazon_discount',
                        'amazon_discount_amount'
                    ),
                ),*/
                array(
                    'column_title' => 'Default Discount',
                    'options' => array(
                        'amazon_discount_percent'
                    ),
                ),

                array(
                    'column_title' => 'Cart',
                    'options' => array(
                        'Product_buy_anable',
                        'Product_cart_anable',

                    ),
                ),
                array(
                    'column_title' => 'Pages',
                    'options' => array(
                        'amazon_addsingPage',
                        'show_sidebar',
                        'l_page_landing_page_enable',
                        'l_page_onepage_landing'
                    ),
                ),

            ),
        ),
    );

    private $amazon_item_settings_columns = array(
        'streamzon-amazon-item-settings' /*page*/ => array(
            'streamzon_amazon_item_settings_section' /*section*/ => array(
                array(
                    'column_title' => 'Amazon Item settings',
                    'options' => array(
                        'amazon_item_attributes',
                    ),
                ),
            ),
        ),
    );

    //SEO
    private $seo_settings_columns = array(
        'streamzon-seo' /*page*/ => array(
            'streamzon_seo_settings_section' /*section*/ => array(
                array(
                    'column_title' => 'Home page',
                    'options' => array(
                        'seo_home_title',
						'seo_home_description',
						'seo_home_keywords',
                    ),
                ),
		array(
                    'column_title' => 'Search page',
                    'options' => array(
						'seo_search_title',
						'seo_search_description',
						'seo_search_keywords',
                    ),
                ),
		array(
                    'column_title' => 'Product page',
                    'options' => array(
						'seo_product_title',
						'seo_product_description',
						'seo_product_keywords',
                    ),
                ),
		array(
                    'column_title' => 'Search page Facebook Pixel ',
                    'options' => array(
						'search_page_facebook_pixel_enable',
                        'search_page_facebook_pixel_code',
                    ),
                ),
		array(
                    'column_title' => 'Landing page Facebook Pixel ',
                    'options' => array(
						'land_page_facebook_pixel_enable',
                        'land_page_facebook_pixel_code',
                    ),
                ),
            ),
        ),
    );
    //END SEO

    

    private $theme_options_columns = array(
        'streamzon-theme-settings' /*page*/ => array(
            'streamzon_theme_settings_section' /*section*/ => array(
                array(
                    'column_title' => 'Top Bar',
                    'options' => array(
                        'top_bar_color',
                        'header_text_color',
                        'text_link_color',
                        'top_bar_opacity',
                        'header_menu',
                    ),
                ),
                array(
                    'column_title' => 'Logo',
                    'options' => array(
                        'top_bar_logo',
                        'top_bar_logo_use',
                        'header_text_enable',

                        'header_text',
                        'header_text_Canada',
                        'header_text_China',
                        'header_text_Germany',
                        'header_text_Spain',
                        'header_text_France',
                        'header_text_India',
                        'header_text_Italy',
                        'header_text_Japan',
                        'header_text_UK',
                        'header_text_USA',

                        'header_text_font_family',
                        'header_text_px',
                        //'header_text_color',
                        'header_text_bold',
                    ),
                ),
                array(
                    'column_title' => 'Background',
                    'options' => array(
                        'search_page_background',
                        'search_page_background_image',
                        'search_page_background_image_use',
						'search_page_background_image_repeat',
						'search_page_background_pattern_enable',
						'search_page_background_pattern',
						'search_page_pattern_opacity',
                        'search_page_background_opacity',
                    ),
                ),
                array(
                    'column_title' => 'Boxes and Side Bar',
                    'options' => array(
                        'boxes_color',
                        'boxes_opacity',
                        'box_text_color',
						'box_text_url_color',
                        'box_bar_background',
                        'box_background_text_color',
                        'box_bar_logo',
                        'box_bar_logo_use',
                        'box_big_small_img'
                    ),
                ),
                /*array(
                    'column_title' => 'Text',
                    'options' => array(
                        'text_link_color',
                    ),
                ),
                */
                array(
                    'column_title' => 'Side Bar',
                    'options' => array(					
					'sidebar_header_color',
					'page_search_button_color',
					//'page_search_button_hover_color',
					'page_search_button_text_color',
					'page_search_button_text_opacity_px',
					'page_search_button_text_border_color',
					'slidebar_searchbar_image',
                    ),
                ),
				array(
                    'column_title' => 'Cart Buttons',
                    'options' => array(						
                        'small_cart_button_color',
                        'small_cart_button_background',
                        'add_to_cart_button_background',
						'add_to_cart_button_background2',
						'add_to_cart_button_color',                        
						'buyitnow_button_background',
						'buyitnow_button_background2',
						'buyitnow_button_color',
                    ),
                ),
                array(
                    'column_title' => 'Banner',
                    'options' => array(
                        'banner_image_use',
                        'banner_image_link',
                        'banner_image_file',
                        'banner_code_use',
                        'banner_code',
                    ),
                ),
                array(
                    'column_title' => 'Revolution slider',
                    'options' => array(
                        'revolution_slider_use',
                        'revolution_slider_code',
			            'revolution_slider_code_Canada',
                        'revolution_slider_code_China',
                        'revolution_slider_code_Germany',
                        'revolution_slider_code_Spain',
                        'revolution_slider_code_France',
                        'revolution_slider_code_India',
                        'revolution_slider_code_Italy',
                        'revolution_slider_code_Japan',
                        'revolution_slider_code_UK',
                        'revolution_slider_code_USA',
                    ),
                ),
                array(
                    'column_title' => 'Search Bar',
                    'options' => array(
						'searchbar_top_enable',
                        'searchbar_topdiscount_enable',
                        'searchbar_border_color',
                        'searchbar_text_color',
                        'searchbar_background_color',
                        'searchbar_opacity',
                        'searchbar_image',
                    ),
                ),
                /* array(
                    'column_title' => 'Header Discount Bar',
                    'options' => array(
                        'show_headersidebar',
                    ),
                ), */
                /*array(
                    'column_title' => 'Header Discountbar',
                    'options' => array(
                        'header_discountbar_enable',
                        'header_discountbar_line_on_color',
                        'header_discountbar_line_off_color',
                        'header_discountbar_ball_color',
                        'header_discountbar_display_color',
                        'header_discountbar_display_text_color',
                        'header_discountbar_display_text_family',
                        'header_discountbar_display_text_px',
                        'header_discountbar_display_opacity',
                        'header_discountbar_display_border_color',
                    ),
                ),*/
                array(
                    'column_title' => 'Side Discountbar',
                    'options' => array(
                        'side_discountbar_enable',
                        'side_discountbar_line_on_color',
                        'side_discountbar_line_off_color',
                        'side_discountbar_ball_color',
                        'side_discountbar_display_color',
                        'side_discountbar_display_text',
                        'side_discountbar_display_text_Canada',
                        'side_discountbar_display_text_China',
                        'side_discountbar_display_text_Germany',
                        'side_discountbar_display_text_Spain',
                        'side_discountbar_display_text_France',
                        'side_discountbar_display_text_India',
                        'side_discountbar_display_text_Italy',
                        'side_discountbar_display_text_Japan',
                        'side_discountbar_display_text_UK',
                        'side_discountbar_display_text_USA',
                        'side_discountbar_display_text_color',
                        'side_discountbar_display_text_family',
                        'side_discountbar_display_text_px',
                        'side_discountbar_display_opacity',
                        'side_discountbar_display_border_color',
                    ),
                ),
            ),
            'streamzon_theme_settings_1_section' /*section*/ => array(
                array(
                    'column_title' => 'General',
                    'options' => array(                        
                        'chiz_flag'
                    ),
                ),
                array(
                    'column_title' => 'Landing Page align',
                    'options' => array(
                        'l_page_landing_page_align',
                        '1_page_landing_page_body_size',
                        'landing_page_context_width',

                    ),
                ),
                array(
                    'column_title' => 'Background',
                    'options' => array(
                        'l_page_body_background_color',
                        'l_page_background_video_use',
                        'l_page_background_video_pattern_enable',
                        'l_page_background_video_pattern',
                        'l_page_background_video_id',
                        'l_page_background_video_opacity',
                        'l_page_background_pattern_opacity',
                        
                        'l_page_background_image',
                        'l_page_background_image_use',
			'l_page_background_image_repeat',
                        'l_page_background_opacity',
                    ),
                ),
                array(
                    'column_title' => 'Logo',
                    'options' => array(
                        'l_page_center_logo',
                        'l_page_center_logo_use',
                        'l_page_center_logo_topsapce',
                    ),
                ),
                array(
                    'column_title' => 'Header',
                    'options' => array(
                        'l_page_landing_headertxt_enable',
                        'l_page_header_text',
                        'l_page_header_text_Canada',
                        'l_page_header_text_China',
                        'l_page_header_text_Germany',
                        'l_page_header_text_Spain',
                        'l_page_header_text_France',
                        'l_page_header_text_India',
                        'l_page_header_text_Italy',
                        'l_page_header_text_Japan',
                        'l_page_header_text_UK',
                        'l_page_header_text_USA',
                        'l_page_header_font_family',
                        'l_page_header_text_px',
                        'l_page_header_color',
                        'l_page_header_bold',
                    ),
                ),
                array(
                    'column_title' => 'Discountbar',
                    'options' => array(
                        'l_page_discountbar_enable',
                        'l_page_discountbar_line_on_color',
                        'l_page_discountbar_line_off_color',
                        'l_page_discountbar_ball_color',
                        'l_page_discountbar_display_color',
                        'l_page_discountbar_display_text',
                        'l_page_discountbar_display_text_Canada',
                        'l_page_discountbar_display_text_China',
                        'l_page_discountbar_display_text_Germany',
                        'l_page_discountbar_display_text_Spain',
                        'l_page_discountbar_display_text_France',
                        'l_page_discountbar_display_text_India',
                        'l_page_discountbar_display_text_Italy',
                        'l_page_discountbar_display_text_Japan',
                        'l_page_discountbar_display_text_UK',
                        'l_page_discountbar_display_text_USA',
                        'l_page_discountbar_display_text_color',
                        'l_page_discountbar_display_text_family',
                        'l_page_discountbar_display_text_px',
                        'l_page_discountbar_display_opacity',
                        'l_page_discountbar_display_border_color',
                    ),
                ),
                array(
                    'column_title' => 'Subheader',
                    'options' => array(
                        'l_page_landing_subheading_enable',
                        'l_page_subheader_text',

                        'l_page_subheader_text_Canada',
                        'l_page_subheader_text_China',
                        'l_page_subheader_text_Germany',
                        'l_page_subheader_text_Spain',
                        'l_page_subheader_text_France',
                        'l_page_subheader_text_India',
                        'l_page_subheader_text_Italy',
                        'l_page_subheader_text_Japan',
                        'l_page_subheader_text_UK',
                        'l_page_subheader_text_USA',

                        'l_page_subheader_font_family',
                        'l_page_subheader_font_px',
                        'l_page_subheader_color',
                        'l_page_subheader_bold',
                    ),
                ),
                array(
                    'column_title' => 'Search Bar',
                    'options' => array(
                        'l_page_searchbar_height_px',
                        'l_page_searchbar_toppadding_px',
                        'l_page_searchbar_bottompadding_px',
                        'l_page_product_topmargin_px'
                        
                    ),
                ),
                array(
                    'column_title' => 'Button',
                    'options' => array(
                        'l_page_button_long_short',
                        'l_page_button_search_gap',

                        'l_page_button_text',
                        'l_page_button_text_Canada',
                        'l_page_button_text_China',
                        'l_page_button_text_Germany',
                        'l_page_button_text_Spain',
                        'l_page_button_text_France',
                        'l_page_button_text_India',
                        'l_page_button_text_Italy',
                        'l_page_button_text_Japan',
                        'l_page_button_text_UK',
                        'l_page_button_text_USA',

                        'l_page_button_color',
                        'l_page_button_hover_color',
                        'l_page_button_text_color',
                        'l_page_button_text_opacity_px',
                        'l_page_button_text_border_color',

                    ),
                ),
                array(
                    'column_title' => 'Space of Search Bar',
                    'options' => array(
                        'l_page_searchbar_border_color',
                        'l_page_searchbar_background_color',
                        'l_page_searchbar_text_color',
                        'l_page_searchbar_opacity_px',

                        'l_page_searchbar_text',
                        'l_page_searchbar_text_Canada',
                        'l_page_searchbar_text_China',
                        'l_page_searchbar_text_Germany',
                        'l_page_searchbar_text_Spain',
                        'l_page_searchbar_text_France',
                        'l_page_searchbar_text_India',
                        'l_page_searchbar_text_Italy',
                        'l_page_searchbar_text_Japan',
                        'l_page_searchbar_text_UK',
                        'l_page_searchbar_text_USA',
                    ),
                ),
                array(
                    'column_title' => 'Footer',
                    'options' => array(
                        'l_page_show_footer',
                        'l_page_footer_text',

                        'l_page_footer_text_Canada',
                        'l_page_footer_text_China',
                        'l_page_footer_text_Germany',
                        'l_page_footer_text_Spain',
                        'l_page_footer_text_France',
                        'l_page_footer_text_India',
                        'l_page_footer_text_Italy',
                        'l_page_footer_text_Japan',
                        'l_page_footer_text_UK',
                        'l_page_footer_text_USA',

                        'l_page_footer_text_font_family',
                        'l_page_footer_text_font_px',
                        'l_page_footer_text_color',
                        'l_page_footer_height_px',
			'l_page_footer_opacity',
			'l_page_background_footer',
                    ),
                ),
            ),
        ),
    );

    /**
     * @return array
     */
    public function getAmazonLocales()
    {
        return self::$amazon_locales;
    }

    private $amazon_country_codes = array(
        'ca' => 'amazon.ca',
        'cn' => 'amazon.cn',
        'de' => 'amazon.de',
        'es' => 'amazon.es',
        'fr' => 'amazon.fr',
        'in' => 'amazon.in',
        'it' => 'amazon.it',
        'co.jp' => 'amazon.co.jp',
        'co.uk' => 'amazon.co.uk',
        'com' => 'amazon.com',
    );

    private $amazon_browse_node_ids = array();

    /**
     * Start up
     */
    public function __construct()
    {		
        $this->set_theme_settings_defaults();
		$this->set_amazon_settings_defaults();
		$this->set_amazon_credentials_option_defaults();

        // Set class property        
        //$this->test_settings = get_option('streamzon_test_settings');
		$this->seo_settings = get_option('streamzon_seo_settings_option');
        $this->amazon_stores = get_option('streamzon_amazon_stores_option');
        $this->amazon_item_settings = get_option('streamzon_amazon_item_settings_option');
        if (!isset($this->amazon_item_settings['amazon_item_attributes']) || !is_array($this->amazon_item_settings['amazon_item_attributes'])) {
            $this->amazon_item_settings['amazon_item_attributes'] = array();
        }
		
		$this->amazon_credentials = get_option('streamzon_amazon_credentials_option');
		$this->amazon_settings = get_option('streamzon_amazon_settings_option');
        $this->theme_settings = get_option('streamzon_theme_settings_option');
        if (!$this->theme_settings) {			
            $this->theme_settings = $this->theme_settings_defaults;
            $this->amazon_settings = $this->amazon_settings_defaults;
            $this->amazon_credentials = $this->amazon_credentials_defaults;
            $this->amazon_item_settings['amazon_item_attributes'] = array('amazon_item_Image','amazon_item_Title','amazon_item_ListPrice','amazon_item_YourPrice','amazon_item_Reviews','amazon_item_Brand','amazon_item_PublicationDate');
            update_option('streamzon_theme_settings_option', $this->theme_settings);
            update_option('streamzon_amazon_settings_option', $this->amazon_settings);
            update_option('streamzon_amazon_item_settings_option', $this->amazon_item_settings);
            update_option('streamzon_amazon_credentials_option', $this->amazon_credentials);
            //update_option('streamzon_theme_test_option', $this->test_settings);
            delete_option('presets');
            $presets = array();
            $presets['presets']['Default Template'] = $this->theme_settings;
            update_option('presets',  $presets);
            update_option('preset_name','Default Template');
        }


        $this->amazon_browse_node_ids = $this->trim_amazon_browse_node_ids(parse_ini_file(get_template_directory() . '/configs/amazon_browse_node_ids.ini', true));

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }
	private function set_amazon_credentials_option_defaults()
    {
		$this-> amazon_credentials_defaults['amazon_associate_id']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_ca']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_uk']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_de']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_fr']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_in']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_it']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_es']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_jp']		='';
		$this-> amazon_credentials_defaults['amazon_associate_id_cn']		='';
		$this-> amazon_credentials_defaults['amazon_associate_auto']		='US';
	}
	
	private function set_amazon_settings_defaults()
    {
		$this-> amazon_settings_defaults['amazon_discount']																="1";
        $this-> amazon_settings_defaults['amazon_additional_search_parameter']                                          ="1";
		$this-> amazon_settings_defaults['amazon_addsingPage']                                                          =1;
        $this-> amazon_settings_defaults['show_sidebar']                                                                =0;        
        $this-> amazon_settings_defaults['l_page_landing_page_enable']                                                  =0;                
        $this-> amazon_settings_defaults['l_page_onepage_landing']                                                      =1;                
		$this-> amazon_settings_defaults['amazon_discount_percent']                                                     =30;
		$this-> amazon_settings_defaults['Product_buy_anable']                                                          = 1;
		$this-> amazon_settings_defaults['Product_cart_anable']                                                         = 1;
		$this-> amazon_settings_defaults['Product_buy_background']                                                      = "#1FA6D5";
		$this-> amazon_settings_defaults['Product_buy_background2']                                                     = "#1589B1";
		$this-> amazon_settings_defaults['Product_buy_color']                                                           = "#fff";
		$this-> amazon_settings_defaults['Product_cart_background']                                                     = "#1FA6D5";
		$this-> amazon_settings_defaults['Product_cart_background2']                                                    = "#1589B1";
		$this-> amazon_settings_defaults['Product_cart_color']                                                          = "#fff";
	}
	
    private function set_theme_settings_defaults()
    {
        $this->theme_settings_defaults['top_bar_logo'] 									= get_bloginfo('template_directory')."/img/default/top.png";
        $this->theme_settings_defaults['banner_image_file'] 							= "";
        $this->theme_settings_defaults['box_bar_logo'] 									= get_bloginfo('template_directory')."/img/default/default-logo4.png";
        $this->theme_settings_defaults['search_page_background_image']                                                  = get_bloginfo('template_directory')."/img/default/greyfloral.png";
        $this->theme_settings_defaults['header_menu'] 									= "1";        
        $this->theme_settings_defaults['show_headersidebar']                                                            = "0";
        $this->theme_settings_defaults['top_bar_color'] 								= "#207ac9";
		$this->theme_settings_defaults['search_page_facebooK_pixel_enable']                                             = "0";
		$this->theme_settings_defaults['search_page_facebook_pixel_code']                                               = "";
		$this->theme_settings_defaults['land_page_facebooK_pixel_enable']                                               = "0";
		$this->theme_settings_defaults['land_page_facebook_pixel_code']                                                 = "";
        $this->theme_settings_defaults['search_page_background']                                                        = "#eaeaea";
        $this->theme_settings_defaults['boxes_color'] 									= "#cecece";
        $this->theme_settings_defaults['box_text_color'] 								= "#595959";
		$this->theme_settings_defaults['box_text_url_color'] 								= "#595959";
        $this->theme_settings_defaults['box_bar_background']                                                            = "#207ac9";
        $this->theme_settings_defaults['box_background_text_color']                                                     = "#ccc";
        $this->theme_settings_defaults['top_bar_opacity'] 								= "100";
        $this->theme_settings_defaults['search_page_pattern_opacity']                                                   = "100";
		$this->theme_settings_defaults['search_page_background_opacity']                                                = "84";
        $this->theme_settings_defaults['boxes_opacity'] 								= "43";
        $this->theme_settings_defaults['searchbar_opacity']                                                             = "40";
        $this->theme_settings_defaults['header_text_px'] 								= "15";
        $this->theme_settings_defaults['header_text'] 									= "Header text here and there";
        $this->theme_settings_defaults['header_text_Canada']                                                            = "Header text here and there";
        $this->theme_settings_defaults['header_text_China']                                                             = "在這裡和那裡標題文本";
        $this->theme_settings_defaults['header_text_Germany']                                                           = "Kopftext hier und da";
        $this->theme_settings_defaults['header_text_Spain']                                                             = "Texto de cabecera de aquí y allá";
        $this->theme_settings_defaults['header_text_France']                                                            = "Tête texte ici et là";
        $this->theme_settings_defaults['header_text_India']                                                             = "यहाँ और वहाँ पाठ शीर्ष लेख";
        $this->theme_settings_defaults['header_text_Italy']                                                             = "Intestazione testo qui e là";
        $this->theme_settings_defaults['header_text_Japan']                                                             = "あちこちのテキストヘッダ";
        $this->theme_settings_defaults['header_text_UK'] 								= "Header text here and there";
        $this->theme_settings_defaults['header_text_USA'] 								= "Header text here and there";
        $this->theme_settings_defaults['header_text_color']                                                             = "#ffffff";
        $this->theme_settings_defaults['header_text_bold'] 								= "1";
        $this->theme_settings_defaults['header_text_font_family']                                               	= "Oxygen";
        $this->theme_settings_defaults['banner_image_link']                                                     	= "";
        $this->theme_settings_defaults['banner_code'] 																= "";
        $this->theme_settings_defaults['revolution_slider_code']                                                	= "";
        $this->theme_settings_defaults['searchbar_border_color']                                                	= "#FFFFFF";
        $this->theme_settings_defaults['searchbar_text_color']                                                  	= "#FFFFFF";
        $this->theme_settings_defaults['searchbar_background_color']                                                    = "#c4c4c4";
        $this->theme_settings_defaults['searchbar_image'] 																= "1";
		$this->theme_settings_defaults['slidebar_searchbar_image']                                                      = "1";
		$this->theme_settings_defaults['searchbar_top_enable']                                                  		= "1";
        $this->theme_settings_defaults['searchbar_topdiscount_enable']                                                  = "1";
        $this->theme_settings_defaults['text_link_color'] 																= "#1e73be";
        $this->theme_settings_defaults['sidebar_header_color']                                                  		= "#b2b2b2";
		$this->theme_settings_defaults['page_search_button_color']                                              		= "#7a7a7a";
		$this->theme_settings_defaults['page_search_button_hover_color']                                        		= "#000000";
		$this->theme_settings_defaults['page_search_button_text_color']                                        		 	= "#FFFFFF";
		$this->theme_settings_defaults['page_search_button_text_opacity_px']                                  		  	= "0.35";
		$this->theme_settings_defaults['page_search_button_text_border_color']                                		  	= "#1e73be";
        $this->theme_settings_defaults['top_bar_logo_use'] 																= "1";
        $this->theme_settings_defaults['box_bar_logo_use'] 																= "1";
        $this->theme_settings_defaults['box_big_small_img']                                                             = "3";
        $this->theme_settings_defaults['search_page_background_image_use']                                              = "1";
		$this->theme_settings_defaults['search_page_background_image_repeat']                                           = "1";
		$this->theme_settings_defaults['l_page_background_image_repeat']                                               	= "0";
		$this->theme_settings_defaults['search_page_background_pattern_enable']                                         = "0";
		$this->theme_settings_defaults['search_page_background_pattern']                                                = "dotted-1";
        $this->theme_settings_defaults['header_text_enable']                                                    		= "1";
        $this->theme_settings_defaults['banner_image_use'] 																= "0";
        $this->theme_settings_defaults['banner_code_use'] 																= "1";
        $this->theme_settings_defaults['revolution_slider_use']                                                 		= "1";
        $this->theme_settings_defaults['bitly_login_id'] 																= "watchco";
        $this->theme_settings_defaults['bitly_api_key'] 																= "R_29a1ba784408460e8ec97d7c990695bf";
        $this->theme_settings_defaults['l_page_center_logo']                                                    		= get_bloginfo('template_directory')."/img/default/amazon_top.png";
        $this->theme_settings_defaults['l_page_background_image']                                            	 	  	= get_bloginfo('template_directory')."/img/default/default-photo.jpg";
        $this->theme_settings_defaults['l_page_header_color']                                                		   	= "#ffffff";
        $this->theme_settings_defaults['l_page_header_font_family']                                                     = "Pacifico";
        $this->theme_settings_defaults['l_page_subheader_color']                                                		= "#ffffff";
        $this->theme_settings_defaults['l_page_body_background_color']                                          		= "#FFFFFF";
        $this->theme_settings_defaults['l_page_footer_text_color']                                              		= "#85a2bf";
        $this->theme_settings_defaults['l_page_footer_height_px']                                               		= "65";
        $this->theme_settings_defaults['l_page_show_footer']                                                    		= "1";
        $this->theme_settings_defaults['l_page_background_opacity']                                                     = "100";
		$this->theme_settings_defaults['l_page_background_pattern_opacity']                                     		= "84";
        $this->theme_settings_defaults['l_page_background_video_opacity']                                               = "100";
        $this->theme_settings_defaults['l_page_background_video_id']                                                	= "mYpxMpNKFfs";
        $this->theme_settings_defaults['l_page_footer_text_font_family']                                        		= "Oxygen";
        $this->theme_settings_defaults['l_page_footer_text_font_px']                                                    = "20";
        $this->theme_settings_defaults['l_page_header_bold']                                                            = "0";
        $this->theme_settings_defaults['l_page_subheader_bold']                                                         = "0";
        $this->theme_settings_defaults['l_page_discountbar_enable']                                                     = "1";
        $this->theme_settings_defaults['l_page_discountbar_line_on_color']                                              = "#1e73be";
        $this->theme_settings_defaults['l_page_discountbar_line_off_color']                                             = "#cccccc";
        $this->theme_settings_defaults['l_page_discountbar_ball_color']                                                 = "#1e73be";
        $this->theme_settings_defaults['l_page_discountbar_display_color']                                              = "#000000";
        $this->theme_settings_defaults['l_page_discountbar_display_text_color']                                         = "#ffffff";
        $this->theme_settings_defaults['l_page_discountbar_display_border_color']                                       = "#ffffff";
        $this->theme_settings_defaults['l_page_discountbar_display_text_family']                                        = "Raleway";
        $this->theme_settings_defaults['l_page_discountbar_display_opacity']                                            = "0.5";
        $this->theme_settings_defaults['l_page_discountbar_display_text_px']                                            = "15";
        $this->theme_settings_defaults['header_discountbar_enable']                                                     = "1";
        $this->theme_settings_defaults['header_discountbar_line_on_color']                                              = "#017afd";
        $this->theme_settings_defaults['header_discountbar_line_off_color']                                             = "#ffffff";
        $this->theme_settings_defaults['header_discountbar_ball_color']                                         		= "#017afd";
        $this->theme_settings_defaults['header_discountbar_display_color']                                              = "#017afd";
        $this->theme_settings_defaults['header_discountbar_display_text_color']                                         = "#017afd";
        $this->theme_settings_defaults['header_discountbar_display_border_color']                                       = "#017afd";
        $this->theme_settings_defaults['header_discountbar_display_text_family']                                        = "Raleway";
        $this->theme_settings_defaults['header_discountbar_display_opacity']                                            = "0.5";
        $this->theme_settings_defaults['header_discountbar_display_text_px']                                            = "18";
        $this->theme_settings_defaults['side_discountbar_enable']                                                       = "1";
        $this->theme_settings_defaults['side_discountbar_line_on_color']                                                = "#207ac9";
        $this->theme_settings_defaults['side_discountbar_line_off_color']                                               = "#d8d8d8";
        $this->theme_settings_defaults['side_discountbar_ball_color']                                                   = "#207ac9";
        $this->theme_settings_defaults['side_discountbar_display_color']                                                = "#017afd";
        $this->theme_settings_defaults['side_discountbar_display_text_color']                                           = "#ffffff";
        $this->theme_settings_defaults['side_discountbar_display_border_color']                                         = "#ffffff";
        $this->theme_settings_defaults['side_discountbar_display_text_family']                                          = "Montserrat";
        $this->theme_settings_defaults['side_discountbar_display_opacity']                                              = "0.2";
        $this->theme_settings_defaults['side_discountbar_display_text_px']                                              = "15";
        $this->theme_settings_defaults['l_page_subheader_font_family']                                                  = "Raleway";
        $this->theme_settings_defaults['l_page_discountbar_display_text_px']                                            = "12";
        $this->theme_settings_defaults['l_page_subheader_font_px']                                                      = "40";
        $this->theme_settings_defaults['l_page_discountbar_display_opacity']                                            = "0.100000";
        $this->theme_settings_defaults['l_page_button_long_short']                                                      = "1";
        $this->theme_settings_defaults['l_page_button_search_gap']                                                      = "1";
        $this->theme_settings_defaults['l_page_button_color']                                                           = "#1e73be";
        $this->theme_settings_defaults['l_page_button_hover_color']                                                     = "#1e73be";
        $this->theme_settings_defaults['l_page_button_text_color']                                                      = "#ffffff";
        $this->theme_settings_defaults['l_page_button_text_opacity_px']                                                 = "0.350000";
        $this->theme_settings_defaults['l_page_button_text_border_color']                                               = "#ffffff";
        $this->theme_settings_defaults['l_page_searchbar_border_color']                                                 = "#FFFFFF";
        $this->theme_settings_defaults['l_page_searchbar_background_color']                                             = "#000000";
        $this->theme_settings_defaults['l_page_searchbar_text_color']                                                   = "#FFFFFF";
        $this->theme_settings_defaults['l_page_header_text']                                                            = "Welcome!";
        $this->theme_settings_defaults['l_page_header_text_Canada']                                                     = "Welcome!";
        $this->theme_settings_defaults['l_page_header_text_China']                                                      = "歡迎！";
        $this->theme_settings_defaults['l_page_header_text_Germany']                                                    = "Herzlich Willkommen!";
        $this->theme_settings_defaults['l_page_header_text_Spain']                                                      = "¡Bienvenido!";
        $this->theme_settings_defaults['l_page_header_text_France']                                                     = "Bienvenue!";
        $this->theme_settings_defaults['l_page_header_text_India']                                                      = "स्वागत!!";
        $this->theme_settings_defaults['l_page_header_text_Italy']                                                      = "Benvenuto!";
        $this->theme_settings_defaults['l_page_header_text_Japan']                                                      = "ようこそ！";
        $this->theme_settings_defaults['l_page_header_text_UK']                                                         = "Welcome!";
        $this->theme_settings_defaults['l_page_header_text_USA']                                                        = "Welcome!";
        $this->theme_settings_defaults['l_page_subheader_text_Canada']                                                  = "Get the Best Deals";
        $this->theme_settings_defaults['l_page_subheader_text_China']                                                   = "獲得最優惠的價格";
        $this->theme_settings_defaults['l_page_subheader_text_Germany']                                                 = "Holen Sie sich die besten Angebote";
        $this->theme_settings_defaults['l_page_subheader_text_Spain']                                                   = "Reciba las mejores ofertas";
        $this->theme_settings_defaults['l_page_subheader_text_France']                                                  = "Obtenez les meilleures offres";
        $this->theme_settings_defaults['l_page_subheader_text_India']                                                   = "सबसे अच्छा सौदा मिल";
        $this->theme_settings_defaults['l_page_subheader_text_Italy']                                                   = "Ricevi le migliori offerte";
        $this->theme_settings_defaults['l_page_subheader_text_Japan']                                                   = "お買い得情報を取得します";
        $this->theme_settings_defaults['l_page_subheader_text_UK']                                                      = "Get the Best Deals";
        $this->theme_settings_defaults['l_page_subheader_text_USA']                                                     = "Get the Best Deals";
		$this->theme_settings_defaults['revolution_slider_code_Canada']                                                 = "";
        $this->theme_settings_defaults['revolution_slider_code_China']                                                  = "";
        $this->theme_settings_defaults['revolution_slider_code_Germany']                                                = "";
        $this->theme_settings_defaults['revolution_slider_code_Spain']                                                  = "";
        $this->theme_settings_defaults['revolution_slider_code_France']                                                 = "";
        $this->theme_settings_defaults['revolution_slider_code_India']                                                  = "";
        $this->theme_settings_defaults['revolution_slider_code_Italy']                                                  = "";
        $this->theme_settings_defaults['revolution_slider_code_Japan']                                                  = "";
        $this->theme_settings_defaults['revolution_slider_code_UK']                                             		= "";
        $this->theme_settings_defaults['revolution_slider_code_USA']                                                    = "";

        $this->theme_settings_defaults['l_page_footer_text']                                                            = "Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_Canada']                                                     = "Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_China']                                                      = "版權所有2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_Germany']                                                    = "German Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_Spain']                                                      = "Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_France']                                                     = "Droit d'auteur 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_India']                                                      = "कॉपीराइट 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_Italy']                                                      = "Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_Japan']                                                      = "著作権2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_UK']                                                         = "Copyright 2015 StreamViralStore.";
        $this->theme_settings_defaults['l_page_footer_text_USA']                                                        = "Copyright 2015 StreamViralStore.";

        $this->theme_settings_defaults['l_page_searchbar_text']                                                         = "Search Store";
        $this->theme_settings_defaults['l_page_searchbar_text_Canada']                                                  = "Search Store";
        $this->theme_settings_defaults['l_page_searchbar_text_China']                                                   = "搜索店鋪";
        $this->theme_settings_defaults['l_page_searchbar_text_Germany']                                                 = "Suche";
        $this->theme_settings_defaults['l_page_searchbar_text_Spain']                                                   = "Buscar";
        $this->theme_settings_defaults['l_page_searchbar_text_France']                                                  = "Recherche";
        $this->theme_settings_defaults['l_page_searchbar_text_India']                                                   = "खोज";
        $this->theme_settings_defaults['l_page_searchbar_text_Italy']                                                   = "Ricerca";
        $this->theme_settings_defaults['l_page_searchbar_text_Japan']                                                   = "検索ストア";
        $this->theme_settings_defaults['l_page_searchbar_text_UK']                                                      = "Search Store";
        $this->theme_settings_defaults['l_page_searchbar_text_USA']                                                     = "Search Store";

        $this->theme_settings_defaults['l_page_discountbar_display_text']                                               = "Discount:";
        $this->theme_settings_defaults['l_page_discountbar_display_text_Canada']                                        = "Discount: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_China']                                         = "折扣： ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_Germany']                                       = "Rabatt: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_Spain']                                         = "Descuento: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_France']                                        = "Remise: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_India']                                         = "डिस्काउंट: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_Italy']                                         = "Sconto: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_Japan']                                         = "ディスカウント： ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_UK']                                            = "Discount: ";
        $this->theme_settings_defaults['l_page_discountbar_display_text_USA']                                           = "Discount: ";

        $this->theme_settings_defaults['side_discountbar_display_text']                                                 = "Discount";
        $this->theme_settings_defaults['side_discountbar_display_text_Canada']                                          = "Discount";
        $this->theme_settings_defaults['side_discountbar_display_text_China']                                           = "折扣";
        $this->theme_settings_defaults['side_discountbar_display_text_Germany']                                         = "Rabatt";
        $this->theme_settings_defaults['side_discountbar_display_text_Spain']                                           = "Descuento";
        $this->theme_settings_defaults['side_discountbar_display_text_France']                                          = "Remise";
        $this->theme_settings_defaults['side_discountbar_display_text_India']                                           = "डिस्काउंट";
        $this->theme_settings_defaults['side_discountbar_display_text_Italy']                                           = "Sconto";
        $this->theme_settings_defaults['side_discountbar_display_text_Japan']                                           = "ディスカウント";
        $this->theme_settings_defaults['side_discountbar_display_text_UK']                                              = "Discount";
        $this->theme_settings_defaults['side_discountbar_display_text_USA']                                             = "Discount";

        $this->theme_settings_defaults['l_page_button_text']                                                            = "Find";
		$this->theme_settings_defaults['l_page_button_text_Canada']                                                     = "Find";
		$this->theme_settings_defaults['l_page_button_text_China']                                                      = "發現";
		$this->theme_settings_defaults['l_page_button_text_Germany']                                                    = "Finden";
		$this->theme_settings_defaults['l_page_button_text_Spain']                                                      = "Encontrar";
		$this->theme_settings_defaults['l_page_button_text_France']                                                     = "Trouver";
		$this->theme_settings_defaults['l_page_button_text_India']                                                      = "खोज";
		$this->theme_settings_defaults['l_page_button_text_Italy']                                                      = "Trovare";
		$this->theme_settings_defaults['l_page_button_text_Japan']                                                      = "見つけます";
		$this->theme_settings_defaults['l_page_button_text_UK']                                                         = "Find";
		$this->theme_settings_defaults['l_page_button_text_USA']                                                        = "Find";
		
        $this->theme_settings_defaults['l_page_subheader_text']                                                         = "Get the Best Deals";
        $this->theme_settings_defaults['l_page_footer_text']                                                            = "copyright � 2013 Hello Inc.";
        $this->theme_settings_defaults['l_page_header_text_px']                                                         = "140";
        $this->theme_settings_defaults['l_page_searchbar_height_px']                                                    = "50";
        $this->theme_settings_defaults['l_page_searchbar_toppadding_px']                                                = "1";
        $this->theme_settings_defaults['l_page_product_topmargin_px']                                                   = "1";
        $this->theme_settings_defaults['l_page_searchbar_bottompadding_px']                                             = "1";
        $this->theme_settings_defaults['l_page_searchbar_opacity_px']                                                   = "0.00000";
        $this->theme_settings_defaults['l_page_subheader_text_px']                                                      = "23";
        $this->theme_settings_defaults['l_page_center_logo_use']                                                        = "1";
        $this->theme_settings_defaults['l_page_center_logo_topsapce']                                                   = "25";
        $this->theme_settings_defaults['l_page_background_image_use']                                                   = "1";
        $this->theme_settings_defaults['chiz_flag']                                                                     = "1";
        $this->theme_settings_defaults['l_page_landing_page_align']                                                     = "2";
        $this->theme_settings_defaults['1_page_landing_page_body_size']                                                 = "20";
        $this->theme_settings_defaults['landing_page_context_width']                                                	= "200";
        $this->theme_settings_defaults['l_page_landing_headertxt_enable']                                               = "1";
        $this->theme_settings_defaults['l_page_landing_subheading_enable']                                              = "1";
        
        $this->theme_settings_defaults['l_page_background_video_use']                                                   = "0";
        $this->theme_settings_defaults['l_page_background_video_pattern_enable']                                        = "1";
        $this->theme_settings_defaults['l_page_background_video_pattern']                                               = "dotted-1";
        $this->theme_settings_defaults['l_page_preset'] 																= "Default Template";
        $this->theme_settings_defaults['l_page_preset_thumbnail']                                                       = "dotted-1";
        $this->theme_settings_defaults['l_page_choose_settings']                                                        = "default";
        $this->theme_settings_defaults['l_page_choose_preset_name']                                                     = "Default Template";
		$this->theme_settings_defaults['l_page_footer_opacity']                                                         = "39";
		$this->theme_settings_defaults['l_page_background_footer']                                                      = "#000";
		
		$this-> theme_settings_defaults['buyitnow_button_background']                                                   = "#1f9bc1";
		$this-> theme_settings_defaults['buyitnow_button_background2']													= "#36a1c1";
		$this-> theme_settings_defaults['buyitnow_button_color']														= "#fff";
		$this-> theme_settings_defaults['add_to_cart_button_background']                                                = "#1e73be";
		$this-> theme_settings_defaults['add_to_cart_button_background2']                                               = "#3d82bf";
		$this-> theme_settings_defaults['add_to_cart_button_color']                                                     = "#fff";
		$this-> theme_settings_defaults['small_cart_button_background']                                                 = "#848484";
		$this-> theme_settings_defaults['small_cart_button_color']                                                      = "#fff";
		
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {   

        // a new top-level menu page
        add_menu_page(
            'Stream Store',          // The text to be displayed in the browser title bar
            'Stream Store',                  // The text to be used for the menu
            'manage_options',               // The required capability of users to access this menu
            'streamzon-theme-settings',         // The slug by which this menu item is accessible
            array( $this, 'dashboard_page' ) // The name of the function used to display the page content
        );
        //first submenu
        add_submenu_page(
            'streamzon-theme-settings',         // The slug for the parent menu page to which this sub menu belongs
            'Setup Wizard',               // The text that's rendered in the browser title bar
            'Setup Wizard',               // The text to be rendered in the menu
            'manage_options',               // The capability required to access this menu item
            'streamzon-theme-settings',             // The slug by which this sub menu is identified
            array( $this, 'dashboard_page' ) // The function used to display this options for this menu's page
        ); 

        //main
        add_submenu_page(
            'streamzon-theme-settings',         // The slug for the parent menu page to which this sub menu belongs
            'Theme Options',               // The text that's rendered in the browser title bar
            'Theme Options',               // The text to be rendered in the menu
            'manage_options',               // The capability required to access this menu item
            'streamzon-theme-options',             // The slug by which this sub menu is identified
            array( $this, 'create_admin_page' ) // The function used to display this options for this menu's page
        ); 

        add_submenu_page(
            'streamzon-theme-settings',         // The slug for the parent menu page to which this sub menu belongs
            'Preferences',              // The text that's rendered in the browser title bar
            'Preferences',              // The text to be rendered in the menu
            'manage_options',               // The capability required to access this menu item
            'streamzon-amazon-settings',                // The slug by which this sub menu is identified
            array( $this, 'amazon_settings_admin_page' )    // The function used to display this options for this menu's page
        );

        add_submenu_page(
            'streamzon-theme-settings',         // The slug for the parent menu page to which this sub menu belongs
            'SEO Settings',                // The text that's rendered in the browser title bar
            'SEO Settings',                // The text to be rendered in the menu
            'manage_options',               // The capability required to access this menu item
            'streamzon-seo',                // The slug by which this sub menu is identified
            array( $this, 'streamzon_seo_admin_page' )  // The function used to display this options for this menu's page
        );        

        add_submenu_page(
            'streamzon-theme-settings',			// The slug for the parent menu page to which this sub menu belongs
            'Amazon Credentials',				// The text that's rendered in the browser title bar
            'Amazon Credentials',				// The text to be rendered in the menu
            'manage_options',				// The capability required to access this menu item
            'streamzon-amazon-credentials',				// The slug by which this sub menu is identified
            array( $this, 'amazon_credentials_admin_page' )	// The function used to display this options for this menu's page
        );

        add_submenu_page(
            'streamzon-theme-settings',         // The slug for the parent menu page to which this sub menu belongs
            'Amazon Categories',                // The text that's rendered in the browser title bar
            'Amazon Categories',                // The text to be rendered in the menu
            'manage_options',               // The capability required to access this menu item
            'streamzon-amazon-stores',              // The slug by which this sub menu is identified
            array( $this, 'amazon_stores_admin_page' )  // The function used to display this options for this menu's page
        );


        add_submenu_page(
            'streamzon-theme-settings',			// The slug for the parent menu page to which this sub menu belongs
            'Amazon Item',				// The text that's rendered in the browser title bar
            'Amazon Item',				// The text to be rendered in the menu
            'manage_options',				// The capability required to access this menu item
            'streamzon-amazon-item-settings',				// The slug by which this sub menu is identified
            array( $this, 'amazon_item_settings_admin_page' )	// The function used to display this options for this menu's page
        );



        add_submenu_page(
            'streamzon-theme-settings',			// The slug for the parent menu page to which this sub menu belongs
            'Analytics',				// The text that's rendered in the browser title bar
            'Analytics',				// The text to be rendered in the menu
            'manage_options',				// The capability required to access this menu item
            'streamzon-amazon-analytics',				// The slug by which this sub menu is identified
            array( $this, 'amazon_analytics_admin_page' )	// The function used to display this options for this menu's page
        );

    }

    /**
     * Options page callback
     */
    public function dashboard_page(){

        if(!fx_testForLicensePro()){return;}

        wp_enqueue_script( 'jquery-form' );?>
        
        <style>
            .flag{
                float: left;
                padding: 1px 2px;
                width: 24px;
            }
            p.submit {
                clear: both;
                margin-top: 20px;
                max-width: 100%;
                padding-top: 10px;
                text-align: left;
            }
            .streamzon-setting-field {
				min-height:0!important;
			}
            p.submit {
                text-align: right;                
            }
            input#submit, #finish, .go_back {
                background: #4d4d4d none repeat scroll 0 0;
                border: 1px solid #333;
                border-radius: 0;
                box-shadow: 0 0 4px #333 inset;
                color: #fff;
                height: auto;
                padding: 0.3em 2em;
            }
            .go_back {
                clear: both;
                float: left;
                font-size: 1.1em;
                position: relative;
                text-decoration: none;
                top: 55px;
            }
            #finish {
                font-size: 1.1em;
                position: relative;
                top: 55px;
                float: right;
                display: none;
                margin-bottom: 22px;
            }
            .streamzon-setting-section {
                visibility: hidden;
                display: block;
            }
            .streamzon-setting-column {
                visibility: hidden;
                display: block!important;
            }
            #import-file {
                display: block;
                float: left;
                margin: 0 4px 0 0;
                visibility: hidden;
            }
            #import_settings {
                display: block;
                visibility: hidden;
            }

            /* presets */
            .wp-color-result{
                height:21px;
            }
            .preset_pics{
                width:250px;
                height: 207px;
                line-height: 232px;
                display:inline-table;
                margin:2px;
                border:solid 3px #fff;
                position:relative;
                vertical-align: top;
            }
            .pics_block{
                height: 180px;
                line-height: 200px;
                overflow: hidden;
                border: 1px solid #ddd;                            
            }
            .onlinepress{
                background:#01c5b5 !important;
            }
            .loadpress{
                background:#1e73be !important;
                -webkit-box-shadow: none!important;
                box-shadow: none!important;
                text-shadow: none!important;
            }
            .loadpress:hover {
                background:#105089 !important;
            }
            .pics_block:hover{
                border:solid 3px #01c5b5;
                box-sizing:border-box;
                -moz-box-sizing:border-box;
                -webkit-box-sizing:border-box;
            }
            .pics_block img {
                -moz-transition: all 1s ease-out;
                -o-transition: all 1s ease-out;
                -webkit-transition: all 1s ease-out;
            }
                 
            .pics_block img:hover{
                 -webkit-transform: scale(1.2);
                 -moz-transform: scale(1.2);
                 -o-transform: scale(1.2);
             }
            .name_block{
                width:100%;
                max-width:266px;
                text-align:center;
                background:#1e73be;
                height:27px;
                color:#fff;
                text-decoration:none;
                display:inline-block;
                overflow:hidden;
                margin:-5px 0 0 0;                
            }

            .focus_preset{
                border:solid 3px #01c5b5;
                box-sizing:border-box;
            }
            .closebtn{
                position:absolute;
                top: 4px;
                right: 4px;
                width: 20px;
                height: 20px;
                z-index: 100;

            }
            .pics_block:hover .closebtn a{
                position:absolute;
                top: 0px;
                right: 0px;
                width: 20px;
                height: 20px;
                z-index: 100;
                background: url(<?php print get_stylesheet_directory_uri()."/img/close_icon.png";?>) no-repeat center;
            }
            .preset_pics preset_hover input{
                margin-left:5px;
            }
            .wp-core-ui .button-disabled, .wp-core-ui .button-secondary.disabled, .wp-core-ui .button-secondary:disabled, .wp-core-ui .button-secondary[disabled], .wp-core-ui .button.disabled, .wp-core-ui .button:disabled, .wp-core-ui .button[disabled]{
                color: #FFF !important; 
                background:#01c5b5 !important;
                -webkit-box-shadow: none!important;
                box-shadow: none!important;
                text-shadow: none!important;
            }

            /* steps */
            .steps {                
                width: 100%;
                position: relative;            
            }
            .steps > li {
                color: #FFF;
                display: inline-block;
                background: #626466 none repeat scroll 0% 0%;
                margin: 0px 2px 3vw 0px;
                position: relative;
                padding: 1em 2em 1em 2.9em;
                width: calc(100% / 4 - 69px);
                min-width: 113px;
            }
            .steps > li.current {
                background: rgb(1, 197, 181);
            }
            .steps > li > em {
                font-size: 4em;
                position: absolute;
                left: 0px;
                font-style: normal;
                font-family: open sans;
                font-weight: 900;
            }
            .steps > li > i { 
                display: none;
            }
            .steps > li.current > i {
                display: block;
                position: absolute;
                left: 41%;
                border-width: 12px 19px;
                border-style: solid;
                border-color: rgb(1, 197, 181) transparent transparent;
                top: 100%;
            }
            #wizard > h3 {
                display: none;
            }
            .streamzon-setting-fieldset.block_1 {
                height: 217px;  
            }

        </style>
        <script>
            jQuery(document).ready(function($){
                //slide 1 select autocomplete zone
                
                //check page
                var part = $("#page_part").val();
                var inp = $("input[name=_wp_http_referer]");
                var curr_part = inp.val().indexOf("part=");
                var next_part = parseInt(part)+1;
                if(curr_part == -1 ){                    
                    inp.val( inp.val()+ "&part="+next_part);
                }else{                    
                    inp.val( inp.val().replace("part="+part,"part="+next_part ));
                    inp.val( inp.val().replace("del=","bell="));
                }

                //change step-item
                $(".steps li").removeClass("current");
                $("li.p"+part).addClass("current");
                

                $("div.streamzon-setting-column").hide();

                $("#ajax-import").hide();

                //hide empty markets
                $("div.streamzon-setting-column:has(div:contains('Please enter associate-id'))").hide(0);
                
                //hide extra fields 
                if(part == 1  ) {//page1
                    $("div.streamzon-setting-column:has(h4:contains('Your Amazon Credentials'))").addClass("isotope").css({"visibility":"visible"});
                    $("div.streamzon-setting-column:has(h4:contains('Amazon Markets IDs'))").addClass("isotope").css({"visibility":"visible"});
                    $("div.streamzon-setting-column:has(h4:contains('Default Market'))").addClass("isotope").css({"visibility":"visible"});                   
                }
                if(part == 2) {//page2

                    $("div.streamzon-setting-column:has(h4:contains('Store Keyword'))").addClass("isotope").css({"visibility":"visible"});
                    $("div.streamzon-setting-column:has(h4:contains('Default Discount'))").addClass("isotope").css({"visibility":"visible"});
                }
                if(part == 3) {//page3
                    $("div.streamzon-setting-column:has(label:contains('Categories to show'))").addClass("isotope").css({"visibility":"visible"});//show work market containers
                }
                if(part == 4) {//page4                                
                    
                    $("div.streamzon-setting-column:has(h4:contains('Pages'))").addClass("isotope").css({"visibility":"visible"});
                    $("div.streamzon-setting-column:has(h4:contains('Default Template'))").addClass("isotope").css({"visibility":"visible"});
                    $("#import-file, #import_settings, #presets .streamzon-setting-column").addClass("isotope").css({"visibility":"visible"});
                    $(".submit").hide();
                    $("#finish, #ajax-import").show();                    

                }
                $('.streamzon-setting-column:not(.isotope)').remove();
                //initial isotope
                $('.streamzon-setting-section').isotope({ itemSelector: '.isotope'});
                $('.streamzon-setting-section').isotope('layout');            
                

                
                //presets
                $('div[name="outer_block"]').click(function(event) {                    
                    $(".pics_block").removeClass('focus_preset');
                    $(this).addClass('focus_preset');
                });
                $('div[name="outer_blocks"]').click(function(event) {
                    var data_id =   $(this).attr("data-id")
                    $("#streamzon_l_page_choose_settings").val($(this).attr("data-id"));

                });
                $(".aclosebtn").click(function() {
                    if(!confirm("Are you sure you want to delete this Preset?") ) {
                        return false;
                    }
                });


            });
        </script>
        <? wp_enqueue_script('streamzon_custom_script', THEMEROOT . '/js/main.js', array(), true);
            wp_localize_script('streamzon_custom_script', 'streamzon_object', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'search_query' => $_REQUEST['s'],
                'search_query_discount' => $_REQUEST['disc_val'],
                'a_cat' => $_REQUEST['a_cat'],
            ));
        ?>
        <div class="wrap">
            <div class="options-page-logo"></div>
            <hr class="options-page-hr" />
            <h2>Setup Wizard</h2>
			<ul class="steps">
                <li class="p1 current"><em>1</em><span>Amazon Settings</span><i></i></li>
                <li class="p2"><em>2</em><span>Prefences</span><i></i></li>
                <li class="p3"><em>3</em><span>Categories</span><i></i></li>
                <li class="p4"><em>4</em><span>Templates</span><i></i></li>
            </ul>
            <?php settings_errors(); ?>
            
            <? if($_GET['part'] < 4):?>
            <form id="wizard" method="post" action="options.php" enctype="multipart/form-data" >
            <?endif;?>
                <?php           
                     
                    if( isset($_GET['part']) && intval($_GET['part']) < 5  && !isset($_GET['del']))  {                        
                        echo "<input type='hidden' name='part' id='page_part' value='".$_GET['part']. "'>";
                    } else if (isset($_GET['del']) || intval($_GET['part']) > 4){
                        echo "<input type='hidden' name='part' id='page_part' value='4'>";
                    } else {
                        echo "<input type='hidden' name='part' id='page_part' value='1'>";                        
                    }
                    
                    if( !isset($_GET['part']) ) {
                        settings_fields('streamzon_amazon_credentials_option_group');//1
                        $back = "false";
                    }
                    if( isset($_GET['part']) && $_GET['part'] == 2 ) {
                        $saso = get_option('streamzon_amazon_settings_option');
                        
                        settings_fields('streamzon_amazon_settings_option_group'); //2    
                        ?><input type="hidden" name="streamzon_amazon_settings_option[Product_cart_anable]" id="streamzon_Product_cart_anable" value="<?=$this->amazon_settings['Product_cart_anable'];?>">
                        <input type="hidden" name="streamzon_amazon_settings_option[Product_buy_anable]" id="streamzon_Product_buy_anable" value="<?=$this->amazon_settings['Product_buy_anable'];?>"><?
                        $back = "";
                    }
                    if( isset($_GET['part']) && $_GET['part'] == 3 ) {
                        settings_fields('streamzon_amazon_stores_option_group');//3
                        $back = "&part=2";
                    }
                    
                    

                    $this->custom_do_settings_sections_dashboard('streamzon-theme-test');


                    //del presets
                    $preset_array   = (array)get_option('presets');
                    $current_preset = get_option('preset_name');
                    $current_theme  =  get_option('preset_name');
                    if(isset($_GET['del']) && !isset($_REQUEST['settings-updated']))
                    {      
                        //var_dump($_REQUEST);
                        if($_GET['del'] != $current_preset) {
                            $del = $_GET['del'];
                           
                            $preset_keys = @array_keys($preset_array['presets']);

                            if (in_array($del, $preset_keys)) {
                                unset($preset_array['presets'][$del]);
                                update_option('presets', $preset_array);
                                $preset_array = (array)get_option('presets');
                                $preset_keys = @array_keys($preset_array['presets']);

                            }
                        }  

                    }
                    

                    //presets
                    if( is_array( $preset_array['presets'] ) ) {
                        ?><div class="streamzon-setting-section" id="presets" style="position: relative;  display: block;">
                        
                        <? if(isset($_GET['part']) && $_GET['part'] > 3):?>
                        <form id="page_ajax" method="post" action="options.php" enctype="multipart/form-data" >
                        <?endif;?>

                        <?if( isset($_GET['part']) && $_GET['part'] > 3 ) :
                        settings_fields('streamzon_amazon_settings_option_group');                        
                        $back = "&part=3";
                        ?>
                        <input type="hidden" name="streamzon_amazon_settings_option[Product_cart_anable]" id="streamzon_Product_cart_anable" value="<?=$this->amazon_settings['Product_cart_anable'];?>">
                        <input type="hidden" name="streamzon_amazon_settings_option[Product_buy_anable]" id="streamzon_Product_buy_anable" value="<?=$this->amazon_settings['Product_buy_anable'];?>">
                        <input type="hidden" id="AmazoSearchByText"  value="1" name="streamzon_amazon_settings_option[amazon_additional_search_parameter]">
                        <input type="hidden" id="default_search_keyword"  value="<?=$this->amazon_settings['default_search_keyword'];?>" name="streamzon_amazon_settings_option[default_search_keyword]">
                        <input type="hidden" id="amazon_discount_percent"  value="<?=$this->amazon_settings['amazon_discount_percent'];?>" name="streamzon_amazon_settings_option[amazon_discount_percent]">
                        <input type="hidden" id="amazon_paid_free"  value="<?=$this->amazon_settings['amazon_paid_free'];?>" name="streamzon_amazon_settings_option[amazon_paid_free]">
                        
                        <?endif;?>
                        <input type="hidden" name="sumbitRequestUrl" value="<? menu_page_url('streamzon-theme-settings',1);?>">
                        <div  class="streamzon-setting-column isotope">
                            <h4>Pages </h4>
                            <div class="streamzon-setting-fieldset block_1">
                                <div class="streamzon-setting-field">        
                                    <fieldset>
                                        <legend class="screen-reader-text"><span>Individual Page for each product</span></legend>
                                        <label for="streamzon_amazon_addsingPage">
                                            <input type="checkbox" <?php checked('1', $this->amazon_settings['amazon_addsingPage']); ?> value="1" id="streamzon_amazon_addsingPage" name="streamzon_amazon_settings_option[amazon_addsingPage]">
                                            Enable individual page
                                        </label>
                                        <p>Enable Details page to see more details about each product </p>
                                    </fieldset>
                                </div>
                                <div class="streamzon-setting-field">        
                                    <fieldset>
                                        <legend class="screen-reader-text"><span>Show Sidebar</span></legend>
                                        <label for="streamzon_show_sidebar">
                                            <input type="checkbox" <?php checked('1', isset($this->amazon_settings['show_sidebar']) ? $this->amazon_settings['show_sidebar'] : ''); ?> value="1" id="streamzon_show_sidebar" name="streamzon_amazon_settings_option[show_sidebar]">
                                            Show Sidebar
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="streamzon-setting-field">        
                                    <fieldset>
                                        <legend class="screen-reader-text"><span>Enable Landing Page</span></legend>
                                        <label for="streamzon_l_page_landing_page_enable">
                                            <input type="checkbox" <?php checked('1', isset($this->amazon_settings['l_page_landing_page_enable']) ? $this->amazon_settings['l_page_landing_page_enable'] : ''); ?> value="1" id="streamzon_l_page_landing_page_enable" name="streamzon_amazon_settings_option[l_page_landing_page_enable]">
                                            Enable Landing Page
                                        </label>
                                    </fieldset>
                                </div>

                                <div class="streamzon-setting-field">        
                                    <fieldset>
                                        <legend class="screen-reader-text"><span>Enable Onepage Landing Page</span></legend>
                                        <label for="streamzon_l_page_onepage_landing">
                                            <input type="checkbox" <?php checked('1', isset($this->amazon_settings['l_page_onepage_landing']) ? $this->amazon_settings['l_page_onepage_landing'] : ''); ?> value="1" id="streamzon_l_page_onepage_landing" name="streamzon_amazon_settings_option[l_page_onepage_landing]">
                                            Enable Onepage Landing Page
                                        </label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <? if(isset($_GET['part']) && $_GET['part'] > 3):?>
                        </form>
                        <?endif;?>

                        <? if(isset($_GET['part']) && $_GET['part'] > 3):?>
                        <form id="wizard" method="post" action="options.php" enctype="multipart/form-data" >
                        <?endif;?>

                        <?if( isset($_GET['part']) && $_GET['part'] > 3 ) {                        
                        settings_fields('streamzon_theme_settings_option_group');                        
                        }?>

                        <?
                        foreach($preset_array['presets'] as $preset) {
                            if($preset['l_page_preset'] != '' || $preset['l_page_preset']=='default') {
                                if($current_theme == $preset['l_page_preset']){ 
                                    $style = "onlinepress";$present = "Online";$btndisable='disabled';
                                } else {    
                                    $style = "loadpress";$present = "Load Template";$btndisable='';
                                }
                                $pre_name   =   $preset['l_page_preset'];
                                $img_attb   =   wp_get_attachment_image_src($preset['l_page_preset_thumbnail'], 'large');
                                $img_path   =   $img_attb[0];
                                
                                if(!$img_attb) $img_path = $preset['l_page_preset_thumbnail'];
                                $img_path = $img_path == "dotted-1" ? get_stylesheet_directory_uri()."/img/default.png" : $img_path;

                                if(!$img_path)  $img_path   =   get_stylesheet_directory_uri()."/img/image-not-available-150x150.jpg";

                            ?>
                                <div class="streamzon-setting-column">
                                    <h4><?=$pre_name;?></h4>
                                    <div class="streamzon-setting-fieldset" style="text-align:center;">                                
                                        <div class="preset_pics preset_hover" style="width=100%; text-align:left;" name="outer_blocks" data-id="<?=$pre_name;?>">
                                            <div class="pics_block" style="background: url(<?=$img_path;?>) no-repeat center / cover;" name="outer_block" data-id="<?php print $pre_name;?>">
                                                <!--<img src="<?=$img_path;?>" width="200px" class="fff">-->
                                                
                                                <span class="closebtn">
                                                    <a class="aclosebtn" href="<?=get_site_url();?>/wp-admin/admin.php?page=streamzon-theme-settings&part=4&del=<?=$pre_name;?>"></a>
                                                </span>
                                            </div>
                                            <div class="name_block" >
                                                <input id="load_settings" class="button button-secondary <?=$style; ?>" style="-webkit-box-shadow:0; color:#fff; width:100%; height:100%;" type="submit" value="<?=$present;?>" name="load_settings" <?=$btndisable; ?>>
                                            </div>
                                        </div>                                                                     
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?></div><?
                    }

                    ?>
                    <p class="progressbar"><i>Wait until the operation is complete</i><span></span></p>
                    <br>
                    <input type='hidden' name="streamzon_theme_settings_option[l_page_choose_settings]" id="streamzon_l_page_choose_settings">
                    <input style="border: 1px solid #ccc; cursor:pointer; padding: 2px 0; margin:0;" type="file" name="import_file" id="import-file">
                    <span id="ajax-import" class="button button-secondary">Import templates</span>
                    <? if($back !='false'): ?><a href="<?=menu_page_url('streamzon-theme-settings',0). $back;?>" class="go_back"> < Prev</a> <? endif;?>
                    <a href="<? menu_page_url('streamzon-theme-settings',1);?>" id="finish">Finish</a>
                    <?
					
                    
                    submit_button("Next&nbsp;&nbsp;>");
                ?>
            </form>
        </div>
    <?
    }



    public function create_admin_page()
    {

        if(!fx_testForLicensePro()){return;}
        ?>
		<style>
			.wp-color-result{
				height:21px;
			}
			.preset_pics{
				width:250px;
				height: 207px;
				line-height: 232px;
				display:inline-table;
				margin:2px;
				border:solid 3px #fff;
				position:relative;
				vertical-align: top;
			}
			.pics_block{
				height: 180px;
				line-height: 200px;
				overflow: hidden;
				border: 1px solid #ddd;
				/*box-shadow: 0px 0px 13px rgb(64, 107, 145);*/
			
			}
			.onlinepress{
				background:#01c5b5 !important;
			}
			.loadpress{
				background:#1e73be !important;
				-webkit-box-shadow: none!important;
				box-shadow: none!important;
				text-shadow: none!important;
			}
			.loadpress:hover {
				background:#105089 !important;
			}
			.pics_block:hover{
				border:solid 3px #01c5b5;
				box-sizing:border-box;
				-moz-box-sizing:border-box;
				-webkit-box-sizing:border-box;
			}
			.pics_block img {
				-moz-transition: all 1s ease-out;
				-o-transition: all 1s ease-out;
				-webkit-transition: all 1s ease-out;
			}
				 
			.pics_block img:hover{
				 -webkit-transform: scale(1.2);
				 -moz-transform: scale(1.2);
				 -o-transform: scale(1.2);
			 }
			.name_block{
				width:100%;
				max-width:266px;
				text-align:center;
				background:#1e73be;
				//padding:2px 0;
				height:27px;
				color:#fff;
				text-decoration:none;
				display:inline-block;
				overflow:hidden;
				margin:-5px 0 0 0;
				/*box-shadow: 0px 0px 7px rgb(0, 0, 0);*/
			}
			.preset_pics:hover{
				//border:solid 3px #8c8c8c;
			}
			.focus_preset{
				border:solid 3px #01c5b5;
				box-sizing:border-box;
			}
			.closebtn{
				position:absolute;
				top: 4px;
				right: 4px;
				width: 20px;
				height: 20px;
				z-index: 100;

			}
			.pics_block:hover .closebtn a{
				position:absolute;
				top: 0px;
				right: 0px;
				width: 20px;
				height: 20px;
				z-index: 100;
				background: url(<?php print get_stylesheet_directory_uri()."/img/close_icon.png";?>) no-repeat center;
			}
			.preset_pics preset_hover input{
				margin-left:5px;
			}
			.wp-core-ui .button-disabled, .wp-core-ui .button-secondary.disabled, .wp-core-ui .button-secondary:disabled, .wp-core-ui .button-secondary[disabled], .wp-core-ui .button.disabled, .wp-core-ui .button:disabled, .wp-core-ui .button[disabled]{
				color: #FFF !important; 
				background:#01c5b5 !important;
				-webkit-box-shadow: none!important;
				box-shadow: none!important;
				text-shadow: none!important;
			}
		</style>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>Theme Options</h2>

            <?php settings_errors(); ?>

           

            <form method="post" action="options.php" enctype="multipart/form-data" id="theme_options_block" name="theme_options_block">
                <?php
                settings_fields('streamzon_theme_settings_option_group');
                $this->custom_do_settings_sections('streamzon-theme-settings');
                ?>
                <p class="progressbar"><i>Wait until the operation is complete</i><span></span></p>
                <br>

                <input type="submit" value="Save Settings" class="button button-primary" id="submit_settingsall" name="submit_settingsall">
                <!-- <input id="load_settings" class="button button-secondary" type="submit" value="Load Settings" name="load_settings"> -->
                <input id="export_settings" class="button button-secondary" type="submit" value="Export All Templates" name="export_settings">
                <input id="export_settings_preset" class="button button-secondary" type="submit" value="Export Template" name="export_settings_preset">
                <input style="border: 1px solid #ccc; cursor:pointer; padding: 2px 0; margin:0;" type="file" name="import_file" id="import-file">                
                <span id="ajax-import" class="button button-secondary">Import templates</span>
            </form>
        </div>

    <?php
    }
    function amazon_credentials_admin_page()
    {
        if(!fx_testForLicensePro()){return;}
        ?>
        <style>
            .flag{
                float: left;
                padding: 1px 2px;
                width: 24px;
            }
            p.submit {
                clear: both;
                margin-top: 20px;
                max-width: 100%;
                padding-top: 10px;
                text-align: left;
            }
            #am_credentials > h3 {
                display: none;
            }
	..wp-admin input[type=file]{
	padding:2px 0;
	}
        </style>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>Amazon Credentials</h2>

            <?php settings_errors(); ?>

            <form id="am_credentials" method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('streamzon_amazon_credentials_option_group');
                //do_settings_sections('streamzon-amazon-credentials');
                $this->custom_do_settings_sections_credentials('streamzon-amazon-credentials');
                submit_button();
                ?>
            </form>
        </div>
    <?php
    }
    function amazon_stores_admin_page(){
        
        if(!fx_testForLicensePro()){return;}

        ?>
        <style>
            .selectbox{
                width:100% !important;
            }
        </style>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>Amazon Categories</h2>

            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <div style="clear:both !important;">
                    <?php
                    streamzon_load_custom_scripts();
                    settings_fields('streamzon_amazon_stores_option_group');
                    $this->custom_do_settings_sections_stores('streamzon-amazon-stores');
                    ?>
                </div>
                <div style="clear:both !important;">
                    <?php submit_button();?>
                </div>
            </form>
        </div>
    <?php
    }
    function amazon_analytics_admin_page()
    {
        if(!fx_testForLicensePro()){return;}
        global $wpdb;
        ?>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>Analytics</h2>

            <?php settings_errors(); ?>

            <?php
            $table_name = $wpdb->prefix . "streamzon_amazon_analytics";

            if(isset($_POST['filterData']))
            {
                $from	=	$_POST['from'];
                $to		=	$_POST['to'];

                if( $from == date('Y-m-d',strtotime($from)) )
                { }
                else
                    $from == date('Y-m-01');

                if( $to == date('Y-m-d',strtotime($to)) )
                { }
                else
                    $to	=	date('Y-m-d');

                $frm_array	=	explode("-",$from);
                $to_array	=	explode("-",$to);

                $fday	=	 $frm_array[2];
                $tday	=	 $to_array[2];

                $querystr = "	SELECT SUM(impressions) as imp, SUM(clicks) as click, SUM(redirects) as red,SUM(search) as totSearch, COUNT( DISTINCT (user_ip) ) as uniqv, visitDate as mydate,
									(SELECT COUNT(DEVICE) from wp_streamzon_amazon_analytics WHERE  device = 'Desktops' AND visitDate = mydate ) as desktop,
									(SELECT COUNT(DEVICE) from wp_streamzon_amazon_analytics WHERE  device = 'Mobiles' AND visitDate = mydate ) as mobile
									FROM $table_name WHERE (`visitDate` BETWEEN '".$from."' AND '".$to."') GROUP by visitDate
							 ";

                $ipQuery = "	SELECT COUNT(country) as userCountry, country  FROM $table_name WHERE (`visitDate` BETWEEN '".$from."' AND '".$to."') GROUP by country limit 0,10";

                $keywordQuery = "	SELECT COUNT(searchfor) as usersearchfor, searchfor  FROM $table_name
										WHERE (`visitDate` BETWEEN '".$from."' AND '".$to."') AND searchfor != '' GROUP by searchfor  limit 0,10";
            }
            else
            {
                $querystr = "	SELECT SUM(impressions) as imp, SUM(clicks) as click, SUM(redirects) as red,SUM(search) as totSearch, COUNT( DISTINCT (user_ip) ) as uniqv, visitDate as mydate,
									(SELECT COUNT(DEVICE) from wp_streamzon_amazon_analytics WHERE  device = 'Desktops' AND visitDate = mydate ) as desktop,
									(SELECT COUNT(DEVICE) from wp_streamzon_amazon_analytics WHERE  device = 'Mobiles' AND visitDate = mydate ) as mobile
									FROM $table_name WHERE (`visitDate` BETWEEN '".date('Y-m-01')."' AND '".date('Y-m-d')."') GROUP by visitDate
							 ";
                $ipQuery = "	SELECT COUNT(country) as userCountry, country  FROM $table_name WHERE (`visitDate` BETWEEN '".date('Y-m-01')."' AND '".date('Y-m-d')."')  GROUP by country  limit 0,10";

                $keywordQuery = "	SELECT COUNT(searchfor) as usersearchfor, searchfor  FROM $table_name
										WHERE (`visitDate` BETWEEN '".date('Y-m-01')."' AND '".date('Y-m-d')."') AND searchfor != ''  GROUP by searchfor  limit 0,10";
            }

            $result = $wpdb->get_results($querystr);
            $result1 = $wpdb->get_results($ipQuery);
            $result2 = $wpdb->get_results($keywordQuery);

            $totSearch	=	$reds	=	$imprs	=	$desktops	=	$mobiles=0;

            $user_data = array();
            foreach ( $result as $row )
            {
                $timestamp = strtotime($row->mydate);
                $day = date("d", $timestamp);
                $mn = date("M", $timestamp);
                $totSearch	+= 	$row->totSearch;
                $reds		+=	$row->red;
                $imprs		+=	$row->imp;
                $desktops	+=	$row->desktop;
                $mobiles	+=	$row->mobile;

                $user_data[]	=	"['".$day." ".$mn."',".$row->imp.",".$row->click.",".$row->red."]";
            }
            $ips = "";
            foreach ( $result1 as $iprow ){
                if($iprow->country)
                    $ips .= "['".$iprow->country."', ".$iprow->userCountry."],";
                else
                    $ips .= "";

            }
            $searchString = "";
            foreach ( $result2 as $searchrow ){
                if($searchrow->usersearchfor)
                    $searchString .= "['".$searchrow->searchfor."', ".$searchrow->usersearchfor."],";
            }

            ?>

            <form method="post" action="">
                <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                <script type="text/javascript">
                    google.load("visualization", "1.1", {packages:["corechart","geochart","table"]});
                    google.setOnLoadCallback(drawChart);
                    google.setOnLoadCallback(drawRegionsMap);
                    google.setOnLoadCallback(drawTable);
                    google.setOnLoadCallback(drawTable1);

                    function drawChart()
                    {
                        ////// data for 4 charts
                        var data = google.visualization.arrayToDataTable
                        ([
                            ['Date', 'Impressions', 'Clicks', 'Redirects'],
                            <?php
                              if(is_array($user_data))
                                  print trim(implode(",",$user_data));
                              else
                                  print "0,0,0,0";
                          ?>,

                        ]);

                        var data1 = google.visualization.arrayToDataTable
                        ([
                            ['Device', 'Hits'],
                            ['Desktops', <?php print ($desktops) ? $desktops:0;?>],
                            ['Mobiles',  <?php print ($mobiles) ? $mobiles:0;?>]
                        ]);

                        var data2 = google.visualization.arrayToDataTable
                        ([
                            ['searchname', 'Hits'],
                            ['Total Searches', <?php print ($totSearch) ?$totSearch:0;?>],
                        ]);

                        var data3 = google.visualization.arrayToDataTable
                        ([
                            ['Redirects', 'Hits'],
                            ['Total Redirects to Amazon',     <?php print ($reds) ? $reds:0;?>],
                        ]);

                        var data4 = google.visualization.arrayToDataTable
                        ([
                            ['Impressions', 'Hits'],
                            ['Total Impeessions', <?php print ($imprs) ? $imprs:0;?>],
                        ]);

                        ////// Options for 4 charts
                        var options = {
                            pointSize: 5,
                            pointShape: 'circle',
                            fontSize: '12',
                            backgroundColor: 'transparent',
                            series: {
                                0: { color: '#ff750a' },
                                1: { color: '#2470b0' },
                                2: { color: '#6f9654' },
                            },
                            legend: { position: 'top', alignment: 'left' },

                            'title': '',
                            'vAxis':{
                                format: '0',
                            },
                            hAxis: {
                                title: 'Date',
                                showTextEvery:2,
                                format:'d-MMM',
                                titleTextStyle: {fontSize: '20', fontWidth: 'bold'},
                            },
                            chartArea:{left:20,top:100,width:'90%',height:'60%'},
                        };

                        var options1 = {
                            title: '',
                            legend: { position: 'top', alignment: 'left' },
                            pieHole: 0.5,
                            width: 230,
                            backgroundColor: 'transparent',
                            height: 200,
                            pieSliceText: 'value',
                            fontSize: '12',
                            chartArea:{left:10,top:20,width:"80%",height:"80%"},
                            colors: ['#2470b0', '#94ccfb'],
                            pieSliceTextStyle: {
                                color: 'cccccc',
                                fontSize: '24',
                            },
                            sliceVisibilityThreshold:0
                        };

                        var options2 = {
                            title: '',
                            legend: { position: 'top', alignment: 'left' },
                            pieHole: 0.5,
                            width: 230,
                            backgroundColor: 'transparent',
                            height: 200,
                            pieSliceText: 'value',
                            fontSize: '12',
                            chartArea:{left:10,top:20,width:"80%",height:"80%"},
                            tooltip: { trigger: 'none' },
                            slices: {
                                0: { color: 'c21185' },
                            },
                            pieSliceTextStyle: {
                                color: 'c21185',
                                fontSize: '24',
                            },
                            sliceVisibilityThreshold:0
                        };

                        var options3 = {
                            title: '',
                            legend: { position: 'top', alignment: 'left' },
                            pieHole: 0.5,
                            width: 230,
                            backgroundColor: 'transparent',
                            height: 200,
                            pieSliceText: 'value',
                            fontSize: '12',
                            chartArea:{left:10,top:20,width:"80%",height:"80%"},
                            tooltip: { trigger: 'none' },
                            slices: {
                                0: { color: '9acd00' },
                            },
                            pieSliceTextStyle: {
                                color: '9acd00',
                                fontSize: '24',
                            },
                            sliceVisibilityThreshold:0
                        };

                        var options4 = {
                            title: '',
                            legend: { position: 'top', alignment: 'left' },
                            pieHole: 0.5,
                            width: 230,
                            backgroundColor: 'transparent',
                            height: 200,
                            pieSliceText: 'value',
                            fontSize: '12',
                            chartArea:{left:10,top:20,width:"80%",height:"80%"},
                            tooltip: { trigger: 'none' },
                            slices: {
                                0: { color: 'ff750a' },
                            },
                            pieSliceTextStyle: {
                                color: 'ff750a',
                                fontSize: '24',
                            },
                            sliceVisibilityThreshold:0
                        };


                        //////init 4 charts
                        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                        var chart1 = new google.visualization.PieChart(document.getElementById('chart_div1'));
                        var chart2 = new google.visualization.PieChart(document.getElementById('chart_div2'));
                        var chart3 = new google.visualization.PieChart(document.getElementById('chart_div3'));
                        var chart4 = new google.visualization.PieChart(document.getElementById('chart_div4'));

                        chart.draw(data, options);
                        chart1.draw(data1, options1);
                        chart2.draw(data2, options2);
                        chart3.draw(data3, options3);
                        chart4.draw(data4, options4);

                    }

                    function drawRegionsMap()
                    {

                        var data = google.visualization.arrayToDataTable([
                            ['Country', 'Hits'],
                            <?php print $ips;?>
                        ]);

                        var options = {
                            width: 600,
                            height: 400,
                            legend: 'none',
                            backgroundColor: 'transparent',
                            chartArea:{left:10,top:20,width:"80%",height:"80%"},
                            colorAxis: {colors: ['#FF0000','#00FFFF','#0000FF','#0000A0','#ADD8E6','#800080','#FFFF00','#00FF00','#FF00FF']},
                        };

                        var chart = new google.visualization.GeoChart(document.getElementById('chart_div5'));
                        chart.draw(data, options);
                    }

                    function drawTable() {
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Country');
                        data.addColumn('number', 'Visitors');
                        data.addRows([<?php print $ips;?>]);
                        data.sort([{column: 1, desc:true}]);

                        var table = new google.visualization.Table(document.getElementById('chart_div6'));
                        table.draw(data, {
                                showRowNumber: true,
                                width:500,
                                alternatingRowStyle:true,
                                page:'enable',
                                pageSize: 5,
                                pagingSymbols: {
                                    prev: 'prev',
                                    next: 'next'
                                },
                            }
                        );
                    }

                    function drawTable1() {
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Keyword');
                        data.addColumn('number', 'Hits');
                        data.addRows([<?php print $searchString;?>]);
                        data.sort([{column: 1, desc:true}]);

                        var table = new google.visualization.Table(document.getElementById('chart_div7'));
                        table.draw(data, {
                                showRowNumber: true,
                                width:500,
                                alternatingRowStyle:true,
                                page:'enable',
                                pageSize: 5,
                                pagingSymbols: {
                                    prev: 'prev',
                                    next: 'next'
                                },
                            }
                        );
                    }

                </script>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
                <link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/css/admin.css">
                <script src="//code.jquery.com/jquery-1.10.2.js"></script>
                <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
                <script>
                    $(function() {
                        $( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd' });
                        $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    });
                    $(window).load(function(){

                        $(".country_table, #chart_div7").css({'width':'86%'});
                        $(".google-visualization-table").css({'width':'100%'});
                    });
                </script>
                <style>
		              .chart{
                        background: #fbfbfb;
                        padding:10px;
                        max-width:960px;
                        width:100%;
                    }
                    .filters{
                        display:inline-block;
                        margin: 0 0 0 30px;
                    }
                    .filters div{
                        padding: 10px;
                        width:200px;
                        float:left;
                    }
                    .chart div.formsubmit{
                        padding: 0 10px;
                        margin:0 0 0 30px;
                    }
                    .filters div span{
                        width:200px !important;
                        display:block;
                        font-weight:bold;
                    }
                    .chart .main_chart{
                        margin:20px auto;
                    }
                    .pieCharts{
                        display:inline-block;
                        max-width:960px;
                        width:100%;
                        margin:0 0 40px;
                        padding-left:40px;
                    }
                    .chart .pieCharts div{
                        margin:0;
                        float:left;
                    }
                    .google-visualization-table-table{
                        margin-bottom:20px;
                    }
                    table.google-visualization-table-table tr td:first-child{
                        width:150px;
                    }
                    table.google-visualization-table-table tr td:last-child{
                        width:450px;
                    }
                    .google-visualization-table-td{
                        padding-left:10px;
                        font-size:14px;
                        text-align:left;
                        border:solid 0px;
                        border-bottom:1px solid #eee;
                    }
                    .google-visualization-table-th{
                        /*display:none;*/
                    }
                    .tableChart{
                        padding:20px;
                    }
                    .google-visualization-table-div-page{
                        width:100%;
                    }
                    .google-visualization-table-div-page > .google-visualization-table-page-numbers{
                        float: right !important;
                    }
                    .google-visualization-table-th:first-child,.google-visualization-table-td:first-child{
                        width:30px !important;
                    }
                    .google-visualization-table-th:nth-child(2),.google-visualization-table-td:nth-child(2){
                        width:300px !important;
                        text-align:left;
                        padding-left:10px;
                    }
                    .google-visualization-table-th:last-child,.google-visualization-table-td:last-child{
                        width:50px !important;
                        text-align:left;
                        padding-left:10px;
                    }
					.google-visualization-table-th, .google-visualization-table-div-page{
                        background:#fff;
                    }
                    .country_table {
                      width: 86%!imporatnt;
                    }
                    .google-visualization-table {
                      width: 100%!imporatnt;
                    }
                </style>


                <div class="chart">
                    <div class="filters">
                        <div><lable for="datepicker1"><span>From: </span></label><input type="text" name="from" id="datepicker1" value="<?php print $from;?>" /></div>
                        <div><lable for="datepicker2"><span>To: </span></label><input type="text" name="to" id="datepicker2" value="<?php print $to;?>" /></div>
                    </div>
                    <div class="formsubmit"><input type="submit" class="button button-primary" name="filterData" value="Apply Filter"/></div>

                    <div id="chart_div" class="main_chart" style="width:  900px; height: 500px;"></div>
                    <div><hr></div>
                    <div class="pieCharts">
                        <div id="chart_div1"></div>
                        <div id="chart_div2"></div>
                        <div id="chart_div3"></div>
                        <div id="chart_div4"></div>
                    </div>
                    <div><hr></div>
                    <div class="pieCharts" style="margin:10px auto;">
                        <h3>Top Countries</h3>
                        <div id="chart_div6" class="country_table"></div>
                        <div id="chart_div5" class="country_chart"></div>
                    </div>
                    <div><hr></div>
                    <div class="pieCharts" style="margin:10px auto;">
                        <h3>Top Searches</h3>
                        <div id="chart_div7"></div>
                    </div>
                </div>
            </form>
        </div>
    <?php
    }

    function amazon_settings_admin_page()
    {
        if(!fx_testForLicensePro()){return;}
        ?>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>Preferences</h2>

            <?php settings_errors(); ?>
            <style>
                p.submit {
                    clear: both;
                    margin-top: 20px;
                    max-width: 100%;
                    padding-top: 10px;
                    text-align: left;
                }
                #am_settings > h3{
                    display: none;
                }
            </style>
            <form id="am_settings" method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('streamzon_amazon_settings_option_group');
                $this->custom_do_settings_amazon('streamzon-amazon-settings');
                //do_settings_sections('streamzon-amazon-settings');
                print "<div style='clear:both !important;'>".submit_button()."</div>";
                ?>
            </form>
        </div>
    <?php
    }

    function streamzon_seo_admin_page()
    {
        if(!fx_testForLicensePro()){return;}
        ?>
        <div class="wrap">

            <div class="options-page-logo"></div>

            <hr class="options-page-hr" />

            <h2>SEO Settings</h2>

            <?php settings_errors(); ?>
            <style>
                p.submit {
                    clear: both;
                    margin-top: 20px;
                    max-width: 100%;
                    padding-top: 10px;
                    text-align: left;
                }
                #seo_form > h3 {
                    display: none;
                }
            </style>
            <form id="seo_form" method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('streamzon_seo_settings_option_group');
                $this->custom_do_settings_seo('streamzon-seo');
                //do_settings_sections('streamzon-amazon-settings');
                print "<div style='clear:both !important;'>".submit_button()."</div>";
                ?>
            </form>
        </div>
    <?php
    }

    function amazon_item_settings_admin_page()
    {
        if(!fx_testForLicensePro()){return;}
        ?>
        <div class="wrap">
            <div class="options-page-logo"></div>
            <hr class="options-page-hr" />
            <h2>Amazon Item Settings</h2>
            <?php settings_errors(); ?>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('streamzon_amazon_item_settings_option_group');
                //do_settings_sections('streamzon-amazon-item-settings');
                $this->custom_do_item_settings_sections('streamzon-amazon-item-settings');

                submit_button();
                ?>
            </form>
        </div>
    <?php
    }


    /**
     * Register and add settings
     */
    public function page_init()
    {
		//test settings
		register_setting(
			'streamzon_theme_test_option_group', // Option group
			'streamzon_theme_test_option', // Option name
			array($this, 'sanitize_test_settings') // Sanitize
		);
		
        add_settings_section(
            'streamzon_theme_test_section', // ID
            'Quick wizard:', // Title
            array( $this, 'theme_settings_section_callback' ), // Callback
            'streamzon-theme-test' // Page
        );		
		
        // Theme Settings

        register_setting(
            'streamzon_theme_settings_option_group',    // Option group
            'streamzon_theme_settings_option',          // Option name
            array( $this, 'sanitize_theme_settings' )      // Sanitize
        );

        add_settings_section(
            'streamzon_theme_settings_section', // ID
            'Products Page Settings', // Title
            array( $this, 'theme_settings_section_callback' ), // Callback
            'streamzon-theme-settings' // Page
        );

        add_settings_section(
            'streamzon_theme_settings_1_section', // ID
            'Landing Page Settings', // Title
            array( $this, 'theme_settings_1_section_callback' ), // Callback
            'streamzon-theme-settings' // Page
        );

        add_settings_field(
            'top_bar_logo', // ID
            'Top Bar Logo (200x50)', // Title
            array( $this, 'top_bar_logo_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	

        add_settings_field(
            'banner_image_file', // ID
            'Banner Image', // Title
            array( $this, 'banner_image_file_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'top_bar_color', // ID
            'Top Bar Color', // Title
            array( $this, 'top_bar_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );


        add_settings_field(
            'show_headersidebar', // ID
            'Show Header Discount Bar', // Title
            array( $this, 'show_headersidebar_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        add_settings_field(
            'header_menu', // ID
            'Show Header Menu', // Title
            array( $this, 'header_menu_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'search_page_background', // ID
            'Page Background', // Title
            array( $this, 'search_page_background_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        /**************************/

        add_settings_field(
            'Product_cart_anable', // ID
            'Product_cart_anable', // Title
            array( $this, 'Product_cart_anable_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        //new
        add_settings_field(
            'Product_buy_anable', // ID
            'Product_buy_anable', // Title
            array( $this, 'Product_buy_anable_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );        
        
        add_settings_field(
            'Product_buy_background', // ID
            'Product_buy_background', // Title
            array( $this, 'Product_buy_background_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        add_settings_field(
            'Product_buy_background2', // ID
            'Product_buy_background', // Title
            array( $this, 'Product_buy_background2_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        add_settings_field(
            'Product_buy_color', // ID
            'Product_buy_color', // Title
            array( $this, 'Product_buy_color_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        add_settings_field(
            'Product_cart_background', // ID
            'Product_cart_background', // Title
            array( $this, 'Product_cart_background_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        add_settings_field(
            'Product_cart_background2', // ID
            'Product_cart_background', // Title
            array( $this, 'Product_cart_background2_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        add_settings_field(
            'Product_cart_color', // ID
            'Product_cart_color', // Title
            array( $this, 'Product_cart_color_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
		/********************************************************************************************************************************/
		add_settings_field(
            'add_to_cart_button_background', // ID
            'add_to_cart_button_background', // Title
            array( $this, 'add_to_cart_button_background_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'add_to_cart_button_background2', // ID
            'add_to_cart_button_background2', // Title
            array( $this, 'add_to_cart_button_background2_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'add_to_cart_button_color', // ID
            'add_to_cart_button_color', // Title
            array( $this, 'add_to_cart_button_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'buyitnow_button_background', // ID
            'buyitnow_button_background', // Title
            array( $this, 'buyitnow_button_background_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'buyitnow_button_background2', // ID
            'buyitnow_button_background2', // Title
            array( $this, 'buyitnow_button_background2_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'buyitnow_button_color', // ID
            'buyitnow_button_color', // Title
            array( $this, 'buyitnow_button_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'small_cart_button_color', // ID
            'small_cart_button_color', // Title
            array( $this, 'small_cart_button_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'small_cart_button_background', // ID
            'small_cart_button_background', // Title
            array( $this, 'small_cart_button_background_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		/********************************************************************************************************************************/
        /**************************/

        add_settings_field(
            'boxes_color', // ID
            'Boxes Color', // Title
            array( $this, 'boxes_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'box_text_color', // ID
            'Box Text Color', // Title
            array( $this, 'box_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
		add_settings_field(
            'box_text_url_color', // ID
            'Box Text Color', // Title
            array( $this, 'box_text_url_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'box_bar_background', // ID
            'Bar of Boxes (Background Color)', // Title
            array( $this, 'box_bar_background_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        add_settings_field(
            'box_background_text_color', // ID
            'Background text color)', // Title
            array( $this, 'box_background_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'box_bar_logo', // ID
            'Bar of Boxes (Logo 128x38)', // Title
            array( $this, 'box_bar_logo_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'top_bar_opacity', // ID
            'Opacity', // Title
            array( $this, 'top_bar_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'search_page_background_opacity', // ID
            'Opacity', // Title
            array( $this, 'search_page_background_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	add_settings_field(
            'search_page_pattern_opacity', // ID
            'Opacity Pattern', // Title
            array( $this, 'search_page_pattern_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_background_opacity', // ID
            'Top Opacity', // Title
            array( $this, 'l_page_background_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
	add_settings_field(
            'l_page_background_pattern_opacity', // ID
            'Pattern Opacity', // Title
            array( $this, 'l_page_background_pattern_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_background_video_opacity', // ID
            'Video Opacity', // Title
            array( $this, 'l_page_background_video_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'boxes_opacity', // ID
            'Opacity', // Title
            array( $this, 'boxes_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'searchbar_opacity', // ID
            'Opacity', // Title
            array( $this, 'searchbar_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'header_text', // ID
            'Header Text', // Title
            array( $this, 'header_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'header_text_font_family', // ID
            'Header Text Fonts', // TitleHeader Text
            array( $this, 'header_text_font_family_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_background_video_id', // ID
            'Default Search Keyword', // Title
            array( $this, 'l_page_background_video_id_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'banner_image_link', // ID
            'Banner Link', // Title
            array( $this, 'banner_image_link_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'banner_code', // ID
            'Banner Code', // Title
            array( $this, 'banner_code_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'header_text_px', // ID
            'Header Text Size', // Title
            array( $this, 'header_text_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'header_text_color', // ID
            'Header Text Color', // Title
            array( $this, 'header_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'header_text_bold', // ID
            'Bold', // Title
            array( $this, 'header_text_bold_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_header_bold', // ID
            'Bold', // Title
            array( $this, 'l_page_header_bold_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_subheader_bold', // ID
            'Bold', // Title
            array( $this, 'l_page_subheader_bold_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_discountbar_enable', // ID
            'Enable Discount seekbar', // Title
            array( $this, 'l_page_discountbar_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_line_on_color', // ID
            'Discountbar Foreground Color', // Title
            array( $this, 'l_page_discountbar_line_on_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_line_off_color', // ID
            'Discountbar Background Color', // Title
            array( $this, 'l_page_discountbar_line_off_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_ball_color', // ID
            'Discountbar Ball color', // Title
            array( $this, 'l_page_discountbar_ball_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_color', // ID
            'Discountbar Box color', // Title
            array( $this, 'l_page_discountbar_display_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_border_color', // ID
            'Discountbar Box Border color', // Title
            array( $this, 'l_page_discountbar_display_border_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_text_color', // ID
            'Discountbar Box text color', // Title
            array( $this, 'l_page_discountbar_display_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_opacity', // ID
            'Discountbar Box Opacity', // Title
            array( $this, 'l_page_discountbar_display_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_text', // ID
            'Discountbar Box text color', // Title
            array( $this, 'l_page_discountbar_display_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field('header_discountbar_enable','Enable Discount seekbar',array( $this, 'header_discountbar_enable_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_line_on_color','Discountbar Foreground Color',array( $this, 'header_discountbar_line_on_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_line_off_color','Discountbar Background Color',array( $this, 'header_discountbar_line_off_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_ball_color','Discountbar Ball color',array( $this, 'header_discountbar_ball_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_color','Discountbar Box color',array( $this, 'header_discountbar_display_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_border_color','Discountbar Box Border color',array( $this, 'header_discountbar_display_border_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_text_color','Discountbar Box text color',array( $this, 'header_discountbar_display_text_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_opacity','Discountbar Box Opacity',array( $this, 'header_discountbar_display_opacity_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_text_px','Discountbar Box Opacity',array( $this, 'header_discountbar_display_text_px_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('header_discountbar_display_text_family','Discountbar Box Opacity',array( $this, 'header_discountbar_display_text_family_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');

        add_settings_field('side_discountbar_enable','Enable Discount seekbar',array( $this, 'side_discountbar_enable_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_line_on_color','Discountbar Foreground Color',array( $this, 'side_discountbar_line_on_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_line_off_color','Discountbar Background Color',array( $this, 'side_discountbar_line_off_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_ball_color','Discountbar Ball color',array( $this, 'side_discountbar_ball_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_color','Discountbar Box color',array( $this, 'side_discountbar_display_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_border_color','Discountbar Box Border color',array( $this, 'side_discountbar_display_border_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_text_color','Discountbar Box text color',array( $this, 'side_discountbar_display_text_color_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_opacity','Discountbar Box Opacity',array( $this, 'side_discountbar_display_opacity_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_text_px','Discountbar Box Opacity',array( $this, 'side_discountbar_display_text_px_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');
        add_settings_field('side_discountbar_display_text_family','Discountbar Box Opacity',array( $this, 'side_discountbar_display_text_family_callback' ),'streamzon-theme-settings','streamzon_theme_settings_section');

        add_settings_field(
            'side_discountbar_display_text', // ID
            'Discountbar Box text', // Title
            array( $this, 'side_discountbar_display_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_subheader_font_family', // ID
            'Discountbar Box Font Style', // Title
            array( $this, 'l_page_subheader_font_family_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_discountbar_display_text_family', // ID
            'Discountbar Box Font Style', // Title
            array( $this, 'l_page_discountbar_display_text_family_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_discountbar_display_text_px', // ID
            'Discountbar Box Font size', // Title
            array( $this, 'l_page_discountbar_display_text_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_subheader_font_px', // ID
            'Discountbar Box Font size', // Title
            array( $this, 'l_page_subheader_font_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'searchbar_border_color', // ID
            'Border Color', // Title
            array( $this, 'searchbar_border_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'searchbar_text_color', // ID
            'Text Color', // Title
            array( $this, 'searchbar_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'searchbar_background_color', // ID
            'Background Color', // Title
            array( $this, 'searchbar_background_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'searchbar_image', // ID
            'Search Image', // Title
            array( $this, 'searchbar_image_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	    add_settings_field(
            'slidebar_searchbar_image', // ID
            'Search Image', // Title
            array( $this, 'slidebar_searchbar_image_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	    add_settings_field(
            'searchbar_top_enable', // ID
            'Search Image', // Title
            array( $this, 'searchbar_top_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        add_settings_field(
            'searchbar_topdiscount_enable', // ID
            'Enable discount', // Title
            array( $this, 'searchbar_topdiscount_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        add_settings_field(
            'text_link_color', // ID
            'Links Color', // Title
            array( $this, 'text_link_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'sidebar_header_color', // ID
            'Header Color', // Title
            array( $this, 'sidebar_header_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	//
	add_settings_field(
            'page_search_button_color', // ID
            'Page Search button color', // Title
            array( $this, 'page_search_button_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	//
	add_settings_field(
            'page_search_button_hover_color', // ID
            'Page search button hover color', // Title
            array( $this, 'page_search_button_hover_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	//
	add_settings_field(
            'page_search_button_text_color', // ID
            'Page search button text color', // Title
            array( $this, 'page_search_button_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	//
	add_settings_field(
            'page_search_button_text_opacity_px', // ID
            'Page search button text opacity', // Title
            array( $this, 'page_search_button_text_opacity_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	//
	add_settings_field(
            'page_search_button_text_border_color', // ID
            'Page search button border color', // Title
            array( $this, 'page_search_button_text_border_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_center_logo', // ID
            'Center Logo', // Title
            array( $this, 'l_page_center_logo_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'search_page_background_image', // ID
            'Page Background Image', // Title
            array( $this, 'search_page_background_image_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_button_color', // ID
            'Button Color', // Title
            array( $this, 'l_page_button_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_hover_color', // ID
            'Button Color', // Title
            array( $this, 'l_page_button_hover_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_text_color', // ID
            'Button Text Color', // Title
            array( $this, 'l_page_button_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_text_opacity_px', // ID
            'Button Opacity', // Title
            array( $this, 'l_page_button_text_opacity_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_text_border_color', // ID
            'Button Opacity', // Title
            array( $this, 'l_page_button_text_border_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_long_short', // ID
            'Button Long/Short', // Title
            array( $this, 'l_page_button_long_short_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_search_gap', // ID
            'Button Long/Short', // Title
            array( $this, 'l_page_button_search_gap_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_border_color', // ID
            'Border Color', // Title
            array( $this, 'l_page_searchbar_border_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_background_color', // ID
            'Background Color', // Title
            array( $this, 'l_page_searchbar_background_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_text_color', // ID
            'Text Color', // Title
            array( $this, 'l_page_searchbar_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );


        add_settings_field(
            'l_page_header_text', // ID
            'Header Text', // Title
            array( $this, 'l_page_header_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        $alocales = array(
            'ca' => 'Canada',
            'cn' => 'China',
            'de' => 'Germany',
            'es' => 'Spain',
            'fr' => 'France',
            'in' => 'India',
            'it' => 'Italy',
            'jp' => 'Japan',
            'uk' => 'UK',
            'us' => 'USA',
        );
        foreach($alocales as $key => $val)
        {
            add_settings_field(
                'header_text_'.$val, // ID
                'Header Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_section' // Section
            );

            add_settings_field(
                'l_page_subheader_text_'.$val, // ID
                'Header Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );

            add_settings_field(
                'l_page_header_text_'.$val, // ID
                'Header Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );

            add_settings_field(
                'l_page_footer_text_'.$val, // ID
                'Footer Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );


            add_settings_field(
                'l_page_searchbar_text_'.$val, // ID
                'Search Bar Color', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );

            add_settings_field(
                'l_page_button_text_'.$val, // ID
                'Button Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );

            add_settings_field(
                'l_page_discountbar_display_text_'.$val, // ID
                'Button Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_1_section' // Section
            );

            add_settings_field(
                'side_discountbar_display_text_'.$val, // ID
                'Button Text', // Title
                array( $this, 'blank_callback' ), // Callback
                'streamzon-theme-settings', // Page
                'streamzon_theme_settings_section' // Section
            );
	
	     add_settings_field(
            'revolution_slider_code_'.$val, // ID
            'Header Text', // Title
            array( $this, 'blank_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        }

        add_settings_field(
            'l_page_searchbar_text', // ID
            'Placeholder Text', // Title
            array( $this, 'l_page_searchbar_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_button_text', // ID
            'Button Text', // Title
            array( $this, 'l_page_button_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_header_text_px', // ID
            'Header Text Size', // Title
            array( $this, 'l_page_header_text_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_height_px', // ID
            'Height', // Title
            array( $this, 'l_page_searchbar_height_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_toppadding_px', // ID
            'Top Padding', // Title
            array( $this, 'l_page_searchbar_toppadding_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );         
        add_settings_field(
            'l_page_product_topmargin_px', // ID
            'Top Padding', // Title
            array( $this, 'l_page_product_topmargin_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );        

        add_settings_field(
            'l_page_searchbar_bottompadding_px', // ID
            'Top Padding', // Title
            array( $this, 'l_page_searchbar_bottompadding_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_searchbar_opacity_px', // ID
            'Opacity', // Title
            array( $this, 'l_page_searchbar_opacity_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_subheader_text', // ID
            'Subheader Text', // Title
            array( $this, 'l_page_subheader_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_subheader_text_px', // ID
            'Subheader Text Size', // Title
            array( $this, 'l_page_subheader_text_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_background_image', // ID
            'Top Background Image', // Title
            array( $this, 'l_page_background_image_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        

        add_settings_field(
            'l_page_header_color', // ID
            'Header Color', // Title
            array( $this, 'l_page_header_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_header_font_family', // ID
            'Font Family', // Title
            array( $this, 'l_page_header_font_family_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_subheader_color', // ID
            'Subheader Color', // Title
            array( $this, 'l_page_subheader_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );



        add_settings_field(
            'top_bar_logo_use', // ID
            '', // Title
            array( $this, 'top_bar_logo_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'box_bar_logo_use', // ID
            '', // Title
            array( $this, 'box_bar_logo_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'box_big_small_img', // ID
            '', // Title
            array( $this, 'box_big_small_img_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
        add_settings_field(
            'l_page_center_logo_use', // ID
            '', // Title
            array( $this, 'l_page_center_logo_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_center_logo_topsapce', // ID
            '', // Title
            array( $this, 'l_page_center_logo_topsapce_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'search_page_background_image_repeat', // ID
            '', // Title
            array( $this, 'search_page_background_image_repeat_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

	add_settings_field(
            'l_page_background_image_repeat', // ID
            '', // Title
            array( $this, 'l_page_background_image_repeat_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
	add_settings_field(
            'search_page_background_image_use', // ID
            '', // Title
            array( $this, 'search_page_background_image_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	add_settings_field(
            'search_page_background_pattern', // ID
            '', // Title
            array( $this, 'search_page_background_pattern_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	add_settings_field(
            'search_page_background_pattern_enable', // ID
            '', // Title
            array( $this, 'search_page_background_pattern_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_background_image_use', // ID
            '', // Title
            array( $this, 'l_page_background_image_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );


        add_settings_field(
            'chiz_flag', // ID
            'Enable Flag', // Title
            array( $this, 'chiz_flag_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_landing_page_align', // ID
            'Enable Landing Page', // Title
            array( $this, 'l_page_landing_page_align_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            '1_page_landing_page_body_size', // ID
            'Enable Landing Page', // Title
            array( $this, '_1_page_landing_page_body_size_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'landing_page_context_width', // ID
            'Landing Page margin size', // Title
            array( $this, 'landing_page_context_width_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_landing_headertxt_enable', // ID
            'Enable Heading Text', // Title
            array( $this, 'l_page_landing_headertxt_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
        add_settings_field(
            'l_page_landing_subheading_enable', // ID
            'Enable Subheading Page', // Title
            array( $this, 'l_page_landing_subheading_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'header_text_enable', // ID
            'Show Header Text', // Title
            array( $this, 'header_text_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        

        add_settings_field(
            'l_page_background_video_use', // ID
            'Use background color', // Title
            array( $this, 'l_page_background_video_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_background_video_pattern_enable', // ID
            'Use background color', // Title
            array( $this, 'l_page_background_video_pattern_enable_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_background_video_pattern', // ID
            'Use background color', // Title
            array( $this, 'l_page_background_video_pattern_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'banner_image_use', // ID
            'Use Image and Link', // Title
            array( $this, 'banner_image_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'banner_code_use', // ID
            'Use Code', // Title
            array( $this, 'banner_code_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'revolution_slider_use', // ID
            'Use Code', // Title
            array( $this, 'revolution_slider_use_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );
	
	
        add_settings_field(
            'revolution_slider_code', // ID
            'Code', // Title
            array( $this, 'revolution_slider_code_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_section' // Section
        );

        add_settings_field(
            'l_page_show_footer', // ID
            'Show Footer', // Title
            array( $this, 'l_page_show_footer_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_footer_text', // ID
            'Footer Text', // Title
            array( $this, 'l_page_footer_text_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_footer_text_font_family', // ID
            'Footer Text', // Title
            array( $this, 'l_page_footer_text_font_family_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_footer_text_font_px', // ID
            'Footer Text', // Title
            array( $this, 'l_page_footer_text_font_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_body_background_color', // ID
            'Full Screen Background Color', // Title
            array( $this, 'l_page_body_background_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_footer_text_color', // ID
            'Text Color', // Title
            array( $this, 'l_page_footer_text_color_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        add_settings_field(
            'l_page_footer_height_px', // ID
            'Footer Height', // Title
            array( $this, 'l_page_footer_height_px_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
	//footer opacity
	add_settings_field(
            'l_page_footer_opacity', // ID
            'Footer Opacity', // Title
            array( $this, 'l_page_footer_opacity_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );
	//Background footer
	add_settings_field(
            'l_page_background_footer', // ID
            'Background footer color', // Title
            array( $this, 'l_page_background_footer_callback' ), // Callback
            'streamzon-theme-settings', // Page
            'streamzon_theme_settings_1_section' // Section
        );

        // Amazon Credentials

        register_setting(
            'streamzon_amazon_credentials_option_group', // Option group
            'streamzon_amazon_credentials_option', // Option name
            array( $this, 'sanitize_credentials' ) // Sanitize
        );

        add_settings_section(
            'your_amazon_credentials', // ID
            'Your Amazon Credentials:', // Title
            array( $this, 'print_section_info' ), // Callback
            'streamzon-amazon-credentials' // Page
        );

        add_settings_field(
            'amazon_associate_id', // ID
            'Amazon Associate ID', // Title
            array( $this, 'amazon_associate_id_callback' ), // Callback
            'streamzon-amazon-credentials', // Page
            'your_amazon_credentials' // Section
        );

        add_settings_field(
            'amazon_associate_id_ca', // ID
            'Amazon Associate ID Canada', // Title
            array( $this, 'amazon_associate_id_ca_callback' ), // Callback
            'streamzon-amazon-credentials', // Page
            'your_amazon_credentials' // Section
        );

        add_settings_field(	'amazon_associate_id_uk','Amazon Associate ID UK',array( $this, 'amazon_associate_id_uk_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_de','Amazon Associate ID Germany',array( $this, 'amazon_associate_id_de_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_fr','Amazon Associate ID France',array( $this, 'amazon_associate_id_fr_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_in','Amazon Associate ID India',array( $this, 'amazon_associate_id_in_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_it','Amazon Associate ID Italy',array( $this, 'amazon_associate_id_it_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_es','Amazon Associate ID Spanish',array( $this, 'amazon_associate_id_es_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_jp','Amazon Associate ID Japan',array( $this, 'amazon_associate_id_jp_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');
        add_settings_field(	'amazon_associate_id_cn','Amazon Associate ID China',array( $this, 'amazon_associate_id_cn_callback' ),'streamzon-amazon-credentials','your_amazon_credentials');

		//==== Dashboard ===
        add_settings_field( 'access_key_id','Access Key ID', array( $this, 'access_key_id_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );        
        add_settings_field( 'access_key_id_2','Access Key ID', array( $this, 'access_key_id_2_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );
        add_settings_field( 'secret_access_key','Secret Access Key', array( $this, 'secret_access_key_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );          
        add_settings_field( 'secret_access_key_2','Secret Access Key 2', array( $this, 'secret_access_key_2_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );        
        add_settings_field( 'high_traffic_site','High traffic', array( $this, 'high_traffic_site_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );
        add_settings_field( 'amazon_associate_id', 'Amazon Associate ID', array( $this, 'amazon_associate_id_callback' ), // Callback
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );      
        add_settings_field( 'amazon_associate_id_ca', 'Amazon Associate ID Canada', array( $this, 'amazon_associate_id_ca_callback' ), // Callback
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );       
		add_settings_field(	'amazon_associate_id_uk','Amazon Associate ID UK',array( $this, 'amazon_associate_id_uk_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_de','Amazon Associate ID Germany',array( $this, 'amazon_associate_id_de_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_fr','Amazon Associate ID France',array( $this, 'amazon_associate_id_fr_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_in','Amazon Associate ID India',array( $this, 'amazon_associate_id_in_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_it','Amazon Associate ID Italy',array( $this, 'amazon_associate_id_it_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_es','Amazon Associate ID Spanish',array( $this, 'amazon_associate_id_es_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_jp','Amazon Associate ID Japan',array( $this, 'amazon_associate_id_jp_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(	'amazon_associate_id_cn','Amazon Associate ID China',array( $this, 'amazon_associate_id_cn_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
		);
        add_settings_field(
            'amazon_associate_auto','Access default market',array( $this, 'amazon_associate_auto_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );
        add_settings_field(
            'default_search_keyword', 'Default Search Keyword', array( $this, 'default_search_keyword_callback' ), // Callback
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        );     
        add_settings_field('amazon_discount_percent','Discount Percent',
            array( $this, 'amazon_discount_percent_callback' ),
            'streamzon-theme-test',
            'streamzon_theme_test_section'
        ); 
        #findme




        add_settings_field('amazon_category'   , 'Amazon Category'   , array( $this, 'amazon_category_callback'    ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_uk_category', 'Amazon UK Category', array( $this, 'amazon_uk_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_ca_category', 'Amazon CA Category', array( $this, 'amazon_ca_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_de_category', 'Amazon DE Category', array( $this, 'amazon_de_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_fr_category', 'Amazon FR Category', array( $this, 'amazon_fr_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_in_category', 'Amazon IN Category', array( $this, 'amazon_in_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_it_category', 'Amazon IT Category', array( $this, 'amazon_it_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_es_category', 'Amazon ES Category', array( $this, 'amazon_es_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_jp_category', 'Amazon JP Category', array( $this, 'amazon_jp_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');
        add_settings_field('amazon_cn_category', 'Amazon CN Category', array( $this, 'amazon_cn_category_callback' ), 'streamzon-theme-test', 'streamzon_theme_test_section');

        // ----------------------------------   




        add_settings_field(
            'amazon_associate_auto',
            'Access default market',
            array( $this, 'amazon_associate_auto_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );
        add_settings_field(
            'access_key_id',
            'Access Key ID',
            array( $this, 'access_key_id_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );        
        add_settings_field(
            'access_key_id_2',
            'Access Key ID 2',
            array( $this, 'access_key_id_2_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );
        add_settings_field(
            'secret_access_key',
            'Secret Access Key',
            array( $this, 'secret_access_key_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );        
        add_settings_field(
            'secret_access_key_2',
            'Secret Access Key 2',
            array( $this, 'secret_access_key_2_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );        
        add_settings_field(
            'high_traffic_site',
            'High traffic',
            array( $this, 'high_traffic_site_callback' ),
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );

        add_settings_field(
            'bitly_login_id', // ID
            'Bitly Login ID', // Title
            array( $this, 'bitly_login_id_callback' ), // Callback
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );

        add_settings_field(
            'bitly_api_key', // ID
            'Bitly API Key', // Title
            array( $this, 'bitly_api_key_callback' ), // Callback
            'streamzon-amazon-credentials',
            'your_amazon_credentials'
        );
	//SEO settings
	register_setting(
            'streamzon_seo_settings_option_group',   // Option group
            'streamzon_seo_settings_option',         // Option name
            array( $this, 'sanitize_settings' )      	// Sanitize
        );
	
	add_settings_section(
            'streamzon_seo_settings_section', // ID
            'SEO Settings:', // Title
            array( $this, 'seo_section_callback' ), // Callback
            'streamzon-seo' // Page,
        );
	
	//Facebook pixel
	add_settings_field(
            'search_page_facebook_pixel_enable', // ID
            'asdasd', // Title
            array($this,'search_pagepixel_enable_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	
	add_settings_field(
            'search_page_facebook_pixel_code', // ID
            '', // Title
            array( $this, 'search_page_facebook_pixel_code_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'land_page_facebook_pixel_enable', // ID
            '', // Title
            array( $this, 'land_page_facebook_pixel_enable_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'land_page_facebook_pixel_code', // ID
            '', // Title
            array( $this, 'land_page_facebook_pixel_code_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	//End facebook pixel
	
	add_settings_field(
            'seo_home_title', // ID
            'Home Title', // Title
            array( $this, 'seo_home_title_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_home_description', // ID
            'Home Description', // Title
            array( $this, 'seo_home_description_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_home_keywords', // ID
            'Home Keywords', // Title
            array( $this, 'seo_home_keywords_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );

	add_settings_field(
            'seo_search_title', // ID
            'Search Title', // Title
            array( $this, 'seo_search_title_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_search_description', // ID
            'Search Description', // Title
            array( $this, 'seo_search_description_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_search_keywords', // ID
            'Search Description', // Title
            array( $this, 'seo_search_keywords_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );

	add_settings_field(
            'seo_product_title', // ID
            'Product Title', // Title
            array( $this, 'seo_product_title_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_product_description', // ID
            'Product Description', // Title
            array( $this, 'seo_product_description_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	add_settings_field(
            'seo_product_keywords', // ID
            'Product Description', // Title
            array( $this, 'seo_product_keywords_callback' ), // Callback
            'streamzon-seo', // Page
            'streamzon_seo_settings_section' // Section
        );
	//END SEO

        // Amazon Settings
        register_setting(
            'streamzon_amazon_settings_option_group',   // Option group
            'streamzon_amazon_settings_option',         // Option name
            array( $this, 'sanitize_settings' )      	// Sanitize
        );

        add_settings_section(
            'streamzon_amazon_settings_section', // ID
            'Default Settings:', // Title
            array( $this, 'amazon_settings_section_callback' ), // Callback
            'streamzon-amazon-settings' // Page,
        );

        add_settings_field(
            'amazon_market',
            'Amazon Market',
            array( $this, 'amazon_market_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );

        add_settings_field(
            'amazon_additional_search_parameter',
            'Additional Search Parameter',
            array( $this, 'amazon_additional_search_parameter_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );        


        add_settings_field(
            'default_search_keyword', // ID
            'Default Search Keyword', // Title
            array( $this, 'default_search_keyword_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );


        add_settings_field(
            'amazon_discount',
            'Discount',
            array( $this, 'amazon_discount_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );

        add_settings_field(
            'amazon_discount_percent',
            'Discount Percent',
            array( $this, 'amazon_discount_percent_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );



        add_settings_field(
            'amazon_flag',
            'Disable Flag',
            array( $this, 'amazon_flag_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );

        add_settings_field(
            'amazon_add2Cart',
            'Add2Cart',
            array( $this, 'amazon_add2Cart_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );

        add_settings_field(
            'amazon_addsingPage',
            'Add Individual Page',
            array( $this, 'amazon_addsingPage_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );        
        add_settings_field(
            'show_sidebar',
            'Show sidebar',
            array( $this, 'show_sidebar_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );
        
        add_settings_field(
            'l_page_landing_page_enable',
            'Enable Landing Page',
            array( $this, 'l_page_landing_page_enable_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );
        
        add_settings_field(
            'l_page_onepage_landing',
            'Enable Onepage Landing Page',
            array( $this, 'l_page_onepage_landing_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );
        
        
        add_settings_field(
            'amazon_discount_amount',
            '',
            array( $this, 'amazon_discount_amount_callback' ),
            'streamzon-amazon-settings',
            'streamzon_amazon_settings_section'
        );
         /**************************/
        add_settings_field(
            'Product_cart_anable', // ID
            'Product_cart_anable', // Title
            array( $this, 'Product_cart_anable_callback' ), // Callback
            'streamzon-amazon-settings', // Page
            'streamzon_amazon_settings_section' // Section
        );
        /**************************/


        // Amazon Store Settings

        register_setting(
            'streamzon_amazon_stores_option_group', // Option group
            'streamzon_amazon_stores_option', // Option name
            array( $this, 'sanitize_stores_settings' ) // Sanitize
        );



        add_settings_section(
            'streamzon_amazon_stores_section', // ID
            'Default Settings:', // Title
            array( $this, 'amazon_stores_section_callback' ), // Callback
            'streamzon-amazon-stores' // Page,
        );
        add_settings_field('amazon_category'   , 'Amazon Category'   , array( $this, 'amazon_category_callback'    ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_uk_category', 'Amazon UK Category', array( $this, 'amazon_uk_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_ca_category', 'Amazon CA Category', array( $this, 'amazon_ca_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_de_category', 'Amazon DE Category', array( $this, 'amazon_de_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_fr_category', 'Amazon FR Category', array( $this, 'amazon_fr_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_in_category', 'Amazon IN Category', array( $this, 'amazon_in_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_it_category', 'Amazon IT Category', array( $this, 'amazon_it_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_es_category', 'Amazon ES Category', array( $this, 'amazon_es_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_jp_category', 'Amazon JP Category', array( $this, 'amazon_jp_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');
        add_settings_field('amazon_cn_category', 'Amazon CN Category', array( $this, 'amazon_cn_category_callback' ), 'streamzon-amazon-stores', 'streamzon_amazon_stores_section');

        /* ***************************** */



        register_setting(
            'streamzon_amazon_item_settings_option_group', // Option group
            'streamzon_amazon_item_settings_option', // Option name
            array( $this, 'sanitize_item_settings' ) // Sanitize
        );

        add_settings_section(
            'streamzon_amazon_item_settings_section', // ID
            '', // Title
            array( $this, 'amazon_item_settings_section_callback' ), // Callback
            'streamzon-amazon-item-settings' // Page
        );

        add_settings_field(
            'amazon_item_attributes', // ID
            'Amazon Item Attributes', // Title
            array( $this, 'amazon_item_attributes_callback' ), // Callback
            'streamzon-amazon-item-settings', // Page
            'streamzon_amazon_item_settings_section' // Section
        );


    }



    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */

    public function uploadfile($image_url)
    {        
        $upload_dir = 	wp_upload_dir(); // Set upload folder
        $img_data	=	@getimagesize($image_url);
        if(is_array($img_data))
        {
            $image_data = file_get_contents($image_url); // Get image data
            $filename   = basename($image_url); // Create image file name

            if( wp_mkdir_p( $upload_dir['path'] ) )
                $file = $upload_dir['path'] . '/' . $filename;
            else
                $file = $upload_dir['basedir'] . '/' . $filename;

            file_put_contents( $file, $image_data );
            $wp_filetype = wp_check_filetype( $filename, null );

            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => sanitize_file_name( $filename ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
            wp_update_attachment_metadata( $attach_id, $attach_data );

            $img	=	array(
                'id'	=>	$attach_id,
                'img'	=> 	$upload_dir['url']."/".$attach_data['sizes']['amazon_footer']['file']
            );

            return $img;
        }
    }

    public function upload_preset_thumbnail($file){
        $upload_dir_strimzone = get_template_directory() . '/img/uploads/preset_logo/';                            
        $upload_dir_strimzone_uri = get_template_directory_uri() . '/img/uploads/preset_logo/';                            
        
        $ext = ".". pathinfo($file['name'], PATHINFO_EXTENSION);
        $fileName = date('dmyhs') . rand(0,19) . $ext;

        $res = move_uploaded_file($file['tmp_name'], $upload_dir_strimzone . $fileName);       

        return $upload_dir_strimzone_uri . $fileName;        
    }
	
	public function sanitize_test_settings($input) {
		return $input;
	}
	
	
    public function sanitize_theme_settings($input)
    {
        global $wpdb;
        /**
         * start of theme design elements
         */


        $existed_settings  = get_option('streamzon_theme_settings_option');


        $alocales = array(
            'ca' => 'Canada',
            'cn' => 'China',
            'de' => 'Germany',
            'es' => 'Spain',
            'fr' => 'France',
            'in' => 'India',
            'it' => 'Italy',
            'jp' => 'Japan',
            'uk' => 'UK',
            'us' => 'USA',
        );

        $existed = false;


        if(isset($_POST['export_settings_preset'])) {// export settings
            $preset_name = trim(get_option('preset_name'));
            $presets = get_option('presets');
            $current_preset = $presets['presets'][$preset_name];
            $attachment = $current_preset['l_page_preset_thumbnail'];
            $current_preset['l_page_preset_thumbnail'] = $attachment;
            $json = json_encode($current_preset);
            $preset_name = str_replace(' ', '-', $preset_name);

            header("Content-disposition: attachment; filename={$preset_name}.json");
            header('Content-type: application/json');
            echo $json;
            die();
        }
        else if(isset($_POST['export_settings'])) { // export presets

            $presets = get_option('presets');
            $preset_name = trim(get_option('preset_name'));
            $options = get_option('streamzon_theme_settings_option');
            
            $attachment = $options['l_page_preset_thumbnail'];

            if($attachment)
                $options['attachment_file'] = $attachment;

            foreach($presets['presets'] as $key => $preset) {
                $attachment = $preset['l_page_preset_thumbnail'];
                if($attachment == 'dotted-1')
                    $presets['presets'][$key]['l_page_preset_thumbnail'] = get_template_directory_uri() . '/img/default.png';
            }
            $settings = array(
                'preset_name' => $preset_name,
                'presets' => $presets,
                'options' => $options
            );

            
            $json = json_encode($settings);

            header('Content-disposition: attachment; filename=all-presets.json');
            header('Content-type: application/json');
            echo $json;
            die();
        }
        else if(isset($_POST['submit_settings']))
        {
            $old_presets   = (array)get_option('presets'); 
            $old_presets   = (array)$old_presets['presets'];

            $old_names = array_keys($old_presets);  

            $input['l_page_preset'] = get_unique_preset_name($input['l_page_preset'], $old_names);
            
            /////NAME create preset
            if (isset($input['l_page_preset'])) {

                $new_input['l_page_preset'] = $input['l_page_preset'];
            } else {
                $new_input['l_page_preset'] = '';
            }
            if(isset($input['l_page_preset_thumbnail'])){
                $new_input['l_page_preset_thumbnail'] = $input['l_page_preset_thumbnail'];                
            }
            else {
                $new_input['l_page_preset_thumbnail'] = get_stylesheet_directory_uri().'/img/default.png';
            }                
                
            
            $pre_setting = $new_input['l_page_preset'];
            update_option('preset_name',$pre_setting);
            
        }
        else if(isset($_POST['load_settings']))
        {            
            //////Select
            if (isset($input['l_page_choose_settings']) && !$new_input['l_page_preset']) {

                $pre_settings	=	$input['l_page_choose_settings'];
                
                if($pre_settings != 'none' /*&& $pre_settings != 'default'*/ )
                {
                    $preset_array			=	get_option('presets');
                    update_option('preset_name',$pre_settings);

                    //if(isset($preset_array['presets'][$pre_settings]))
                    {
                        $existed	=	True;
                        $presetting_values	=	$preset_array['presets'][$pre_settings];
                    }
                }
                else if($pre_settings == 'none' /*|| $pre_settings == 'default'*/)
                {
                    $existed	=	false;
                }
            } else {
                $new_input['l_page_choose_settings'] = '';
                $pre_settings	=	'';
            }

        }



        if($existed == true)
        {
            $new_input['top_bar_logo'] = $presetting_values['top_bar_logo'];
        }
        else if (!empty($_FILES['top_bar_logo']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['top_bar_logo'], $override);
            $attachment_id = media_handle_upload( 'my_image_upload', $_POST['post_id'] );
            $new_input['top_bar_logo'] = $file['url'];
        } else {
            $new_input['top_bar_logo'] = $existed_settings['top_bar_logo'];

        }


        ////////preset thumbnail
        if($existed == true){
           $new_input['l_page_preset_thumbnail'] = $presetting_values['l_page_preset_thumbnail'];
           //die('stop1');
        }
        else if (!empty($_FILES['l_page_preset_thumbnail']['tmp_name'])) {            
            //die('stop2');
            
            $imgLink	= $this->upload_preset_thumbnail($_FILES['l_page_preset_thumbnail']);
            
            $new_input['l_page_preset_thumbnail'] = $imgLink;


        } else {        
            //if create preset without image            
            if(trim($input['l_page_preset']) !== "" ){
                $new_input['l_page_preset_thumbnail'] = get_stylesheet_directory_uri().'/img/default.png';
            }else{
                $new_input['l_page_preset_thumbnail'] = $existed_settings['l_page_preset_thumbnail'];
            }
                    
        }


        if($existed == true)
        {
            $new_input['banner_image_file'] = $presetting_values['banner_image_file'];
        }
        else if (!empty($_FILES['banner_image_file']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['banner_image_file'], $override);
            $new_input['banner_image_file'] = $file['url'];
        } else {
            $new_input['banner_image_file'] = $existed_settings['banner_image_file'];
            //$new_input['banner_image_file'] = "";
        }

        if($existed == true)
        {
            $new_input['box_bar_logo'] = $presetting_values['box_bar_logo'];
        }
        else if (!empty($_FILES['box_bar_logo']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['box_bar_logo'], $override);
            $myfile = $this->uploadfile($file['url']);
            $new_input['box_bar_logo']	=	$myfile['img'];

        } else {
            $new_input['box_bar_logo'] = $existed_settings['box_bar_logo'];
            //$new_input['box_bar_logo'] = "";
        }

        if($existed == true)
        {
            $new_input['l_page_center_logo'] = $presetting_values['l_page_center_logo'];
        }
        else if (!empty($_FILES['l_page_center_logo']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['l_page_center_logo'], $override);
            $new_input['l_page_center_logo'] = $file['url'];
        } else {
            $new_input['l_page_center_logo'] = $existed_settings['l_page_center_logo'];
            //$new_input['l_page_center_logo'] = "";
        }


        if($existed == true)
        {
            $new_input['search_page_background_image'] = $presetting_values['search_page_background_image'];

        }
        else if (!empty($_FILES['search_page_background_image']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['search_page_background_image'], $override);
            $new_input['search_page_background_image'] = $file['url'];
        } else {
            $new_input['search_page_background_image'] = $existed_settings['search_page_background_image'];
            //$new_input['search_page_background_image'] = "";
        }


        if($existed == true)
        {
            $new_input['l_page_background_image'] = $presetting_values['l_page_background_image'];
        }
        else if (!empty($_FILES['l_page_background_image']['tmp_name'])) {
            $override = array('test_form' => false);
            $file = wp_handle_upload($_FILES['l_page_background_image'], $override);
            $new_input['l_page_background_image'] = $file['url'];
        }
        else {
            $new_input['l_page_background_image'] = $existed_settings['l_page_background_image'];
            //$new_input['l_page_background_image'] = "";
        }


        


        if($existed == true)
        {
            $new_input['l_page_header_color'] = $presetting_values['l_page_header_color'];
        }
        else if (isset($input['l_page_header_color'])) {
            $new_input['l_page_header_color'] = $input['l_page_header_color'];
        } else {
            $new_input['l_page_header_color'] = $this->theme_settings_defaults['l_page_header_color'];
        }


        if($existed == true)
        {
            $new_input['l_page_header_font_family'] = $presetting_values['l_page_header_font_family'];
        }
        else if (isset($input['l_page_header_font_family'])) {
            $new_input['l_page_header_font_family'] = $input['l_page_header_font_family'];
        } else {
            $new_input['l_page_header_font_family'] = $this->theme_settings_defaults['l_page_header_font_family'];
        }

        if($existed == true)
        {
            $new_input['l_page_subheader_color'] = $presetting_values['l_page_subheader_color'];
        }
        else if (isset($input['l_page_subheader_color'])) {
            $new_input['l_page_subheader_color'] = $input['l_page_subheader_color'];
        } else {
            $new_input['l_page_subheader_color'] = $this->theme_settings_defaults['l_page_subheader_color'];
        }


        if($existed == true)
        {
            $new_input['l_page_subheader_font_family'] = $presetting_values['l_page_subheader_font_family'];
        }
        else if (isset($input['l_page_subheader_font_family'])) {
            $new_input['l_page_subheader_font_family'] = $input['l_page_subheader_font_family'];
        } else {
            $new_input['l_page_subheader_font_family'] = $this->theme_settings_defaults['l_page_subheader_font_family'];
        }


        if($existed == true)
        {
            $new_input['l_page_body_background_color'] = $presetting_values['l_page_body_background_color'];
        }
        else if (isset($input['l_page_body_background_color'])) {
            $new_input['l_page_body_background_color'] = $input['l_page_body_background_color'];
        } else {
            $new_input['l_page_body_background_color'] = $this->theme_settings_defaults['l_page_body_background_color'];
        }


        if($existed == true)
            $new_input['l_page_footer_text_color'] = $presetting_values['l_page_footer_text_color'];
        else if (isset($input['l_page_footer_text_color']))
            $new_input['l_page_footer_text_color'] = $input['l_page_footer_text_color'];
        else
            $new_input['l_page_footer_text_color'] =  $this->theme_settings_defaults['l_page_footer_text_color'];

        if($existed == true)
            $new_input['l_page_footer_text_font_family'] = $presetting_values['l_page_footer_text_font_family'];
        else if (isset($input['l_page_footer_text_font_family']))
            $new_input['l_page_footer_text_font_family'] = $input['l_page_footer_text_font_family'];
        else
            $new_input['l_page_footer_text_font_family'] =  $this->theme_settings_defaults['l_page_footer_text_font_family'];

        if($existed == true)
            $new_input['l_page_footer_text_font_px'] = $presetting_values['l_page_footer_text_font_px'];
        else if (isset($input['l_page_footer_text_font_px']))
            $new_input['l_page_footer_text_font_px'] = $input['l_page_footer_text_font_px'];
        else
            $new_input['l_page_footer_text_font_px'] =  $this->theme_settings_defaults['l_page_footer_text_font_px'];


        if($existed == true)
        {
            $new_input['l_page_footer_height_px'] = $presetting_values['l_page_footer_height_px'];
        }
        else if (isset($input['l_page_footer_height_px'])) {
            $new_input['l_page_footer_height_px'] = $input['l_page_footer_height_px'];
        } else {
            $new_input['l_page_footer_height_px'] = $this->theme_settings_defaults['l_page_footer_height_px'];
        }

        if($existed == true)
        {
            $new_input['box_big_small_img'] = $presetting_values['box_big_small_img'];
        }
        else if (isset($input['box_big_small_img'])) {
            $new_input['box_big_small_img'] = $input['box_big_small_img'];
        } else {
            $new_input['box_big_small_img'] = $this->theme_settings_defaults['box_big_small_img'];
        }


        if($existed == true)
        {
            $new_input['header_menu'] = $presetting_values['header_menu'];
        }
        else if (isset($input['header_menu'])) {
            $new_input['header_menu'] = $input['header_menu'];
        } else {
            $new_input['header_menu'] = 0;
        }


        if($existed == true)
        {
            $new_input['show_headersidebar'] = $presetting_values['show_headersidebar'];
        }
        else if (isset($input['show_headersidebar'])) {
            $new_input['show_headersidebar'] = $input['show_headersidebar'];
        } else {
            $new_input['show_headersidebar'] = 0;
        }



        if($existed == true)
        {
            $new_input['l_page_show_footer'] = $presetting_values['l_page_show_footer'];
        }
        else if (isset($input['l_page_show_footer'])) {
            $new_input['l_page_show_footer'] = $input['l_page_show_footer'];
        } else {
            $new_input['l_page_show_footer'] = 0;
        }


        if($existed == true)
        {
            $new_input['top_bar_color'] = $presetting_values['top_bar_color'];
        }
        else if (isset($input['top_bar_color'])) {
            $new_input['top_bar_color'] = $input['top_bar_color'];
        } else {
            $new_input['top_bar_color'] = $this->theme_settings_defaults['top_bar_color'];
        }

	

        if($existed == true)
        {
            $new_input['search_page_background'] = $presetting_values['search_page_background'];
        }
        else if (isset($input['search_page_background'])) {
            $new_input['search_page_background'] = $input['search_page_background'];
        } else {
            $new_input['search_page_background'] = $this->theme_settings_defaults['search_page_background'];
        }

        if($existed == true)
        {
            $new_input['boxes_color'] = $presetting_values['boxes_color'];
        }
        else if (isset($input['boxes_color'])) {
            $new_input['boxes_color'] = $input['boxes_color'];
        } else {
            $new_input['boxes_color'] =  $this->theme_settings_defaults['boxes_color'];
        }
        
        /**************************************/
        if($existed == true)
        {
            $new_input['Product_buy_anable'] = $presetting_values['Product_buy_anable'];
        }
        else if (isset($input['Product_buy_anable'])) {
            $new_input['Product_buy_anable'] = $input['Product_buy_anable'];
        } else {
            $new_input['Product_buy_anable'] = 0;
        }
        if($existed == true)
        {
            $new_input['Product_cart_anable'] = $presetting_values['Product_cart_anable'];
        }
        else if (isset($input['Product_cart_anable'])) {
            $new_input['Product_cart_anable'] = $input['Product_cart_anable'];
        } else {
            $new_input['Product_cart_anable'] = 0;
        }
		
		if($existed == true)
        {
            $new_input['small_cart_button_color'] = $presetting_values['small_cart_button_color'];
        }
        else if (isset($input['small_cart_button_color'])) {
            $new_input['small_cart_button_color'] = $input['small_cart_button_color'];
        } else {
            $new_input['small_cart_button_color'] = 0;
        }
		if($existed == true)
        {
            $new_input['small_cart_button_background'] = $presetting_values['small_cart_button_background'];
        }
        else if (isset($input['small_cart_button_background'])) {
            $new_input['small_cart_button_background'] = $input['small_cart_button_background'];
        } else {
            $new_input['small_cart_button_background'] = 0;
        }
		
		if($existed == true)
        {
            $new_input['add_to_cart_button_background'] = $presetting_values['add_to_cart_button_background'];
        }
        else if (isset($input['add_to_cart_button_background'])) {
            $new_input['add_to_cart_button_background'] = $input['add_to_cart_button_background'];
        } else {
            $new_input['add_to_cart_button_background'] = 0;
        }
		if($existed == true)
        {
            $new_input['add_to_cart_button_background2'] = $presetting_values['add_to_cart_button_background2'];
        }
        else if (isset($input['add_to_cart_button_background2'])) {
            $new_input['add_to_cart_button_background2'] = $input['add_to_cart_button_background2'];
        } else {
            $new_input['add_to_cart_button_background2'] = 0;
        }
		if($existed == true)
        {
            $new_input['add_to_cart_button_color'] = $presetting_values['add_to_cart_button_color'];
        }
        else if (isset($input['add_to_cart_button_color'])) {
            $new_input['add_to_cart_button_color'] = $input['add_to_cart_button_color'];
        } else {
            $new_input['add_to_cart_button_color'] = 0;
        }
		if($existed == true)
        {
            $new_input['buyitnow_button_background'] = $presetting_values['buyitnow_button_background'];
        }
        else if (isset($input['buyitnow_button_background'])) {
            $new_input['buyitnow_button_background'] = $input['buyitnow_button_background'];
        } else {
            $new_input['buyitnow_button_background'] = 0;
        }
		if($existed == true)
        {
            $new_input['buyitnow_button_background2'] = $presetting_values['buyitnow_button_background2'];
        }
        else if (isset($input['buyitnow_button_background2'])) {
            $new_input['buyitnow_button_background2'] = $input['buyitnow_button_background2'];
        } else {
            $new_input['buyitnow_button_background2'] = 0;
        }
		if($existed == true)
        {
            $new_input['buyitnow_button_color'] = $presetting_values['buyitnow_button_color'];
        }
        else if (isset($input['buyitnow_button_color'])) {
            $new_input['buyitnow_button_color'] = $input['buyitnow_button_color'];
        } else {
            $new_input['buyitnow_button_color'] = 0;
        }
        /***************************************/

        if($existed == true)
        {
            $new_input['box_text_color'] = $presetting_values['box_text_color'];
        }
        else if (isset($input['box_text_color'])) {
            $new_input['box_text_color'] = $input['box_text_color'];
        } else {
            $new_input['box_text_color'] = $this->theme_settings_defaults['box_text_color'];
        }
		
		if($existed == true)
        {
            $new_input['box_text_url_color'] = $presetting_values['box_text_url_color'];
        }
        else if (isset($input['box_text_url_color'])) {
            $new_input['box_text_url_color'] = $input['box_text_url_color'];
        } else {
            $new_input['box_text_url_color'] = $this->theme_settings_defaults['box_text_url_color'];
        }

        if($existed == true)
        {
            $new_input['box_bar_background'] = $presetting_values['box_bar_background'];
        }
        else if (isset($input['box_bar_background'])) {
            $new_input['box_bar_background'] = $input['box_bar_background'];
        } else {
            $new_input['box_bar_background'] = $this->theme_settings_defaults['box_bar_background'];
        }

        if($existed == true)
        {
            $new_input['box_background_text_color'] = $presetting_values['box_background_text_color'];
        }
        else if (isset($input['box_background_text_color'])) {
            $new_input['box_background_text_color'] = $input['box_background_text_color'];
        } else {
            $new_input['box_background_text_color'] = $this->theme_settings_defaults['box_background_text_color'];
        }

        if($existed == true)
        {
            $new_input['top_bar_opacity'] = $presetting_values['top_bar_opacity'];
        }
        else if (isset($input['top_bar_opacity'])) {
            $new_input['top_bar_opacity'] = $input['top_bar_opacity'];
        }
        else {
            $new_input['top_bar_opacity'] = $this->theme_settings_defaults['top_bar_opacity'];
        }


        if($existed == true)
        {
            $new_input['search_page_background_opacity'] = $presetting_values['search_page_background_opacity'];
        }
        else if (isset($input['search_page_background_opacity'])) {
            $new_input['search_page_background_opacity'] = $input['search_page_background_opacity'];
        }
        else {
            $new_input['search_page_background_opacity'] = $this->theme_settings_defaults['search_page_background_opacity'];
        }

	if($existed == true)
        {
            $new_input['search_page_pattern_opacity'] = $presetting_values['search_page_pattern_opacity'];
        }
        else if (isset($input['search_page_pattern_opacity'])) {
            $new_input['search_page_pattern_opacity'] = $input['search_page_pattern_opacity'];
        }
        else {
            $new_input['search_page_pattern_opacity'] = $this->theme_settings_defaults['search_page_pattern_opacity'];
        }


        if($existed == true)
        {
            $new_input['l_page_background_opacity'] = $presetting_values['l_page_background_opacity'];
        }
        else if (isset($input['l_page_background_opacity'])) {
            $new_input['l_page_background_opacity'] = $input['l_page_background_opacity'];
        }
        else {
            $new_input['l_page_background_opacity'] = $this->theme_settings_defaults['l_page_background_opacity'];
        }

	if($existed == true)
        {
            $new_input['l_page_background_pattern_opacity'] = $presetting_values['l_page_background_pattern_opacity'];
        }
        else if (isset($input['l_page_background_pattern_opacity'])) {
            $new_input['l_page_background_pattern_opacity'] = $input['l_page_background_pattern_opacity'];
        }
        else {
            $new_input['l_page_background_pattern_opacity'] = $this->theme_settings_defaults['l_page_background_pattern_opacity'];
        }


        if($existed == true)
        {
            $new_input['l_page_background_video_opacity'] = $presetting_values['l_page_background_video_opacity'];
        }
        else if (isset($input['l_page_background_video_opacity'])) {
            $new_input['l_page_background_video_opacity'] = $input['l_page_background_video_opacity'];
        }
        else {
            $new_input['l_page_background_video_opacity'] = $this->theme_settings_defaults['l_page_background_video_opacity'];
        }
//Background footer
	if($existed == true)
        {
            $new_input['l_page_background_footer'] = $presetting_values['l_page_background_footer'];
        }
        else if (isset($input['l_page_background_footer'])) {
            $new_input['l_page_background_footer'] = $input['l_page_background_footer'];
        }
        else {
            $new_input['l_page_background_footer'] = $this->theme_settings_defaults['l_page_background_footer'];
        }
//foter opacity
	if($existed == true)
        {
            $new_input['l_page_footer_opacity'] = $presetting_values['l_page_footer_opacity'];
        }
        else if (isset($input['l_page_footer_opacity'])) {
            $new_input['l_page_footer_opacity'] = $input['l_page_footer_opacity'];
        }
        else {
            $new_input['l_page_footer_opacity'] = $this->theme_settings_defaults['l_page_footer_opacity'];
        }
//
        if($existed == true)
        {
            $new_input['boxes_opacity'] = $presetting_values['boxes_opacity'];
        }
        else if (isset($input['boxes_opacity'])) {
            $new_input['boxes_opacity'] = $input['boxes_opacity'];
        }
        else {
            $new_input['boxes_opacity'] = $this->theme_settings_defaults['boxes_opacity'];
        }

        if($existed == true)
        {
            $new_input['searchbar_opacity'] = $presetting_values['searchbar_opacity'];
        }
        else if (isset($input['searchbar_opacity'])) {
            $new_input['searchbar_opacity'] = $input['searchbar_opacity'];
        }
        else {
            $new_input['searchbar_opacity'] = $this->theme_settings_defaults['searchbar_opacity'];
        }

        if($existed == true)
        {
            $new_input['header_text_px'] = $presetting_values['header_text_px'];
        }
        else if (isset($input['header_text_px'])) {
            $new_input['header_text_px'] = $input['header_text_px'];
        }
        else {
            $new_input['header_text_px'] = $this->theme_settings_defaults['header_text_px'];
        }



        if($existed == true)
        {
            $new_input['header_text'] = $presetting_values['header_text'];
            foreach($alocales as $key => $val)
                $new_input['header_text_'.$val] = $presetting_values['header_text_'.$val];
        }
        else if (isset($input['header_text'])) {
            $new_input['header_text'] = $input['header_text'];
            foreach($alocales as $key => $val)
                $new_input['header_text_'.$val] = $input['header_text_'.$val];
        }
        else {
            $new_input['header_text'] = $this->theme_settings_defaults['header_text'];
            foreach($alocales as $key => $val)
                $new_input['header_text_'.$val] = $this->theme_settings_defaults['header_text_'.$val];
        }

        if($existed == true)
        {
            $new_input['header_text_font_family'] = $presetting_values['header_text_font_family'];
        }
        else if (isset($input['header_text_font_family'])) {
            $new_input['header_text_font_family'] = $input['header_text_font_family'];
        }
        else {
            $new_input['header_text_font_family'] = $this->theme_settings_defaults['header_text_font_family'];
        }

        if($existed == true)
        {
            $new_input['l_page_background_video_id'] = $presetting_values['l_page_background_video_id'];
        }
        else if (isset($input['l_page_background_video_id'])) {
            $new_input['l_page_background_video_id'] = $input['l_page_background_video_id'];
        }
        else {
            $new_input['l_page_background_video_id'] = $this->theme_settings_defaults['l_page_background_video_id'];
        }

        if($existed == true)
        {
            $new_input['banner_image_link'] = $presetting_values['banner_image_link'];
        }
        else if (isset($input['banner_image_link'])) {
            $new_input['banner_image_link'] = $input['banner_image_link'];
        }
        else {
            $new_input['banner_image_link'] = $this->theme_settings_defaults['banner_image_link'];
        }


        if($existed == true)
        {
            $new_input['banner_code'] = $presetting_values['banner_code'];
        }
        else if (isset($input['banner_code'])) {
            $new_input['banner_code'] = $input['banner_code'];
        }
        else {
            $new_input['banner_code'] = $this->theme_settings_defaults['banner_code'];
        }

        if($existed == true)
        {
            $new_input['revolution_slider_code'] = $presetting_values['revolution_slider_code'];
        }
        else if (isset($input['revolution_slider_code'])) {
            $new_input['revolution_slider_code'] = $input['revolution_slider_code'];
        }
        else {
            $new_input['revolution_slider_code'] = $this->theme_settings_defaults['revolution_slider_code'];
        }

        if($existed == true)
        {
            $new_input['header_text_color'] = $presetting_values['header_text_color'];
        }
        else if (isset($input['header_text_color'])) {
            $new_input['header_text_color'] = $input['header_text_color'];
        } else {
            $new_input['header_text_color'] = $this->theme_settings_defaults['header_text_color'];
        }

        if($existed == true)
        {
            $new_input['header_text_bold'] = $presetting_values['header_text_bold'];
        }
        else if (isset($input['header_text_bold'])) {
            $new_input['header_text_bold'] = $input['header_text_bold'];
        } else {
            $new_input['header_text_bold'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_header_bold'] = $presetting_values['l_page_header_bold'];
        }
        else if (isset($input['l_page_header_bold'])) {
            $new_input['l_page_header_bold'] = $input['l_page_header_bold'];
        } else {
            $new_input['l_page_header_bold'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_subheader_bold'] = $presetting_values['l_page_subheader_bold'];
        }
        else if (isset($input['l_page_subheader_bold'])) {
            $new_input['l_page_subheader_bold'] = $input['l_page_subheader_bold'];
        } else {
            $new_input['l_page_subheader_bold'] = 0;
        }

        ///////////////////SeekBar////////////////////
        if($existed == true)	$new_input['l_page_discountbar_enable'] = $presetting_values['l_page_discountbar_enable'];
        else if (isset($input['l_page_discountbar_enable'])) $new_input['l_page_discountbar_enable'] = $input['l_page_discountbar_enable'];
        else	$new_input['l_page_discountbar_enable'] = 0;

        if($existed == true)	$new_input['l_page_discountbar_line_on_color'] = $presetting_values['l_page_discountbar_line_on_color'];
        else if (isset($input['l_page_discountbar_line_on_color'])) $new_input['l_page_discountbar_line_on_color'] = $input['l_page_discountbar_line_on_color'];
        else	$new_input['l_page_discountbar_line_on_color'] = $this->theme_settings_defaults['l_page_discountbar_line_on_color'];

        if($existed == true)	$new_input['l_page_discountbar_line_off_color'] = $presetting_values['l_page_discountbar_line_off_color'];
        else if (isset($input['l_page_discountbar_line_off_color'])) 	$new_input['l_page_discountbar_line_off_color'] = $input['l_page_discountbar_line_off_color'];
        else	$new_input['l_page_discountbar_line_off_color'] = $this->theme_settings_defaults['l_page_discountbar_line_off_color'];

        if($existed == true)	$new_input['l_page_discountbar_ball_color'] = $presetting_values['l_page_discountbar_ball_color'];
        else if (isset($input['l_page_discountbar_ball_color'])) 	$new_input['l_page_discountbar_ball_color'] = $input['l_page_discountbar_ball_color'];
        else	$new_input['l_page_discountbar_ball_color'] = $this->theme_settings_defaults['l_page_discountbar_ball_color'];

        if($existed == true)	$new_input['l_page_discountbar_display_color'] = $presetting_values['l_page_discountbar_display_color'];
        else if (isset($input['l_page_discountbar_display_color'])) 	$new_input['l_page_discountbar_display_color'] = $input['l_page_discountbar_display_color'];
        else	$new_input['l_page_discountbar_display_color'] = $this->theme_settings_defaults['l_page_discountbar_display_color'];

        if($existed == true)	$new_input['l_page_discountbar_display_border_color'] = $presetting_values['l_page_discountbar_display_border_color'];
        else if (isset($input['l_page_discountbar_display_border_color'])) 	$new_input['l_page_discountbar_display_border_color'] = $input['l_page_discountbar_display_border_color'];
        else	$new_input['l_page_discountbar_display_border_color'] = $this->theme_settings_defaults['l_page_discountbar_display_border_color'];

        if($existed == true)	$new_input['l_page_discountbar_display_text_color'] = $presetting_values['l_page_discountbar_display_text_color'];
        else if (isset($input['l_page_discountbar_display_text_color'])) 	$new_input['l_page_discountbar_display_text_color'] = $input['l_page_discountbar_display_text_color'];
        else	$new_input['l_page_discountbar_display_text_color'] = $this->theme_settings_defaults['l_page_discountbar_display_text_color'];

        if($existed == true)	$new_input['l_page_discountbar_display_text'] = $presetting_values['l_page_discountbar_display_text'];
        else if (isset($input['l_page_discountbar_display_text'])) 	$new_input['l_page_discountbar_display_text'] = $input['l_page_discountbar_display_text'];
        else	$new_input['l_page_discountbar_display_text'] = $this->theme_settings_defaults['l_page_discountbar_display_text'];

        if($existed == true)	$new_input['l_page_discountbar_display_text_family'] = $presetting_values['l_page_discountbar_display_text_family'];
        else if (isset($input['l_page_discountbar_display_text_family'])) 	$new_input['l_page_discountbar_display_text_family'] = $input['l_page_discountbar_display_text_family'];
        else	$new_input['l_page_discountbar_display_text_family'] = $this->theme_settings_defaults['l_page_discountbar_display_text_family'];

        if($existed == true)	$new_input['l_page_subheader_font_px'] = $presetting_values['l_page_subheader_font_px'];
        else if (isset($input['l_page_subheader_font_px'])) 	$new_input['l_page_subheader_font_px'] = $input['l_page_subheader_font_px'];
        else	$new_input['l_page_subheader_font_px'] = $this->theme_settings_defaults['l_page_subheader_font_px'];

        if($existed == true)	$new_input['l_page_discountbar_display_text_px'] = $presetting_values['l_page_discountbar_display_text_px'];
        else if (isset($input['l_page_discountbar_display_text_px'])) 	$new_input['l_page_discountbar_display_text_px'] = $input['l_page_discountbar_display_text_px'];
        else	$new_input['l_page_discountbar_display_text_px'] = $this->theme_settings_defaults['l_page_discountbar_display_text_px'];

        if($existed == true)	$new_input['l_page_discountbar_display_opacity'] = $presetting_values['l_page_discountbar_display_opacity'];
        else if (isset($input['l_page_discountbar_display_opacity'])) 	$new_input['l_page_discountbar_display_opacity'] = $input['l_page_discountbar_display_opacity'];
        else	$new_input['l_page_discountbar_display_opacity'] = $this->theme_settings_defaults['l_page_discountbar_display_opacity'];
        ///////////////////seekbar end/////////////////////

        ///////////////////SeekBar header////////////////////
        if($existed == true)	$new_input['header_discountbar_enable'] = $presetting_values['header_discountbar_enable'];
        else if (isset($input['header_discountbar_enable'])) 	$new_input['header_discountbar_enable'] = $input['header_discountbar_enable'];
        else	$new_input['header_discountbar_enable'] = 0;

        if($existed == true)	$new_input['header_discountbar_line_on_color'] = $presetting_values['header_discountbar_line_on_color'];
        else if (isset($input['header_discountbar_line_on_color'])) 	$new_input['header_discountbar_line_on_color'] = $input['header_discountbar_line_on_color'];
        else	$new_input['header_discountbar_line_on_color'] = $this->theme_settings_defaults['header_discountbar_line_on_color'];

        if($existed == true)	$new_input['header_discountbar_line_off_color'] = $presetting_values['header_discountbar_line_off_color'];
        else if (isset($input['header_discountbar_line_off_color'])) 	$new_input['header_discountbar_line_off_color'] = $input['header_discountbar_line_off_color'];
        else	$new_input['header_discountbar_line_off_color'] = $this->theme_settings_defaults['header_discountbar_line_off_color'];

        if($existed == true)	$new_input['header_discountbar_ball_color'] = $presetting_values['header_discountbar_ball_color'];
        else if (isset($input['header_discountbar_ball_color'])) 	$new_input['header_discountbar_ball_color'] = $input['header_discountbar_ball_color'];
        else	$new_input['header_discountbar_ball_color'] = $this->theme_settings_defaults['header_discountbar_ball_color'];

        if($existed == true)	$new_input['header_discountbar_display_color'] = $presetting_values['header_discountbar_display_color'];
        else if (isset($input['header_discountbar_display_color'])) 	$new_input['header_discountbar_display_color'] = $input['header_discountbar_display_color'];
        else	$new_input['header_discountbar_display_color'] = $this->theme_settings_defaults['header_discountbar_display_color'];

        if($existed == true)	$new_input['header_discountbar_display_border_color'] = $presetting_values['header_discountbar_display_border_color'];
        else if (isset($input['header_discountbar_display_border_color'])) 	$new_input['header_discountbar_display_border_color'] = $input['header_discountbar_display_border_color'];
        else	$new_input['header_discountbar_display_border_color'] = $this->theme_settings_defaults['header_discountbar_display_border_color'];

        if($existed == true)	$new_input['header_discountbar_display_text_color'] = $presetting_values['header_discountbar_display_text_color'];
        else if (isset($input['header_discountbar_display_text_color'])) 	$new_input['header_discountbar_display_text_color'] = $input['header_discountbar_display_text_color'];
        else	$new_input['header_discountbar_display_text_color'] = $this->theme_settings_defaults['header_discountbar_display_text_color'];

        if($existed == true)	$new_input['header_discountbar_display_text_family'] = $presetting_values['header_discountbar_display_text_family'];
        else if (isset($input['header_discountbar_display_text_family'])) 	$new_input['header_discountbar_display_text_family'] = $input['header_discountbar_display_text_family'];
        else	$new_input['header_discountbar_display_text_family'] = $this->theme_settings_defaults['header_discountbar_display_text_family'];

        if($existed == true)	$new_input['header_subheader_font_px'] = $presetting_values['header_subheader_font_px'];
        else if (isset($input['header_subheader_font_px'])) 	$new_input['header_subheader_font_px'] = $input['header_subheader_font_px'];
        else	$new_input['header_subheader_font_px'] = $this->theme_settings_defaults['header_subheader_font_px'];

        if($existed == true)	$new_input['header_discountbar_display_text_px'] = $presetting_values['header_discountbar_display_text_px'];
        else if (isset($input['header_discountbar_display_text_px'])) 	$new_input['header_discountbar_display_text_px'] = $input['header_discountbar_display_text_px'];
        else	$new_input['header_discountbar_display_text_px'] = $this->theme_settings_defaults['header_discountbar_display_text_px'];

        if($existed == true)	$new_input['header_discountbar_display_opacity'] = $presetting_values['header_discountbar_display_opacity'];
        else if (isset($input['header_discountbar_display_opacity'])) 	$new_input['header_discountbar_display_opacity'] = $input['header_discountbar_display_opacity'];
        else	$new_input['header_discountbar_display_opacity'] = $this->theme_settings_defaults['header_discountbar_display_opacity'];

        ///////////////////seekbar end	///////////////////
        ///////////////////side SeekBar ////////////////////
        if($existed == true)	$new_input['side_discountbar_enable'] = $presetting_values['side_discountbar_enable'];
        else if (isset($input['side_discountbar_enable'])) 	$new_input['side_discountbar_enable'] = $input['side_discountbar_enable'];
        else	$new_input['side_discountbar_enable'] = 0;

        if($existed == true)	$new_input['side_discountbar_line_on_color'] = $presetting_values['side_discountbar_line_on_color'];
        else if (isset($input['side_discountbar_line_on_color'])) 	$new_input['side_discountbar_line_on_color'] = $input['side_discountbar_line_on_color'];
        else	$new_input['side_discountbar_line_on_color'] = $this->theme_settings_defaults['side_discountbar_line_on_color'];

        if($existed == true)	$new_input['side_discountbar_line_off_color'] = $presetting_values['side_discountbar_line_off_color'];
        else if (isset($input['side_discountbar_line_off_color'])) 	$new_input['side_discountbar_line_off_color'] = $input['side_discountbar_line_off_color'];
        else	$new_input['side_discountbar_line_off_color'] = $this->theme_settings_defaults['side_discountbar_line_off_color'];

        if($existed == true)	$new_input['side_discountbar_ball_color'] = $presetting_values['side_discountbar_ball_color'];
        else if (isset($input['side_discountbar_ball_color'])) 	$new_input['side_discountbar_ball_color'] = $input['side_discountbar_ball_color'];
        else	$new_input['side_discountbar_ball_color'] = $this->theme_settings_defaults['side_discountbar_ball_color'];

        if($existed == true)	$new_input['side_discountbar_display_color'] = $presetting_values['side_discountbar_display_color'];
        else if (isset($input['side_discountbar_display_color'])) 	$new_input['side_discountbar_display_color'] = $input['side_discountbar_display_color'];
        else	$new_input['side_discountbar_display_color'] = $this->theme_settings_defaults['side_discountbar_display_color'];

        if($existed == true)	$new_input['side_discountbar_display_border_color'] = $presetting_values['side_discountbar_display_border_color'];
        else if (isset($input['side_discountbar_display_border_color'])) 	$new_input['side_discountbar_display_border_color'] = $input['side_discountbar_display_border_color'];
        else	$new_input['side_discountbar_display_border_color'] = $this->theme_settings_defaults['side_discountbar_display_border_color'];

        if($existed == true)	$new_input['side_discountbar_display_text_color'] = $presetting_values['side_discountbar_display_text_color'];
        else if (isset($input['side_discountbar_display_text_color'])) 	$new_input['side_discountbar_display_text_color'] = $input['side_discountbar_display_text_color'];
        else	$new_input['side_discountbar_display_text_color'] = $this->theme_settings_defaults['side_discountbar_display_text_color'];

        if($existed == true)	$new_input['side_discountbar_display_text_family'] = $presetting_values['side_discountbar_display_text_family'];
        else if (isset($input['side_discountbar_display_text_family'])) 	$new_input['side_discountbar_display_text_family'] = $input['side_discountbar_display_text_family'];
        else	$new_input['side_discountbar_display_text_family'] = $this->theme_settings_defaults['side_discountbar_display_text_family'];

        if($existed == true)	$new_input['side_discountbar_display_text_px'] = $presetting_values['side_discountbar_display_text_px'];
        else if (isset($input['side_discountbar_display_text_px'])) 	$new_input['side_discountbar_display_text_px'] = $input['side_discountbar_display_text_px'];
        else	$new_input['side_discountbar_display_text_px'] = $this->theme_settings_defaults['side_discountbar_display_text_px'];

        if($existed == true)	$new_input['side_discountbar_display_opacity'] = $presetting_values['side_discountbar_display_opacity'];
        else if (isset($input['side_discountbar_display_opacity'])) 	$new_input['side_discountbar_display_opacity'] = $input['side_discountbar_display_opacity'];
        else	$new_input['side_discountbar_display_opacity'] = $this->theme_settings_defaults['side_discountbar_display_opacity'];

        ///////////////////seekbar end

        if($existed == true)
        {
            $new_input['searchbar_border_color'] = $presetting_values['searchbar_border_color'];
        }
        else if (isset($input['searchbar_border_color'])) {
            $new_input['searchbar_border_color'] = $input['searchbar_border_color'];
        } else {
            $new_input['searchbar_border_color'] = $this->theme_settings_defaults['searchbar_border_color'];
        }

        if($existed == true)
        {
            $new_input['searchbar_text_color'] = $presetting_values['searchbar_text_color'];
        }
        else if (isset($input['searchbar_text_color'])) {
            $new_input['searchbar_text_color'] = $input['searchbar_text_color'];
        } else {
            $new_input['searchbar_text_color'] = $this->theme_settings_defaults['searchbar_text_color'];
        }

        if($existed == true)
        {
            $new_input['searchbar_background_color'] = $presetting_values['searchbar_background_color'];
        }
        else if (isset($input['searchbar_background_color'])) {
            $new_input['searchbar_background_color'] = $input['searchbar_background_color'];
        } else {
            $new_input['searchbar_background_color'] = $this->theme_settings_defaults['searchbar_background_color'];
        }


        if($existed == true)
        {
            $new_input['searchbar_image'] = $presetting_values['searchbar_image'];
        }
        else if (isset($input['searchbar_image'])) {
            $new_input['searchbar_image'] = $input['searchbar_image'];
        } else {
            $new_input['searchbar_image'] = 0;
        } 

	if($existed == true)
        {
            $new_input['slidebar_searchbar_image'] = $presetting_values['slidebar_searchbar_image'];
        }
        else if (isset($input['slidebar_searchbar_image'])) {
            $new_input['slidebar_searchbar_image'] = $input['slidebar_searchbar_image'];
        } else {
            $new_input['slidebar_searchbar_image'] = 0;
        }

	//SERCHBAR TOP ENABLE
	    if($existed == true)
        {
            $new_input['searchbar_top_enable'] = $presetting_values['searchbar_top_enable'];
        }
        else if (isset($input['searchbar_top_enable'])) {
            $new_input['searchbar_top_enable'] = $input['searchbar_top_enable'];
        } else {
            $new_input['searchbar_top_enable'] = 0;
        }
        
        if($existed == true)
        {
            $new_input['searchbar_topdiscount_enable'] = $presetting_values['searchbar_topdiscount_enable'];
        }
        else if (isset($input['searchbar_topdiscount_enable'])) {
            $new_input['searchbar_topdiscount_enable'] = $input['searchbar_topdiscount_enable'];
        } else {
            $new_input['searchbar_topdiscount_enable'] = 0;
        }        


        if($existed == true)
        {
            $new_input['text_link_color'] = $presetting_values['text_link_color'];
        }
        else if (isset($input['text_link_color'])) {
            $new_input['text_link_color'] = $input['text_link_color'];
        } else {
            $new_input['text_link_color'] = $this->theme_settings_defaults['text_link_color'];
        }

        if($existed == true)
        {
            $new_input['sidebar_header_color'] = $presetting_values['sidebar_header_color'];
        }
        else if (isset($input['sidebar_header_color'])) {
            $new_input['sidebar_header_color'] = $input['sidebar_header_color'];
        } else {
            $new_input['sidebar_header_color'] = $this->theme_settings_defaults['sidebar_header_color'];
        }
	//
	if($existed == true)
        {
            $new_input['page_search_button_color'] = $presetting_values['page_search_button_color'];
        }
        else if (isset($input['page_search_button_color'])) {
            $new_input['page_search_button_color'] = $input['page_search_button_color'];
        } else {
            $new_input['page_search_button_color'] = $this->theme_settings_defaults['page_search_button_color'];
        }
	//
	if($existed == true)
        {
            $new_input['page_search_button_hover_color'] = $presetting_values['page_search_button_hover_color'];
        }
        else if (isset($input['page_search_button_hover_color'])) {
            $new_input['page_search_button_hover_color'] = $input['page_search_button_hover_color'];
        } else {
            $new_input['page_search_button_hover_color'] = $this->theme_settings_defaults['page_search_button_hover_color'];
        }
	//
	if($existed == true)
        {
            $new_input['page_search_button_text_color'] = $presetting_values['page_search_button_text_color'];
        }
        else if (isset($input['page_search_button_text_color'])) {
            $new_input['page_search_button_text_color'] = $input['page_search_button_text_color'];
        } else {
            $new_input['page_search_button_text_color'] = $this->theme_settings_defaults['page_search_button_text_color'];
        }
	//
	if($existed == true)
        {
            $new_input['page_search_button_text_opacity_px'] = $presetting_values['page_search_button_text_opacity_px'];
        }
        else if (isset($input['page_search_button_text_opacity_px'])) {
            $new_input['page_search_button_text_opacity_px'] = $input['page_search_button_text_opacity_px'];
        } else {
            $new_input['page_search_button_text_opacity_px'] = $this->theme_settings_defaults['page_search_button_text_opacity_px'];
        }
	//
	if($existed == true)
        {
            $new_input['page_search_button_text_border_color'] = $presetting_values['page_search_button_text_border_color'];
        }
        else if (isset($input['page_search_button_text_border_color'])) {
            $new_input['page_search_button_text_border_color'] = $input['page_search_button_text_border_color'];
        } else {
            $new_input['page_search_button_text_border_color'] = $this->theme_settings_defaults['page_search_button_text_border_color'];
        }


        if($existed == true)
        {
            $new_input['l_page_button_color'] = $presetting_values['l_page_button_color'];
        }
        else if (isset($input['l_page_button_color'])) {
            $new_input['l_page_button_color'] = $input['l_page_button_color'];
        } else {
            $new_input['l_page_button_color'] = $this->theme_settings_defaults['l_page_button_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_hover_color'] = $presetting_values['l_page_button_hover_color'];
        }
        else if (isset($input['l_page_button_hover_color'])) {
            $new_input['l_page_button_hover_color'] = $input['l_page_button_hover_color'];
        } else {
            $new_input['l_page_button_hover_color'] = $this->theme_settings_defaults['l_page_button_hover_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_text_color'] = $presetting_values['l_page_button_text_color'];
        }
        else if (isset($input['l_page_button_text_color'])) {
            $new_input['l_page_button_text_color'] = $input['l_page_button_text_color'];
        } else {
            $new_input['l_page_button_text_color'] = $this->theme_settings_defaults['l_page_button_text_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_text_opacity_px'] = $presetting_values['l_page_button_text_opacity_px'];
        }
        else if (isset($input['l_page_button_text_opacity_px'])) {
            $new_input['l_page_button_text_opacity_px'] = $input['l_page_button_text_opacity_px'];
        } else {
            $new_input['l_page_button_text_opacity_px'] =  $this->theme_settings_defaults['l_page_button_text_opacity_px'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_text_border_color'] = $presetting_values['l_page_button_text_border_color'];
        }
        else if (isset($input['l_page_button_text_border_color'])) {
            $new_input['l_page_button_text_border_color'] = $input['l_page_button_text_border_color'];
        } else {
            $new_input['l_page_button_text_border_color'] = $this->theme_settings_defaults['l_page_button_text_border_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_long_short'] = $presetting_values['l_page_button_long_short'];
        }
        else if (isset($input['l_page_button_long_short'])) {
            $new_input['l_page_button_long_short'] = $input['l_page_button_long_short'];
        } else {
            $new_input['l_page_button_long_short'] = $this->theme_settings_defaults['l_page_button_long_short'];
        }

        if($existed == true)
        {
            $new_input['l_page_button_search_gap'] = $presetting_values['l_page_button_search_gap'];
        }
        else if (isset($input['l_page_button_search_gap'])) {
            $new_input['l_page_button_search_gap'] = $input['l_page_button_search_gap'];
        } else {
            $new_input['l_page_button_search_gap'] = 0;
        }
        if($existed == true)
        {
            $new_input['l_page_searchbar_border_color'] = $presetting_values['l_page_searchbar_border_color'];
        }
        else if (isset($input['l_page_searchbar_border_color'])) {
            $new_input['l_page_searchbar_border_color'] = $input['l_page_searchbar_border_color'];
        } else {
            $new_input['l_page_searchbar_border_color'] = $this->theme_settings_defaults['l_page_searchbar_border_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_searchbar_background_color'] = $presetting_values['l_page_searchbar_background_color'];
        }
        else if (isset($input['l_page_searchbar_background_color'])) {
            $new_input['l_page_searchbar_background_color'] = $input['l_page_searchbar_background_color'];
        } else {
            $new_input['l_page_searchbar_background_color'] = $this->theme_settings_defaults['l_page_searchbar_background_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_searchbar_text_color'] = $presetting_values['l_page_searchbar_text_color'];
        }
        else if (isset($input['l_page_searchbar_text_color'])) {
            $new_input['l_page_searchbar_text_color'] = $input['l_page_searchbar_text_color'];
        } else {
            $new_input['l_page_searchbar_text_color'] = $this->theme_settings_defaults['l_page_searchbar_text_color'];
        }

        if($existed == true)
        {
            $new_input['l_page_header_text'] = $presetting_values['l_page_header_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_header_text_'.$val] 	= $presetting_values['l_page_header_text_'.$val];
        }
        else if (isset($input['l_page_header_text'])) {
            $new_input['l_page_header_text'] = $input['l_page_header_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_header_text_'.$val] 	= $input['l_page_header_text_'.$val];
        }
        else
        {
            $new_input['l_page_header_text'] = $this->theme_settings_defaults['l_page_header_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_header_text_'.$val] 	= $this->theme_settings_defaults['l_page_header_text_'.$val];
        }



        if($existed == true)
        {
            $new_input['l_page_searchbar_text'] = $presetting_values['l_page_searchbar_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_searchbar_text_'.$val] = $presetting_values['l_page_searchbar_text_'.$val];
        }
        else if (isset($input['l_page_searchbar_text'])) {
            $new_input['l_page_searchbar_text'] = $input['l_page_searchbar_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_searchbar_text_'.$val] = $input['l_page_searchbar_text_'.$val];
        }
        else{
            $new_input['l_page_searchbar_text'] = $this->theme_settings_defaults['l_page_searchbar_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_searchbar_text_'.$val] = $this->theme_settings_defaults['l_page_searchbar_text_'.$val];
        }


        if($existed == true)
        {
            $new_input['side_discountbar_display_text'] = $presetting_values['side_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['side_discountbar_display_text_'.$val] = $presetting_values['side_discountbar_display_text_'.$val];
        }
        else if (isset($input['side_discountbar_display_text'])) {
            $new_input['side_discountbar_display_text'] = $input['side_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['side_discountbar_display_text_'.$val] = $input['side_discountbar_display_text_'.$val];
        }
        else{
            $new_input['side_discountbar_display_text'] = $this->theme_settings_defaults['side_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['side_discountbar_display_text_'.$val] = $this->theme_settings_defaults['side_discountbar_display_text_'.$val];
        }

        if($existed == true)
        {
            $new_input['l_page_discountbar_display_text'] = $presetting_values['l_page_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_discountbar_display_text_'.$val] = $presetting_values['l_page_discountbar_display_text_'.$val];
        }
        else if (isset($input['l_page_discountbar_display_text'])) {
            $new_input['l_page_discountbar_display_text'] = $input['l_page_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_discountbar_display_text_'.$val] = $input['l_page_discountbar_display_text_'.$val];
        }
        else{
            $new_input['l_page_discountbar_display_text'] = $this->theme_settings_defaults['l_page_discountbar_display_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_discountbar_display_text_'.$val] = $this->theme_settings_defaults['l_page_discountbar_display_text_'.$val];
        }


if($existed == true)
        {
            $new_input['l_page_footer_text'] = $presetting_values['l_page_footer_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_footer_text_'.$val] = $presetting_values['l_page_footer_text_'.$val];
        }
        else if (isset($input['l_page_footer_text'])) {
            $new_input['l_page_footer_text'] = $input['l_page_footer_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_footer_text_'.$val] = $input['l_page_footer_text_'.$val];
        }
        else{
            $new_input['l_page_footer_text'] = $this->theme_settings_defaults['l_page_footer_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_footer_text_'.$val] = $this->theme_settings_defaults['l_page_footer_text_'.$val];
        }



        if($existed == true)
        {
            $new_input['l_page_button_text'] = $presetting_values['l_page_button_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_button_text_'.$val] = $presetting_values['l_page_button_text_'.$val];

        }
        else if (isset($input['l_page_button_text'])) {
            $new_input['l_page_button_text'] = $input['l_page_button_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_button_text_'.$val] = $input['l_page_button_text_'.$val];
        }
        else{
            $new_input['l_page_button_text'] = $this->theme_settings_defaults['l_page_button_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_button_text_'.$val] = $this->theme_settings_defaults['l_page_button_text_'.$val];
        }

        if($existed == true)
        {
            $new_input['l_page_subheader_text'] = $presetting_values['l_page_subheader_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_subheader_text_'.$val] = $presetting_values['l_page_subheader_text_'.$val];
        }
        else if (isset($input['l_page_subheader_text'])) {
            $new_input['l_page_subheader_text'] = $input['l_page_subheader_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_subheader_text_'.$val] = $input['l_page_subheader_text_'.$val];
        }
        else
        {
            $new_input['l_page_subheader_text'] = $this->theme_settings_defaults['l_page_subheader_text'];
            foreach($alocales as $key => $val)
                $new_input['l_page_subheader_text_'.$val] = $this->theme_settings_defaults['l_page_subheader_text_'.$val];
        }
	
	if($existed == true)
        {
            $new_input['revolution_slider_code'] = $presetting_values['revolution_slider_code'];
            foreach($alocales as $key => $val)
                $new_input['revolution_slider_code_'.$val] = $presetting_values['revolution_slider_code_'.$val];
        }
        else if (isset($input['revolution_slider_code'])) {
            $new_input['revolution_slider_code'] = $input['revolution_slider_code'];
            foreach($alocales as $key => $val)
                $new_input['revolution_slider_code_'.$val] = $input['revolution_slider_code_'.$val];
        }
        else
        {
            $new_input['revolution_slider_code'] = $this->theme_settings_defaults['revolution_slider_code'];
            foreach($alocales as $key => $val)
                $new_input['revolution_slider_code_'.$val] = $this->theme_settings_defaults['revolution_slider_code_'.$val];
        }

        if($existed == true)
        {
            $new_input['l_page_footer_text'] = $presetting_values['l_page_footer_text'];
        }
        else if (isset($input['l_page_footer_text'])) {
            $new_input['l_page_footer_text'] = $input['l_page_footer_text'];
        }
        else
            $new_input['l_page_footer_text'] = $this->theme_settings_defaults['l_page_footer_text'];

        if($existed == true)
        {
            $new_input['l_page_header_text_px'] = $presetting_values['l_page_header_text_px'];

        }
        else if (isset($input['l_page_header_text_px'])) {
            $new_input['l_page_header_text_px'] = $input['l_page_header_text_px'];
        }
        else
            $new_input['l_page_header_text_px'] = $this->theme_settings_defaults['l_page_header_text_px'];

        if($existed == true)
        {
            $new_input['l_page_searchbar_height_px'] = $presetting_values['l_page_searchbar_height_px'];
        }
        else if (isset($input['l_page_searchbar_height_px'])) {
            $new_input['l_page_searchbar_height_px'] = $input['l_page_searchbar_height_px'];
        }
        else
            $new_input['l_page_searchbar_height_px'] = $this->theme_settings_defaults['l_page_searchbar_height_px'];

        if($existed == true)
        {
            $new_input['l_page_searchbar_toppadding_px'] = $presetting_values['l_page_searchbar_toppadding_px'];
        }
        else if (isset($input['l_page_searchbar_toppadding_px'])) {
            $new_input['l_page_searchbar_toppadding_px'] = $input['l_page_searchbar_toppadding_px'];
        }
        else
            $new_input['l_page_searchbar_toppadding_px'] = $this->theme_settings_defaults['l_page_searchbar_toppadding_px'];
       
        if($existed == true)
        {
            $new_input['l_page_product_topmargin_px'] = $presetting_values['l_page_product_topmargin_px'];
        }
        else if (isset($input['l_page_product_topmargin_px'])) {
            $new_input['l_page_product_topmargin_px'] = $input['l_page_product_topmargin_px'];
        }
        else
            $new_input['l_page_product_topmargin_px'] = $this->theme_settings_defaults['l_page_product_topmargin_px'];

        if($existed == true)
        {
            $new_input['l_page_searchbar_bottompadding_px'] = $presetting_values['l_page_searchbar_bottompadding_px'];
        }
        else if (isset($input['l_page_searchbar_bottompadding_px'])) {
            $new_input['l_page_searchbar_bottompadding_px'] = $input['l_page_searchbar_bottompadding_px'];
        }
        else
            $new_input['l_page_searchbar_bottompadding_px'] = $this->theme_settings_defaults['l_page_searchbar_bottompadding_px'];



        if($existed == true)
        {
            $new_input['l_page_searchbar_opacity_px'] = $presetting_values['l_page_searchbar_opacity_px'];
        }
        else if (isset($input['l_page_searchbar_opacity_px'])) {
            $new_input['l_page_searchbar_opacity_px'] = $input['l_page_searchbar_opacity_px'];
        }
        else
            $new_input['l_page_searchbar_opacity_px'] = $this->theme_settings_defaults['l_page_searchbar_opacity_px'];

        if($existed == true)
        {
            $new_input['l_page_subheader_text_px'] = $presetting_values['l_page_subheader_text_px'];
        }
        else if (isset($input['l_page_subheader_text_px'])) {
            $new_input['l_page_subheader_text_px'] = $input['l_page_subheader_text_px'];
        }
        else
            $new_input['l_page_subheader_text_px'] = $this->theme_settings_defaults['l_page_subheader_text_px'];

        if($existed == true)
        {
            $new_input['top_bar_logo_use'] = $presetting_values['top_bar_logo_use'];
        }
        else if (isset($input['top_bar_logo_use'])) {
            $new_input['top_bar_logo_use'] = $input['top_bar_logo_use'];
        } else {
            $new_input['top_bar_logo_use'] = 0;
        }

        if($existed == true)
        {
            $new_input['box_bar_logo_use'] = $presetting_values['box_bar_logo_use'];
        }
        else if (isset($input['box_bar_logo_use'])) {
            $new_input['box_bar_logo_use'] = $input['box_bar_logo_use'];
        } else {
            $new_input['box_bar_logo_use'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_center_logo_use'] = $presetting_values['l_page_center_logo_use'];
        }
        else if (isset($input['l_page_center_logo_use'])) {
            $new_input['l_page_center_logo_use'] = $input['l_page_center_logo_use'];
        } else {
            $new_input['l_page_center_logo_use'] =  0;
        }

        if($existed == true)
        {
            $new_input['l_page_center_logo_topsapce'] = $presetting_values['l_page_center_logo_topsapce'];
        }
        else if (isset($input['l_page_center_logo_topsapce'])) {
            $new_input['l_page_center_logo_topsapce'] = $input['l_page_center_logo_topsapce'];
        } else {
            $new_input['l_page_center_logo_topsapce'] =   $this->theme_settings_defaults['l_page_center_logo_topsapce'];
        }

        if($existed == true)
        {
            $new_input['search_page_background_image_use'] = $presetting_values['search_page_background_image_use'];

        }
        else if (isset($input['search_page_background_image_use'])) {
            $new_input['search_page_background_image_use'] = $input['search_page_background_image_use'];
        } else {
            $new_input['search_page_background_image_use'] = 0;
        }

	if($existed == true)
        {
            $new_input['search_page_background_image_repeat'] = $presetting_values['search_page_background_image_repeat'];

        }
        else if (isset($input['search_page_background_image_repeat'])) {
            $new_input['search_page_background_image_repeat'] = $input['search_page_background_image_repeat'];
        } else {
            $new_input['search_page_background_image_repeat'] = 0;
        }

	if($existed == true)
        {
            $new_input['l_page_background_image_repeat'] = $presetting_values['l_page_background_image_repeat'];

        }
        else if (isset($input['l_page_background_image_repeat'])) {
            $new_input['l_page_background_image_repeat'] = $input['l_page_background_image_repeat'];
        } else {
            $new_input['l_page_background_image_repeat'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_background_image_use'] = $presetting_values['l_page_background_image_use'];
        }
        else if (isset($input['l_page_background_image_use'])) {
            $new_input['l_page_background_image_use'] = $input['l_page_background_image_use'];
        } else {
            $new_input['l_page_background_image_use'] = 0;
        }



        //my add
        if (isset($input['chiz_flag'])) {
            $new_input['chiz_flag'] = trim($input['chiz_flag']);
        } else {
            $new_input['chiz_flag'] = 0;
        }


        if($existed == true){
            $new_input['l_page_landing_page_align'] = $presetting_values['l_page_landing_page_align'];
        }
        else if (isset($input['l_page_landing_page_align'])) {
            $new_input['l_page_landing_page_align'] = $input['l_page_landing_page_align'];
        } else {
            $new_input['l_page_landing_page_align'] = 0;
        }

        if($existed == true){
            $new_input['1_page_landing_page_body_size'] = $presetting_values['1_page_landing_page_body_size'];
        }
        else if (isset($input['1_page_landing_page_body_size'])) {
            $new_input['1_page_landing_page_body_size'] = $input['1_page_landing_page_body_size'];
        } else {
            $new_input['1_page_landing_page_body_size'] = 10;
        }
        
        if($existed == true){
            $new_input['landing_page_context_width'] = $presetting_values['landing_page_context_width'];
        }
        else if (isset($input['landing_page_context_width'])) {
            $new_input['landing_page_context_width'] = $input['landing_page_context_width'];
        } else {
            $new_input['landing_page_context_width'] = 10;
        }

        if($existed == true)
        {
            $new_input['l_page_landing_headertxt_enable'] = $presetting_values['l_page_landing_headertxt_enable'];
        }
        else if (isset($input['l_page_landing_headertxt_enable'])) {
            $new_input['l_page_landing_headertxt_enable'] = $input['l_page_landing_headertxt_enable'];
        } else {
            $new_input['l_page_landing_headertxt_enable'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_landing_subheading_enable'] = $presetting_values['l_page_landing_subheading_enable'];
        }
        else if (isset($input['l_page_landing_subheading_enable'])) {
            $new_input['l_page_landing_subheading_enable'] = $input['l_page_landing_subheading_enable'];
        } else {
            $new_input['l_page_landing_subheading_enable'] = 0;
        }

        if($existed == true)
        {
            $new_input['header_text_enable'] = $presetting_values['header_text_enable'];
        }
        else if (isset($input['header_text_enable'])) {
            $new_input['header_text_enable'] = $input['header_text_enable'];
        } else {
            $new_input['header_text_enable'] = 0;
        }

        

        if($existed == true)
        {
            $new_input['l_page_background_video_use'] = $presetting_values['l_page_background_video_use'];
        }
        else if (isset($input['l_page_background_video_use'])) {
            $new_input['l_page_background_video_use'] = $input['l_page_background_video_use'];
        } else {
            $new_input['l_page_background_video_use'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_background_video_pattern_enable'] = $presetting_values['l_page_background_video_pattern_enable'];
        }
        else if (isset($input['l_page_background_video_pattern_enable'])) {
            $new_input['l_page_background_video_pattern_enable'] = $input['l_page_background_video_pattern_enable'];
        } else {
            $new_input['l_page_background_video_pattern_enable'] = 0;
        }

        if($existed == true)
        {
            $new_input['l_page_background_video_pattern'] = $presetting_values['l_page_background_video_pattern'];
        }
        else if (isset($input['l_page_background_video_pattern'])) {
            $new_input['l_page_background_video_pattern'] = $input['l_page_background_video_pattern'];
        } else {
            $new_input['l_page_background_video_pattern'] = "dotted-1.png";
        }
	//PATTERN SERCH PAGE
	if($existed == true)
        {
            $new_input['search_page_background_pattern_enable'] = $presetting_values['search_page_background_pattern_enable'];
        }
        else if (isset($input['search_page_background_pattern_enable'])) {
            $new_input['search_page_background_pattern_enable'] = $input['search_page_background_pattern_enable'];
        } else {
            $new_input['search_page_background_pattern_enable'] = 0;
        }

        if($existed == true)
        {
            $new_input['search_page_background_pattern'] = $presetting_values['search_page_background_pattern'];
        }
        else if (isset($input['search_page_background_pattern'])) {
            $new_input['search_page_background_pattern'] = $input['search_page_background_pattern'];
        } else {
            $new_input['search_page_background_pattern'] = "dotted-1.png";
        }

        if($existed == true)
        {
            $new_input['banner_image_use'] = $presetting_values['banner_image_use'];
        }
        else if (isset($input['banner_image_use'])) {
            $new_input['banner_image_use'] = $input['banner_image_use'];
        } else {
            $new_input['banner_image_use'] = 0;
        }

        if($existed == true)
        {
            $new_input['banner_code_use'] = $presetting_values['banner_code_use'];
        }
        else if (isset($input['banner_code_use'])) {
            $new_input['banner_code_use'] = $input['banner_code_use'];
        } else {
            $new_input['banner_code_use'] = 0;
        }

        if($existed == true)
        {
            $new_input['revolution_slider_code_use'] = $presetting_values['revolution_slider_code_use'];
        }
        else if (isset($input['revolution_slider_code_use'])) {
            $new_input['revolution_slider_code_use'] = $input['revolution_slider_code_use'];
        } else {
            $new_input['revolution_slider_code_use'] = 0;
        }


        if (isset($input['l_page_preset'])) {
            $presetName		=	$input['l_page_preset'];
            $presetThumb	=	$input['l_page_preset_thumbnail'];

            if($presetName != "")
            {
                $new_settings	=	@get_option('presets');
                if(!is_array($new_settings))
                {
                    $new_settings	=	array();
                    $new_settings['presets']['default']	=	get_option('streamzon_theme_settings_option');
                }
                $new_settings['presets'][$presetName] =	(array)$new_input;

                update_option( "presets", $new_settings );

                if(isset($_POST['submit_settingsall'])) {
                    //


                }
            }
            else
            {

            }
        } else {
            $new_input['l_page_preset'] = 0;
        }

        if(isset($_POST['submit_settingsall']) && !$presetName) {
            $current_preset = get_option('preset_name');            
            $all_presets = get_option('presets');   

            //$new_input['l_page_preset_thumbnail'] = $_POST['l_page_preset_thumbnail_1'];            
            $all_presets['presets'][$current_preset] = $new_input;
            $all_presets['presets'][$current_preset]['l_page_preset'] = $current_preset;
            update_option('presets', $all_presets);

        }
        if(isset($_POST['import_settings']) && $_FILES['import_file']['name'] != '')
            return $options;

        return $new_input;
    }
    /**
     * end of theme design elements
     */



    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize_credentials($input)
    {
        $new_input = array();

        if (isset($input['amazon_associate_id'])) {
            $new_input['amazon_associate_id'] = trim($input['amazon_associate_id']);
        }
        if (isset($input['amazon_associate_id_ca'])) {
            $new_input['amazon_associate_id_ca'] = trim($input['amazon_associate_id_ca']);
        }
        if (isset($input['amazon_associate_id_uk'])) {
            $new_input['amazon_associate_id_uk'] = trim($input['amazon_associate_id_uk']);
        }
        if (isset($input['amazon_associate_id_de'])) {
            $new_input['amazon_associate_id_de'] = trim($input['amazon_associate_id_de']);
        }
        if (isset($input['amazon_associate_id_fr'])) {
            $new_input['amazon_associate_id_fr'] = trim($input['amazon_associate_id_fr']);
        }
        if (isset($input['amazon_associate_id_in'])) {
            $new_input['amazon_associate_id_in'] = trim($input['amazon_associate_id_in']);
        }
        if (isset($input['amazon_associate_id_it'])) {
            $new_input['amazon_associate_id_it'] = trim($input['amazon_associate_id_it']);
        }
        if (isset($input['amazon_associate_id_es'])) {
            $new_input['amazon_associate_id_es'] = trim($input['amazon_associate_id_es']);
        }
        if (isset($input['amazon_associate_id_jp'])) {
            $new_input['amazon_associate_id_jp'] = trim($input['amazon_associate_id_jp']);
        }
        if (isset($input['amazon_associate_id_cn'])) {
            $new_input['amazon_associate_id_cn'] = trim($input['amazon_associate_id_cn']);
        }
        if (isset($input['amazon_associate_auto'])) {
            $new_input['amazon_associate_auto'] = trim($input['amazon_associate_auto']);
        }

        if($existed == true)
        {
            $new_input['bitly_login_id'] = @trim($input['bitly_login_id']);
        }
        else if (isset($input['bitly_login_id'])) {
            $new_input['bitly_login_id'] = @$input['bitly_login_id'];
        } else {
            $new_input['bitly_login_id'] = '';
        }


        if($existed == true)
        {
            $new_input['bitly_api_key'] = @trim($input['bitly_api_key']);
        }
        else if (isset($input['bitly_api_key'])) {
            $new_input['bitly_api_key'] = @$input['bitly_api_key'];
        } else {
            $new_input['bitly_api_key'] = '';
        }

        if (isset($input['access_key_id'])) {
            $new_input['access_key_id'] = trim($input['access_key_id']);
        }        

        if (isset($input['access_key_id_2'])) {
            $new_input['access_key_id_2'] = trim($input['access_key_id_2']);
        }

        if (isset($input['secret_access_key'])) {
            $new_input['secret_access_key'] = trim($input['secret_access_key']);
        }

        if (isset($input['secret_access_key_2'])) {
            $new_input['secret_access_key_2'] = trim($input['secret_access_key_2']);
        }

        if (isset($input['high_traffic_site'])) {
            $new_input['high_traffic_site'] = trim($input['high_traffic_site']);
        }

        return $new_input;
    }

    public function sanitize_stores_settings($input)
    {
        $new_input = array();

        if (isset($input['amazon_category'])) {
            $new_input['amazon_category'] = $input['amazon_category'];
        } else {
            $new_input['amazon_category'] = array('');
        }

        if (isset($input['amazon_uk_category'])) {
            $new_input['amazon_uk_category'] = $input['amazon_uk_category'];
        } else {
            $new_input['amazon_uk_category'] = array('');
        }

        if (isset($input['amazon_ca_category'])) $new_input['amazon_ca_category'] = $input['amazon_ca_category'];
        else $new_input['amazon_ca_category'] = array('');

        if (isset($input['amazon_de_category'])) $new_input['amazon_de_category'] = $input['amazon_de_category'];
        else $new_input['amazon_de_category'] = array('');

        if (isset($input['amazon_fr_category'])) $new_input['amazon_fr_category'] = $input['amazon_fr_category'];
        else $new_input['amazon_fr_category'] = array('');

        if (isset($input['amazon_in_category'])) $new_input['amazon_in_category'] = $input['amazon_in_category'];
        else $new_input['amazon_in_category'] = array('');

        if (isset($input['amazon_it_category'])) $new_input['amazon_it_category'] = $input['amazon_it_category'];
        else $new_input['amazon_it_category'] = array('');

        if (isset($input['amazon_es_category'])) $new_input['amazon_es_category'] = $input['amazon_es_category'];
        else $new_input['amazon_es_category'] = array('');

        if (isset($input['amazon_jp_category'])) $new_input['amazon_jp_category'] = $input['amazon_jp_category'];
        else $new_input['amazon_jp_category'] = array('');

        if (isset($input['amazon_cn_category'])) $new_input['amazon_cn_category'] = $input['amazon_cn_category'];
        else $new_input['amazon_cn_category'] = array('');

        return $new_input;

    }
    public function sanitize_settings($input)
    {
        $new_input = array();

        if (isset($input['amazon_market'])) {
            $new_input['amazon_market'] = trim($input['amazon_market']);
        }

/**************************************/
        if($existed == true)
        {
            $new_input['Product_buy_anable'] = $presetting_values['Product_buy_anable'];
        }
        else if (isset($input['Product_buy_anable'])) {
            $new_input['Product_buy_anable'] = $input['Product_buy_anable'];
        } else {
            $new_input['Product_buy_anable'] = 0;
        }
        if($existed == true)
        {
            $new_input['Product_cart_anable'] = $presetting_values['Product_cart_anable'];
        }
        else if (isset($input['Product_cart_anable'])) {
            $new_input['Product_cart_anable'] = $input['Product_cart_anable'];
        } else {
            $new_input['Product_cart_anable'] = 0;
        }
        /***************************************/
        // additional search parameter
        if (isset($input['amazon_additional_search_parameter'])) {
            $new_input['amazon_additional_search_parameter'] = trim($input['amazon_additional_search_parameter']);
        }        

  

        if (isset($input['amazon_additional_price_start'])) {
            $new_input['amazon_additional_price_start'] = trim($input['amazon_additional_price_start']);
        }

        if (isset($input['amazon_additional_price_step'])) {
            $new_input['amazon_additional_price_step'] = trim($input['amazon_additional_price_step']);
        }

        if (isset($input['amazon_additional_price_max'])) {
            $new_input['amazon_additional_price_max'] = trim($input['amazon_additional_price_max']);
        }

        if ($new_input['amazon_additional_price_step'] == '0') {
            $new_input['amazon_additional_price_step'] = '1';
        }

        if ($new_input['amazon_additional_price_max'] < $new_input['amazon_additional_price_start']) {
            $new_input['amazon_additional_price_max'] = $new_input['amazon_additional_price_start'] + 1;
        }
        // end additional search parameter

	//Facebook pixel
	if($existed == true)
        {
            $new_input['search_page_facebook_pixel_enable'] = $presetting_values['search_page_facebook_pixel_enable'];
        }
        else if (isset($input['search_page_facebook_pixel_enable'])) {
            $new_input['search_page_facebook_pixel_enable'] = $input['search_page_facebook_pixel_enable'];
        } else {
            $new_input['search_page_facebook_pixel_enable'] = 0;
        }

	if($existed == true)
        {
            $new_input['search_page_facebook_pixel_code'] = $presetting_values['search_page_facebook_pixel_code'];
        }
        else if (isset($input['search_page_facebook_pixel_code'])) {
            $new_input['search_page_facebook_pixel_code'] = $input['search_page_facebook_pixel_code'];
        }
        else {
            $new_input['search_page_facebook_pixel_code'] = "";
        }

	if($existed == true)
        {
            $new_input['land_page_facebook_pixel_enable'] = $presetting_values['land_page_facebook_pixel_enable'];
        }
        else if (isset($input['land_page_facebook_pixel_enable'])) {
            $new_input['land_page_facebook_pixel_enable'] = $input['land_page_facebook_pixel_enable'];
        } else {
            $new_input['land_page_facebook_pixel_enable'] = 0;
        }

	if($existed == true)
        {
            $new_input['land_page_facebook_pixel_code'] = $presetting_values['land_page_facebook_pixel_code'];
        }
        else if (isset($input['land_page_facebook_pixel_code'])) {
            $new_input['land_page_facebook_pixel_code'] = $input['land_page_facebook_pixel_code'];
        }
        else {
            $new_input['land_page_facebook_pixel_code'] = "";
        }
	//End Facebook Pixel
	//SEO
	
	if (isset($input['seo_home_title'])) {
            $new_input['seo_home_title'] = $input['seo_home_title'];
        }
	if (isset($input['seo_home_description'])) {
            $new_input['seo_home_description'] = $input['seo_home_description'];
        }
	if (isset($input['seo_home_keywords'])) {
            $new_input['seo_home_keywords'] = $input['seo_home_keywords'];
        }

	if (isset($input['seo_search_title'])) {
            $new_input['seo_search_title'] = $input['seo_search_title'];
        }
	if (isset($input['seo_search_description'])) {
            $new_input['seo_search_description'] = $input['seo_search_description'];
        }
	if (isset($input['seo_search_keywords'])) {
            $new_input['seo_search_keywords'] = $input['seo_search_keywords'];
        }
	
	if (isset($input['seo_product_title'])) {
            $new_input['seo_product_title'] = $input['seo_product_title'];
        }
	if (isset($input['seo_product_description'])) {
            $new_input['seo_product_description'] = $input['seo_product_description'];
        }
	if (isset($input['seo_product_keywords'])) {
            $new_input['seo_product_keywords'] = $input['seo_product_keywords'];
        }
	
	//END SEO
        if (isset($input['default_search_keyword'])) {
            $new_input['default_search_keyword'] = $input['default_search_keyword'];
        }

        if (isset($input['amazon_paid_free'])) {
            $new_input['amazon_paid_free'] = trim($input['amazon_paid_free']);
        } else {
            $new_input['amazon_paid_free'] = 0;
        }

        if (isset($input['amazon_discount'])) {
            $new_input['amazon_discount'] = trim($input['amazon_discount']);
        } else {
            $new_input['amazon_discount'] = 0;
        }

        if (isset($input['amazon_discount_percent'])) {
            $new_input['amazon_discount_percent'] = trim($input['amazon_discount_percent']);
        } else {
            $new_input['amazon_discount_percent'] = 50;
        }

        if (isset($input['amazon_flag'])) {
            $new_input['amazon_flag'] = trim($input['amazon_flag']);
        } else {
            $new_input['amazon_flag'] = 0;
        }

        if (isset($input['amazon_discount_amount'])) {
            $new_input['amazon_discount_amount'] = trim($input['amazon_discount_amount']);
        } else {
            $new_input['amazon_discount_amount'] = 0;
        }

        if (isset($input['amazon_add2Cart'])) {
            $new_input['amazon_add2Cart'] = trim($input['amazon_add2Cart']);
        } else {
            $new_input['amazon_add2Cart'] = 0;
        }

        if (isset($input['amazon_addsingPage'])) {
            $new_input['amazon_addsingPage'] = trim($input['amazon_addsingPage']);
        } else {
            $new_input['amazon_addsingPage'] = 0;
        }
        
        if (isset($input['show_sidebar'])) {
            $new_input['show_sidebar'] = trim($input['show_sidebar']);
        } else {
            $new_input['show_sidebar'] = 0;
        }    
        
        if (isset($input['l_page_landing_page_enable'])) {
            $new_input['l_page_landing_page_enable'] = trim($input['l_page_landing_page_enable']);
        } else {
            $new_input['l_page_landing_page_enable'] = 0;
        }            
                   
        if (isset($input['l_page_onepage_landing'])) {
            $new_input['l_page_onepage_landing'] = trim($input['l_page_onepage_landing']);
        } else {
            $new_input['l_page_onepage_landing'] = 0;
        }            
           
        
        return $new_input;
    }

    public function sanitize_item_settings($input)
    {
        $new_input = array();

        if (isset($input['amazon_item_attributes'])) {
            $new_input['amazon_item_attributes'] = $input['amazon_item_attributes'];
        } else {
            $new_input['amazon_item_attributes'] = array();
        }

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        //print 'Enter your settings below:';
        //print_r($this->amazon_credentials);
    }

    /**
     * Print the Section text
     */
    public function theme_settings_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->theme_settings);
    }

    /**
     * Print the Section text
     */
    public function theme_settings_1_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->theme_settings);
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_associate_id_callback()
    {
        printf(
            '<label for="amazon_associate_id">Amazon Associate ID</label>
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/United-States.png" title="United States"/></div>
			<input data-country="US" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id" name="streamzon_amazon_credentials_option[amazon_associate_id]" value="%s" /> <a href="https://affiliate-program.amazon.com/" target="_blank" data-country="us">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id'] ) ? $this->amazon_credentials['amazon_associate_id'] : ''
        );
    }
    public function amazon_associate_id_ca_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/Canada.png"  title="Canada"/>	</div>
			<input data-country="CA" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_ca" name="streamzon_amazon_credentials_option[amazon_associate_id_ca]" value="%s" /> <a href="https://associates.amazon.ca/" target="_blank" data-country="ca">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_ca'] ) ? $this->amazon_credentials['amazon_associate_id_ca'] : ''
        );
    }
    public function amazon_associate_id_uk_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/United-Kingdom.png" title="United-Kingdom" />	</div>
			<input data-country="UK" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_uk" name="streamzon_amazon_credentials_option[amazon_associate_id_uk]" value="%s" /> <a href="https://affiliate-program.amazon.co.uk/" target="_blank" data-country="uk">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_uk'] ) ? $this->amazon_credentials['amazon_associate_id_uk'] : ''
        );
    }
    public function amazon_associate_id_de_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/Germany.png" title="Germany"/>	</div>
			<input data-country="DE" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_de" name="streamzon_amazon_credentials_option[amazon_associate_id_de]" value="%s" /> <a href="https://partnernet.amazon.de/" target="_blank" data-country="de">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_de'] ) ? $this->amazon_credentials['amazon_associate_id_de'] : ''
        );
    }
    public function amazon_associate_id_fr_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/France.png" title="France"/>	</div>
			<input data-country="FR" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_fr" name="streamzon_amazon_credentials_option[amazon_associate_id_fr]" value="%s" /> <a href="https://partenaires.amazon.fr/" target="_blank" data-country="fr">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_fr'] ) ? $this->amazon_credentials['amazon_associate_id_fr'] : ''
        );
    }
    public function amazon_associate_id_in_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/India.png" title="India"/>	</div>
			<input data-country="IN" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_in" name="streamzon_amazon_credentials_option[amazon_associate_id_in]" value="%s" /> <a href="https://affiliate-program.amazon.in/" target="_blank" data-country="in">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_in'] ) ? $this->amazon_credentials['amazon_associate_id_in'] : ''
        );
    }
    public function amazon_associate_id_it_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/Italy.png" title="Italy"/>	</div>
			<input data-country="IT" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_it" name="streamzon_amazon_credentials_option[amazon_associate_id_it]" value="%s" /> <a href="https://programma-affiliazione.amazon.it/" target="_blank" data-country="it">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_it'] ) ? $this->amazon_credentials['amazon_associate_id_it'] : ''
        );
    }
    public function amazon_associate_id_es_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/Spain.png" title="Spain"/>	</div>
			<input data-country="ES" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_es" name="streamzon_amazon_credentials_option[amazon_associate_id_es]" value="%s" /> <a href="https://afiliados.amazon.es/" target="_blank" data-country="es">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_es'] ) ? $this->amazon_credentials['amazon_associate_id_es'] : ''
        );
    }
    public function amazon_associate_id_jp_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/Japan.png" title="Japan"/>	</div>
			<input data-country="JP" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_jp" name="streamzon_amazon_credentials_option[amazon_associate_id_jp]" value="%s" /> <a href="https://affiliate.amazon.co.jp/" target="_blank" data-country="jp">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_jp'] ) ? $this->amazon_credentials['amazon_associate_id_jp'] : ''
        );
    }
    public function amazon_associate_id_cn_callback()
    {
        printf('
			<div class="flag"><img src="'.get_stylesheet_directory_uri().'/img/flags/China.png" title="China"/>	</div>
			<input data-country="CN" class="regular-text" style="width:240px;" type="text" id="amazon_associate_id_cn" name="streamzon_amazon_credentials_option[amazon_associate_id_cn]" value="%s" /> <a href="https://associates.amazon.cn/" target="_blank" data-country="cn">?</a>',
            isset( $this->amazon_credentials['amazon_associate_id_cn'] ) ? $this->amazon_credentials['amazon_associate_id_cn'] : ''
        );
    }

    public function amazon_associate_auto_callback()
    {
        $amazon_markets = array(
            'US' => 'US',
            'CA' => 'CA',
            'CN' => 'CN',
            'DE' => 'DE',
            'ES' => 'ES',
            'FR' => 'FR',
            'IN' => 'IN',
            'IT' => 'IT',
            'JP' => 'JP',
            'GB' => 'UK',
        );


        ?>
        <select name="streamzon_amazon_credentials_option[amazon_associate_auto]" id="amazon_associate_auto" >
            <?php
            $selected  = $this->amazon_credentials['amazon_associate_auto'];
			//print "<option value=''>Select</option>";
            foreach($amazon_markets as $key => $value)
            {
                $select = ($selected == $key) ? "selected" : "" ;
                if($key == 'US' && $this->amazon_credentials['amazon_associate_id'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'CA' && $this->amazon_credentials['amazon_associate_id_ca'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'CN' && $this->amazon_credentials['amazon_associate_id_cn'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'DE' && $this->amazon_credentials['amazon_associate_id_de'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'ES' && $this->amazon_credentials['amazon_associate_id_es'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'FR' && $this->amazon_credentials['amazon_associate_id_fr'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'IN' && $this->amazon_credentials['amazon_associate_id_in'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'IT' && $this->amazon_credentials['amazon_associate_id_it'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'JP' && $this->amazon_credentials['amazon_associate_id_jp'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
                else if($key == 'GB' && $this->amazon_credentials['amazon_associate_id_uk'])
                    print "<option value='".$key."' ".$select.">".$value." Listings</option>";
            }
            ?>
        </select>
        <p>* Please fill the appropriate country Associate Id to visible in the above dropdown box.</p>
    <?php
    }

    public function bitly_login_id_callback()
    {
        printf(
            '<label for="streamzon_bitly_login_id">Bitly Login ID</label>
            <input placeholder="Ex: Your Bitly login ID" class="regular-text" type="text" id="streamzon_bitly_login_id" name="streamzon_amazon_credentials_option[bitly_login_id]" value="%s" />',
            isset( $this->amazon_credentials['bitly_login_id'] ) ? $this->amazon_credentials['bitly_login_id'] : ''
        );

    }

    public function bitly_api_key_callback()
    {
        printf(
            '<label for="streamzon_bitly_api_key">Bitly API Key</label>
            <input placeholder="Ex: Your Bitly API Key" class="regular-text" type="text" id="streamzon_bitly_api_key" name="streamzon_amazon_credentials_option[bitly_api_key]" value="%s" /><br><br>
			You can <a target="_blank" href="https://bitly.com/a/your_api_key">get your API key here</a>',
            isset( $this->amazon_credentials['bitly_api_key'] ) ? $this->amazon_credentials['bitly_api_key'] : ''
        );

    }


    /**
     * Get the settings option array and print one of its values Access Key ID
     */
    public function access_key_id_callback()
    {
        printf(
            '<label for="access_key_id">Access Key ID</label>
            <input class="regular-text" type="text" id="access_key_id" name="streamzon_amazon_credentials_option[access_key_id]" value="%s" />',
            isset( $this->amazon_credentials['access_key_id'] ) ? $this->amazon_credentials['access_key_id'] : ''
        );
    }    

    public function access_key_id_2_callback()
    {
        printf(

            '<div class="hide-high-traffic">
            <label for="access_key_id_2">Access Key ID2</label>
			<input class="regular-text" type="text" id="access_key_id_2" name="streamzon_amazon_credentials_option[access_key_id_2]" value="%s" />
            </div>',
            isset( $this->amazon_credentials['access_key_id_2'] ) ? $this->amazon_credentials['access_key_id_2'] : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function secret_access_key_callback()
    {
        printf(
            '<label for="access_key_id">Secret Key</label>
            <input class="regular-text" type="text" id="secret_access_key" name="streamzon_amazon_credentials_option[secret_access_key]" value="%s" />',
            isset( $this->amazon_credentials['secret_access_key'] ) ? $this->amazon_credentials['secret_access_key'] : ''
        );
    }

    public function secret_access_key_2_callback()
    {
        printf(
            '<div class="hide-high-traffic">
            <label for="secret_access_key_2">Secret Key 2</label>
			<input class="regular-text" type="text" id="secret_access_key_2" name="streamzon_amazon_credentials_option[secret_access_key_2]" value="%s" />
            </div>',
            isset( $this->amazon_credentials['secret_access_key_2'] ) ? $this->amazon_credentials['secret_access_key_2'] : ''
        );
    }

    public function high_traffic_site_callback(){
    ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Hight traffic site</span></legend>
            <label for="streamzon_high_traffic_site">
                <input type="checkbox" <?php checked('1', isset($this->amazon_credentials['high_traffic_site']) ? $this->amazon_credentials['high_traffic_site'] : ''); ?> value="1" id="streamzon_high_traffic_site" name="streamzon_amazon_credentials_option[high_traffic_site]">
                High Traffic Sites
            </label>
        </fieldset>
    <?php
    }


    /**
     * Print the Section text
     */
    public function amazon_settings_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->amazon_settings);
    }
    public function seo_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->seo_settings);
    }
    public function amazon_stores_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->amazon_settings);
    }
    /**
     * Get the settings option array and print one of its values
     */
    public function printr($arr)
    {
        print "<pre>";
        print_r($arr);
        print "</pre>";
    }
    public function amazon_market_callback()
    {
        /* ?>
         <label for="amazon_market">Amazon Market</label>
         <select disabled name="streamzon_amazon_settings_option[amazon_market]" id="streamzon_amazon_market">
             <option value="">Select Market</option>
             <?php foreach ($this->amazon_country_codes as $code => $url): ?>
                 <option <?php selected($code, $this->amazon_settings['amazon_market']); ?> value="<?php echo $code; ?>"><?php echo $url; ?></option>
             <?php endforeach; ?>
         </select>
         <?php
         */
    }

    /**
     * Get the settings option array and print one of its values
     */
    ///////////////////////////////////////////USA dropdown//////////////////////////////////////////////////////
    public function amazon_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id'])
        {?>
            <label for="amazon_category">Choose Amazon USA Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_market]" id="streamzon_amazon_market_us">
                <option value="com" selected>amazon.com</option>
            </select>
            <?php
            $c_market = "com";
            $c_category = isset($this->amazon_stores['amazon_category']) ? $this->amazon_stores['amazon_category'] : array();
            echo ''.$this->generate_amazon_us_category_selects($c_market, $c_category,'amazon_category');
        }
        else
            print "Please enter associate-id for Amazon.com";
    }
    public function generate_amazon_us_category_selects($c_market, array $c_category)
    {
        $html = '<ul id="streamzom_amazon_categories_list_us">';
        $html .= '<li>';
        $html .= $this->generate_amazon_us_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_us_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_us_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_category][]" id="streamzom_amazon_category_us_0" class="streamzom_amazon_category_us selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_us_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_category][]" id="streamzom_amazon_category_us_' . $index_number . '" class="streamzom_amazon_category_us selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////USA dropdown//////////////////////////////////////////////////////

    ///////////////////////////////////////////Uk dropdown//////////////////////////////////////////////////////
    public function amazon_uk_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_uk'])
        {?>
            <label for="amazon_category">Choose Amazon UK Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_uk_market]" id="streamzon_amazon_market_uk">
                <option value="co.uk" selected>amazon.co.uk</option>
            </select>
            <?php
            $c_market = "co.uk";
            $c_category = isset($this->amazon_stores['amazon_uk_category']) ? $this->amazon_stores['amazon_uk_category'] : array();
            echo $this->generate_amazon_uk_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.co.uk Market";

    }
    public function generate_amazon_uk_category_selects($c_market, array $c_category)
    {
        $html = '<ul id="streamzom_amazon_categories_list_uk">';
        $html .= '<li>';
        $html .= $this->generate_amazon_uk_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_uk_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_uk_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_uk_category][]" id="streamzom_amazon_category_uk_0" class="streamzom_amazon_category_uk selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_uk_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_uk_category][]" id="streamzom_amazon_category_uk_' . $index_number . '" class="streamzom_amazon_category_uk selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////Uk dropdown//////////////////////////////////////////////////////

    ///////////////////////////////////////////ca dropdown//////////////////////////////////////////////////////
    public function amazon_ca_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_ca'])
        {?>
            <label for="amazon_category">Choose Amazon ca Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_ca_market]" id="streamzon_amazon_market_ca">
                <option value="ca" selected>amazon.ca</option>
            </select>
            <?php
            $c_market = "ca";
            $c_category = isset($this->amazon_stores['amazon_ca_category']) ? $this->amazon_stores['amazon_ca_category'] : array();
            echo $this->generate_amazon_ca_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.ca Market";

    }
    public function generate_amazon_ca_category_selects($c_market, array $c_category)
    {
        $html = '<ul id="streamzom_amazon_categories_list_ca">';
        $html .= '<li>';
        $html .= $this->generate_amazon_ca_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_ca_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_ca_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_ca_category][]" id="streamzom_amazon_category_ca_0" class="streamzom_amazon_category_ca selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_ca_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_ca_category][]" id="streamzom_amazon_category_ca_' . $index_number . '" class="streamzom_amazon_category_ca selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////ca dropdown//////////////////////////////////////////////////////

    ///////////////////////////////////////////de dropdown//////////////////////////////////////////////////////
    public function amazon_de_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_de'])
        {?>
            <label for="amazon_category">Choose Amazon DE Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_de_market]" id="streamzon_amazon_market_de">
                <option value="de" selected>amazon.de</option>
            </select>
            <?php
            $c_market = "de";
            $c_category = isset($this->amazon_stores['amazon_de_category']) ? $this->amazon_stores['amazon_de_category'] : array();
            echo $this->generate_amazon_de_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.de Market";

    }
    public function generate_amazon_de_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_de">';
        $html .= '<li>';
        $html .= $this->generate_amazon_de_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_de_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_de_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_de_category][]" id="streamzom_amazon_category_de_0" class="streamzom_amazon_category_de selectbox selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_de_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_de_category][]" id="streamzom_amazon_category_de_' . $index_number . '" class="streamzom_amazon_category_de selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////de dropdown//////////////////////////////////////////////////////

    ///////////////////////////////////////////fr dropdown//////////////////////////////////////////////////////
    public function amazon_fr_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_fr'])
        {?>
            <label for="amazon_category">Choose Amazon fr Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_fr_market]" id="streamzon_amazon_market_fr">
                <option value="fr" selected>amazon.fr</option>
            </select>
            <?php
            $c_market = "fr";
            $c_category = isset($this->amazon_stores['amazon_fr_category']) ? $this->amazon_stores['amazon_fr_category'] : array();
            echo $this->generate_amazon_fr_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.fr Market";

    }
    public function generate_amazon_fr_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_fr">';
        $html .= '<li>';
        $html .= $this->generate_amazon_fr_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_fr_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_fr_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_fr_category][]" id="streamzom_amazon_category_fr_0" class="streamzom_amazon_category_fr selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_fr_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_fr_category][]" id="streamzom_amazon_category_fr_' . $index_number . '" class="streamzom_amazon_category_fr selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////fr dropdown//////////////////////////////////////////////////////

    ///////////////////////////////////////////in dropdown//////////////////////////////////////////////////////
    public function amazon_in_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_in'])
        {?>
            <label for="amazon_category">Choose Amazon in Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_in_market]" id="streamzon_amazon_market_in">
                <option value="in" selected>amazon.in</option>
            </select>
            <?php
            $c_market = "in";
            $c_category = isset($this->amazon_stores['amazon_in_category']) ? $this->amazon_stores['amazon_in_category'] : array();
            echo $this->generate_amazon_in_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.in Market";

    }
    public function generate_amazon_in_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_in">';
        $html .= '<li>';
        $html .= $this->generate_amazon_in_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_in_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_in_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_in_category][]" id="streamzom_amazon_category_in_0" class="streamzom_amazon_category_in selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_in_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_in_category][]" id="streamzom_amazon_category_in_' . $index_number . '" class="streamzom_amazon_category_in selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////in dropdown//////////////////////////////////////////////////////


    ///////////////////////////////////////////it dropdown//////////////////////////////////////////////////////
    public function amazon_it_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_it'])
        {?>
            <label for="amazon_category">Choose Amazon it Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_it_market]" id="streamzon_amazon_market_it">
                <option value="it" selected>amazon.it</option>
            </select>
            <?php
            $c_market = "it";
            $c_category = isset($this->amazon_stores['amazon_it_category']) ? $this->amazon_stores['amazon_it_category'] : array();
            echo $this->generate_amazon_it_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.it Market";

    }
    public function generate_amazon_it_category_selects($c_market, array $c_category)
    {
        $html = '<ul id="streamzom_amazon_categories_list_it">';
        $html .= '<li>';
        $html .= $this->generate_amazon_it_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_it_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_it_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_it_category][]" id="streamzom_amazon_category_it_0" class="streamzom_amazon_category_it selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_it_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_it_category][]" id="streamzom_amazon_category_it_' . $index_number . '" class="streamzom_amazon_category_it selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////it dropdown//////////////////////////////////////////////////////
    ///////////////////////////////////////////es dropdown//////////////////////////////////////////////////////
    public function amazon_es_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_es'])
        {?>
            <label for="amazon_category">Choose Amazon es Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_es_market]" id="streamzon_amazon_market_es">
                <option value="es" selected>amazon.es</option>
            </select>
            <?php
            $c_market = "es";
            $c_category = isset($this->amazon_stores['amazon_es_category']) ? $this->amazon_stores['amazon_es_category'] : array();
            echo $this->generate_amazon_es_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.es Market";

    }
    public function generate_amazon_es_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_es">';
        $html .= '<li>';
        $html .= $this->generate_amazon_es_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_es_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_es_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_es_category][]" id="streamzom_amazon_category_es_0" class="streamzom_amazon_category_es selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_es_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_es_category][]" id="streamzom_amazon_category_es_' . $index_number . '" class="streamzom_amazon_category_es selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////es dropdown//////////////////////////////////////////////////////
    ///////////////////////////////////////////jp dropdown//////////////////////////////////////////////////////
    public function amazon_jp_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_jp'])
        {?>
            <label for="amazon_category">Choose Amazon jp Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_jp_market]" id="streamzon_amazon_market_jp">
                <option value="co.jp" selected>amazon.co.jp</option>
            </select>
            <?php
            $c_market = "co.jp";
            $c_category = isset($this->amazon_stores['amazon_jp_category']) ? $this->amazon_stores['amazon_jp_category'] : array();
            echo $this->generate_amazon_jp_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.jp Market";

    }
    public function generate_amazon_jp_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_jp">';
        $html .= '<li>';
        $html .= $this->generate_amazon_jp_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_jp_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_jp_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_jp_category][]" id="streamzom_amazon_category_jp_0" class="streamzom_amazon_category_jp selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_jp_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_jp_category][]" id="streamzom_amazon_category_jp_' . $index_number . '" class="streamzom_amazon_category_jp selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////jp dropdown//////////////////////////////////////////////////////
    ///////////////////////////////////////////cn dropdown//////////////////////////////////////////////////////
    public function amazon_cn_category_callback()
    {
        if($this->amazon_credentials['amazon_associate_id_cn'])
        {?>
            <label for="amazon_category">Choose Amazon cn Categories to show</label>
            <select  style="visibility: hidden" name="streamzon_amazon_stores_option[amazon_cn_market]" id="streamzon_amazon_market_cn">
                <option value="cn" selected>amazon.cn</option>
            </select>
            <?php
            $c_market = "cn";
            $c_category = isset($this->amazon_stores['amazon_cn_category']) ? $this->amazon_stores['amazon_cn_category'] : array();
            echo $this->generate_amazon_cn_category_selects($c_market, $c_category);
        }
        else
            print "Please enter associate-id for Amazon.cn Market";

    }
    public function generate_amazon_cn_category_selects($c_market, array $c_category)
    {

        $html = '<ul id="streamzom_amazon_categories_list_cn">';
        $html .= '<li>';
        $html .= $this->generate_amazon_cn_category_select($c_market, $c_category);
        $html .= '</ll>';
        if (!empty($c_category)) {
            foreach ($c_category as $k => $v) {
                $category = $c_category[$k];
                $subcategory = isset($c_category[$k + 1]) ? $c_category[$k + 1] : '';
                $index_number = $k + 1;
                if ($category) {
                    $html .= '<li>';
                    $html .= $this->generate_amazon_cn_subcategory_select($c_market, $category, $subcategory, $index_number);
                    $html .= '</ll>';
                }
            }
        }
        $html .= '</ul>';
        return $html;
    }
    public function generate_amazon_cn_category_select($c_market, array $c_category, $only_options = false)
    {
        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled('', $c_market, false) . ' name="streamzon_amazon_stores_option[amazon_cn_category][]" id="streamzom_amazon_category_cn_0" class="streamzom_amazon_category_cn selectbox" data-category-index="0">';
        }
        $html .= '<option ' . selected(in_array('', $c_category), true, false) . ' value="">All</option>';
        if ($c_market) {
            $locales = $this->getAmazonLocales();
            $locale = $locales[$c_market];
            if (isset($this->amazon_browse_node_ids[$locale]) && is_array($this->amazon_browse_node_ids[$locale])) {
                foreach ($this->amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
                    if (is_array($node_titles)) {
                        foreach ($node_titles as $node_title) {
                            $html .= '<option ' . selected(in_array($node_id, $c_category), true, false) . ' value="' . $node_id . '">' . $node_title . '</option>';
                        }
                    }
                }
            }
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    public function generate_amazon_cn_subcategory_select($c_market, $c_category, $c_subcategory, $index_number, $only_options = false)
    {
        $subcategories = null;
        if ($c_category) {
            try {
                $subcategories = $this->load_amazon_subcategories($c_market, $c_category);
            } catch (Exception $e) {
                //return '<code>Error loading categories. ' . $e->getMessage() . '</code>';
            }
        }

        $html = '';
        if (!$only_options) {
            $html .= '<select ' . disabled(!$subcategories, true, false) . ' name="streamzon_amazon_stores_option[amazon_cn_category][]" id="streamzom_amazon_category_cn_' . $index_number . '" class="streamzom_amazon_category_cn selectbox" data-category-index="' . $index_number . '">';
        }
        if ($subcategories && is_array($subcategories)) {
            $html .= '<option ' . selected('', $c_subcategory, false) . ' value="">All</option>';
            foreach ($subcategories as $subcategory) {
                $subcat_id = $subcategory['BrowseNodeId'];
                $subcat_name = $subcategory['Name'];
                $html .= '<option ' . selected($subcat_id, $c_subcategory, false) . ' value="' . $subcat_id . '">' . $subcat_name . '</option>';
            }
        } else {
            $html .= '<option value="">No subcategories</option>';
        }
        if (!$only_options) {
            $html .= '</select>';
        }
        return $html;
    }
    ///////////////////////////////////////////cn dropdown//////////////////////////////////////////////////////

    /**
     * New Additional Search Parameter
     */
    public function amazon_additional_search_parameter_callback()
    {
        $onlfr = $this->amazon_settings['amazon_paid_free'];
        $AmAdSechPar = $this->amazon_settings['amazon_additional_search_parameter'];                
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Additional Search Parameter</span></legend>
            <p>
                <label><input type="radio" id="AmazonSearchByText"  <?php if('1' == $AmAdSechPar || $onlfr == '1') echo "checked='checked'"; ?> value="1" name="streamzon_amazon_settings_option[amazon_additional_search_parameter]">Default always new products</label>
          
                
                <label><input type="radio" id="AmazonSearchByPrice" <?php if('0' == $AmAdSechPar && $onlfr == '0') echo "checked='checked'"; ?> value="0" name="streamzon_amazon_settings_option[amazon_additional_search_parameter]">By price</label>

                <blockquote>
                    <label for="amazon_additional_price_start">Start</label>
                    $<input type="number" class="small-text" value="<?php echo $this->amazon_settings['amazon_additional_price_start']; ?>" id="streamzon_amazon_additional_price_start" min="0" step="1" name="streamzon_amazon_settings_option[amazon_additional_price_start]">
                    <p class="description">Specifies the starting price.<br><br></p>

                    <label for="amazon_additional_price_step">Step</label>
                    $<input type="number" class="small-text" value="<?php echo $this->amazon_settings['amazon_additional_price_step']; ?>" id="streamzon_amazon_additional_price_step" min="0.5" step="0.5" name="streamzon_amazon_settings_option[amazon_additional_price_step]">
                    <p class="description">Specifies value to increase the price range with on every new search.<br><br></p>

                    <label for="amazon_additional_price_step">Maximum</label>
                    $<input type="number" class="small-text" value="<?php echo $this->amazon_settings['amazon_additional_price_max']; ?>" id="streamzon_amazon_additional_price_max" min="0" step="1" name="streamzon_amazon_settings_option[amazon_additional_price_max]">
                    <p class="description">Specifies the maximum price to search for.<br><br></p>
                </blockquote>
            
            </p>
        </fieldset>
    <?php
    }

    public function default_search_keyword_callback()
    {
        printf(
            '<label for="streamzon_default_search_keyword">Default Search Keyword</label>
			<textarea col="2" row="2" class="regular-text" type="text" id="streamzon_default_search_keyword" name="streamzon_amazon_settings_option[default_search_keyword]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->amazon_settings['default_search_keyword'] ) ? $this->amazon_settings['default_search_keyword'] : '',
            'In case if landing page is disabled.'
        );
    }
	//SEO
    public function seo_home_title_callback()
    {
        printf(
            '<label for="streamzon_seo_home_title">Home title</label>
			<textarea col="2" row="2" maxlength="60" class="regular-text" type="text" id="streamzon_seo_home_title" name="streamzon_seo_settings_option[seo_home_title]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_home_title'] ) ? $this->seo_settings['seo_home_title'] : '',
            'Most search engines use a maximum of 60 chars'
        );
    }

    public function seo_home_description_callback()
    {
        printf(
            '<label for="streamzon_seo_home_description">Home Description</label>
			<textarea col="2" row="2" maxlength="160" class="regular-text" type="text" id="streamzon_seo_home_description" name="streamzon_seo_settings_option[seo_home_description]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_home_description'] ) ? $this->seo_settings['seo_home_description'] : '',
            'Most search engines use a maximum of 160 chars'
        );
    }

    public function seo_home_keywords_callback()
    {
        printf(
            '<label for="streamzon_seo_home_keywords">Home Keywords</label>
			<textarea col="2" row="2" maxlength="160" class="regular-text" type="text" id="streamzon_seo_home_keywords" name="streamzon_seo_settings_option[seo_home_keywords]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_home_keywords'] ) ? $this->seo_settings['seo_home_keywords'] : '',
            'Most search engines use a maximum of 160 chars (comma separated)'
        );
    }

    public function seo_search_title_callback()
    {
        printf(
            '<label for="streamzon_seo_search_title">Search Title</label>
			<textarea col="2" row="2" maxlength="60" class="regular-text" type="text" id="streamzon_seo_search_title" name="streamzon_seo_settings_option[seo_search_title]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_search_title'] ) ? $this->seo_settings['seo_search_title'] : '',
            'Most search engines use a maximum of 60 chars'
        );
    }
    public function seo_search_description_callback()
    {
        printf(
            '<label for="streamzon_seo_search_description">Search Description</label>
			<textarea col="2" row="2" maxlength="160" class="regular-text" type="text" id="streamzon_seo_search_description" name="streamzon_seo_settings_option[seo_search_description]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_search_description'] ) ? $this->seo_settings['seo_search_description'] : '',
            'Most search engines use a maximum of 160 chars'
        );
    }
    public function seo_search_keywords_callback()
    {
        printf(
            '<label for="streamzon_seo_search_keywords">Search Keywords</label>
			<textarea col="2" row="2" maxlength="160" class="regular-text" type="text" id="streamzon_seo_search_keywords" name="streamzon_seo_settings_option[seo_search_keywords]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_search_keywords'] ) ? $this->seo_settings['seo_search_keywords'] : '',
            'Most search engines use a maximum of 160 chars (comma separated)'
        );
    }

    public function seo_product_title_callback()
    {
        printf(
            '<label for="streamzon_seo_product_title">Product Title</label>
			<textarea col="2" row="2" maxlength="60" class="regular-text" type="text" id="streamzon_seo_product_title" name="streamzon_seo_settings_option[seo_product_title]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_product_title'] ) ? $this->seo_settings['seo_product_title'] : '',
            'Most search engines use a maximum of 60 chars'
        );
    }
    public function seo_product_description_callback()
    {
        printf(
            '<label for="streamzon_seo_product_description">Product Description</label>
			<textarea col="2" row="2"  maxlength="160" class="regular-text" type="text" id="streamzon_seo_product_description" name="streamzon_seo_settings_option[seo_product_description]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_product_description'] ) ? $this->seo_settings['seo_product_description'] : '',
            'Most search engines use a maximum of 160 chars'
        );
    }
    public function seo_product_keywords_callback()
    {
        printf(
            '<label for="streamzon_seo_product_keywords">Product Keywords</label>
			<textarea maxlength="160" class="regular-text" type="text" col="2" row="2" id="streamzon_seo_product_keywords" name="streamzon_seo_settings_option[seo_product_keywords]" >%s</textarea>
            <p class="description">%s</p>',
            isset( $this->seo_settings['seo_product_keywords'] ) ? $this->seo_settings['seo_product_keywords'] : '',
            'Most search engines use a maximum of 160 chars (comma separated)'
        );
    }
	
    //PIXEL
    public function search_page_facebook_pixel_code_callback()
    {
        printf(
            '<label for="streamzon_search_page_facebook_pixel_code">Facebook Pixel Code</label>
            <textarea class="large-text code" id="streamzon_search_page_facebook_pixel_code" cols="50" rows="5" name="streamzon_seo_settings_option[search_page_facebook_pixel_code]">%s</textarea>',
            isset( $this->seo_settings['search_page_facebook_pixel_code'] ) ? $this->seo_settings['search_page_facebook_pixel_code'] : ''
        );
    }
    
    /*********************************************************/
    public function product_buy_anable_callback()
    {        
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable cart</span></legend>
            <label for="streamzon_Product_buy_anable">
                <input type="checkbox" <?php if(isset($this->amazon_settings['Product_buy_anable'])){echo "st='".$this->amazon_settings['Product_buy_anable']."' ";}?> <?php checked('1', isset($this->amazon_settings['Product_buy_anable']) ? $this->amazon_settings['Product_buy_anable'] : ''); ?> value="1" id="streamzon_Product_buy_anable" name="streamzon_amazon_settings_option[Product_buy_anable]">
                Enable button Buy
            </label>
        </fieldset>
    <?php
    }
    public function product_cart_anable_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable cart</span></legend>
            <label for="streamzon_Product_cart_anable">
                <input type="checkbox" <?php checked('1', isset($this->amazon_settings['Product_cart_anable']) ? $this->amazon_settings['Product_cart_anable'] : ''); ?> value="1" id="streamzon_Product_cart_anable" name="streamzon_amazon_settings_option[Product_cart_anable]">
                Enable button Add to cart
            </label>
        </fieldset>
    <?php
    }
    
    public function small_cart_button_color_callback()
    {
        ?>
        <label for="streamzon_small_cart_button_color">Small cart button color</label>
        <input id="streamzon_small_cart_button_color" name="streamzon_theme_settings_option[small_cart_button_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['small_cart_button_color']) ? $this->theme_settings['small_cart_button_color'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }
	public function small_cart_button_background_callback()
    {
        ?>
        <label for="streamzon_small_cart_button_background">Small cart button background</label>
        <input id="streamzon_small_cart_button_background" name="streamzon_theme_settings_option[small_cart_button_background]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['small_cart_button_background']) ? $this->theme_settings['small_cart_button_background'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }
	public function add_to_cart_button_background_callback()
    {
        ?>
        <label for="streamzon_add_to_cart_button_background">add to cart button background</label>
        <input id="streamzon_add_to_cart_button_background" name="streamzon_theme_settings_option[add_to_cart_button_background]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['add_to_cart_button_background']) ? $this->theme_settings['add_to_cart_button_background'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }
	public function add_to_cart_button_background2_callback()
    {
        ?>
        <label for="streamzon_add_to_cart_button_background2">Add to cart button background 2</label>
        <input id="streamzon_add_to_cart_button_background2" name="streamzon_theme_settings_option[add_to_cart_button_background2]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['add_to_cart_button_background2']) ? $this->theme_settings['add_to_cart_button_background2'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }
	public function add_to_cart_button_color_callback()
    {
        ?>
        <label for="streamzon_add_to_cart_button_color">Add to cart button color</label>
        <input id="streamzon_add_to_cart_button_color" name="streamzon_theme_settings_option[add_to_cart_button_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['add_to_cart_button_color']) ? $this->theme_settings['add_to_cart_button_color'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }
	public function buyitnow_button_background_callback()
    {
        ?>
        <fieldset class="buitnow_set">
        <label for="streamzon_buyitnow_button_background">Buy it now button background</label>
        <input id="streamzon_buyitnow_button_background" name="streamzon_theme_settings_option[buyitnow_button_background]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['buyitnow_button_background']) ? $this->theme_settings['buyitnow_button_background'] : '' ; ?>" data-default-color="#FFFFFF" />
        </fieldset>
    <?php
    }
	public function buyitnow_button_background2_callback()
    {
        ?>
        <fieldset class="buitnow_set">
        <label for="streamzon_buyitnow_button_background2">Buy it now button background 2</label>
        <input id="streamzon_buyitnow_button_background2" name="streamzon_theme_settings_option[buyitnow_button_background2]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['buyitnow_button_background2']) ? $this->theme_settings['buyitnow_button_background2'] : '' ; ?>" data-default-color="#FFFFFF" />
        </fieldset>
    <?php
    }
	public function buyitnow_button_color_callback()
    {
        ?>
        <fieldset class="buitnow_set">
        <label for="streamzon_buyitnow_button_color">Buy it now button color</label>
        <input id="streamzon_buyitnow_button_color" name="streamzon_theme_settings_option[buyitnow_button_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['buyitnow_button_color']) ? $this->theme_settings['buyitnow_button_color'] : '' ; ?>" data-default-color="#FFFFFF" />
        </fieldset>
    <?php
    }
    /**************************************************************/

    public function search_pagepixel_enable_callback()
    {
        ?>
	<fieldset>
            <legend class="screen-reader-text"><span>Facebook Pixel enable</span></legend>
            <label for="streamzon_search_page_facebook_pixel_enable">
		<input type="checkbox" <?php checked('1', isset($this->seo_settings['search_page_facebook_pixel_enable']) ? $this->seo_settings['search_page_facebook_pixel_enable'] : ''); ?> value="1" id="streamzon_search_page_facebook_pixel_enable" name="streamzon_seo_settings_option[search_page_facebook_pixel_enable]">
                Facebook Pixel enable
            </label>
        </fieldset>
    <?php
    }

 
    public function land_page_facebook_pixel_code_callback()
    {
        printf(
            '<label for="streamzon_land_page_facebook_pixel_code">Facebook Pixel Code</label>
            <textarea class="large-text code" id="streamzon_land_page_facebook_pixel_code" cols="50" rows="5" name="streamzon_seo_settings_option[land_page_facebook_pixel_code]">%s</textarea>',
            isset( $this->seo_settings['land_page_facebook_pixel_code'] ) ? $this->seo_settings['land_page_facebook_pixel_code'] : ''
        );
    }

    public function land_page_facebook_pixel_enable_callback()
    {
        ?>
	<fieldset>
            <legend class="screen-reader-text"><span>Facebook Pixel enable</span></legend>
            <label for="streamzon_land_page_facebook_pixel_enable">
		<input type="checkbox" <?php checked('1', isset($this->seo_settings['land_page_facebook_pixel_enable']) ? $this->seo_settings['land_page_facebook_pixel_enable'] : ''); ?> value="1" id="streamzon_land_page_facebook_pixel_enable" name="streamzon_seo_settings_option[land_page_facebook_pixel_enable]">
                Facebook Pixel enable
            </label>
        </fieldset>
    <?php
    }
	//END SEO
    /*public function amazon_subcategory_callback()
    {
        $c_market = $this->amazon_settings['amazon_market'];
        $c_category = $this->amazon_settings['amazon_category'];
        $c_subcategory = isset($this->amazon_settings['amazon_subcategory']) ? $this->amazon_settings['amazon_subcategory'] : array();

        echo $this->generate_amazon_subcategory_select($c_market, $c_category, $c_subcategory);
    }*/

	

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_paid_free_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Free or Paid</span></legend>
            <p>
                <label><input type="radio" <?php checked('0', $this->amazon_settings['amazon_paid_free']); ?> value="0" name="streamzon_amazon_settings_option[amazon_paid_free]">Any</label><br>
                <label><input type="radio" <?php checked('1', $this->amazon_settings['amazon_paid_free']); ?> value="1" name="streamzon_amazon_settings_option[amazon_paid_free]">Free</label><br>
            </p>
        </fieldset>
    <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_discount_callback()
    {
        ?>

        <fieldset>
            <legend class="screen-reader-text"><span>Enable Discount Bar</span></legend>
            <label for="streamzon_amazon_discount">
                <input type="checkbox" <?php checked('1', $this->amazon_settings['amazon_discount']); ?> value="1" id="streamzon_amazon_discount" name="streamzon_amazon_settings_option[amazon_discount]">
                Enable Discount Bar
            </label>
        </fieldset><br>
        <p class="description"><b>"Only Free" option must be unchecked. Otherwise Discount Bars won't display / work</b></p>

    <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_discount_percent_callback()
    {
        ?>

        <fieldset id="discount">
            <legend class="screen-reader-text"><span>Discount %</span></legend>
            <label for="streamzon_amazon_discount_percent">
                <input type="number" max="100" min="0" value="<?php echo isset($this->amazon_settings['amazon_discount_percent']) ? $this->amazon_settings['amazon_discount_percent'] : 50; ?>" id="streamzon_amazon_discount_percent" name="streamzon_amazon_settings_option[amazon_discount_percent]">
                %
            </label>
            <p class="description"><b>Discount percents</b></p>
        </fieldset><br>
                
        <label id="only_free" style="display: inline-block;"><input type="checkbox" <? if($this->amazon_settings['amazon_paid_free']) echo 'checked="true"'. ' ch="1"'; ?> value="1" id="streamzon_amazon_paid_free" name="streamzon_amazon_settings_option[amazon_paid_free]">Only Free</label>

    <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_flag_callback()
    {
        ?>

        <fieldset>
            <legend class="screen-reader-text"><span>Disable flag</span></legend>
            <label for="streamzon_amazon_flag">
                <input type="checkbox" <?php checked('1', $this->amazon_settings['amazon_flag']); ?> value="1" id="streamzon_amazon_flag" name="streamzon_amazon_settings_option[amazon_flag]">
                Disable flag
            </label>
        </fieldset><br>
        <p class="description"><b>Show/hide flag image</b></p>

    <?php
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_add2Cart_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Add to cart / Detail page</span></legend>
            <label for="streamzon_amazon_add2Cart">
                <input type="checkbox" <?php checked('1', $this->amazon_settings['amazon_add2Cart']); ?> value="1" id="streamzon_amazon_add2Cart" name="streamzon_amazon_settings_option[amazon_add2Cart]">
                Enable Add to cart
            </label>
            <p>Enable "add 2 cart" when you click on product link or you can redirect it to detail product page </p>
        </fieldset>
    <?php
    }

    public function amazon_addsingPage_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Individual Page for each product</span></legend>
            <label for="streamzon_amazon_addsingPage">
                <input type="checkbox" <?php checked('1', $this->amazon_settings['amazon_addsingPage']); ?> value="1" id="streamzon_amazon_addsingPage" name="streamzon_amazon_settings_option[amazon_addsingPage]">
                Enable individual page
            </label>
            <p>Enable Details page to see more details about each product </p>
        </fieldset>
    <?php
    }

    /**
     * Print the Section text
     */
    public function amazon_item_settings_section_callback()
    {
        //print 'Enter your settings below:';
        //print_r($this->amazon_item_settings);
    }

    public function amazon_item_attributes_callback()
    {
        ?>
        <select multiple="multiple" name="streamzon_amazon_item_settings_option[amazon_item_attributes][]" id="streamzon_amazon_item_attributes">
            <option <?php selected(true, in_array('amazon_item_Image', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Image">Image</option>
            <option <?php selected(true, in_array('amazon_item_Title', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Title">Title</option>
            <option <?php selected(true, in_array('amazon_item_ListPrice', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_ListPrice">List Price</option>
            <option <?php selected(true, in_array('amazon_item_YourPrice', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_YourPrice">Your Price</option>
            <option <?php selected(true, in_array('amazon_item_Reviews', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Reviews">Reviews</option>
            <option <?php selected(true, in_array('amazon_item_ISBN', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_ISBN">ISBN</option>
            <option <?php selected(true, in_array('amazon_item_Author', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Author">Author</option>
            <option <?php selected(true, in_array('amazon_item_Binding', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Binding">Binding</option>
            <option <?php selected(true, in_array('amazon_item_Brand', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Brand">Brand</option>
            <option <?php selected(true, in_array('amazon_item_EAN', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_EAN">EAN</option>
            <option <?php selected(true, in_array('amazon_item_EANList', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_EANList">EAN List</option>
            <option <?php selected(true, in_array('amazon_item_Edition', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Edition">Edition</option>
            <option <?php selected(true, in_array('amazon_item_IsEligibleForTradeIn', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_IsEligibleForTradeIn">Eligible For Trade In</option>
            <option <?php selected(true, in_array('amazon_item_Language', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Language">Language</option>
            <option <?php selected(true, in_array('amazon_item_Manufacturer', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Manufacturer">Manufacturer</option>
            <option <?php selected(true, in_array('amazon_item_NumberOfItems', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_NumberOfItems">Number Of Items</option>
            <option <?php selected(true, in_array('amazon_item_NumberOfPages', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_NumberOfPages">Number Of Pages</option>
            <option <?php selected(true, in_array('amazon_item_ProductGroup', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_ProductGroup">Product Group</option>
            <option <?php selected(true, in_array('amazon_item_ProductTypeName', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_ProductTypeName">Product Type Name</option>
            <option <?php selected(true, in_array('amazon_item_PublicationDate', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_PublicationDate">Publication Date</option>
            <option <?php selected(true, in_array('amazon_item_Studio', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Studio">Studio</option>
            <option <?php selected(true, in_array('amazon_item_TradeInValue', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_TradeInValue">Trade In Value</option>
            <option <?php selected(true, in_array('amazon_item_CatalogNumberList', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_CatalogNumberList">Catalog Number List</option>
            <option <?php selected(true, in_array('amazon_item_Creator', $this->amazon_item_settings['amazon_item_attributes'])); ?> value="amazon_item_Creator">Creator</option>
        </select>
    <?php
    }


    public function top_bar_logo_callback()
    {
        echo '<label for="streamzon_top_bar_logo">Top Bar Logo (200x50)</label>';
        echo '<input id="streamzon_top_bar_logo" type="file" name="top_bar_logo" />';

        if (isset($this->theme_settings['top_bar_logo'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['top_bar_logo']}' alt='' /></div>";
        }?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use uploaded image</span></legend>
            <label for="streamzon_top_bar_logo_use">
                <input type="radio" <?php checked('1', isset($this->theme_settings['top_bar_logo_use']) ? $this->theme_settings['top_bar_logo_use'] : ''); ?> value="1" id="streamzon_top_bar_logo_use" name="streamzon_theme_settings_option[top_bar_logo_use]">
                Use uploaded image
            </label>
        </fieldset>

    <?php
    }

    public function banner_image_file_callback()
    {
        echo '<label for="streamzon_banner_image_file">Banner Image</label>';
        echo '<input id="streamzon_banner_image_file" type="file" name="banner_image_file" />';
        if (isset($this->theme_settings['banner_image_file'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['banner_image_file']}' alt='' /></div>";
        }
    }

    public function top_bar_color_callback()
    {
        ?>
        <label for="streamzon_top_bar_color">Color</label>
        <input id="streamzon_top_bar_color" name="streamzon_theme_settings_option[top_bar_color]" class="streamzon-color-picker" type="text" value="<?php echo $this->theme_settings['top_bar_color']; ?>" data-default-color="#212121" />
    <?php
    }

    //facebook
	
    public function facebook_pixel_search_callback()
    {
        printf(
            '<label for="streamzon_facebook_pixel_search">Facebook Pixel Code</label>
            <textarea class="large-text code" id="streamzon_facebook_pixel_search" cols="50" rows="5" name="streamzon_theme_settings_option[facebook_pixel_search]">%s</textarea>',
            isset( $this->theme_settings['facebook_pixel_search'] ) ? $this->theme_settings['facebook_pixel_search'] : ''
        );
    }

    public function l_facebooK_pixel_enable_callback()
    {
        ?>
	<fieldset>
            <legend class="screen-reader-text"><span>Facebook Pixel enable</span></legend>
            <label for="streamzon_l_facebook_pixel_enable">
		<input type="checkbox" <?php checked('1', isset($this->theme_settings['l_facebook_pixel_enable']) ? $this->theme_settings['l_facebook_pixel_enable'] : ''); ?> value="1" id="streamzon_l_facebook_pixel_enable" name="streamzon_theme_settings_option[l_facebook_pixel_enable]">
                Facebook Pixel enable
            </label>
        </fieldset>
    <?php
    }

    public function search_facebook_pixel_enable_callback()
    {
        
    }

    public function facebook_pixel_l_callback()
    {
	printf(
            '<label for="streamzon_facebook_pixel_l">Facebook Pixel Code</label>
            <textarea class="large-text code" id="streamzon_facebook_pixel_l" cols="50" rows="5" name="streamzon_theme_settings_option[facebook_pixel_l]">%s</textarea>',
            isset( $this->theme_settings['facebook_pixel_l'] ) ? $this->theme_settings['facebook_pixel_l'] : ''
        );
    }
    
    
    //end facebook

    public function show_headersidebar_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Show Header Discount Bar</span></legend>
            <label for="streamzon_show_headersidebar">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['show_headersidebar']) ? $this->theme_settings['show_headersidebar'] : ''); ?> value="1" id="streamzon_show_headersidebar" name="streamzon_theme_settings_option[show_headersidebar]">
                Show Header Discount Bar
            </label>
        </fieldset>
    <?php
    }

    public function show_sidebar_callback()
    {            
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Show Sidebar</span></legend>
            <label for="streamzon_show_sidebar">
                <input type="checkbox" <?php checked('1', isset($this->amazon_settings['show_sidebar']) ? $this->amazon_settings['show_sidebar'] : ''); ?> value="1" id="streamzon_show_sidebar" name="streamzon_amazon_settings_option[show_sidebar]">
                Show Sidebar
            </label>
        </fieldset>
    <?php
    }

    public function header_menu_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Show Header Menu</span></legend>
            <label for="streamzon_header_menu">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['header_menu']) ? $this->theme_settings['header_menu'] : ''); ?> value="1" id="streamzon_header_menu" name="streamzon_theme_settings_option[header_menu]">
                Show Header Menu
            </label>
        </fieldset>
    <?php
    }

    public function search_page_background_callback()
    {
        ?>
        <label for="streamzon_search_page_background">Page Background</label>
        <input id="streamzon_search_page_background" name="streamzon_theme_settings_option[search_page_background]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['search_page_background']) ? $this->theme_settings['search_page_background'] : ''; ?>" data-default-color="#F5F5F5" />
    <?php
    }

    public function boxes_color_callback()
    {
        ?>
        <label for="streamzon_boxes_color">Boxes Color</label>
        <input id="streamzon_boxes_color" name="streamzon_theme_settings_option[boxes_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['boxes_color']) ? $this->theme_settings['boxes_color'] : '' ; ?>" data-default-color="#FFFFFF" />
    <?php
    }

    public function box_text_color_callback()
    {
        ?>
        <label for="streamzon_box_text_color">Box Text Color</label>
        <input name="streamzon_theme_settings_option[box_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['box_text_color']) ? $this->theme_settings['box_text_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
	public function box_text_url_color_callback()
    {
        ?>
        <label for="streamzon_box_text_url_color">Box url Color</label>
        <input id="streamzon_box_text_url_color" name="streamzon_theme_settings_option[box_text_url_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['box_text_url_color']) ? $this->theme_settings['box_text_url_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }

    public function box_bar_background_callback()
    {
        ?>
        <label for="streamzon_box_bar_background">Bar of Boxes (Background Color)</label>
        <input id="streamzon_box_bar_background" name="streamzon_theme_settings_option[box_bar_background]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['box_bar_background']) ? $this->theme_settings['box_bar_background'] : ''; ?>" data-default-color="#FF9900" />
    <?php
    }

    public function box_background_text_color_callback()
    {
        ?>
        <label for="streamzon_box_background_text_color">Background Text Color</label>
        <input id="streamzon_box_background_text_color" name="streamzon_theme_settings_option[box_background_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['box_background_text_color']) ? $this->theme_settings['box_background_text_color'] : ''; ?>" data-default-color="#CCC" />
    <?php
    }

    public function box_bar_logo_callback()
    {
        echo '<label for="streamzon_box_bar_logo">Bar of Boxes (Logo 128x38)</label>';
        echo '<input id="streamzon_box_bar_logo" type="file" name="box_bar_logo" />';
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use uploaded image</span></legend>
            <label for="streamzon_box_bar_logo_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['box_bar_logo_use']) ? $this->theme_settings['box_bar_logo_use'] : ''); ?> value="1" id="streamzon_box_bar_logo_use" name="streamzon_theme_settings_option[box_bar_logo_use]">
                Use uploaded image
            </label>
        </fieldset>
        <?php
        if (isset($this->theme_settings['box_bar_logo'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['box_bar_logo']}' alt='' /></div>";
        }
    }

    public function top_bar_opacity_callback()
    {
        printf(
            '<label for="streamzon_top_bar_opacity">Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_top_bar_opacity" name="streamzon_theme_settings_option[top_bar_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['top_bar_opacity'] ) ? $this->theme_settings['top_bar_opacity'] : 100
        );
    }

    public function search_page_background_opacity_callback()
    {
        printf(
            '<label for="streamzon_search_page_background_opacity">Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_search_page_background_opacity" name="streamzon_theme_settings_option[search_page_background_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['search_page_background_opacity'] ) ? $this->theme_settings['search_page_background_opacity'] : 100
        );
    }

    public function search_page_pattern_opacity_callback()
    {
        printf(
            '<label for="streamzon_search_page_pattern_opacity">Pattern Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_search_page_pattern_opacity" name="streamzon_theme_settings_option[search_page_pattern_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['search_page_pattern_opacity'] ) ? $this->theme_settings['search_page_pattern_opacity'] : 100
        );
    }

    public function l_page_background_opacity_callback()
    {
        printf(
            '<label for="streamzon_l_page_background_opacity">Image Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_l_page_background_opacity" name="streamzon_theme_settings_option[l_page_background_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['l_page_background_opacity'] ) ? $this->theme_settings['l_page_background_opacity'] : 100
        );
    }

    public function l_page_background_pattern_opacity_callback()
    {
        printf(
            '<label for="streamzon_l_page_background_pattern_opacity">Pattern Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_l_page_background_pattern_opacity" name="streamzon_theme_settings_option[l_page_background_pattern_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['l_page_background_pattern_opacity'] ) ? $this->theme_settings['l_page_background_pattern_opacity'] : 100
        );
    }

    public function l_page_background_video_opacity_callback()
    {
        printf(
            '<label for="streamzon_l_page_background_video_opacity">Video Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_l_page_background_video_opacity" name="streamzon_theme_settings_option[l_page_background_video_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['l_page_background_video_opacity'] ) ? $this->theme_settings['l_page_background_video_opacity'] : 100
        );
    }

    public function boxes_opacity_callback()
    {
        printf(
            '<label for="streamzon_boxes_opacity">Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_boxes_opacity" name="streamzon_theme_settings_option[boxes_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['boxes_opacity'] ) ? $this->theme_settings['boxes_opacity'] : 100
        );
    }

    public function searchbar_opacity_callback()
    {
        printf(
            '<label for="streamzon_searchbar_opacity">Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_searchbar_opacity" name="streamzon_theme_settings_option[searchbar_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['searchbar_opacity'] ) ? $this->theme_settings['searchbar_opacity'] : 100
        );
    }

    public function header_text_callback()
    {
        printf(
            '<label for="streamzon_header_text">Header Text</label>
            <input class="regular-text" type="text" id="streamzon_header_text" name="streamzon_theme_settings_option[header_text]" value="%s" />',
            isset( $this->theme_settings['header_text'] ) ? $this->theme_settings['header_text'] : 'header text'
        );
        ?>
        <?php add_thickbox(); ?>
        <div id="thick_header_text" style="display:none;"  >
            <h2>Header Text in Other languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
								<div style="width:200px;">	
									<label for="streamzon_header_text_'.$val.'" >Header Text '.$val.'</label>
								</div>
								<div style="width:400px;">
				            		<input form="theme_options_block" class="regular-text" type="text" id="streamzon_header_text_'.$val.'" name="streamzon_theme_settings_option[header_text_'.$val.']" value="%s" />
								</div>
							</div>',
                    isset( $this->theme_settings['header_text_'.$val] ) ? $this->theme_settings['header_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=thick_header_text" title="Change Text for Other languages" class="button thickbox">Add Header text for other languages</a>

    <?php
    }


    public function header_text_font_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['header_text_font_family']) ? $this->theme_settings['header_text_font_family'] : '' ;

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_header_text_font_family">Font Family</label>
        <select id="streamzon_header_text_font_family"  name="streamzon_theme_settings_option[header_text_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
							    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
							</optgroup>";
            }
            ?>
        </select>
    <?php

    }

    public function l_page_background_video_id_callback()
    {
        printf(
            '<label for="streamzon_l_page_background_video_id">YouTube video ID</label>
            <input class="regular-text" type="text" id="streamzon_l_page_background_video_id" name="streamzon_theme_settings_option[l_page_background_video_id]" value="%s" />',
            isset( $this->theme_settings['l_page_background_video_id'] ) ? $this->theme_settings['l_page_background_video_id'] : ''
        );
    }

    public function banner_image_link_callback()
    {
        printf(
            '<label for="streamzon_banner_image_link">Banner Link</label>
            <input class="regular-text" type="text" id="streamzon_banner_image_link" name="streamzon_theme_settings_option[banner_image_link]" value="%s" />',
            isset( $this->theme_settings['banner_image_link'] ) ? $this->theme_settings['banner_image_link'] : ''
        );
    }

    public function banner_code_callback()
    {
        printf(
            '<label for="streamzon_banner_code">Banner Code</label>
            <textarea class="large-text code" id="streamzon_banner_code" cols="50" rows="5" name="streamzon_theme_settings_option[banner_code]">%s</textarea>',
            isset( $this->theme_settings['banner_code'] ) ? $this->theme_settings['banner_code'] : ''
        );
    }

    public function revolution_slider_code_callback()
    {
        printf(
            '<label for="streamzon_revolution_slider_code">Revolution Slider Code</label>
            <textarea class="large-text code" id="streamzon_revolution_slider_code" cols="50" rows="5" name="streamzon_theme_settings_option[revolution_slider_code]">%s</textarea>',
            isset( $this->theme_settings['revolution_slider_code'] ) ? $this->theme_settings['revolution_slider_code'] : ''
        );
	?>
	<?php add_thickbox(); ?>
        <div id="my-content-id2" style="display:none;"  >
            <h2>Revolution Slider in Other languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
			<div style="width:200px;">	
				<label for="streamzon_revolution_slider_code_'.$val.'" >Revolution Slider Code '.$val.'</label>
			</div>
			<div style="width:400px;">
	            		<textarea rows="2" cols="55" form="theme_options_block" class="regular-text" type="text" id="streamzon_revolution_slider_code_'.$val.'" name="streamzon_theme_settings_option[revolution_slider_code_'.$val.']" >%s</textarea>
			</div>
		    </div>',
                    isset( $this->theme_settings['revolution_slider_code_'.$val] ) ? $this->theme_settings['revolution_slider_code_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=my-content-id2" title="Change Text for Other languages" class="button thickbox">Add Revolution Slider for other languages</a>
	<?php
    }

    public function header_text_px_callback()
    {
        printf(
            '<label for="streamzon_header_text_px">Header Text Size</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_header_text_px" name="streamzon_theme_settings_option[header_text_px]" value="%s" /> px',
            isset( $this->theme_settings['header_text_px'] ) ? $this->theme_settings['header_text_px'] : ''
        );
    }

    public function header_text_color_callback()
    {
        ?>
        <label for="streamzon_header_text_color">Header Text Color</label>
        <input id="streamzon_header_text_color" name="streamzon_theme_settings_option[header_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_text_color']) ? $this->theme_settings['header_text_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function header_text_bold_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Bold</span></legend>
            <label for="streamzon_header_text_bold">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['header_text_bold']) ? $this->theme_settings['header_text_bold'] : ''); ?> value="1" id="streamzon_header_text_bold" name="streamzon_theme_settings_option[header_text_bold]">
                Bold
            </label>
        </fieldset>
    <?php
    }

    public function l_page_header_bold_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Bold</span></legend>
            <label for="streamzon_l_page_header_bold">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_header_bold']) ? $this->theme_settings['l_page_header_bold'] : ''); ?> value="1" id="streamzon_l_page_header_bold" name="streamzon_theme_settings_option[l_page_header_bold]">
                Bold
            </label>
        </fieldset>
    <?php
    }

    public function l_page_subheader_bold_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Bold</span></legend>
            <label for="streamzon_l_page_subheader_bold">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_subheader_bold']) ? $this->theme_settings['l_page_subheader_bold'] : ''); ?> value="1" id="streamzon_l_page_subheader_bold" name="streamzon_theme_settings_option[l_page_subheader_bold]">
                Bold
            </label>
        </fieldset>
    <?php
    }

    ///////////////////////////seekbar////////////////////////////////
    public function l_page_discountbar_enable_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_l_page_discountbar_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_discountbar_enable']) ? $this->theme_settings['l_page_discountbar_enable'] : ''); ?> value="1" id="streamzon_l_page_discountbar_enable" name="streamzon_theme_settings_option[l_page_discountbar_enable]">
                Enable Discount Seekbar
            </label>
        </fieldset>
    <?php
    }
    public function l_page_discountbar_line_on_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_line_on_color">Discountbar Line Fill Color</label>
        <input id="streamzon_l_page_discountbar_line_on_color" name="streamzon_theme_settings_option[l_page_discountbar_line_on_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_line_on_color']) ? $this->theme_settings['l_page_discountbar_line_on_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function l_page_discountbar_line_off_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_line_off_color">Discountbar Line Background Color</label>
        <input id="streamzon_l_page_discountbar_line_off_color" name="streamzon_theme_settings_option[l_page_discountbar_line_off_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_line_off_color']) ? $this->theme_settings['l_page_discountbar_line_off_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function l_page_discountbar_ball_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_ball_color">Discountbar Handle Color (Ball)</label>
        <input id="streamzon_l_page_discountbar_ball_color" name="streamzon_theme_settings_option[l_page_discountbar_ball_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_ball_color']) ? $this->theme_settings['l_page_discountbar_ball_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_discountbar_display_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_display_color">Discountbar Scale Color</label>
        <input id="streamzon_l_page_discountbar_display_color" name="streamzon_theme_settings_option[l_page_discountbar_display_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_display_color']) ? $this->theme_settings['l_page_discountbar_display_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function l_page_discountbar_display_border_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_display_border_color">Discountbar Box Border Color</label>
        <input id="streamzon_l_page_discountbar_display_border_color" name="streamzon_theme_settings_option[l_page_discountbar_display_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_display_border_color']) ? $this->theme_settings['l_page_discountbar_display_border_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function l_page_discountbar_display_text_color_callback()
    {
        ?>
        <label for="streamzon_l_page_discountbar_display_text_color">Discountbar Box Text Color</label>
        <input id="streamzon_l_page_discountbar_display_text_color" name="streamzon_theme_settings_option[l_page_discountbar_display_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_discountbar_display_text_color']) ? $this->theme_settings['l_page_discountbar_display_text_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_discountbar_display_text_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['l_page_discountbar_display_text_family']) ? $this->theme_settings['l_page_discountbar_display_text_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_l_page_discountbar_display_text_family">Font Family</label>
        <select id="streamzon_l_page_discountbar_display_text_family"  name="streamzon_theme_settings_option[l_page_discountbar_display_text_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
						    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
						</optgroup>";
            }
            ?>
        </select>
    <?php
    }
    public function l_page_subheader_font_family_callback()
    {
        $fonts = self::$fonts;

        $value	=	isset($this->theme_settings['l_page_subheader_font_family']) ? $this->theme_settings['l_page_subheader_font_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_l_page_subheader_font_family">Font Family</label>
        <select id="streamzon_l_page_subheader_font_family"  name="streamzon_theme_settings_option[l_page_subheader_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
						    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
						</optgroup>";
            }
            ?>
        </select>
    <?php
    }

    public function l_page_discountbar_display_text_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_discountbar_display_text_px">Header Text Size</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_l_page_discountbar_display_text_px" name="streamzon_theme_settings_option[l_page_discountbar_display_text_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_discountbar_display_text_px'] ) ? $this->theme_settings['l_page_discountbar_display_text_px'] : ''
        );
    }
    public function l_page_subheader_font_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_subheader_font_px">Subheader Text Size</label>
            <input class="small-text" type="number" min="1" max="100" step="1" id="streamzon_l_page_subheader_font_px" name="streamzon_theme_settings_option[l_page_subheader_font_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_subheader_font_px'] ) ? $this->theme_settings['l_page_subheader_font_px'] : '14'
        );
    }

    public function l_page_discountbar_display_opacity_callback()
    {
        printf(
            '<label for="streamzon_l_page_discountbar_display_opacity">Discount Box Opacity</label>
            <input class="small-text" type="number" min="0" max="1" step="0.01" id="streamzon_l_page_discountbar_display_opacity" name="streamzon_theme_settings_option[l_page_discountbar_display_opacity]" value="%s" />',
            isset( $this->theme_settings['l_page_discountbar_display_opacity'] ) ? $this->theme_settings['l_page_discountbar_display_opacity'] : 0.70
        );
    }
    //////////////////////////seekbar end///////////////////////////
    ///////////////////////////header seekbar////////////////////////////////
    public function header_discountbar_enable_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_header_discountbar_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['header_discountbar_enable']) ? $this->theme_settings['header_discountbar_enable'] : ''); ?> value="1" id="streamzon_header_discountbar_enable" name="streamzon_theme_settings_option[header_discountbar_enable]">
                Enable Header Discount Seekbar
            </label>
        </fieldset>
    <?php
    }
    public function header_discountbar_line_on_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_line_on_color">Header Discountbar Line Fill Color</label>
        <input id="streamzon_header_discountbar_line_on_color" name="streamzon_theme_settings_option[header_discountbar_line_on_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_line_on_color']) ? $this->theme_settings['header_discountbar_line_on_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function header_discountbar_line_off_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_line_off_color">Header Discountbar Line Background Color</label>
        <input id="streamzon_header_discountbar_line_off_color" name="streamzon_theme_settings_option[header_discountbar_line_off_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_line_off_color']) ? $this->theme_settings['header_discountbar_line_off_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function header_discountbar_ball_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_ball_color">Header Discountbar Handle Color (Ball)</label>
        <input id="streamzon_header_discountbar_ball_color" name="streamzon_theme_settings_option[header_discountbar_ball_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_ball_color']) ? $this->theme_settings['header_discountbar_ball_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function header_discountbar_display_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_display_color">Header Discountbar Box Color</label>
        <input id="streamzon_header_discountbar_display_color" name="streamzon_theme_settings_option[header_discountbar_display_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_display_color']) ? $this->theme_settings['header_discountbar_display_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function header_discountbar_display_border_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_display_border_color">Header Discountbar Box Border Color</label>
        <input id="streamzon_header_discountbar_display_border_color" name="streamzon_theme_settings_option[header_discountbar_display_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_display_border_color']) ? $this->theme_settings['header_discountbar_display_border_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function header_discountbar_display_text_color_callback()
    {
        ?>
        <label for="streamzon_header_discountbar_display_text_color">Header Discountbar Box Color</label>
        <input id="streamzon_header_discountbar_display_text_color" name="streamzon_theme_settings_option[header_discountbar_display_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['header_discountbar_display_text_color']) ? $this->theme_settings['header_discountbar_display_text_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function header_discountbar_display_text_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	$this->theme_settings['header_discountbar_display_text_family'];

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_header_discountbar_display_text_family">Font Family</label>
        <select id="streamzon_header_discountbar_display_text_family"  name="streamzon_theme_settings_option[header_discountbar_display_text_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
						    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
						</optgroup>";
            }
            ?>
        </select>
    <?php
    }
    public function header_subheader_font_family_callback()
    {
        $fonts = self::$fonts;

        $value	=	isset($this->theme_settings['header_subheader_font_family']) ? $this->theme_settings['header_subheader_font_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_header_subheader_font_family">Font Family</label>
        <select id="streamzon_header_subheader_font_family"  name="streamzon_theme_settings_option[header_subheader_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
						    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
						</optgroup>";
            }
            ?>
        </select>
    <?php
    }

    public function header_discountbar_display_text_px_callback()
    {
        printf(
            '<label for="streamzon_header_discountbar_display_text_px">Header Text Size</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_header_discountbar_display_text_px" name="streamzon_theme_settings_option[header_discountbar_display_text_px]" value="%s" /> px',
            isset( $this->theme_settings['header_discountbar_display_text_px'] ) ? $this->theme_settings['header_discountbar_display_text_px'] : ''
        );
    }
    public function header_subheader_font_px_callback()
    {
        printf(
            '<label for="streamzon_header_subheader_font_px">Subheader Text Size</label>
            <input class="small-text" type="number" min="1" max="100" step="1" id="streamzon_header_subheader_font_px" name="streamzon_theme_settings_option[header_subheader_font_px]" value="%s" /> px',
            isset( $this->theme_settings['header_subheader_font_px'] ) ? $this->theme_settings['header_subheader_font_px'] : '14'
        );
    }

    public function header_discountbar_display_opacity_callback()
    {
        printf(
            '<label for="streamzon_header_discountbar_display_opacity">Discount Box Opacity</label>
            <input class="small-text" type="number" min="0" max="1" step="0.01" id="streamzon_header_discountbar_display_opacity" name="streamzon_theme_settings_option[header_discountbar_display_opacity]" value="%s" />',
            isset( $this->theme_settings['header_discountbar_display_opacity'] ) ? $this->theme_settings['header_discountbar_display_opacity'] : 0.70
        );
    }
    //////////////////////////header seekbar end///////////////////////////
    ///////////////////////////side seekbar////////////////////////////////
    public function side_discountbar_enable_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_side_discountbar_enable">
                <input disabled type="checkbox" <?php checked('1', isset($this->theme_settings['side_discountbar_enable']) ? $this->theme_settings['side_discountbar_enable'] : ''); ?> value="1" id="streamzon_side_discountbar_enable" name="streamzon_theme_settings_option[side_discountbar_enable]">
                To Disable Discount Seekbar <br>(remove it from widget panel<br>Admin > appearance > widgets)
            </label>
        </fieldset>
    <?php
    }
    public function side_discountbar_line_on_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_line_on_color">Discountbar Line Fill Color</label>
        <input id="streamzon_side_discountbar_line_on_color" name="streamzon_theme_settings_option[side_discountbar_line_on_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_line_on_color']) ? $this->theme_settings['side_discountbar_line_on_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function side_discountbar_line_off_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_line_off_color">Discountbar Line Background Color</label>
        <input id="streamzon_side_discountbar_line_off_color" name="streamzon_theme_settings_option[side_discountbar_line_off_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_line_off_color']) ? $this->theme_settings['side_discountbar_line_off_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function side_discountbar_ball_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_ball_color">Discountbar Handle Color (Ball)</label>
        <input id="streamzon_side_discountbar_ball_color" name="streamzon_theme_settings_option[side_discountbar_ball_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_ball_color']) ? $this->theme_settings['side_discountbar_ball_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function side_discountbar_display_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_display_color">Discountbar Box Text Color</label>
        <input id="streamzon_side_discountbar_display_color" name="streamzon_theme_settings_option[side_discountbar_display_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_display_color']) ? $this->theme_settings['side_discountbar_display_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function side_discountbar_display_border_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_display_border_color">Discountbar Box Border Color</label>
        <input id="streamzon_side_discountbar_display_border_color" name="streamzon_theme_settings_option[side_discountbar_display_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_display_border_color']) ? $this->theme_settings['side_discountbar_display_border_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function side_discountbar_display_text_color_callback()
    {
        ?>
        <label for="streamzon_side_discountbar_display_text_color">Discountbar Box Text Color</label>
        <input id="streamzon_side_discountbar_display_text_color" name="streamzon_theme_settings_option[side_discountbar_display_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['side_discountbar_display_text_color']) ? $this->theme_settings['side_discountbar_display_text_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }
    public function side_discountbar_display_text_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['side_discountbar_display_text_family']) ? $this->theme_settings['side_discountbar_display_text_family'] : '' ;

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>
        <label for="streamzon_side_discountbar_display_text_family">Font Family</label>
        <select id="streamzon_side_discountbar_display_text_family"  name="streamzon_theme_settings_option[side_discountbar_display_text_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";
                print "<optgroup style=\"font-family: ".$font[2].";\">
								<option value=".$font[0]." ".$selected.">".$font[1]."</option>
							</optgroup>";
            }
            ?>
        </select>
    <?php
    }
    public function side_subheader_font_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['side_subheader_font_family']) ? $this->theme_settings['side_subheader_font_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>
        <label for="streamzon_side_subheader_font_family">Font Family</label>
        <select id="streamzon_side_subheader_font_family"  name="streamzon_theme_settings_option[side_subheader_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";
                print "<optgroup style=\"font-family: ".$font[2].";\">
								<option value=".$font[0]." ".$selected.">".$font[1]."</option>
							</optgroup>";
            }
            ?>
        </select>
    <?php
    }
    public function side_discountbar_display_text_px_callback()
    {
        printf(
            '<label for="streamzon_side_discountbar_display_text_px">Header Text Size</label>
			<input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_side_discountbar_display_text_px" name="streamzon_theme_settings_option[side_discountbar_display_text_px]" value="%s" /> px',
            isset( $this->theme_settings['side_discountbar_display_text_px'] ) ? $this->theme_settings['side_discountbar_display_text_px'] : ''
        );
    }
    public function side_subheader_font_px_callback()
    {
        printf(
            '<label for="streamzon_side_subheader_font_px">Subheader Text Size</label>
			<input class="small-text" type="number" min="1" max="100" step="1" id="streamzon_side_subheader_font_px" name="streamzon_theme_settings_option[side_subheader_font_px]" value="%s" /> px',
            isset( $this->theme_settings['side_subheader_font_px'] ) ? $this->theme_settings['side_subheader_font_px'] : '14'
        );
    }
    public function side_discountbar_display_opacity_callback()
    {
        printf(
            '<label for="streamzon_side_discountbar_display_opacity">Discount Box Opacity</label>
			<input class="small-text" type="number" min="0" max="1" step="0.01" id="streamzon_side_discountbar_display_opacity" name="streamzon_theme_settings_option[side_discountbar_display_opacity]" value="%s" />',
            isset( $this->theme_settings['side_discountbar_display_opacity'] ) ? $this->theme_settings['side_discountbar_display_opacity'] : 0.70
        );
    }
    //////////////////////////seekbar end///////////////////////////



    public function searchbar_border_color_callback()
    {
        ?>
        <label for="streamzon_searchbar_border_color">Border Color</label>
        <input id="streamzon_searchbar_border_color" name="streamzon_theme_settings_option[searchbar_border_color]" class="streamzon-color-picker" type="text" value="<?php echo $this->theme_settings['searchbar_border_color']; ?>" data-default-color="#cacaca" />
    <?php
    }

    public function searchbar_text_color_callback()
    {
        ?>
        <label for="streamzon_searchbar_text_color">Text Color</label>
        <input id="streamzon_searchbar_text_color" name="streamzon_theme_settings_option[searchbar_text_color]" class="streamzon-color-picker" type="text" value="<?php echo $this->theme_settings['searchbar_text_color']; ?>" data-default-color="#000000" />
    <?php
    }

    public function searchbar_background_color_callback()
    {
        ?>
        <label for="streamzon_searchbar_background_color">Background Color</label>
        <input id="streamzon_searchbar_background_color" name="streamzon_theme_settings_option[searchbar_background_color]" class="streamzon-color-picker" type="text" value="<?php echo $this->theme_settings['searchbar_background_color']; ?>" data-default-color="#f5f5ee" />
    <?php
    }

    public function searchbar_image_callback()
    {
        ?>
        <label for="streamzon_searchbar_image">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['searchbar_image']) ? $this->theme_settings['searchbar_image'] : ''); ?> value="1" id="streamzon_searchbar_image" name="streamzon_theme_settings_option[searchbar_image]">
            Show search image (Magnifying Glass)
        </label>    <?php
    }

    public function slidebar_searchbar_image_callback()
    {
        ?>
        <label for="streamzon_slidebar_searchbar_image">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['slidebar_searchbar_image']) ? $this->theme_settings['slidebar_searchbar_image'] : ''); ?> value="1" id="streamzon_slidebar_searchbar_image" name="streamzon_theme_settings_option[slidebar_searchbar_image]">
            Show search image (Magnifying Glass)
        </label>    <?php
    }
	
    public function searchbar_top_enable_callback()
    {
        ?>
        <label for="streamzon_searchbar_top_enable">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['searchbar_top_enable']) ? $this->theme_settings['searchbar_top_enable'] : ''); ?> value="1" id="streamzon_searchbar_top_enable" name="streamzon_theme_settings_option[searchbar_top_enable]">
            Enable top search bar
        </label>    <?php
    }
    
    public function searchbar_topdiscount_enable_callback()
    {
        ?>
        <label for="streamzon_searchbar_topdiscount_enable">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['searchbar_topdiscount_enable']) ? $this->theme_settings['searchbar_topdiscount_enable'] : ''); ?> value="1" id="streamzon_searchbar_topdiscount_enable" name="streamzon_theme_settings_option[searchbar_topdiscount_enable]">
            Enable top discount
        </label>    <?php
    } 

    public function text_link_color_callback()
    {
        ?>
        <label for="streamzon_text_link_color">Links Color</label>
        <input id="streamzon_text_link_color" name="streamzon_theme_settings_option[text_link_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['text_link_color']) ? $this->theme_settings['text_link_color'] : ''; ?>" data-default-color="#c16b05" />
    <?php
    }

    public function sidebar_header_color_callback()
    {
        ?>
        <label for="streamzon_sidebar_header_color">Header Color</label>
        <input id="streamzon_sidebar_header_color" name="streamzon_theme_settings_option[sidebar_header_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['sidebar_header_color']) ? $this->theme_settings['sidebar_header_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
//
    public function page_search_button_color_callback()
    {
        ?>
        <label for="streamzon_page_search_button_color">Search background colorr</label>
        <input id="streamzon_page_search_button_color" name="streamzon_theme_settings_option[page_search_button_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['page_search_button_color']) ? $this->theme_settings['page_search_button_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
//
    public function page_search_button_hover_color_callback()
    {
        ?>
        <label for="streamzon_page_search_button_hover_color">Search button hover color</label>
        <input id="streamzon_page_search_button_hover_color" name="streamzon_theme_settings_option[page_search_button_hover_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['page_search_button_hover_color']) ? $this->theme_settings['page_search_button_hover_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
//
    public function page_search_button_text_color_callback()
    {
        ?>
        <label for="streamzon_page_search_button_text_color">Search text color</label>
        <input id="streamzon_page_search_button_text_color" name="streamzon_theme_settings_option[page_search_button_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['page_search_button_text_color']) ? $this->theme_settings['page_search_button_text_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
//
    public function page_search_button_text_border_color_callback()
    {
        ?>
        <label for="streamzon_page_search_button_text_border_color">Search border</label>
        <input id="streamzon_page_search_button_text_border_color" name="streamzon_theme_settings_option[page_search_button_text_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['page_search_button_text_border_color']) ? $this->theme_settings['page_search_button_text_border_color'] : ''; ?>" data-default-color="#000000" />
    <?php
    }
//
    public function page_search_button_text_opacity_px_callback()
    {
        printf(
            '<label for="streamzon_page_search_button_text_opacity_px">Search opacity</label>
            <input class="small-text" type="number" min="0" max="1" step="0.05" id="streamzon_page_search_button_text_opacity_px" name="streamzon_theme_settings_option[page_search_button_text_opacity_px]" value="%f" />',
            isset( $this->theme_settings['page_search_button_text_opacity_px'] ) ? $this->theme_settings['page_search_button_text_opacity_px'] : ''
        );
    }


    public function l_page_center_logo_callback()
    {
        echo '<label for="streamzon_l_page_center_logo">Center Logo</label>';
        echo '<input id="streamzon_l_page_center_logo" type="file" name="l_page_center_logo" />';
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use uploaded image</span></legend>
            <label for="streamzon_l_page_center_logo_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_center_logo_use']) ? $this->theme_settings['l_page_center_logo_use'] : ''); ?> value="1" id="streamzon_l_page_center_logo_use" name="streamzon_theme_settings_option[l_page_center_logo_use]">
                Use uploaded image
            </label>
        </fieldset>
        <?php
        if (isset($this->theme_settings['l_page_center_logo'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['l_page_center_logo']}' alt='' /></div>";
        }
    }

    public function l_page_center_logo_topsapce_callback()
    {

        printf(
            '<label for="streamzon_l_page_center_logo_topsapce">Top paddding</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_l_page_center_logo_topsapce" name="streamzon_theme_settings_option[l_page_center_logo_topsapce]" value="%s" /> px',
            isset( $this->theme_settings['l_page_center_logo_topsapce'] ) ? $this->theme_settings['l_page_center_logo_topsapce'] : ''
        );
    }

    public function search_page_background_image_callback()
    {
        echo '<label for="streamzon_search_page_background_image">Page Background Image</label>';
        echo '<input id="streamzon_search_page_background_image" type="file" name="search_page_background_image" />';
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use uploaded image</span></legend>
            <label for="streamzon_search_page_background_image_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['search_page_background_image_use']) ? $this->theme_settings['search_page_background_image_use'] : ''); ?> value="1" id="streamzon_search_page_background_image_use" name="streamzon_theme_settings_option[search_page_background_image_use]">
                Use uploaded image
            </label>
        </fieldset>
        <?php
        if (isset($this->theme_settings['search_page_background_image'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['search_page_background_image']}' alt='' /></div>";
        }
    }

    public function l_page_background_image_repeat_callback()
    {
	?>
        <label for="streamzon_l_page_background_image_repeat">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_background_image_repeat']) ? $this->theme_settings['l_page_background_image_repeat'] : ''); ?> value="1" id="streamzon_l_page_background_image_repeat" name="streamzon_theme_settings_option[l_page_background_image_repeat]">
            Enable repeat background
        </label>    <?php
    }

    public function search_page_background_image_repeat_callback()
    {
	?>
        <label for="streamzon_search_page_background_image_repeat">
            <input type="checkbox" <?php checked('1', isset($this->theme_settings['search_page_background_image_repeat']) ? $this->theme_settings['search_page_background_image_repeat'] : ''); ?> value="1" id="streamzon_search_page_background_image_repeat" name="streamzon_theme_settings_option[search_page_background_image_repeat]">
            Enable repeat background
        </label>    <?php
    }
	

    public function l_page_button_color_callback()
    {
        ?>
        <label for="streamzon_l_page_button_color">Button Color</label>
        <input id="streamzon_l_page_button_color" name="streamzon_theme_settings_option[l_page_button_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_button_color']) ? $this->theme_settings['l_page_button_color'] : ''; ?>" data-default-color="#FEDA71" />
    <?php
    }
    public function l_page_button_hover_color_callback()
    {
        ?>
        <label for="streamzon_l_page_button_hover_color">Button  Hover Color</label>
        <input id="streamzon_l_page_button_hover_color" name="streamzon_theme_settings_option[l_page_button_hover_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_button_hover_color']) ? $this->theme_settings['l_page_button_hover_color'] : ''; ?>" data-default-color="#FEDA71" />
    <?php
    }

    public function l_page_button_text_color_callback()
    {
        ?>
        <label for="streamzon_l_page_button_text_color">Button Text Color</label>
        <input id="streamzon_l_page_button_text_color" name="streamzon_theme_settings_option[l_page_button_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_button_text_color']) ? $this->theme_settings['l_page_button_text_color'] : ''; ?>" data-default-color="#996633" />
    <?php
    }

    public function l_page_button_text_opacity_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_button_text_opacity_px">Opacity</label>
            <input class="small-text" type="number" min="0" max="1" step="0.05" id="streamzon_l_page_button_text_opacity_px" name="streamzon_theme_settings_option[l_page_button_text_opacity_px]" value="%f" />',
            isset( $this->theme_settings['l_page_button_text_opacity_px'] ) ? $this->theme_settings['l_page_button_text_opacity_px'] : ''
        );
    }

    public function l_page_button_text_border_color_callback()
    {
        ?>
        <label for="streamzon_l_page_searchbar_border_color">Border Color</label>
        <input id="streamzon_l_page_button_text_border_color" name="streamzon_theme_settings_option[l_page_button_text_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_button_text_border_color']) ? $this->theme_settings['l_page_button_text_border_color'] : ''; ?>" data-default-color="#cccccc" />
    <?php
    }

    public function l_page_button_long_short_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_l_page_button_long_short">Landing Page Button Long / Short</label>
            <label for="streamzon_l_page_button_long">
                <input type="radio" <?php checked('1', isset($this->theme_settings['l_page_button_long_short']) ? $this->theme_settings['l_page_button_long_short'] : ''); ?> value="1" id="streamzon_l_page_button_long" name="streamzon_theme_settings_option[l_page_button_long_short]">
                Long
            </label>
            <label for="streamzon_l_page_button_short">
                <input type="radio" <?php checked('2', isset($this->theme_settings['l_page_button_long_short']) ? $this->theme_settings['l_page_button_long_short'] : ''); ?> value="2" id="streamzon_l_page_button_short" name="streamzon_theme_settings_option[l_page_button_long_short]">
                Short
            </label>
        </fieldset>
    <?php
    }



    public function l_page_button_search_gap_callback()
    {

        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Remove Gap b/w search field and search button</span></legend>
            <label for="streamzon_l_page_button_search_gap">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_button_search_gap']) ? $this->theme_settings['l_page_button_search_gap'] : ''); ?> value="1" id="streamzon_l_page_button_search_gap" name="streamzon_theme_settings_option[l_page_button_search_gap]">
                Remove Gap b/w search field and search button
            </label>
        </fieldset>
    <?php
    }

    public function l_page_searchbar_border_color_callback()
    {
        ?>
        <label for="streamzon_l_page_searchbar_border_color">Border Color</label>
        <input id="streamzon_l_page_searchbar_border_color" name="streamzon_theme_settings_option[l_page_searchbar_border_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_searchbar_border_color']) ? $this->theme_settings['l_page_searchbar_border_color'] : ''; ?>" data-default-color="#cccccc" />
    <?php
    }

    public function l_page_searchbar_background_color_callback()
    {
        ?>
        <label for="streamzon_l_page_searchbar_background_color">Background Color</label>
        <input id="streamzon_l_page_searchbar_background_color" name="streamzon_theme_settings_option[l_page_searchbar_background_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_searchbar_background_color']) ? $this->theme_settings['l_page_searchbar_background_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_searchbar_text_color_callback()
    {
        ?>
        <label for="streamzon_l_page_searchbar_text_color">Text Color</label>
        <input id="streamzon_l_page_searchbar_text_color" name="streamzon_theme_settings_option[l_page_searchbar_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_searchbar_text_color']) ? $this->theme_settings['l_page_searchbar_text_color'] : ''; ?>" data-default-color="#555555" />
    <?php
    }


    public function l_page_landing_headertxt_enable_callback()
    {

        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable Header Text</span></legend>
            <label for="streamzon_l_page_landing_headertxt_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_landing_headertxt_enable']) ? $this->theme_settings['l_page_landing_headertxt_enable'] : '');?> value="1" id="streamzon_l_page_landing_headertxt_enable" name="streamzon_theme_settings_option[l_page_landing_headertxt_enable]">
                Enable Header Text
            </label>
        </fieldset>
    <?php
    }

    public function l_page_landing_subheading_enable_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable Subheader Text</span></legend>
            <label for="streamzon_l_page_landing_subheading_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_landing_subheading_enable']) ? $this->theme_settings['l_page_landing_subheading_enable'] : '');?> value="1" id="streamzon_l_page_landing_subheading_enable" name="streamzon_theme_settings_option[l_page_landing_subheading_enable]">
                Enable Subheader Text
            </label>
        </fieldset>
    <?php
    }

    public function l_page_header_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_header_text">Header Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_header_text" name="streamzon_theme_settings_option[l_page_header_text]" value="%s" />',
            isset( $this->theme_settings['l_page_header_text'] ) ? $this->theme_settings['l_page_header_text'] : ''
        );
        ?>

        <?php add_thickbox(); ?>
        <div id="my-content-id" style="display:none;"  >
            <h2>Header Text in Other languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                //'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
					<div style="width:200px;">	
						<label for="streamzon_l_page_header_text_'.$val.'" >Header Text '.$val.'</label>
					</div>
					<div style="width:400px;">
	            		<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_header_text_'.$val.'" name="streamzon_theme_settings_option[l_page_header_text_'.$val.']" value="%s" />
					</div>
				</div>',
                    isset( $this->theme_settings['l_page_header_text_'.$val] ) ? $this->theme_settings['l_page_header_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=my-content-id" title="Change Text for Other languages" class="button thickbox">Add Header text for other languages</a>
    <?php
    }

    public function blank_callback(){}

    public function l_page_searchbar_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_searchbar_text">Placeholder Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_searchbar_text" name="streamzon_theme_settings_option[l_page_searchbar_text]" value="%s" />',
            isset( $this->theme_settings['l_page_searchbar_text'] ) ? $this->theme_settings['l_page_searchbar_text_USA'] : ''
        );
        ?>

        <?php add_thickbox(); ?>
        <div id="thick_1_searchbar" style="display:none;"  >
            <h2>Add searchbar text in Other languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
						<div style="width:200px;">	
							<label for="streamzon_l_page_searchbar_text_'.$val.'" >Search Bar Text '.$val.'</label>
						</div>
						<div style="width:400px;">
		            		<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_searchbar_text_'.$val.'" name="streamzon_theme_settings_option[l_page_searchbar_text_'.$val.']" value="%s" />
						</div>
					</div>',
                    isset( $this->theme_settings['l_page_searchbar_text_'.$val] ) ? $this->theme_settings['l_page_searchbar_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=thick_1_searchbar" title="Change Searchbar Text for Other languages" class="button thickbox">Add Searchbar text for other languages</a>
    <?php
    }

    public function l_page_button_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_button_text">Button Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_button_text" name="streamzon_theme_settings_option[l_page_button_text]" value="%s" />',
            isset( $this->theme_settings['l_page_button_text'] ) ? $this->theme_settings['l_page_button_text_USA'] : ''
        );
        ?>

        <?php add_thickbox(); ?>
        <div id="thick_1_button" style="display:none;"  >
            <h2>Add searchbar text in Other languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
						<div style="width:200px;">	
							<label for="streamzon_l_page_button_text_'.$val.'" >Search Bar Text '.$val.'</label>
						</div>
						<div style="width:400px;">
		            		<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_button_text_'.$val.'" name="streamzon_theme_settings_option[l_page_button_text_'.$val.']" value="%s" />
						</div>
					</div>',
                    isset( $this->theme_settings['l_page_button_text_'.$val] ) ? $this->theme_settings['l_page_button_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=thick_1_button" title="Add button text for Other languages" class="button thickbox">Add Button text for other languages</a>
    <?php
    }

    public function l_page_subheader_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_subheader_text">Subheader Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_subheader_text" name="streamzon_theme_settings_option[l_page_subheader_text]" value="%s" />',
            isset( $this->theme_settings['l_page_subheader_text'] ) ? $this->theme_settings['l_page_subheader_text_USA'] : ''
        );
        //new think
        ?>
        <?php add_thickbox(); ?>
        <div id="think_1_subheader" style="display:none;"  >
            <h2>Header Text in different languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
							<div style="width:200px;">	
								<label for="streamzon_l_page_subheader_text_'.$val.'" >Subheader Text '.$val.'</label>
							</div>
							<div style="width:400px;">
			           	<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_subheader_text_'.$val.'" name="streamzon_theme_settings_option[l_page_subheader_text_'.$val.']" value="%s" />
							</div>
						</div>',
                    isset( $this->theme_settings['l_page_subheader_text_'.$val] ) ? $this->theme_settings['l_page_subheader_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=think_1_subheader" title="Add Subheader text for other languages" class="button thickbox">Change Subheader text for other languages</a>

    <?php
    }

    public function l_page_footer_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_footer_text">Footer Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_footer_text" name="streamzon_theme_settings_option[l_page_footer_text]" value="%s" />',
            isset( $this->theme_settings['l_page_footer_text'] ) ? $this->theme_settings['l_page_footer_text_USA'] : ''
        );
        ?>
        <?php add_thickbox(); ?>
        <div id="think_1_pg_footer" style="display:none;"  >
            <h2>Header Text in different languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
							<div style="width:200px;">	
								<label for="streamzon_l_page_footer_text_'.$val.'" >Footer Text '.$val.'</label>
							</div>
							<div style="width:400px;">
			           	<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_footer_text_'.$val.'" name="streamzon_theme_settings_option[l_page_footer_text_'.$val.']" value="%s" />
							</div>
						</div>',
                    isset( $this->theme_settings['l_page_footer_text_'.$val] ) ? $this->theme_settings['l_page_footer_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=think_1_pg_footer" title="Add Footer text for other languages" class="button thickbox">Add Footer text for other languages</a>

    <?php
    }

    public function l_page_discountbar_display_text_callback()
    {
        printf(
            '<label for="streamzon_l_page_discountbar_display_text">Discount Text USA</label>
            <input class="regular-text" type="text" id="streamzon_l_page_discountbar_display_text" name="streamzon_theme_settings_option[l_page_discountbar_display_text]" value="%s" />',
            isset( $this->theme_settings['l_page_discountbar_display_text'] ) ? $this->theme_settings['l_page_discountbar_display_text_USA'] : ''
	    );
	
        ?>
        <?php add_thickbox(); ?>
        <div id="discount_txt_id" style="display:none;"  >
            <h2>Discount Text in different languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
							<div style="width:200px;">	
								<label for="streamzon_l_page_discountbar_display_text_'.$val.'" >Discount Text '.$val.'</label>
							</div>
							<div style="width:400px;">
			           	<input form="theme_options_block" class="regular-text" type="text" id="streamzon_l_page_discountbar_display_text_'.$val.'" name="streamzon_theme_settings_option[l_page_discountbar_display_text_'.$val.']" value="%s" />
							</div>
						</div>',
                    isset( $this->theme_settings['l_page_discountbar_display_text_'.$val] ) ? $this->theme_settings['l_page_discountbar_display_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=discount_txt_id" title="Add Discount text for other languages" class="button thickbox">Add Discount text for other languages</a>

    <?php
    }
    public function side_discountbar_display_text_callback()
    {
        printf(
            '<label for="streamzon_side_discountbar_display_text">Sidebar Discount Text</label>
            <input class="regular-text" type="text" id="streamzon_side_discountbar_display_text" name="streamzon_theme_settings_option[side_discountbar_display_text]" value="%s" />',
            isset( $this->theme_settings['side_discountbar_display_text'] ) ? $this->theme_settings['side_discountbar_display_text'] : ''
        );
        ?>
        <?php add_thickbox(); ?>
        <div id="side_discount_txt_id" style="display:none;"  >
            <h2>sidebar Discount Text in different languages</h2>

            <?php
            $alocales = array(
                'ca' => 'Canada',
                'cn' => 'China',
                'de' => 'Germany',
                'es' => 'Spain',
                'fr' => 'France',
                'in' => 'India',
                'it' => 'Italy',
                'jp' => 'Japan',
                'uk' => 'UK',
                'us' => 'USA',
            );
            foreach($alocales as $key => $val)
            {
                printf(
                    '<div>
							<div style="width:200px;">	
								<label for="streamzon_side_discountbar_display_text_'.$val.'" >sidebar Discount Text '.$val.'</label>
							</div>
							<div style="width:400px;">
			           	<input form="theme_options_block" class="regular-text" type="text" id="streamzon_side_discountbar_display_text_'.$val.'" name="streamzon_theme_settings_option[side_discountbar_display_text_'.$val.']" value="%s" />
							</div>
						</div>',
                    isset( $this->theme_settings['side_discountbar_display_text_'.$val] ) ? $this->theme_settings['side_discountbar_display_text_'.$val] : ''
                );
            }
            ?>
            <p class="submit">
                <input form="theme_options_block" class="button button-primary" type="submit" value="Save Changes" name="submit" />
            </p>
        </div>

        <a href="#TB_inline?width=600&height=450&inlineId=side_discount_txt_id" style="overflow:hidden; white-space: nowrap; width: 100%;" title="Add Sidebar Discount text for other languages" class="button thickbox ">Add Sidebar Discount text for other languages</a>

    <?php
    }
    public function l_page_footer_text_font_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['l_page_footer_text_font_family']) ? $this->theme_settings['l_page_footer_text_font_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_l_page_footer_text_font_family">Font Family</label>
        <select id="streamzon_l_page_footer_text_font_family"  name="streamzon_theme_settings_option[l_page_footer_text_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
							    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
							</optgroup>";
            }
            ?>
        </select>
    <?php  }

    public function l_page_footer_text_font_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_footer_text_font_px">Font Size</label>
            <input class="small-text" type="number" min="12" max="100" step="1" id="streamzon_l_page_footer_text_font_px" name="streamzon_theme_settings_option[l_page_footer_text_font_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_footer_text_font_px'] ) ? $this->theme_settings['l_page_footer_text_font_px'] : ''
        );
    }

    public function l_page_header_text_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_header_text_px">Header Text Size</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_l_page_header_text_px" name="streamzon_theme_settings_option[l_page_header_text_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_header_text_px'] ) ? $this->theme_settings['l_page_header_text_px'] : ''
        );
    }

    public function l_page_searchbar_height_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_searchbar_height_px">Height</label>
            <input class="small-text" type="number" min="1" max="100" step="1" id="streamzon_l_page_searchbar_height_px" name="streamzon_theme_settings_option[l_page_searchbar_height_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_searchbar_height_px'] ) ? $this->theme_settings['l_page_searchbar_height_px'] : ''
        );
    }

    public function l_page_searchbar_toppadding_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_searchbar_toppadding_px">Top Margin </label>
            <input class="small-text" type="number" min="1" max="100" step="1" id="streamzon_l_page_searchbar_toppadding_px" name="streamzon_theme_settings_option[l_page_searchbar_toppadding_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_searchbar_toppadding_px'] ) ? $this->theme_settings['l_page_searchbar_toppadding_px'] : ''
        );
    }      

    public function l_page_product_topmargin_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_product_topmargin_px">Products Top Margin (only onepage)</label>
            <input class="small-text" type="number" min="1" max="999" step="1" id="streamzon_l_page_product_topmargin_px" name="streamzon_theme_settings_option[l_page_product_topmargin_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_product_topmargin_px'] ) ? $this->theme_settings['l_page_product_topmargin_px'] : ''
        );
    }    

    public function l_page_searchbar_bottompadding_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_searchbar_bottompadding_px">Bottom Margin (only onepage)</label>
            <input class="small-text" type="number" min="1" max="999" step="1" id="streamzon_l_page_searchbar_bottompadding_px" name="streamzon_theme_settings_option[l_page_searchbar_bottompadding_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_searchbar_bottompadding_px'] ) ? $this->theme_settings['l_page_searchbar_bottompadding_px'] : ''
        );
    }

    public function l_page_searchbar_opacity_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_searchbar_opacity_px">Opacity</label>
            <input class="small-text" type="number" min="0" max="1" step="0.05" id="streamzon_l_page_searchbar_opacity_px" name="streamzon_theme_settings_option[l_page_searchbar_opacity_px]" value="%f" />',
            isset( $this->theme_settings['l_page_searchbar_opacity_px'] ) ? $this->theme_settings['l_page_searchbar_opacity_px'] : ''
        );
    }
    public function l_page_subheader_text_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_subheader_text_px">Subheader Text Size</label>
            <input class="small-text" type="number" min="1" max="200" step="1" id="streamzon_l_page_subheader_text_px" name="streamzon_theme_settings_option[l_page_subheader_text_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_subheader_text_px'] ) ? $this->theme_settings['l_page_subheader_text_px'] : ''
        );
    }

    public function l_page_show_footer_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Show Footer</span></legend>
            <label for="streamzon_l_page_show_footer">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_show_footer']) ? $this->theme_settings['l_page_show_footer'] : ''); ?> value="1" id="streamzon_l_page_show_footer" name="streamzon_theme_settings_option[l_page_show_footer]">
                Show Footer
            </label>
        </fieldset>
    <?php
    }

    public function l_page_background_image_callback()
    {
        echo '<label for="streamzon_l_page_background_image">Top Background Image</label>';
        echo '<input id="streamzon_l_page_background_image" type="file" name="l_page_background_image" />';
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use uploaded image</span></legend>
            <label for="streamzon_l_page_background_image_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_background_image_use']) ? $this->theme_settings['l_page_background_image_use'] : ''); ?> value="1" id="streamzon_l_page_background_image_use" name="streamzon_theme_settings_option[l_page_background_image_use]">
                Use uploaded image
            </label>
        </fieldset>
        <?php
        if (isset($this->theme_settings['l_page_background_image'])) {
            echo "<div class='image-container'><img src='{$this->theme_settings['l_page_background_image']}' alt='' /></div>";
        }
    }

    public function l_page_landing_page_enable_callback()
    {        
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable Landing Page</span></legend>
            <label for="streamzon_l_page_landing_page_enable">
                <input type="checkbox" <?php checked('1', isset($this->amazon_settings['l_page_landing_page_enable']) ? $this->amazon_settings['l_page_landing_page_enable'] : ''); ?> value="1" id="streamzon_l_page_landing_page_enable" name="streamzon_amazon_settings_option[l_page_landing_page_enable]">
                Enable Landing Page
            </label>
        </fieldset>
    <?php
    }    

    public function l_page_onepage_landing_callback()
    {        
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable Onepage Landing Page</span></legend>
            <label for="streamzon_l_page_onepage_landing">
                <input type="checkbox" <?php checked('1', isset($this->amazon_settings['l_page_onepage_landing']) ? $this->amazon_settings['l_page_onepage_landing'] : ''); ?> value="1" id="streamzon_l_page_onepage_landing" name="streamzon_amazon_settings_option[l_page_onepage_landing]">
                Enable Onepage Landing Page
            </label>
        </fieldset>
    <?php
    }

    public function chiz_flag_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Enable flag</span></legend>
            <label for="streamzon_chiz_flag">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['chiz_flag']) ? $this->theme_settings['chiz_flag'] : ''); ?> value="1" id="streamzon_chiz_flag" name="streamzon_theme_settings_option[chiz_flag]">
                Disable Flag
            </label>
            <p class="description"><b>Show/hide flag image</b></p>
        </fieldset>
    <?php
    }

    public function l_page_landing_page_align_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_l_page_landing_page_align">Landing Page Align left / center / right</label>
            <label for="streamzon_l_page_landing_page_align_left">
                <input type="radio" <?php checked('1', isset($this->theme_settings['l_page_landing_page_align']) ? $this->theme_settings['l_page_landing_page_align'] : ''); ?> value="1" id="streamzon_l_page_landing_page_align_left" name="streamzon_theme_settings_option[l_page_landing_page_align]">
                Left
            </label>
            <label for="streamzon_l_page_landing_page_align_center">
                <input type="radio" <?php checked('2', isset($this->theme_settings['l_page_landing_page_align']) ? $this->theme_settings['l_page_landing_page_align'] : ''); ?> value="2" id="streamzon_l_page_landing_page_align_center" name="streamzon_theme_settings_option[l_page_landing_page_align]">
                Center
            </label>
            <label for="streamzon_l_page_landing_page_align_right">
                <input type="radio" <?php checked('3', isset($this->theme_settings['l_page_landing_page_align']) ? $this->theme_settings['l_page_landing_page_align'] : ''); ?> value="3" id="streamzon_l_page_landing_page_align_right" name="streamzon_theme_settings_option[l_page_landing_page_align]">
                Right
            </label>
        </fieldset>
    <?php
    }
    public function _1_page_landing_page_body_size_callback()
    {
        printf(
            '<label for="streamzon_1_page_landing_page_body_size">Landing page Body Height (Margin top&bottom)</label>
            <input class="small-text" type="number" min="10" max="500" step="1" id="streamzon_1_page_landing_page_body_size" name="streamzon_theme_settings_option[1_page_landing_page_body_size]" value="%s" /> px',
            isset( $this->theme_settings['1_page_landing_page_body_size'] ) ? $this->theme_settings['1_page_landing_page_body_size'] : ''
        );
    }
    public function landing_page_context_width_callback()
    {
        printf(
            '<label for="streamzon_landing_page_context_width">Landing page Body width (Margin left&right)</label>
            <input class="small-text" type="number" min="10" max="500" step="1" id="streamzon_landing_page_context_width" name="streamzon_theme_settings_option[landing_page_context_width]" value="%s" /> px',
            isset( $this->theme_settings['landing_page_context_width'] ) ? $this->theme_settings['landing_page_context_width'] : ''
        );
    }
    public function header_text_enable_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Show Header Text</span></legend>
            <label for="streamzon_header_text_enable">
                <input type="radio" <?php checked('2', isset($this->theme_settings['top_bar_logo_use']) ? $this->theme_settings['top_bar_logo_use'] : ''); ?> value="2" id="streamzon_header_text_enable" name="streamzon_theme_settings_option[top_bar_logo_use]">
                Show Header Text
            </label>
        </fieldset>
    <?php
    }

    

    public function l_page_background_video_use_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use background vide</span></legend>
            <label for="streamzon_l_page_background_video_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_background_video_use']) ? $this->theme_settings['l_page_background_video_use'] : ''); ?> value="1" id="streamzon_l_page_background_video_use" name="streamzon_theme_settings_option[l_page_background_video_use]">
                Use background video
            </label>
        </fieldset>
    <?php
    }

    public function l_page_background_video_pattern_enable_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use pattern on background (video/image)</span></legend>
            <label for="streamzon_l_page_background_video_pattern_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['l_page_background_video_pattern_enable']) ? $this->theme_settings['l_page_background_video_pattern_enable'] : ''); ?> value="1" id="streamzon_l_page_background_video_pattern_enable" name="streamzon_theme_settings_option[l_page_background_video_pattern_enable]">
                Use pattern on background (video/image)
            </label>
        </fieldset>
    <?php
    }

    public function l_page_background_video_pattern_callback()
    {
       ?>
            <style>
                .patterns{
                    heihgt:300px;
                    width:100%;
                    overflow:auto;
                }
                .block{
                    width:20px;height:20px;
                    border:solid 2px #ccc;
                    margin:1px;
                    float:left;
                }
                .block:hover,
                .focus{border:solid 2px #101010;}

                <?php
                    $patterns = array("dotted-1"	=>	"dotted-1","dotted-2"	=>	"dotted-2","dotted-3"	=>	"dotted-3","dotted-4"	=>	"dotted-4","horizontal-1"	=>	"horizontal-1","horizontal-2"	=>	"horizontal-2","horizontal-3"	=>	"horizontal-3","horizontal-4"	=>	"horizontal-4","horizontal-5"	=>	"horizontal-5","horizontal-6"	=>	"horizontal-6","oblique-l-1"	=>	"oblique-l-1","oblique-l-2"	=>	"oblique-l-2","oblique-l-3"	=>	"oblique-l-3","oblique-l-4"	=>	"oblique-l-4","oblique-l-5"	=>	"oblique-l-5","oblique-l-6"	=>	"oblique-l-6","oblique-l-7"	=>	"oblique-l-7","oblique-l-8"	=>	"oblique-l-8","oblique-r-1"	=>	"oblique-r-1","oblique-r-2"	=>	"oblique-r-2","oblique-r-3"	=>	"oblique-r-3","oblique-r-4"	=>	"oblique-r-4","oblique-r-5"	=>	"oblique-r-5","oblique-r-6"	=>	"oblique-r-6","oblique-r-7"	=>	"oblique-r-7","oblique-r-8"	=>	"oblique-r-8","others-1"	=>	"others-1","others-2"	=>	"others-2","others-3"	=>	"others-3","others-4"	=>	"others-4","others-5"	=>	"others-5","others-6"	=>	"others-6","others-7"	=>	"others-7","others-8"	=>	"others-8","others-9"	=>	"others-9","vertical-1"	=>	"vertical-1","vertical-2"	=>	"vertical-2","vertical-3"	=>	"vertical-3","vertical-4"	=>	"vertical-4","vertical-5"	=>	"vertical-5","vertical-6"	=>	"vertical-6",);
                    foreach($patterns as $key => $val){
                        print "#".$key."{ background: url(".get_stylesheet_directory_uri()."/landing-page/patterns/".$key.".png); }\n";
                    }
                ?>
            </style>
            <fieldset>
                <legend class="screen-reader-text"><span>Choose overlay pattern</span></legend>
                <div class="patterns">
                    <?php
                    foreach($patterns as $key => $val){
                        $selected = ($this->theme_settings['l_page_background_video_pattern'] == $key) ? " focus" : "";
                        print '<div id="'.$key.'" class="block'.$selected.'"> </div>';
                    }
                    ?>
                    <input type="hidden" id="streamzon_l_page_background_video_pattern" name="streamzon_theme_settings_option[l_page_background_video_pattern]" value="<?php print $this->theme_settings['l_page_background_video_pattern'];?>" />
                </div>
            </fieldset>
            <script type="text/javascript">
                jQuery(function() {
                    jQuery(".block").click(function() {
                        jQuery(".block").removeClass('focus');
                        jQuery(this).addClass('focus');
                        jQuery("#streamzon_l_page_background_video_pattern").val(jQuery(this).attr('id'));
                    });
                });
		jQuery( document ).ready(function() {
			jQuery("[name='streamzon_theme_settings_option[l_page_discountbar_display_text]']").change(function() {
				jQuery("[name='streamzon_theme_settings_option[l_page_discountbar_display_text_USA]']").val(jQuery(this).val());
			});

			jQuery("[name='streamzon_theme_settings_option[l_page_subheader_text]']").change(function() {
				jQuery("[name='streamzon_theme_settings_option[l_page_subheader_text_USA]']").val(jQuery(this).val());
			});

			jQuery("[name='streamzon_theme_settings_option[l_page_footer_text]']").change(function() {
				jQuery("[name='streamzon_theme_settings_option[l_page_footer_text_USA]']").val(jQuery(this).val());
			});

			jQuery("[name='streamzon_theme_settings_option[l_page_searchbar_text]']").change(function() {		
				jQuery("[name='streamzon_theme_settings_option[l_page_searchbar_text_USA]']").val(jQuery(this).val());
			});

			jQuery("[name='streamzon_theme_settings_option[l_page_button_text]']").change(function() {		
				jQuery("[name='streamzon_theme_settings_option[l_page_button_text_USA]']").val(jQuery(this).val());
			});
			
			jQuery("[name='streamzon_theme_settings_option[l_page_header_text]']").change(function() {
				jQuery("[name='streamzon_theme_settings_option[l_page_header_text_USA]']").val(jQuery(this).val());
			});
		});
            </script>
       
        </fieldset>
        <?php
    }
	//SERCH PAGE PATTERN
public function search_page_background_pattern_enable_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use pattern on background image</span></legend>
            <label for="streamzon_search_page_background_pattern_enable">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['search_page_background_pattern_enable']) ? $this->theme_settings['search_page_background_pattern_enable'] : ''); ?> value="1" id="streamzon_search_page_background_pattern_enable" name="streamzon_theme_settings_option[search_page_background_pattern_enable]">
                Use pattern on background image
            </label>
        </fieldset>
    <?php
    }

    public function search_page_background_pattern_callback()
    {
       ?>
            <style>
                .patterns{
                    heihgt:300px;
                    width:100%;
                    overflow:auto;
                }
                .block2{
                    width:20px;height:20px;
                    border:solid 2px #ccc;
                    margin:1px;
                    float:left;
                }
                .block2:hover,
                .focus{border:solid 2px #101010;}

                <?php
                    $patterns = array("dotted-1"	=>	"dotted-1","dotted-2"	=>	"dotted-2","dotted-3"	=>	"dotted-3","dotted-4"	=>	"dotted-4","horizontal-1"	=>	"horizontal-1","horizontal-2"	=>	"horizontal-2","horizontal-3"	=>	"horizontal-3","horizontal-4"	=>	"horizontal-4","horizontal-5"	=>	"horizontal-5","horizontal-6"	=>	"horizontal-6","oblique-l-1"	=>	"oblique-l-1","oblique-l-2"	=>	"oblique-l-2","oblique-l-3"	=>	"oblique-l-3","oblique-l-4"	=>	"oblique-l-4","oblique-l-5"	=>	"oblique-l-5","oblique-l-6"	=>	"oblique-l-6","oblique-l-7"	=>	"oblique-l-7","oblique-l-8"	=>	"oblique-l-8","oblique-r-1"	=>	"oblique-r-1","oblique-r-2"	=>	"oblique-r-2","oblique-r-3"	=>	"oblique-r-3","oblique-r-4"	=>	"oblique-r-4","oblique-r-5"	=>	"oblique-r-5","oblique-r-6"	=>	"oblique-r-6","oblique-r-7"	=>	"oblique-r-7","oblique-r-8"	=>	"oblique-r-8","others-1"	=>	"others-1","others-2"	=>	"others-2","others-3"	=>	"others-3","others-4"	=>	"others-4","others-5"	=>	"others-5","others-6"	=>	"others-6","others-7"	=>	"others-7","others-8"	=>	"others-8","others-9"	=>	"others-9","vertical-1"	=>	"vertical-1","vertical-2"	=>	"vertical-2","vertical-3"	=>	"vertical-3","vertical-4"	=>	"vertical-4","vertical-5"	=>	"vertical-5","vertical-6"	=>	"vertical-6",);
                    foreach($patterns as $key => $val){
                        print "#".$key."{ background: url(".get_stylesheet_directory_uri()."/landing-page/patterns/".$key.".png); }\n";
                    }
                ?>
            </style>
            <fieldset>
                <legend class="screen-reader-text"><span>Choose overlay pattern</span></legend>
                <div class="patterns">
                    <?php
                    foreach($patterns as $key => $val){
                        $selected = ($this->theme_settings['search_page_background_pattern'] == $key) ? " focus" : "";
                        print '<div id="'.$key.'" class="block2'.$selected.'"> </div>';
                    }
                    ?>
                    <input type="hidden" id="streamzon_search_page_background_pattern" name="streamzon_theme_settings_option[search_page_background_pattern]" value="<?php print $this->theme_settings['search_page_background_pattern'];?>" />
                </div>
            </fieldset>
            <script type="text/javascript">
                jQuery(function() {
                    jQuery(".block2").click(function() {
                        jQuery(".block2").removeClass('focus');
                        jQuery(this).addClass('focus');
                        jQuery("#streamzon_search_page_background_pattern").val(jQuery(this).attr('id'));
                    });
                });
            </script>
       
        </fieldset>
        <?php
    }
	//END SERCH PAGE PATTERN

    public function banner_image_use_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use Image and Link</span></legend>
            <label for="streamzon_banner_image_use">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['banner_image_use']) ? $this->theme_settings['banner_image_use'] : ''); ?> value="1" id="streamzon_banner_image_use" name="streamzon_theme_settings_option[banner_image_use]">
                Use Image and Link
            </label>
        </fieldset>
    <?php
    }

    public function banner_code_use_callback()
    {
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use Code</span></legend>
            <?                            
            $img_attb     =   $this->theme_settings['l_page_preset_thumbnail'];
            ?>            
            <label for="streamzon_banner_code_use">
                <input type="hidden" name="l_page_preset_thumbnail_1" value="<?=$img_path;?>">
                <input type="checkbox" <?php checked('1', isset($this->theme_settings['banner_code_use']) ? $this->theme_settings['banner_code_use'] : ''); ?> value="1" id="streamzon_banner_code_use" name="streamzon_theme_settings_option[banner_code_use]">
                Use Code
            </label>
        </fieldset>
    <?php
    }

    public function revolution_slider_use_callback()
    {
        $as = get_option('streamzon_amazon_settings_option');
        $disabled = $as['l_page_onepage_landing']== 1 ? 'disabled' : '';
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Use Code</span></legend>
            <label for="streamzon_revolution_slider_code_use">
                <input type="checkbox" <?=$disabled; checked('1', isset($this->theme_settings['revolution_slider_code_use']) ? $this->theme_settings['revolution_slider_code_use'] : ''); ?> value="1" id="streamzon_revolution_slider_code_use" name="streamzon_theme_settings_option[revolution_slider_code_use]">
                Use Code
            </label>
        </fieldset>
    <?php
    }

    public function l_page_footer_opacity_callback()
    {
        printf(
            '<label for="streamzon_l_page_footer_opacity">Footer Opacity</label>
            <input class="small-text" type="number" min="0" max="100" step="1" id="streamzon_l_page_footer_opacity" name="streamzon_theme_settings_option[l_page_footer_opacity]" value="%s" /> %%',
            isset( $this->theme_settings['l_page_footer_opacity'] ) ? $this->theme_settings['l_page_footer_opacity'] : 100
        );
    }
    
    public function l_page_background_footer_callback()
    {
        ?>
        <label for="streamzon_l_page_background_footer">Background footer color</label>
        <input id="streamzon_l_page_background_footer" name="streamzon_theme_settings_option[l_page_background_footer]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_background_footer']) ? $this->theme_settings['l_page_background_footer'] : ''; ?>" data-default-color="#60313b" />
    <?php
    }

    

    public function l_page_header_color_callback()
    {
        ?>
        <label for="streamzon_l_page_header_color">Header Color</label>
        <input id="streamzon_l_page_header_color" name="streamzon_theme_settings_option[l_page_header_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_header_color']) ? $this->theme_settings['l_page_header_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_header_font_family_callback()
    {
        $fonts = self::$fonts;
        $value	=	isset($this->theme_settings['l_page_header_font_family']) ? $this->theme_settings['l_page_header_font_family'] : '';

        ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto|Oswald|Lato|Roboto+Condensed|Open+Sans+Condensed:300|Raleway|Droid+Serif|Montserrat|Roboto+Slab|PT+Sans+Narrow|Lora|Arimo|Bitter|Merriweather|Oxygen|Lobster|Titillium+Web|Poiret+One|Play|Pacifico|Questrial|Dancing+Script|Exo+2|Comfortaa|Ropa+Sans|Lobster+Two|EB+Garamond' rel='stylesheet' type='text/css'>

        <label for="streamzon_l_page_header_font_family">Font Family</label>
        <select id="streamzon_l_page_header_font_family"  name="streamzon_theme_settings_option[l_page_header_font_family]" style="width:100%;">
            <?php
            print '<option value="0">Choose Font</option>';
            foreach($fonts as $font)
            {
                $selected	=		($value == $font[0]) ? "selected" : "";

                print "<optgroup style=\"font-family: ".$font[2].";\">
							    <option value=".$font[0]." ".$selected.">".$font[1]."</option>
							</optgroup>";
            }
            ?>
        </select>
    <?php
    }

    public function l_page_subheader_color_callback()
    {
        ?>
        <label for="streamzon_l_page_subheader_color">Subheader Color</label>
        <input id="streamzon_l_page_subheader_color" name="streamzon_theme_settings_option[l_page_subheader_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_subheader_color']) ? $this->theme_settings['l_page_subheader_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_body_background_color_callback()
    {
        ?>
        <label for="streamzon_l_page_body_background_color">Full Screen Background Color</label>
        <input id="streamzon_l_page_body_background_color" name="streamzon_theme_settings_option[l_page_body_background_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_body_background_color']) ? $this->theme_settings['l_page_body_background_color'] : ''; ?>" data-default-color="#ffffff" />
    <?php
    }

    public function l_page_footer_text_color_callback()
    {
        ?>
        <label for="streamzon_l_page_footer_text_color">Text Color</label>
        <input id="streamzon_l_page_footer_text_color" name="streamzon_theme_settings_option[l_page_footer_text_color]" class="streamzon-color-picker" type="text" value="<?php echo isset($this->theme_settings['l_page_footer_text_color']) ? $this->theme_settings['l_page_footer_text_color'] : ''; ?>" data-default-color="#333333" />
    <?php
    }

    public function l_page_footer_height_px_callback()
    {
        printf(
            '<label for="streamzon_l_page_footer_height_px">Footer Height</label>
            <input class="small-text" type="number" min="30" max="100" step="1" id="streamzon_l_page_footer_height_px" name="streamzon_theme_settings_option[l_page_footer_height_px]" value="%s" /> px',
            isset( $this->theme_settings['l_page_footer_height_px'] ) ? $this->theme_settings['l_page_footer_height_px'] : ''
        );
    }
    public function top_bar_logo_use_callback()
    {

    }

    public function box_big_small_img_callback()
    {
        ?>
        <fieldset>
            <label for="streamzon_box_big_small_img">Box image sizes</label>
            <label for="streamzon_box_big_small_img_small">
                <input type="radio" <?php checked('1', isset($this->theme_settings['box_big_small_img']) ? $this->theme_settings['box_big_small_img'] : ''); ?> value="1" id="streamzon_box_big_small_img_small" name="streamzon_theme_settings_option[box_big_small_img]">
                Small Image
            </label>
            <label for="streamzon_box_big_small_img_big">
                <input type="radio" <?php checked('2', isset($this->theme_settings['box_big_small_img']) ? $this->theme_settings['box_big_small_img'] : ''); ?> value="2" id="streamzon_box_big_small_img_big" name="streamzon_theme_settings_option[box_big_small_img]">
                Big Image
            </label>
	    <label for="streamzon_box_big_small_img_bigger">
                <input type="radio" <?php checked('3', isset($this->theme_settings['box_big_small_img']) ? $this->theme_settings['box_big_small_img'] : ''); ?> value="3" id="streamzon_box_big_small_img_bigger" name="streamzon_theme_settings_option[box_big_small_img]">
                Bigger Image
            </label>
        </fieldset>
    <?php
    }
    public function box_bar_logo_use_callback()
    {

    }

    public function l_page_center_logo_use_callback()
    {

    }

    public function search_page_background_image_use_callback()
    {

    }

    public function l_page_background_image_use_callback()
    {

    }

    /**
     * Get the settings option array and print one of its values
     */
    public function amazon_discount_amount_callback()
    {
    }

    private function trim_amazon_browse_node_ids($config)
    {
        array_walk_recursive($config, array($this, 'trim_string_in_array'));

        return $config;
    }

    private function trim_string_in_array(&$item, $key)
    {
        if (is_string($item)) $item = trim($item);
    }

    private function load_amazon_subcategories($market, $category)
    {
        return streamzon_amazon_browse_node_lookup($market, $category);
    }


	//test custom_do
	private function custom_do_settings_sections_dashboard($page) {
		
		global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_dashboard_fields( $page, $section['id'] );
            echo '</div>';
        }




		
	}
	//test
    private function custom_do_dashboard_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;
		
        foreach ($this->streamzon_theme_test_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column"style="float:left;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field" style="min-height:150px">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }

    }	


    private function custom_do_settings_sections_credentials( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_settings_credential_fields( $page, $section['id'] );
            echo '</div>';
        }
    }

    private function custom_do_settings_sections_stores( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_stores_fields( $page, $section['id'] );
            echo '</div>';
        }
    }
    private function custom_do_stores_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;

        foreach ($this->amazon_stores_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column"style="float:left;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field" style="min-height:150px">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }

    }

    private function custom_do_settings_credential_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;

        foreach ($this->amazon_credential_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column"style="float:left;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }

    }



    private function custom_do_item_settings_sections( $page ) {
        global $wp_settings_sections, $wp_settings_fields;
		
        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_item_settings_fields( $page, $section['id'] );
            echo '</div>';
        }
    }
    private function custom_do_item_settings_fields($page, $section) {
        global $wp_settings_fields;
		
        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;

        echo '<style>
				.pickList_list{
					padding:5px !important;
					border:solid 1px #ccc;
				}
			</style>';
			
        foreach ($this->amazon_item_settings_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column" style="width:100%; max-width:430px;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }
    }

    private function custom_do_settings_amazon( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_settings_amazon_fields( $page, $section['id'] );
            echo '</div>';
        }
    }

    private function custom_do_settings_amazon_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;

        foreach ($this->amazon_settings_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column" style="float:left;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }
    }
	//SEO page
    private function custom_do_settings_seo( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;
	

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;


            echo '<div class="streamzon-setting-section">';
            $this->custom_do_settings_seo_fields( $page, $section['id'] );
            echo '</div>';
        }
    }

    private function custom_do_settings_seo_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;
	
        foreach ($this->seo_settings_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column" style="float:left;">';
            echo '<h4>' . $column['column_title'] . '</h4>';
            echo '<div class="streamzon-setting-fieldset">';

            foreach ( $column['options'] as $option ) {

                echo '<div class="streamzon-setting-field">';
                call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                echo '</div>';

            }

            echo '</div>';
            echo '</div>';

        }
    }
	//END SEO PAGE

    private function custom_do_settings_sections( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h3>{$section['title']}</h3>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;
            echo '<div class="streamzon-setting-section">';
            $this->custom_do_settings_fields( $page, $section['id'] );
            echo '</div>';
        }

        ///get all presets
        $preset_array =	(array)get_option('presets');
        $current_preset = get_option('preset_name');
        

        if(isset($_GET['del']))
        {
            if($_GET['del'] != $current_preset) {
                $del = $_GET['del'];
                $preset_keys = @array_keys($preset_array['presets']);

                if (in_array($del, $preset_keys)) {
                    unset($preset_array['presets'][$del]);
                    update_option('presets', $preset_array);
                    $preset_array = (array)get_option('presets');
                    $preset_keys = @array_keys($preset_array['presets']);

                }
            }
        }

        ?>

        <h3>Templates</h3>
	<div class="streamzon-setting-section">
		<div class="streamzon-setting-column">
		  <h4>Load Template</h4>
                    <div class="streamzon-setting-fieldset">
                        <div style="height:217px;">
                        <fieldset>
                            <label for="preset_name">Template Name</label>
                            <label for="preset_name">
                                <input type="text" id="streamzon_l_page_preset" name="streamzon_theme_settings_option[l_page_preset]"  value="" />
                            </label>
                            <label for="preset_name">
                                <input id="streamzon_" type="file" name="l_page_preset_thumbnail" />
                            </label>
                            <input type="hidden" id="streamzon_l_page_choose_settings" name="streamzon_theme_settings_option[l_page_choose_settings]" value="none">
                            <?php
                            //print $val = $this->theme_settings['l_page_choose_settings'];
                            $current_theme	=	get_option('preset_name');
                            ?>
                            <!--
                            <label for="preset_choose">Choose Preset settings
                                <select id="streamzon_l_page_choose_settings" name="streamzon_theme_settings_option[l_page_choose_settings]">                                    
                                    <option value="default">Default</option>
                                    <?php
                                    if(is_array($preset_array['presets']))
                                    {
                                        foreach($preset_array['presets'] as $preset)
                                        {
                                            if($preset['l_page_preset_thumbnail'] > 0)
                                            {
                                                $selected = ($current_theme == $preset['l_page_preset']) ? 'selected': "";
                                                print '<option value="'.$preset['l_page_preset'].'" '.$selected.'>'.ucfirst($preset['l_page_preset']).'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </label> -->
                        </label>
                        </fieldset>
                        <div style="margin: 0 0 10px 5px; bottom: 0; position: absolute;" ><input type="submit" value="Create Template" class="button button-secondary" id="submit_settings" name="submit_settings"></div>
                        </div>
                    </div>
                </div>

	            <?php                
                //08dfcd
                if(is_array($preset_array['presets']))
                {
                    foreach($preset_array['presets'] as $preset)
                    {
                        if($preset['l_page_preset'] != '' || $preset['l_page_preset']=='Default Template')
                        {
                            if($current_theme == $preset['l_page_preset']){	
                                $style = "onlinepress";$present = "Online";$btndisable='disabled';
                            } else {	
                                $style = "loadpress";$present = "Load Template";$btndisable='';
                            }
                            $pre_name	=	$preset['l_page_preset'];
                            
                            $img_attb 	=	wp_get_attachment_image_src($preset['l_page_preset_thumbnail'], 'large');
                            
                            $img_path	=	$img_attb[0];
                            if(img_path){
                                $img_path = get_stylesheet_directory_uri().'/img/uploads/preset_logo/'.$img_path;
                            }
                            
                            if(!$img_attb) $img_path = $preset['l_page_preset_thumbnail'];
                            $img_path = $img_path == "dotted-1" ? get_stylesheet_directory_uri()."/img/default.png" : $img_path;

                            if(!$img_path)  $img_path	=	get_stylesheet_directory_uri()."/img/image-not-available-150x150.jpg";
                        ?>
                            <div class="streamzon-setting-column">
        					    <h4><?=$pre_name;?></h4>
        					    <div class="streamzon-setting-fieldset" style="text-align:center;">
        					
                                    <div class="preset_pics preset_hover" style="width=100%; text-align:left;" name="outer_blocks" data-id="<?=$pre_name;?>">
                                        <div class="pics_block" style="background: url(<?=$img_path;?>) no-repeat center/cover;" name="outer_block" data-id="<?php print $pre_name;?>">
                                            <!--<img src="<?=$img_path;?>" width="200px" class="fff">-->
                                            
                                            <span class="closebtn">
                                                <a class="aclosebtn" href="<?=get_site_url();?>/wp-admin/admin.php?page=streamzon-theme-options&del=<?=$pre_name;?>"></a>
                                            </span>
                                        </div>
                                        <div class="name_block" >
                                            <input id="load_settings" class="button button-secondary <?=$style; ?>" style="-webkit-box-shadow:0; color:#fff; width:100%; height:100%;" type="submit" value="<?=$present;?>" name="load_settings" <?=$btndisable; ?>>
                                        </div>
                                    </div>
    		                      <!-- <div style="margin-left:5px; left:0; text-align:left;"><input id="load_settings" class="button button-secondary" type="submit" value="Load Settings" name="load_settings"></div> -->
                            
    	                        </div>
                            </div>
                        <?php
                        }
	                }
                }
                ?>
		
	</div>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('div[name="outer_block"]').click(function(event) {
                    //var data_id	=	jQuery(this).attr("data-id")
                    //jQuery("#streamzon_l_page_choose_settings").val(jQuery(this).attr("data-id"));
                    jQuery(".pics_block").removeClass('focus_preset');
                    jQuery(this).addClass('focus_preset');

                });
		        jQuery('div[name="outer_blocks"]').click(function(event) {
                    var data_id	=	jQuery(this).attr("data-id")
                    jQuery("#streamzon_l_page_choose_settings").val(jQuery(this).attr("data-id"));

                });
                jQuery(".aclosebtn").click(function() {
                    var answer = confirm("Are you sure you want to delete this Preset?");
                    if (answer) {

                    }
                    else{
                        return false;
                    }
                });
            });
        </script>

    <?php
    }

    private function custom_do_settings_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )
            return;

        foreach ($this->theme_options_columns[$page][$section] as $column) {

            echo '<div class="streamzon-setting-column">';
            echo '<h4>' . $column['column_title'] . '</h4>';
                echo '<div class="streamzon-setting-fieldset">';
                foreach ( $column['options'] as $option ) {
                    echo '<div class="streamzon-setting-field">';                
                    call_user_func($wp_settings_fields[$page][$section][$option]['callback'], $wp_settings_fields[$page][$section][$option]['args']);
                    echo '</div>';
                }
                echo '</div>';
            echo '</div>';
        }
    }
	

}

if( is_admin() )
    $streamzon_settings_page = new StreamzonSettingsPage();

