<?php

/**
 * A item custom search operation
 *
 * @see    http://docs.aws.amazon.com/AWSECommerceService/2011-08-01/DG/ItemSearch.html
 */
class StreamzonAmazonCustomSearch extends \ApaiIO\Operations\AbstractOperation
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ItemSearch';
    }

    /**
     * Sets the amazon Sort
     *
     * @param string $sort
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setSort($sort)
    {
        $this->parameter['Sort'] = $sort;

        return $this;
    }

    /**
     * Sets the amazon MinimumPrice
     *
     * @param string $minimumPrice
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setMinimumPrice($minimumPrice)
    {
        $this->parameter['MinimumPrice'] = $minimumPrice;

        return $this;
    }

    /**
     * Sets the amazon MaximumPrice
     *
     * @param string $maximumPrice
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setMaximumPrice($maximumPrice)
    {
        $this->parameter['MaximumPrice'] = $maximumPrice;

        return $this;
    }

    /**
     * Sets the amazon BrowseNode
     *
     * @param string $browseNode
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setBrowseNode($browseNode)
    {
        $this->parameter['BrowseNode'] = $browseNode;

        return $this;
    }

    /**
     * Sets the amazon MinPercentageOff
     *
     * @param string $minPercentageOff
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setMinPercentageOff($minPercentageOff)
    {
        $this->parameter['MinPercentageOff'] = $minPercentageOff;

        return $this;
    }

    /**
     * Sets the amazon category
     *
     * @param string $category
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setCategory($category)
    {
        $this->parameter['SearchIndex'] = $category;

        return $this;
    }

    /**
     * Sets the keywords
     *
     * @param string $keywords
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setKeywords($keywords)
    {
        $this->parameter['Keywords'] = $keywords;

        return $this;
    }

    /**
     * Sets the resultpage to a specified value
     * Allows to browse resultsets which have more than one page
     *
     * @param integer $page
     *
     * @return \ApaiIO\Operations\Search
     */
    public function setPage($page)
    {
        if (false === is_numeric($page) || $page < 1 || $page > 10) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s is an invalid page value. It has to be numeric, positive and between 1 and 10',
                    $page
                )
            );
        }

        $this->parameter['ItemPage'] = $page;

        return $this;
    }
}
