<?php
session_start();

//reset settings
if(isset($_GET['reset']) && $_GET['reset'] == 'true'){
    update_option('streamzon_theme_settings_option', '');
    update_option('streamzon_amazon_settings_option', '');
    update_option('streamzon_amazon_stores_option', '');
    update_option('streamzon_amazon_item_settings_option', '');
    update_option('presets', '');
    die('data clear');
}

show_admin_bar(false);


/* Define Constants */
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT . '/img');


include get_template_directory() . '/inc/options.php';
include get_template_directory() . '/vendor/autoload.php';
include get_template_directory() . '/inc/amazon_functions.php';


/* Actions */
add_action('widgets_init', 'streamzon_widgets_init' );
add_action('after_setup_theme', 'streamzon_setup' );
add_action('wp_enqueue_scripts', 'streamzon_load_custom_scripts');
add_action('wp_ajax_streamzon_get_books', 'ajax_streamzon_display_books');
add_action('wp_ajax_nopriv_streamzon_get_books', 'ajax_streamzon_display_books');
add_action('admin_enqueue_scripts', 'streamzon_admin_enqueue_scripts' );
add_action('wp_ajax_streamzon_load_subcategories', 'ajax_streamzon_load_subcategories');
add_action('wp_ajax_streamzon_load_categories', 'ajax_streamzon_load_categories');
add_action('after_switch_theme', 'create_analytics_table');
add_action('init', 'create_analytics_table');

add_action('wp_ajax_streamzon-import', 'ajax_streamzon_import_function');
add_action('wp_ajax_streamzon-import-dn', 'ajax_streamzon_import_dn_images');

function ajax_streamzon_import_function(){
    $fileContent = file_get_contents($_FILES['file']['tmp_name']);
    $contents = json_decode($fileContent, true);
    
    
    $presets_images = array();
    $presets_logo = array();

    if(is_array($contents)) {
        //import all settings
        if(count($contents) == 3) {

            $old_presets   = (array)get_option('presets'); 
            $old_presets   = (array)$old_presets['presets'];//old presets

            $presets = $contents['presets']; //new presets
            $options = $contents['options']; //new options
            $preset_name = $contents['preset_name'];                      
            
            $old_names = array_keys($old_presets); 


            foreach($presets['presets'] as $prKey => $prVal){
                    $res = get_unique_preset_name($prKey, $old_names);
                   
                    if( $prKey !== $res){
                        $presets['presets'][$res] =  $presets['presets'][$prKey];
                        $presets['presets'][$res]['l_page_preset'] = $res;
                        unset($presets['presets'][$prKey]); 
                    }
            }




            foreach($presets['presets'] as $presetName => $pr) {
                
                foreach ($pr as $key => $string) {
                    if (stristr($string, 'http')) {                                
                        $fileName = basename($string);
                        $url = $string; // image url on import server
                        $img_dir = get_template_directory() . '/img/uploads/' . $fileName;
                        $img_url = get_template_directory_uri() . '/img/uploads/' . $fileName;
                                                        
                        $presets['presets'][$presetName][$key] = $img_url;
                        $presets_images[] = $url;
                    }
                }
                
                //save preset logo
                if(isset($pr['attachment_file']) && $pr['attachment_file']) {
                    $att_url = $pr['attachment_file'];
                    $upload_dir_strimzone = get_template_directory_uri() . '/img/uploads/preset_logo/';                    
                    
                    $fileName = basename($att_url);

                    $presets['presets'][$presetName]['l_page_preset_thumbnail'] = $upload_dir_strimzone . $fileName;
                    $presets_logo[] = $att_url;
                }
            }//endforeach

            //save images to temp options and download it later..
            foreach($options as $key => $string) {
                if(stristr($string, 'http')) {
                    $fileName = basename($string);
                    $url = $string;
                    $img_dir = get_template_directory() . '/img/uploads/' . $fileName;
                    $img_url = get_template_directory_uri() . '/img/uploads/' . $fileName;

                    $options[$key] = $img_url;

                    $presets_images[] = $url;
                }
            }
                                
           
            $presets['presets'] = $presets['presets'] + $old_presets;                     
            $presets_sirialize = serialize($presets);

            global $wpdb;
            $wpdb->update( $wpdb->prefix . 'options',
                array( 'option_value' => $presets_sirialize ),
                array( 'option_name' => 'presets' )
            );
           
             
            $res2 = update_option('preset_name', $preset_name);
            $options_serialize = serialize($options);

            $res3 = $wpdb->update( 'wp_options',
                array( 'option_value' => $options_serialize ),
                array( 'option_name' => 'streamzon_theme_settings_option' )
            );
            
        }//endif
        else{
            //import one preset
            $preset_name = $contents['l_page_preset'];
            $old_presets   = $presets =  (array)get_option('presets'); 
            $old_presets   = (array)$old_presets['presets'];
            $old_names = array_keys($old_presets); 
       
            
            $current_preset = trim(get_option('preset_name'));
            $options = $contents;

            $newName = get_unique_preset_name($preset_name, $old_names);
        
            if( $newName !== $preset_name) {
                $options['l_page_preset'] = $newName;
                $preset_name = $newName;
            }
            

            //save images to temp options and download it later..
            foreach($options as $key => $string) {
                if(stristr($string, 'http')) {
                    $fileName = basename($string);
                    $url = $string;
                    $img_dir = get_template_directory() . '/img/uploads/' . $fileName;
                    $img_url = get_template_directory_uri() . '/img/uploads/' . $fileName;

                    $options[$key] = $img_url;

                    $presets_images[] = $url;
                }
            }

            // save preset thumbnail
            if(isset($options['attachment_file']) && $options['attachment_file']) {
                $att_url = $pr['attachment_file'];
                $upload_dir_strimzone = get_template_directory_uri() . '/img/uploads/preset_logo/';

                $fileName = basename($att_url);            
                $options['l_page_preset_thumbnail'] = $upload_dir_strimzone . $fileName;
                $presets_logo[] = $att_url;
            }

            $presets['presets'][$preset_name] = $options;

            if($current_preset == $preset_name) {

                $options['l_page_preset'] = '';
                $options_serialize = serialize($options);

                global $wpdb;
                $wpdb->update( 'wp_options',
                    array( 'option_value' => $options_serialize ),
                    array( 'option_name' => 'streamzon_theme_settings_option' )
                );
            } else {
                $options = get_option('streamzon_theme_settings_option');
            }                        
            update_option('presets', $presets);
        }


        $images_serialize = serialize($presets_images);
            
        wp_cache_delete ( 'alloptions', 'options' );
        update_option( 'streamzon_preset_temp_img', $images_serialize);

        $images_logo_serialize = serialize($presets_logo);
        
        update_option( 'streamzon_preset_temp_logo', $images_logo_serialize);
    }//endif
    
    $count = count($presets_images) > count($presets_logo) ? count($presets_images) : count(preset_logo);
    echo 'uploaded'."#".$count;

    die();
}

function ajax_streamzon_import_dn_images(){
    
    $images  = unserialize(get_option('streamzon_preset_temp_img', 0));
    $logos   = unserialize(get_option('streamzon_preset_temp_logo', 0));

    if(count($images) == 0 && count($logos) == 0){
        $status = 1;
    }else {
        $status = 0;
    }

    //take element
    if(count($images)>0){
        $image =  array_shift($images);
            $fileName = str_replace(" ","",basename($image));
            $img_dir = get_template_directory() . '/img/uploads/' . $fileName;
            $img_url = get_template_directory_uri() . '/img/uploads/' . $fileName;
            
            $img = file_get_contents($image);


            $res = file_put_contents($img_dir, $img);
    }


    if(count($logos)>0){
        $logo  =  array_shift($logos);
            $fileName = str_replace(" ","",basename($logo));
            $img_dir = get_template_directory() . '/img/uploads/preset_logo/' . $fileName;
            $img_url = get_template_directory_uri() . '/img/uploads/preset_logo/' . $fileName;
            $img = file_get_contents($logo);
            $res = file_put_contents($img_dir, $img);
        
    }

    //update options
    wp_cache_delete ( 'alloptions', 'options' );
    
    $images_serialize = serialize($images);
    update_option( 'streamzon_preset_temp_img', $images_serialize);

    $images_logo_serialize = serialize($logos);
    update_option( 'streamzon_preset_temp_logo', $images_logo_serialize);

    echo $status;
    die();
}

add_image_size( 'amazon_footer', 128, 38, false);

$streamzon_theme_main_settings = get_option('streamzon_theme_settings_option');
$seo_settings = get_option('streamzon_seo_settings_option');
$amazon_credentials = get_option('streamzon_amazon_credentials_option');

function get_unique_preset_name($name, $old_names){


    if($name == 'Default Template'){
        return $name;
    }


    if($k =  array_search($name, $old_names)) { 

        //if name already contain _NEW
        if(stristr($old_names[$k],"_NEW")){

            $index = 1;
            $result = $name."_".$index;
            while (in_array($name."_".$index, $old_names, true)) {
                $index++;
                $result = $name."_".$index;
            }
        }else{

            if(in_array($name."_NEW", $old_names)) {

                $index = 1;
                $result = $name."_NEW_".$index;

           
                while (in_array($name."_NEW_".$index, $old_names,true)) {
                    
                    $index++;
                    $result = $name."_NEW_".$index;
                }

            }else{
                $result = $name."_NEW";
            }
        }
    }else{
        $result = $name;
    }
    
    return $result;
}

function reset_permalinks() {
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure( '/%postname%/' );
}
add_action( 'init', 'reset_permalinks' );


/**
 * Register widget areas.
 */
function streamzon_widgets_init() {

    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'streamzon' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Main sidebar that appears on the right.', 'streamzon' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

}

function streamzon_setup() {

    // This theme uses wp_nav_menu()
    register_nav_menus( array(
        'primary'   => __( 'Top Primary Menu', 'streamzon' ),
    ) );

    register_nav_menus( array(
        'footer_menu'   => __( 'Footer Menu', 'streamzon' ),
    ) );

}


/* Load JS and CSS Files */
function streamzon_load_custom_scripts()
{
    global $amazon_settings;

    if(is_page()){
        wp_enqueue_script('image_zoom', THEMEROOT . '/js/jquery.ez-plus.js', array(), true);
        wp_enqueue_script('image_zoom_init', THEMEROOT . '/js/jquery.elevateZoom-init.js', array(), true);
        
    }

    
    if($amazon_settings['l_page_onepage_landing']=='1'){
        wp_enqueue_script('onepage_script', THEMEROOT . '/js/onepage_scripts.js', array(), true);
    }


    wp_enqueue_script('streamzon_custom_script', THEMEROOT . '/js/main.js', array(), true);
    wp_localize_script('streamzon_custom_script', 'streamzon_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'search_query' => $_REQUEST['s'],
		'search_query_discount' => $_REQUEST['disc_val'],
        'a_cat' => $_REQUEST['a_cat'],
    ));
}


function streamzon_display_books($streamzon_books,$search_query,$search_query_discount)
{
    global $streamzon_theme_main_settings;

    $amazon_item_settings = get_option('streamzon_amazon_item_settings_option');
    if (!isset($amazon_item_settings['amazon_item_attributes']) || !is_array($amazon_item_settings['amazon_item_attributes'])) {
        $amazon_item_settings['amazon_item_attributes'] = array();
    }
    $amazon_settings = get_option('streamzon_amazon_settings_option');
    $amazon_stores = get_option('streamzon_amazon_stores_option');

    foreach ($streamzon_books as $streamzon_book) {
		
        $streamzon_book_DetailPageURL = isset($streamzon_book['DetailPageURL']) ? $streamzon_book['DetailPageURL'] : '';
        $streamzon_book_MediumImageURL = isset($streamzon_book['MediumImage']['URL']) ? $streamzon_book['MediumImage']['URL'] : '';
        $streamzon_book_MediumImageWidth = isset($streamzon_book['MediumImage']['Width']['_']) ? $streamzon_book['MediumImage']['Width']['_'] : '';
        $streamzon_book_MediumImageHeight = isset($streamzon_book['MediumImage']['Height']['_']) ? $streamzon_book['MediumImage']['Height']['_'] : '';
        $streamzon_book_Title = isset($streamzon_book['ItemAttributes']['Title']) ? $streamzon_book['ItemAttributes']['Title'] : '';
        $streamzon_book_Reviews = isset($streamzon_book['ItemLinks']['ItemLink'][5]['URL']) ? $streamzon_book['ItemLinks']['ItemLink'][5]['URL'] : '';
        $streamzon_book_ListPrice = isset($streamzon_book['ItemAttributes']['ListPrice']['FormattedPrice']) ? $streamzon_book['ItemAttributes']['ListPrice']['FormattedPrice'] : null;
        $streamzon_book_OfferListingPrice = isset($streamzon_book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice']) ? $streamzon_book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'] : null;

        $streamzon_book_ISBN = isset($streamzon_book['ItemAttributes']['ISBN']) ? $streamzon_book['ItemAttributes']['ISBN'] : '';
        $streamzon_book_EISBN = isset($streamzon_book['ItemAttributes']['EISBN']) ? $streamzon_book['ItemAttributes']['EISBN'] : '';
        $streamzon_book_Author = isset($streamzon_book['ItemAttributes']['Author']) ? $streamzon_book['ItemAttributes']['Author'] : '';
        $streamzon_book_Binding = isset($streamzon_book['ItemAttributes']['Binding']) ? $streamzon_book['ItemAttributes']['Binding'] : '';
        $streamzon_book_Brand = isset($streamzon_book['ItemAttributes']['Brand']) ? $streamzon_book['ItemAttributes']['Brand'] : '';
        $streamzon_book_EAN = isset($streamzon_book['ItemAttributes']['EAN']) ? $streamzon_book['ItemAttributes']['EAN'] : '';
        $streamzon_book_EANList = isset($streamzon_book['ItemAttributes']['EANList']['EANListElement']) ? $streamzon_book['ItemAttributes']['EANList']['EANListElement'] : ''; // array
        $streamzon_book_Edition = isset($streamzon_book['ItemAttributes']['Edition']) ? $streamzon_book['ItemAttributes']['Edition'] : '';
        $streamzon_book_IsEligibleForTradeIn = isset($streamzon_book['ItemAttributes']['IsEligibleForTradeIn']) ? $streamzon_book['ItemAttributes']['IsEligibleForTradeIn'] : '';
        $streamzon_book_Language = isset($streamzon_book['ItemAttributes']['Languages']['Language']) ? $streamzon_book['ItemAttributes']['Languages']['Language'] : '';
        $streamzon_book_Manufacturer = isset($streamzon_book['ItemAttributes']['Manufacturer']) ? $streamzon_book['ItemAttributes']['Manufacturer'] : '';
        $streamzon_book_NumberOfItems = isset($streamzon_book['ItemAttributes']['NumberOfItems']) ? $streamzon_book['ItemAttributes']['NumberOfItems'] : '';
        $streamzon_book_NumberOfPages = isset($streamzon_book['ItemAttributes']['NumberOfPages']) ? $streamzon_book['ItemAttributes']['NumberOfPages'] : '';
        $streamzon_book_ProductGroup = isset($streamzon_book['ItemAttributes']['ProductGroup']) ? $streamzon_book['ItemAttributes']['ProductGroup'] : '';
        $streamzon_book_ProductTypeName = isset($streamzon_book['ItemAttributes']['ProductTypeName']) ? $streamzon_book['ItemAttributes']['ProductTypeName'] : '';
        $streamzon_book_PublicationDate = isset($streamzon_book['ItemAttributes']['PublicationDate']) ? $streamzon_book['ItemAttributes']['PublicationDate'] : '';
        $streamzon_book_Studio = isset($streamzon_book['ItemAttributes']['Studio']) ? $streamzon_book['ItemAttributes']['Studio'] : '';
        $streamzon_book_TradeInValue = isset($streamzon_book['ItemAttributes']['TradeInValue']['FormattedPrice']) ? $streamzon_book['ItemAttributes']['TradeInValue']['FormattedPrice'] : '';
        $streamzon_book_CatalogNumberList = isset($streamzon_book['ItemAttributes']['CatalogNumberList']['CatalogNumberListElement']) ? $streamzon_book['ItemAttributes']['CatalogNumberList']['CatalogNumberListElement'] : ''; // array
        $streamzon_book_Creator = isset($streamzon_book['ItemAttributes']['Creator']) ? $streamzon_book['ItemAttributes']['Creator'] : ''; // array

		$streamzon_book_price = isset($streamzon_book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice']) ? $streamzon_book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'] : ''; // array
		$streamzon_book_SalePrice_price = isset($streamzon_book['Offers']['Offer']['OfferListing']['SalePrice']['FormattedPrice']) ? $streamzon_book['Offers']['Offer']['OfferListing']['SalePrice']['FormattedPrice'] : ''; // array
		$streamzon_book_LowestNewPrice_price = isset($streamzon_book['OfferSummary']['LowestNewPrice']['FormattedPrice']) ? $streamzon_book['OfferSummary']['LowestNewPrice']['FormattedPrice'] : ''; // array

		if($streamzon_theme_main_settings['box_big_small_img'] == 2)
		{
		    $streamzon_book_FullImageURL = isset($streamzon_book['LargeImage']['URL']) ? $streamzon_book['LargeImage']['URL'] : '';
	        $streamzon_book_FullImageWidth = isset($streamzon_book['LargeImage']['Width']['_']) ? $streamzon_book['LargeImage']['Width']['_'] : '';
	        $streamzon_book_FullImageHeight = isset($streamzon_book['LargeImage']['Height']['_']) ? $streamzon_book['LargeImage']['Height']['_'] : '';
		}	
		else if($streamzon_theme_main_settings['box_big_small_img'] == 1){
			$streamzon_book_FullImageURL = isset($streamzon_book['LargeImage']['URL']) ? $streamzon_book['LargeImage']['URL'] : '';
	        $streamzon_book_FullImageWidth = isset($streamzon_book['LargeImage']['Width']['_']) ? $streamzon_book['LargeImage']['Width']['_'] : '';
	        $streamzon_book_FullImageHeight = isset($streamzon_book['LargeImage']['Height']['_']) ? $streamzon_book['LargeImage']['Height']['_'] : '';
		}
		else{
			$streamzon_book_FullImageURL = isset($streamzon_book['LargeImage']['URL']) ? $streamzon_book['LargeImage']['URL'] : '';
	        $streamzon_book_FullImageWidth = isset($streamzon_book['LargeImage']['Width']['_']) ? $streamzon_book['LargeImage']['Width']['_'] : '';
	        $streamzon_book_FullImageHeight = isset($streamzon_book['LargeImage']['Height']['_']) ? $streamzon_book['LargeImage']['Height']['_'] : '';
		}
		
		if($streamzon_book['OfferSummary']['LowestNewPrice']['Amount'] > 0){
			$offer_price	=	$streamzon_book['OfferSummary']['LowestNewPrice']['FormattedPrice'];
			$offer_amount	=	$streamzon_book['OfferSummary']['LowestNewPrice']['Amount'];	
		}
		else
		{
			$offer_price	=	"";
			$offer_amount	=	"";
		}


		if($streamzon_book['Offers']['Offer']['OfferListing']['Price']['Amount'] > 0){
			$list_price		=	$streamzon_book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'];
			$list_amount	=	$streamzon_book['Offers']['Offer']['OfferListing']['Price']['Amount'];	
		}
		else if($streamzon_book['ItemAttributes']['ListPrice']['Amount'] > 0){
			$list_price		=	$streamzon_book['ItemAttributes']['ListPrice']['FormattedPrice'];
			$list_amount	=	$streamzon_book['ItemAttributes']['ListPrice']['Amount'];	
		}

		if($streamzon_book['ItemAttributes']['ListPrice']['Amount'] > 0){
			$high_price		=	$streamzon_book['ItemAttributes']['ListPrice']['FormattedPrice'];
			$high_amount	=	$streamzon_book['ItemAttributes']['ListPrice']['Amount'];					
		}
		
		if($offer_amount > $list_amount)
		{
			$stage = $list_price;
			$list_price = $offer_price;
			$offer_price = $stage;
		}

		
	
		///custom variable by VJ
			$streamzon_book_Asin	=	isset($streamzon_book['ASIN']) ? $streamzon_book['ASIN'] : ''; // array
			$twitterTxt				=	(strlen($streamzon_book_Title) > 118) ? substr($streamzon_book_Title, 0, 114)."..." : $streamzon_book_Title;		
		////vj end


        if ($amazon_settings['amazon_paid_free'] == 1 && !$streamzon_book_ListPrice && !$streamzon_book_OfferListingPrice) {
            $streamzon_book_ListPrice = '$0.00';
            $streamzon_book_OfferListingPrice = '$0.00';
        }

        $no_image_class = 'no-image';

        

        if($high_amount > $list_amount):
            $LPRICE = $high_price;
        elseif ($offer_price): 
            $LPRICE = $list_price;
        elseif ($list_price):
            $LPRICE = $list_price;
        else:
         $rawPrice = getPricefromRaw($streamzon_book_Asin);
            $LPRICE = ($rawPrice) ?  $rawPrice : "$0.00";
        endif; 

        if ($offer_price):
            $YOURPRICE = $list_price;
        else:
            $rawPrice = getPricefromRaw($streamzon_book_Asin);
            $YOURPRICE = ($rawPrice) ?  $rawPrice : "$0.00";
        endif;

        if($LPRICE == "$0.00"){
            $LPRICE = $YOURPRICE;
        }elseif($YOURPRICE == "$0.00"){
            $YOURPRICE = $LPRICE;
        }

        ?>



        <article class="post-168877 post type-post status-publish format-standard hentry category-mystery-crime-thrillers-books category-fiction category-kindle-ebooks tag-julie-smith post clearfix isotope-item">
            <div class="post-inner">
                <?php if ($streamzon_book_MediumImageURL && in_array('amazon_item_Image', $amazon_item_settings['amazon_item_attributes'])): ?>
                    <figure class="post-image ">

						<?php if($amazon_settings['amazon_add2Cart'] == 0 && $amazon_settings['amazon_addsingPage'] != 1 ): ?>
	                        <a href="<?php echo $streamzon_book_DetailPageURL; ?>" target="_blank" name="openprod">
								<img style="" src="<?php echo $streamzon_book_FullImageURL; ?>"  width="<?php echo $streamzon_book_FullImageWidth; ?>">
							</a>
						<?php elseif($amazon_settings['amazon_add2Cart'] == 1 && $amazon_settings['amazon_addsingPage'] != 1) : ?>
							<a href="javascript:void(0);" name="add2cart" data-asin="<?php print $streamzon_book_Asin;?>">
								<img style="" src="<?php echo $streamzon_book_FullImageURL; ?>" width="<?php echo $streamzon_book_FullImageWidth; ?>">
							</a>
						<?php endif;?>
						<?php if($amazon_settings['amazon_addsingPage'] == 1) : ?>
							<a href="<?php print get_site_url();?>/products/?asin=<?php print $streamzon_book_Asin;?>&disc=<?php echo $search_query_discount;?>&search=<?php echo $search_query; ?>" name="addsingPage" data-asin="<?php print $streamzon_book_Asin;?>">
								<img style="" src="<?php echo $streamzon_book_FullImageURL; ?>" width="<?php echo $streamzon_book_FullImageWidth; ?>">
							</a>
						<?php endif;?>
						</figure>
                    <?php $no_image_class = ''; ?>
                <?php endif; ?>

                <div class="post-content clearfix <?php echo $no_image_class; ?>">

                    <?php if (in_array('amazon_item_Title', $amazon_item_settings['amazon_item_attributes'])): ?>
						<?php if($amazon_settings['amazon_add2Cart'] == 0 && $amazon_settings['amazon_addsingPage'] != 1 ): ?>
	                        <h1 class="post-title"><a href="<?php echo $streamzon_book_DetailPageURL; ?>" target="_blank">
								<?php echo $streamzon_book_Title; ?>
							</a></h1>
						<?php elseif($amazon_settings['amazon_add2Cart'] == 1 && $amazon_settings['amazon_addsingPage'] != 1) : ?>
							<h1 class="post-title"><a href="javascript:void(0);" name="add2cart" data-asin="<?php print $streamzon_book_Asin;?>">
								<?php echo $streamzon_book_Title; ?>
							</a></h1>
						<?php endif;?>
						<?php if($amazon_settings['amazon_addsingPage'] == 1) : ?>
							<h1 class="post-title"><a href="<?php print get_site_url();?>/products/?asin=<?php print $streamzon_book_Asin;?>&disc=<?php echo $search_query_discount;?>&search=<?php echo $search_query; ?>" name="addsingPage" data-asin="<?php print $streamzon_book_Asin;?>">
								<?php echo $streamzon_book_Title; ?>
							</a></h1>
						<?php endif;?>
          
		          <?php endif; ?>

                    <?php if (in_array('amazon_item_Reviews', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <span class="swSprite s_chevron "></span>
                            <a href="<?php echo $streamzon_book_Reviews; ?>">Reviews</a>
                        </div>
                    <?php endif; ?>

                    <div class="amazon-prices">

                        <?php if (in_array('amazon_item_ListPrice', $amazon_item_settings['amazon_item_attributes'])): ?>
                            <div class="amazon-list-price">
                                <strong>List Price</strong>: <br/>
		                        <span><?=$LPRICE;?></span>
                            </div>
                        <?php endif; ?>

                        <?php if (in_array('amazon_item_YourPrice', $amazon_item_settings['amazon_item_attributes'])): ?>
                            <div class="amazon-your-price">
							
                                <strong>Your Price</strong>: <br/>
                                <span><?=$YOURPRICE;?></span>
                            </div>
                        <?php endif; ?>

                    </div>

                    <?php if ($streamzon_book_ISBN && in_array('amazon_item_ISBN', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>ISBN:</strong> <?php echo $streamzon_book_ISBN; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_EISBN && in_array('amazon_item_ISBN', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>EISBN:</strong> <?php echo $streamzon_book_EISBN; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Author && in_array('amazon_item_Author', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Author:</strong> <?php echo !is_array($streamzon_book_Author) ? $streamzon_book_Author : implode(', ', $streamzon_book_Author); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Binding && in_array('amazon_item_Binding', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Binding:</strong> <?php echo $streamzon_book_Binding; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Brand && in_array('amazon_item_Brand', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Brand:</strong> <?php echo $streamzon_book_Brand; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_EAN && in_array('amazon_item_EAN', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>EAN:</strong> <?php echo $streamzon_book_EAN; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_EANList && in_array('amazon_item_EANList', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>EAN List:</strong> <?php echo !is_array($streamzon_book_EANList) ? $streamzon_book_EANList : implode(', ', $streamzon_book_EANList); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Edition && in_array('amazon_item_Edition', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Edition:</strong> <?php echo $streamzon_book_Edition; ?>
                        </div>
                    <?php endif; ?>

                    <?php if (in_array('amazon_item_IsEligibleForTradeIn', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Eligible For Trade In:</strong> <?php echo $streamzon_book_IsEligibleForTradeIn ? 'yes' : 'no'; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Language && is_array($streamzon_book_Language) && in_array('amazon_item_Language', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <?php foreach ($streamzon_book_Language as $streamzon_book_Language_pub): ?>
                                <?php if (!isset($streamzon_book_Language_pub['Type']) || $streamzon_book_Language_pub['Type'] != 'Published') continue; ?>
                                <strong>Language:</strong> <?php echo $streamzon_book_Language_pub['Name']; ?>
                            <?php endforeach; ?>
                            <?php if(isset($streamzon_book_Language['Type']) && $streamzon_book_Language['Type'] == 'Published'): ?>
                                <strong>Language:</strong> <?php echo $streamzon_book_Language['Name']; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Manufacturer && in_array('amazon_item_Manufacturer', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Manufacturer:</strong> <?php echo $streamzon_book_Manufacturer; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_NumberOfItems && in_array('amazon_item_NumberOfItems', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Number Of Items:</strong> <?php echo $streamzon_book_NumberOfItems; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_NumberOfPages && in_array('amazon_item_NumberOfPages', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Number Of Pages:</strong> <?php echo $streamzon_book_NumberOfPages; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_ProductGroup && in_array('amazon_item_ProductGroup', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Product Group:</strong> <?php echo $streamzon_book_ProductGroup; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_ProductTypeName && in_array('amazon_item_ProductTypeName', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Product Type Name:</strong> <?php echo $streamzon_book_ProductTypeName; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_PublicationDate && in_array('amazon_item_PublicationDate', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Publication Date:</strong> <?php echo $streamzon_book_PublicationDate; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Studio && in_array('amazon_item_Studio', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Studio:</strong> <?php echo $streamzon_book_Studio; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_TradeInValue && in_array('amazon_item_TradeInValue', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Trade In Value:</strong> <?php echo $streamzon_book_TradeInValue; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_CatalogNumberList && in_array('amazon_item_CatalogNumberList', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <div class="book-attribute">
                            <strong>Catalog Number List:</strong> <?php echo !is_array($streamzon_book_CatalogNumberList) ? $streamzon_book_CatalogNumberList : implode(', ', $streamzon_book_CatalogNumberList); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($streamzon_book_Creator && is_array($streamzon_book_Creator) && in_array('amazon_item_Creator', $amazon_item_settings['amazon_item_attributes'])): ?>
                        <?php $creators = array(); ?>
                        <div class="book-attribute">
                            <?php foreach ($streamzon_book_Creator as $streamzon_book_Creator_v): ?>
                                <?php if (!isset($streamzon_book_Creator_v['_']) || !isset($streamzon_book_Creator_v['Role'])) continue; ?>
                                <?php $creators[] = $streamzon_book_Creator_v['_'] . ' (' . $streamzon_book_Creator_v['Role'] . ')'; ?>
                            <?php endforeach; ?>
                            <?php if (!empty($creators)): ?>
                                <strong>Creators:</strong> <?php echo implode(', ', $creators); ?>
                            <?php endif; ?>
                            <?php if (isset($streamzon_book_Creator['_']) && isset($streamzon_book_Creator['Role'])): ?>
                                <strong>Creator:</strong> <?php echo $streamzon_book_Creator['_'] . ' (' . $streamzon_book_Creator['Role'] . ')'; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                </div>
				<div class="post-social">
					<a href="javascript:void(0);" class="sb-sicon sb-facebook fa-hover" name="fblinks"
							data-peru="<?php echo $streamzon_book_Title; ?>" 
							data-detail="<?php echo $streamzon_book_DetailPageURL; ?>"
							data-photo="<?php echo $streamzon_book_MediumImageURL; ?>"
						>
						
					</a>
					<a href="https://twitter.com/intent/tweet" class="sb-sicon sb-twitter fa-hover" name="twlinks" data-peru="<?php echo urlencode($twitterTxt); ?>" data-detail="<?php echo urlencode($streamzon_book_DetailPageURL); ?>"></a>
					<a href="javascript:void(0);" class="sb-sicon sb-google fa-hover" name="gplinks" data-detail="<?php echo urlencode($streamzon_book_DetailPageURL); ?>"></a>
				</div>
                                <div class="cartbtn">
                                    <?php if($amazon_settings['Product_cart_anable']): ?>
                                    <div class="simpleCart_shelfItem">
                                    <img style="display: none;" class="item_image" src="<?php print $streamzon_book_FullImageURL;?>"/>
                                    <div style="display: none;" class="item_asin"><?php echo $streamzon_book_Asin; ?></div>
                                    <div style="display: none;" class="item_name"><?php echo $streamzon_book_Title; ?></div>
                                    <div style="display: none;" class="item_price"><?php
                                   $amount=str_replace(",",".",$list_price);
                                    print $amount; ?></div>
                                    <a class="item_add color-11 puerto-btn-2" href="javascript:;" >
                                        <span><i class="fa fa-shopping-cart"></i></span>
                                        <small>Add to cart</small>
                                    </a>
                                    </div>
                                    <?php endif;?>
                                </div>	
                <div class="post-footer clear">
                    <img src="<?php echo isset($streamzon_theme_main_settings['box_bar_logo_use']) && $streamzon_theme_main_settings['box_bar_logo_use'] == 1 && isset($streamzon_theme_main_settings['box_bar_logo']) && !empty($streamzon_theme_main_settings['box_bar_logo']) ? $streamzon_theme_main_settings['box_bar_logo'] : IMAGES . '/amazon.png'; ?>" alt="">
                </div>
            </div>
            <!-- /.post-inner -->
        </article>
        <!-- /.post -->

        <?php

    }

}


function ajax_streamzon_display_books()
{
    error_log("ajax_streamzon_display_books: ".$_POST['search_query']." -- page = ".$_POST['page']);

    $streamzon_error_message = null;
    $streamzon_books = null;

    $streamzon_theme_settings = get_option('streamzon_theme_settings_option');
    $streamzon_amazon_settings = get_option('streamzon_amazon_settings_option');

    $search_query = '';
    if (isset($_POST['search_query']) && !empty($_POST['search_query'])) {
        $search_query = $_POST['search_query'] . " " . $streamzon_amazon_settings['default_search_keyword'];
    }
	if (isset($_POST['disc']) && !empty($_POST['disc'])) {
    $search_query_discount = $_POST['disc'];
	}
    if ((!isset($_POST['search_query']) || empty($_POST['search_query'])) && isset($streamzon_amazon_settings['default_search_keyword']) && !empty($streamzon_amazon_settings['default_search_keyword'])) {
        $search_query = $streamzon_amazon_settings['default_search_keyword'];
    }

    try {
        $streamzon_books = streamzon_amazon_search_books($search_query, $_POST['page'], $_POST['extra_step'], $market);
    } catch (Exception $e) {
        $streamzon_error_message = $e->getMessage();
        die();
    }

    if ($streamzon_books) {
        streamzon_display_books($streamzon_books,$search_query,$search_query_discount);
    }

    die();
}


function streamzon_admin_enqueue_scripts($hook)
{
    
    //var_dump($hook);die;
    wp_enqueue_script('streamzon-export-main-script', get_stylesheet_directory_uri() . '/js/export.js');

    wp_enqueue_style('streamzon_admin_css', get_template_directory_uri() . '/css/admin.css');
    
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'streamzon-color-picker', get_stylesheet_directory_uri() . '/js/color_picker_script.js', array( 'wp-color-picker' ), false, true );

    if ('stream-store_page_streamzon-amazon-settings' == $hook) {
        //wp_enqueue_script('streamzon-admin-main-script');
        wp_enqueue_script( 'streamzon-admin-js', get_stylesheet_directory_uri() . '/js/admin.js', array(), false, true );
        wp_localize_script('streamzon-admin-main-script', 'streamzon_object', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
        ));
    }

    if ('toplevel_page_streamzon-theme-settings' == $hook) { 
    
        wp_enqueue_script( 'streamzon-admin-isotop-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), false, true );
        wp_enqueue_script( 'streamzon-admin-js', get_stylesheet_directory_uri() . '/js/admin.js', array(), false, true );           
        wp_enqueue_script( 'streamzon-form-js', get_stylesheet_directory_uri() . '/js/jquery.form.min.js', array('jquery'), false, true );           
    }
    if ('stream-store_page_streamzon-amazon-credentials' == $hook) {
        wp_enqueue_script( 'streamzon-admin-isotop-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), false, true );
        wp_enqueue_script( 'streamzon-admin-js', get_stylesheet_directory_uri() . '/js/admin.js', array(), false, true );
    }

    if ('stream-store_page_streamzon-theme-options' == $hook) {      
        wp_enqueue_script( 'streamzon-admin-isotop-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), false, true );
        wp_enqueue_script( 'streamzon-admin-js', get_stylesheet_directory_uri() . '/js/admin.js', array(), false, true );
    }
    if ('stream-store_page_streamzon-amazon-settings' == $hook) {  
        wp_enqueue_script( 'streamzon-admin-isotop-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), false, true );
    }     

    if ('stream-store_page_streamzon-amazon-item-settings' == $hook) {
        wp_enqueue_style('streamzon-jquery-picklist-css', get_template_directory_uri() . '/vendor/jquery-picklist/jquery-picklist.css');
        wp_enqueue_style('streamzon_admin_css', get_template_directory_uri() . '/css/admin.css');
        wp_enqueue_script( 'streamzon-jquery-picklist', get_stylesheet_directory_uri() . '/vendor/jquery-picklist/jquery-picklist.min.js', array('jquery', 'jquery-ui-widget'), false, true );
        wp_enqueue_script( 'streamzon-jquery-picklist-js', get_stylesheet_directory_uri() . '/js/admin_jquery_picklist_script.js', array(), false, true );
    }
    wp_register_script('streamzon-admin-main-script', get_stylesheet_directory_uri() . '/js/main.js');
}

function ajax_streamzon_load_subcategories()
{
    global $streamzon_settings_page;

    $domain = $market = trim($_POST['market']);

	if($domain == "co.uk")
		$domain = 'uk';
	else if($market == "com")
		$domain = 'us';
	else if($market == "co.jp")
		$domain = 'jp';
	
    $category = $_POST['category'];

    if (!is_array($category)) die(0);

    if (!array_key_exists($market, $streamzon_settings_page->getAmazonLocales())) {
        die(0);
    }
    //$subcategories = $streamzon_settings_page->generate_amazon_category_selects($market, $category, $market);

	$string = "generate_amazon_".$domain."_category_selects";
    $subcategories = $streamzon_settings_page->$string($market, $category, $market);


    if (!$subcategories) die(0);

    echo $subcategories;
    die();
}

function ajax_streamzon_load_categories()
{
    global $streamzon_settings_page;

    $domain = $market = trim($_POST['market']);

	if($domain == "co.uk")
		$domain = 'uk';
	else if($market == "com")
		$domain = 'us';
	else if($market == "co.jp")
		$domain = 'jp';


    if (!array_key_exists($market, $streamzon_settings_page->getAmazonLocales())) {
        die(0);
    }
	$string = "generate_amazon_".$domain."_category_select";
	$categories = $streamzon_settings_page->$string($market, array(''), true);

    //$categories = $streamzon_settings_page->generate_amazon_category_select($market, array(''), true);

    echo $categories;
    die();
}

function streamzon_get_browse_node_ids_by_market($market)
{
    $arr = array();
    $locales = StreamzonSettingsPage::$amazon_locales;
    $locale = $locales[$market];
    $amazon_browse_node_ids = streamzon_trim_amazon_browse_node_ids(parse_ini_file(get_template_directory() . '/configs/amazon_browse_node_ids.ini', true));
    if (isset($amazon_browse_node_ids[$locale]) && is_array($amazon_browse_node_ids[$locale])) {
        foreach ($amazon_browse_node_ids[$locale] as $node_id => $node_titles) {
            if (is_array($node_titles)) {
                foreach ($node_titles as $node_title) {
                    $arr[] = array(
                        'BrowseNodeId' => $node_id,
                        'Name' => $node_title
                    );
                }
            }
        }
    }

    return $arr;
}

function streamzon_trim_amazon_browse_node_ids($config)
{
    array_walk_recursive($config, 'streamzon_trim_string_in_array');

    return $config;
}

function streamzon_trim_string_in_array(&$item, $key)
{
    if (is_string($item)) $item = trim($item);
}

function streamzon_hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    }
    $rgb = array($r, $g, $b);
    //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; // returns an array with the rgb values
}

/* WIDGETS */

/**
 * Categories widget class
 *
 * @since 2.8.0
 */
class WP_Widget_Streamzon_Amazon_Categories extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array(
            'classname' => 'widget_streamzon_amazon_categories',
            'description' => __("Streamzon: a list of Amazon categories.")
        );
        parent::__construct('streamzon-amazon-categories', __('Stream Store - Amazon Categories'), $widget_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? __('Amazon Categories') : $instance['title'], $instance, $this->id_base);

        echo $before_widget;
        if ($title) echo $before_title . $title . $after_title;


        $amazon_settings = $this->amazon_stores = get_option('streamzon_amazon_stores_option');
        //$market = $amazon_stores['amazon_market'];
		$market_dynamic = getMarketplace(getCountryCode());
		$market	=	$market_dynamic['marketplace'];

		if($market){
			switch($market)
			{
				case 'com':
					$amazoncategory	=	'amazon_category';
					break;
				case 'co.uk':
					$amazoncategory	=	'amazon_uk_category';
					break;
				case 'ca':
					$amazoncategory	=	'amazon_ca_category';
					break;
				case 'cn':
					$amazoncategory	=	'amazon_cn_category';
					break;
				case 'de':
					$amazoncategory	=	'amazon_de_category';
					break;
				case 'es':
					$amazoncategory	=	'amazon_es_category';
					break;
				case 'fr':
					$amazoncategory	=	'amazon_fr_category';
					break;
				case 'in':
					$amazoncategory	=	'amazon_in_category';
					break;
				case 'it':
					$amazoncategory	=	'amazon_it_category';
					break;
				case 'co.jp':
					$amazoncategory	=	'amazon_jp_category';
					break;
				default:
					$amazoncategory	=	'amazon_category';
					break;
			}
		}

        $categories = $amazon_settings[$amazoncategory];

        $category = '';
        while (!$category) {
            if(!is_array($categories))
                break;

            $category = array_pop($categories);
            if ($category === null) break;
        }


        $subcategories = streamzon_amazon_browse_node_lookup($market, $category);

	
        // if no subcategories in the category, get adjacent categories
        if (!$subcategories || empty($subcategories)) {
            if(!is_array($categories))
                $category = null;
            else
                $category = array_pop($categories);
            $subcategories = streamzon_amazon_browse_node_lookup($market, $category);
        }

        // if no category is selected, get root categories from config
        if ($category === null) {
            $subcategories = streamzon_get_browse_node_ids_by_market($market);
        }

        if (!empty($subcategories) && is_array($subcategories)) {
            ?>
            <ul>
                <?php
				//$searchval	=	$_GET['s'];
                /*foreach ($subcategories as $subcategory) {?>
	                <li><a href="<?php print get_site_url();?>/?s=<?php print $searchval?>&a_cat=<?php echo $subcategory['BrowseNodeId']; ?>"><?php echo $subcategory['Name']; ?></a></li>
                <?php }
				*/
                ?>
				<?php
                foreach ($subcategories as $subcategory) {
                    ?>
                 <li><a href="<?php print get_site_url();?>/?s=<?php echo $_GET['s']; ?>&a_cat=<?php echo $subcategory['BrowseNodeId']; ?>"><?php echo $subcategory['Name']; ?></a></li>
                <?php
                }
                ?>
            </ul>

        <?php
        }

        echo $after_widget;
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    function form($instance)
    {
        //Defaults
        $instance = wp_parse_args((array)$instance, array('title' => ''));
        $title = esc_attr($instance['title']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>

    <?php
    }

}
add_action('widgets_init', create_function('', 'register_widget("WP_Widget_Streamzon_Amazon_Categories");'));

//////////grabing price from raw amazon page////////////////////
	function getPricefromRaw($asin)
	{
		$ctryCode	=	getMarketplace(getCountryCode());
		$url 		= 	"http://www.amazon.".$ctryCode['marketplace']."/gp/aw/ol/".$asin;
		$data		=	send_Curl($url);
		$code		=	explode("<br />",preg_replace("/\s+/", " ", $data));
		
		switch($ctryCode['marketplace'])
		{
			case "com":
				$expl = "$";
				$sym	=	"$";
				break;
			case "fr":
			case "de":
			case "it":
			case "es":
				$expl = "EUR";
				$sym	=	"&euro;";
				break;
			case "co.uk":
				$expl = "New - ";
				$sym	=	"&pound;";
				break;
			case "ca":
				$expl = "CDN$";
				$sym	=	"CDN$";
				break;
			case "in":
				$expl = "New - Rs.";
				$sym	=	"&#8377;";
				break;
			case "co.jp":
			case "cn":
				$expl = "�";
				$sym	=	"&yen;";
				break;
			
		}
		
		foreach($code as $line)
		{
			//print $line."<br>";
			if(strpos(strip_tags($line), $expl))
			{
				//print $line."<br>";
				$price2 = explode($expl,strip_tags($line));
				$myPrice = 	preg_replace("/[^0-9.,]/", "",$price2[1]);
				return $sym.$myPrice;
			}
		}
		
		return $myPrice;
	}
	
	function send_Curl($url)
	{
		$ch = curl_init();
		$timeout = 5;
		$headers = array("Content-type: text/xml;charset=\"iso-8859-1\""); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
//////////////////CUSTOM WIDGET $ DISCOUNTS SIDEBAR//////////////////////////////////////////////
class wpb_widget extends WP_Widget {
	
	function __construct() {
		parent::__construct(
		'wpb_widget', 
		__('Stream store - Search with Discount', 'wpb_widget_domain'), 
		array( 'description' => __( 'Stream store - Search with Discount', 'wpb_widget_domain' ), ) 
		);
	}

	public function widget( $args, $instance ) {
		global $streamzon_theme_main_settings;
		$amazon_settings = get_option('streamzon_amazon_settings_option');


		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];

		if ( ! empty( $title ) ){
            //echo $args['before_title'] . $title . $args['after_title'];
        //echo __( 'Hello, World!', 'wpb_widget_domain' );
        }
		
            
		if(isset($_GET['disc_val'])){
            $disc_val = ($_GET['disc_val'] > 0) ? $_GET['disc_val'] : 0 ;
        }else{
            $disc_val = $amazon_settings['amazon_discount_percent'];
        }
		$market_place	=	getMarketplace(getCountryCode());
		$mct_title		=	"_".$market_place['ct_title'];
		?>
			
			<form id="slidebarsubmit" class="form-search slidebarsearch" action="<?php echo home_url( '/' ); ?>" method="get">				
				<section id="rightDiscSlider">
                <? $mct_title = $mct_title == "_USA" ? "" : $mct_title;?>
    				<div align="center"><div class="display-box"><?php print $streamzon_theme_main_settings['side_discountbar_display_text'.$mct_title]?><span id="js-display-change"></span></div></div>
    				<div class="slider-wrapper">
                		<input type="text" class="js-check-change" />
                		<input type="hidden" name="disc_val" id="disc_val" value="<?php echo $disc_val;?>"/>
    			  	</div>
                </section>	
                			
				<div class="searchinput">
				<input class="input-xxlarge dddddd slidebarsinput" type="text" value="" id="search" name="s"   placeholder="Find...">
				<?php if ($streamzon_theme_main_settings['slidebar_searchbar_image']==1) : ?>
				<a  onclick="return false;" href="javascript:void(0);" name="clickable_image2" id="submitslidebarsubmit"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/search_icon.png" /></a>
				<?php endif; ?>	
				</div>
			</form>
			<script src="<?php bloginfo('stylesheet_directory'); ?>/landing-page/js/powerange.min.js"></script>
			<script>
			    var changeInput = document.querySelector('.js-check-change'), initChangeInput = new Powerange(changeInput, { hideRange: true, start: <?php echo $disc_val;?> });
				document.getElementById('js-display-change').innerHTML = ": <?php echo $disc_val;?>%";
				jQuery('#disc_val').val(<?php echo $disc_val;?>);

			    changeInput.onchange = function() {
			      	document.getElementById('js-display-change').innerHTML = ": "+changeInput.value+"%";
					jQuery('#disc_val').val(changeInput.value);
			    };
			</script>


		<?php
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

function create_analytics_table(){
	global $wpdb;

	$table_name = $wpdb->prefix . "streamzon_amazon_analytics"; 

	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
	{
	    $charset_collate = $wpdb->get_charset_collate();		
				$sql = "CREATE TABLE $table_name (
					  	id mediumint(9) NOT NULL AUTO_INCREMENT,
					  	impressions int(5) DEFAULT 0  NOT NULL,
					 	clicks int(5) DEFAULT 0,
						redirects int(5) DEFAULT 0,
						search varchar(64) DEFAULT '',
						user_ip INT UNSIGNED,
						country varchar(64) DEFAULT '',
						device varchar(64) DEFAULT '',
						user_agent varchar(256) DEFAULT '',
						searchfor varchar(256) DEFAULT '',
						page varchar(256) DEFAULT '',
						referer varchar(256) DEFAULT '',
					  	visitDate date DEFAULT '0000-00-00' NOT NULL,
					  	visittime time DEFAULT '00:00:00' NOT NULL,
					  	UNIQUE KEY id (id)
					) $charset_collate;";
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	else
	{
		$sQry	=	get_search_query(); 
		if(!is_admin() && $sQry) 
		{
			$table_name = $wpdb->prefix . "streamzon_amazon_analytics"; 
			$wpdb->insert( 
						$table_name, 
						array( 
							'impressions' 		=> 	0, 
							'clicks' 			=> 	0, 
							'redirects' 		=> 	0, 
							'search' 			=> 	1, 
							'user_ip'			=> 	getip2long(),
							'country'			=>	getCountry(),
							'device'			=>	getDeviceType(),
							'user_agent'		=>	@$_SERVER['HTTP_USER_AGENT'],
							'searchfor'			=>	$sQry,
							'page'				=>	getSlug(),
							'referer'			=>	@$_SERVER['HTTP_REFERER'],
							'visitDate'			=>	date('Y-m-d'),
							'visittime'			=>	date('H:i:s'),
						) 
					);
			//print $wpdb->last_query;
		}
		
	}	

	// create new page for individual products Individual_product_page 
	
	$existing_pages = get_pages();
    $existing_titles = array();

	foreach ($existing_pages as $page) 
    {
        $existing_titles[] = $page->post_title;
    }	
	if (!in_array("products", $existing_titles)) {

		$add_default_pages = array(
									'post_title' 	=> "products",
									'post_content' 	=> "",
									'post_status' 	=> "publish",
									'post_type' 	=> "page",
									'page_template'	=> "product_page.php",                                    
								);

		// insert the post into the database
		$result = wp_insert_post($add_default_pages); 
        //save post id (before hide page in menu)
        update_option('streamzon_page_id', $result);

	}
    


}
function getip2long()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	  $ip	=	$_SERVER['HTTP_CLIENT_IP'];
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	  $ip	=	$_SERVER['HTTP_X_FORWARDED_FOR'];
	else
	  $ip	=	$_SERVER['REMOTE_ADDR'];

	return ip2long($ip);
}

function getCountry()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		$ip	=	$_SERVER['HTTP_CLIENT_IP'];
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))	  
		$ip	=	$_SERVER['HTTP_X_FORWARDED_FOR'];
	else
		$ip	=	$_SERVER['REMOTE_ADDR'];

	$website_root_path	=	get_template_directory() . '/inc/GeoIP/';
	require($website_root_path . 'geoip.inc');
	$gi	=	geoip_open($website_root_path . 'GeoIP.dat',GEOIP_STANDARD);
	$country = geoip_country_name_by_addr($gi, $ip);geoip_close($gi);
	//print $country ;
	return ($country) ? $country : "";
}
function getCountryCode()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		$ip	=	$_SERVER['HTTP_CLIENT_IP'];
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))	  
		$ip	=	$_SERVER['HTTP_X_FORWARDED_FOR'];
	else
		$ip	=	$_SERVER['REMOTE_ADDR'];

	$website_root_path	=	get_template_directory() . '/inc/GeoIP/';
	require_once($website_root_path . 'geoip.inc');
	$gi	=	geoip_open($website_root_path . 'GeoIP.dat',GEOIP_STANDARD);
	$code	=	geoip_country_code_by_addr($gi, $ip);

	return trim($code);
}

function getMarketplace($country_code)
{
	$credentials 		= 	get_option( 'streamzon_amazon_credentials_option' );

	$marketPlace 		= 	"";
	$associated_tag 	= 	"";
	$associateId		=	"";


	if($country_code == "US"){
        $marketPlace 	= 	"com";
		$associateId	=	$credentials['amazon_associate_id'];
		$ct_title		=	"USA";
	}
	else if($country_code == "GB"){
        $marketPlace = "co.uk";
		$associateId	=	$credentials['amazon_associate_id_uk'];
		$ct_title		=	"UK";
    }
    else if($country_code == "DE"){
        $marketPlace = "de";
		$associateId	=	$credentials['amazon_associate_id_de'];
		$ct_title		=	"Germany";
	}
    else if($country_code == "CA"){
        $marketPlace = "ca";
		$associateId	=	$credentials['amazon_associate_id_ca'];
		$ct_title		=	"Canada";
    }
	else if($country_code == "FR"){
		$marketPlace = "fr";
		$associateId	=	$credentials['amazon_associate_id_fr'];
		$ct_title		=	"France";
	}
	else if($country_code == "JP"){
		$marketPlace = "co.jp";
		$associateId	=	$credentials['amazon_associate_id_jp'];
		$ct_title		=	"Japan";
	}
	else if($country_code == "IT"){
		$marketPlace = "it";
		$associateId	=	$credentials['amazon_associate_id_it'];
		$ct_title		=	"Italy";
	}
	else if($country_code == "CN"){
		$marketPlace = "cn";
		$associateId	=	$credentials['amazon_associate_id_cs'];
		$ct_title		=	"China";
	}
	else if($country_code == "ES"){
		$marketPlace = "es";
		$associateId	=	$credentials['amazon_associate_id_es'];
		$ct_title		=	"Spain";
	}
	else if($country_code == "IN"){
		$marketPlace = "in";
		$associateId	=	$credentials['amazon_associate_id_in'];
		$ct_title		=	"India";
	}
	else
	{
		$default_deails	=	getDefaultMarket();
		$marketPlace	=	$default_deails['marketplace'];
		$associateId	=	$default_deails['associateId'];
		$ct_title		=	$default_deails['ct_title'];
	}

	if(!$associateId)
	{	
		$default_deails	=	getDefaultMarket();
		$marketPlace	=	$default_deails['marketplace'];
		$associateId	=	$default_deails['associateId'];
		$ct_title		=	$default_deails['ct_title'];
	}
	$return = array(
			'marketplace'	=>	$marketPlace, 
			'associateId'	=> 	$associateId,
			'ct_title'		=>	$ct_title
			);

	return $return;
}
function getDefaultMarket()
{
	$credentials 		= 	get_option( 'streamzon_amazon_credentials_option' );
	$marketPlace_def	= 	$credentials['amazon_associate_auto'];
	$associateId		=	"thefinalecono-20";
	
	if($marketPlace_def == "US"){
        $marketPlace 	= 	"com";
		$associateId	=	$credentials['amazon_associate_id'];
		$ct_title		=	"USA";
	}
	else if($marketPlace_def == "GB"){
        $marketPlace = "co.uk";
		$associateId	=	$credentials['amazon_associate_id_uk'];
		$ct_title		=	"UK";
    }
    else if($marketPlace_def == "DE"){
        $marketPlace = "de";
		$associateId	=	$credentials['amazon_associate_id_de'];
		$ct_title		=	"Germany";
	}
    else if($marketPlace_def == "CA"){
        $marketPlace = "ca";
		$associateId	=	$credentials['amazon_associate_id_ca'];
		$ct_title		=	"Canada";
    }
	else if($marketPlace_def == "FR"){
		$marketPlace = "fr";
		$associateId	=	$credentials['amazon_associate_id_fr'];
		$ct_title		=	"France";
	}
	else if($marketPlace_def == "JP"){
		$marketPlace = "co.jp";
		$associateId	=	$credentials['amazon_associate_id_jp'];
		$ct_title		=	"Japan";
	}
	else if($marketPlace_def == "IT"){
		$marketPlace = "it";
		$associateId	=	$credentials['amazon_associate_id_it'];
		$ct_title		=	"Italy";
	}
	else if($marketPlace_def == "CN"){
		$marketPlace = "cn";
		$associateId	=	$credentials['amazon_associate_id_cs'];
		$ct_title		=	"China";
	}
	else if($marketPlace_def == "ES"){
		$marketPlace = "es";
		$associateId	=	$credentials['amazon_associate_id_es'];
		$ct_title		=	"Spain";
	}
	else if($marketPlace_def == "IN"){
		$marketPlace = "in";
		$associateId	=	$credentials['amazon_associate_id_in'];
		$ct_title		=	"India";
	}	

	if(!$associateId)
		$associateId	=	"thefinalecono-20";

	$return = array(
		'marketplace'	=>	$marketPlace, 
		'associateId'	=> 	$associateId,
		'ct_title'		=>	$ct_title
		);

	return $return;
}
function getDeviceType()
{
	$iPod 	= stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$iPad 	= stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$Android= stripos($_SERVER['HTTP_USER_AGENT'],"Android");
	$webOS	= stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$wow	= stripos($_SERVER['HTTP_USER_AGENT'],"WOW64");	
	$mac	= stripos($_SERVER['HTTP_USER_AGENT'],"mac");		

	if( $iPod || $iPhone )
		$device	=	"Mobiles";
	else if($iPad)
		$device	=	"Mobiles";
	else if($Android)
		$device	=	"Mobiles";
	else if($webOS)
		$device	=	"TVs";
	else if($wow || $mac)
		$device	=	"Desktops";
	else
		$device	=	"Desktops";

	return $device;
}

function getSlug(){	global $post;	$return =	$post->post_name;	return ($return != 'wewewewe') ? $return : "";}

require_once('wp-updates-theme.php');
new WPUpdatesThemeUpdater_1194( 'http://wp-updates.com/api/2/theme', basename( get_template_directory() ) );

add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    $amazon_settings = get_option('streamzon_amazon_settings_option');
    if ( isset( $_GET['s']) &&  $amazon_settings['default_search_keyword'] !== "" ) {                
        $result = strpos($_GET['s'], $amazon_settings['default_search_keyword'] ) === false ? "+". trim($amazon_settings['default_search_keyword']) : '';
        
        $query_vars['s'] = $_GET['s'] . $result;
    }
    elseif( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {        
		$amazon_settings = get_option('streamzon_amazon_settings_option');
		$query_vars['s'] =	$amazon_settings['default_search_keyword'];
    }
    return $query_vars;
}

add_action('wp_ajax_export_to_file', 'export_to_file');
add_action('wp_ajax_nopriv_export_to_file', 'export_to_file');
function export_to_file() {
    $options = get_option('streamzon_theme_settings_option');
    $json = json_encode($options);
    header('Content-disposition: attachment; filename=file.json');
    header('Content-type: application/json');
    readfile($json);


}

function get_unique_header_bg(){   
    
    if(!isset($_GET['p']) && !isset($_SESSION['uniq_header_img'])) 
    {
        return false;
    }

    $imagePath = "/streamimages/";
    $uploadDir = get_template_directory().$imagePath;

    $upl = wp_upload_dir(); //['baseurl']
    $uploadDir = $upl['basedir'].$imagePath;

    
    $filename = isset($_GET['p']) ? $_GET['p'] : $_SESSION['uniq_header_img'];
    $file = $uploadDir.$filename.".*";

    $file_arr = glob($file);
    $fileExists = count($file_arr) > 0 ? $file_arr[0] : false;    
    
    if($fileExists)
    {
        if(isset($_GET['p']))
        {
            $_SESSION['uniq_header_img'] = $_GET['p'];
        }

        $ext = substr(strrchr($fileExists, '.'), 1);
        return $upl['baseurl'].$imagePath. $filename.".".$ext;
    }else{
        return false;
    }
    
}
function pre($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

//get associated markets for onepage language switch
function get_credentials(){
    global $amazon_credentials;

    $countries = array(
        'amazon_associate_id' => 'usa',
        'amazon_associate_id_ca' => 'ca',
        'amazon_associate_id_uk' => 'uk',
        'amazon_associate_id_de' => 'de',
        'amazon_associate_id_fr' => 'fr',
        'amazon_associate_id_in' => 'in',
        'amazon_associate_id_it' => 'it',
        'amazon_associate_id_jp' => 'jp',
        'amazon_associate_id_cn' => 'cn'
        );
    
    $credentials = array();
    
    foreach($amazon_credentials as $key=>$val){
        if(array_key_exists($key, $countries) && $val !== ""){
            $credentials[] = $countries[$key];
        }
    }

    return $credentials;
}

include_once('extension.php');
//wp_deregister_script('jquery');