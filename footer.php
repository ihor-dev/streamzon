</div>
<!-- /#pagewrap -->

<?php

global $streamzon_theme_main_settings;
$credentials = get_option( 'streamzon_amazon_credentials_option' );
$streamzon_amazon_settings = get_option('streamzon_amazon_settings_option');

$sQry	=	get_search_query(); 

if($sQry) 
{
	$table_name = $wpdb->prefix . "streamzon_amazon_analytics"; 
	$wpdb->insert( 
				$table_name, 
				array( 
					'impressions' 		=> 	0, 
					'clicks' 			=> 	0, 
					'redirects' 		=> 	0, 
					'search' 			=> 	1, 
					'user_ip'			=> 	getip2long(),
					'device'			=>	getDeviceType(),
					'user_agent'		=>	@$_SERVER['HTTP_USER_AGENT'],
					'searchfor'			=>	$sQry,
					'page'				=>	getSlug(),
					'referer'			=>	@$_SERVER['HTTP_REFERER'],
					'visitDate'			=>	date('Y-m-d'),
					'visittime'			=>	date('H:i:s'),
				) 
			);
	//print $wpdb->last_query;
} 		

?>

<!-- wp_footer -->
<script type="text/javascript">
    /* <![CDATA[ */
    var themeStream = {"loadingImg": "", "maxPages": "414", "autoInfinite": "auto", "lightbox": {"lightboxSelector": ".lightbox", "lightboxOn": true, "lightboxContentImages": false, "lightboxContentImagesSelector": ".post-content a[href$=jpg],.page-content a[href$=jpg],.post-content a[href$=gif],.page-content a[href$=gif],.post-content a[href$=png],.page-content a[href$=png],.post-content a[href$=JPG],.page-content a[href$=JPG],.post-content a[href$=GIF],.page-content a[href$=GIF],.post-content a[href$=PNG],.page-content a[href$=PNG],.post-content a[href$=jpeg],.page-content a[href$=jpeg],.post-content a[href$=JPEG],.page-content a[href$=JPEG]", "theme": "pp_default", "social_tools": false, "allow_resize": true, "show_title": false, "overlay_gallery": false, "screenWidthNoLightbox": 600, "deeplinking": false, "contentImagesAreas": ".post, .type-page, .type-highlight, .type-slider", "gallerySelector": ".gallery-icon > a[href$=jpg],.gallery-icon > a[href$=gif],.gallery-icon > a[href$=png],.gallery-icon > a[href$=JPG],.gallery-icon > a[href$=GIF],.gallery-icon > a[href$=PNG],.gallery-icon > a[href$=jpeg],.gallery-icon > a[href$=JPEG]", "lightboxGalleryOn": true}, "lightboxContext": "#pagewrap", "sharrrephp": "", "sharehtml": "<a class=\"box\" href=\"#\"><div class=\"share\"><span>share<\/span><\/div><div class=\"count\" href=\"#\">{total}<\/div><\/a>", "fixedHeader": "", "ajax_nonce": "f8a93dd016", "ajax_url": "http:\/\/\/wordpress\/wp-admin\/admin-ajax.php", "itemBoard": "yes", "": "", "": "", "user_taken": "", "site_avail": "", "user_avail": "", "": "", "checking": "Checking...", "": "", "fillthisfield": "", "fillfields": "", "invalidEmail": "", "creationOk": ""};
    /* ]]> */
</script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/foot-a.js" async=""></script>

<?php 
	////javascript for social popup starts//////

    if (isset($streamzon_theme_main_settings['bitly_login_id']) && !empty($streamzon_theme_main_settings['bitly_login_id'])){
		$bitly_login_id	=	 $streamzon_theme_main_settings['bitly_login_id'];
	}

	if (isset($streamzon_theme_main_settings['bitly_api_key']) && !empty($streamzon_theme_main_settings['bitly_api_key'])){
		$bitly_api_key	=	 $streamzon_theme_main_settings['bitly_api_key'];
	}

?>
<div id="submit2amazon"></div>
<?php 
$streamzon_amazon_credentials_option = get_option('streamzon_amazon_credentials_option');
echo '<div id="associate_id">'.$streamzon_amazon_credentials_option['amazon_associate_id'].'</div>';
echo '<div id="associate_auto">'.$streamzon_amazon_credentials_option['amazon_associate_auto'].'</div>';
?>
<script>
function goBack() {
    window.history.back();
}
</script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/simpleCart.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/cart.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		jQuery.get( "<?php bloginfo('stylesheet_directory'); ?>/wp-stat.php?act=1", function( data ) {});

		<?php if($streamzon_theme_main_settings['searchbar_image'] == 1) : ?>
			jQuery("a[name=clickable_image]").on("click", function () {
				if(jQuery("input[name=s]").val()==""){
					jQuery("input[name=s]").val("*");
				}
				jQuery( "#searchForm" ).submit();
			});// end of code
		<?php endif; ?>

	<?php if($streamzon_amazon_settings['amazon_add2Cart'] == 1) : ?>
							
		jQuery("a[name=add2cart]").on("click", function () {
			
			jQuery.get( "<?php bloginfo('stylesheet_directory'); ?>/wp-stat.php?act=3", function( data ) {	});
			var asin = jQuery(this).data('asin');

			var frmstr = jQuery('<form id="tbl_tmpfrm" method="get" action="http://www.amazon.com/gp/aws/cart/add.html" target="_blank"></form>');
			jQuery("#submit2amazon").append(frmstr);
			frmstr.append('<input type="hidden" name="SubscriptionId" value="<?php print $credentials['access_key_id']?>" />');
			frmstr.append('<input type="hidden" name="AssociateTag" value="<?php print $credentials['amazon_associate_id']?>" />');
			frmstr.append('<input type="hidden" name="ASIN.1" value="'+asin+'" />');
			frmstr.append('<input type="hidden" name="Quantity.1" value="1" />');
			frmstr.submit();

		});//end of add2cart
	<?php endif; ?>

		<?php if($streamzon_amazon_settings['amazon_add2Cart'] == 0) : ?>
			jQuery("a[name=openprod]").on("click", function () {	
				jQuery.get( "<?php bloginfo('stylesheet_directory'); ?>/wp-stat.php?act=3", function( data ) {	});
			});//end of openprod
		<?php endif; ?>

		jQuery("body").on("click", "a[name=twlinks]", function () {
				var long_url = jQuery(this).data('detail');
				var input_msg	=	jQuery(this).data('peru');
				var msg = "?text=" + input_msg;
				var tag = encodeURIComponent(get_short_url("<?php echo $bitly_login_id;?>","<?php echo $bitly_api_key;?>",long_url));
				var msg_tag = msg +' '+ tag;

				var width  = 575,
			        height = 400,
			        left   = (jQuery(window).width()  - width)  / 2,
			        top    = (jQuery(window).height() - height) / 2,
			        url    = this.href + msg_tag,
			        opts   = 'status=1' +
			                 ',width='  + width  +
			                 ',height=' + height +
			                 ',top='    + top    +
			                 ',left='   + left;
			    
			    window.open(url, 'twitter', opts);
			 
			    return false;

		});//end of TW

		jQuery("body").on("click", "a[name=fblinks]", function () {

			var tag 		= jQuery(this).data('detail');
			var input_msg	= jQuery(this).data('peru');
		   	var img 		= jQuery(this).data('photo');
			
			fbShare(tag, input_msg, input_msg, img, 600, 600);
	
			/*FB.ui({
			  method: 'share',
			  href: tag,
			}, function(response){});	
			*/
	  	});


		jQuery("body").on("click", "a[name=gplinks]", function () {
			var long_url = jQuery(this).data('detail');
			
			var tag = encodeURIComponent(get_short_url("<?php echo $bitly_login_id;?>","<?php echo $bitly_api_key;?>",long_url));

			var msg = "https://plus.google.com/share?url=" + long_url;

			var width  = 600,
		        height = 600,
		        left   = (jQuery(window).width()  - width)  / 2,
		        top    = (jQuery(window).height() - height) / 2,
		        url    = msg,
		        opts   = 'status=1' +
		                 ',width='  + width  +
		                 ',height=' + height +
		                 ',top='    + top    +
		                 ',left='   + left;
		    
		    window.open(url, 'google', opts);
	  	});


/******************************************************/

/*******************************************************/
	});	//end of Jquery

	function get_short_url(login,api,long_url)
	{
		var myurl	=	null;
		var xhr 	= 	new XMLHttpRequest();
		var my_url	=	"http://api.bitly.com/v3/shorten?login="+ login + "&apiKey="+ api + "&longUrl="+ long_url;

		xhr.open("GET", my_url,false);
		xhr.onreadystatechange = function() {
		    if(xhr.readyState == 4) {
		        if(xhr.status==200) {
		            json = JSON.parse(xhr.responseText);
					myurl = json.data.url;
		        } 
		    }
		}
		xhr.send();
		return myurl;
	}

	function fbShare(url, title, descr, image, winWidth, winHeight) {
		var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }

	jQuery("a").click(function(){
		jQuery.get( "<?php bloginfo('stylesheet_directory'); ?>/wp-stat.php?act=2", function( data ) {
			
		});
	});

</script>

<div class="md-modal md-effect-7" id="modal-1">
    <div class="md-content">
        <h3>Cart</h3>
        <div>
            <div class="simpleCart_items shop-cart"></div>
        </div>
            <h3>
                <button class="md-close">Close</button>
                <a class="simpleCart_checkout">checkout</a>
            </h3>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>